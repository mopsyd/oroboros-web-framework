<?php

//defines the base directory
define('_BASE', __DIR__.'/');

//defines the base connection type as http://. Will be removed later when ssl compatibility is enabled
define('_CON', 'http://');

//defines the base url
define('_URL', _CON . $_SERVER['HTTP_HOST'] . '/');

//defines the application path
define('_APP', _BASE . 'oroboros/');

//defines the core fileset path
define('_CORE', _APP . 'core/');

//defines the local fileset path
define('_LOCAL', _APP . 'local/');

//defines the staging fileset path
define('_STAGING', _APP . 'staging/');

//defines the public directory
define('_PUBLIC', _URL . 'public/');

//checks if the system is to be bootstrapped by another method,
//if not defined, sets the system to bootstrap as a website
if (!defined('_DOC')) {
    define('_DOC', 'site');
}
//loads the core configuration
require_once _APP . '/definitions.php';
require_once _APP . '/config.php';

//bootstraps the system using the appropriate method
$_oroboros = new  oroboros\core\libs\Bootstrap(_DOC);