<?php
define('_DOC', 'css'); //Sets the doctype as css so the bootstrap will load the correct settings
//Basic path definitions
define('_IMG', '/public/media/images/');
define('_THEME_IMG', '/public/media/images/background/'.$_GET['theme'].'/');
/**
 * Style base definitions TODO:
 * Write a class that checks the database against the site and page, fetches what theme
 * is applied, then parses out the theme from the database, recursively checking parent
 * theme for inheritance where elements are not defined (defaults to default theme if no 
 * parent is specified). If the base definitions are not present in the database, the system
 * should attempt to parse them from the themes config.xml document and add them to the database
 * if they are not already present there (first run instance of the theme), and fallback to the default
 * theme if they are not present there either. If changes are made to the theme after initialized in the 
 * database, a modified parameter is to be applied so the system knows to ignore the config.xml in the
 * future, which allows the system admin to rollback to the original theme or clone from the theme's default,
 * but still retain any modifications made after the fact without having to make a child theme as is neccessary
 * in other content management systems. If the theme developer wants to make changes to the base stylesheet directly,
 * they should still create another sheet and register it to inherit from the original, so updates do not overwrite
 * their changes. Theme developers must be able to explicitly deny inheritance in config.xml if they wish to use raw, 
 * unedited html with no styles applied for some reason.
 * 
 * ---------------------------
 * 
 * HTML base entity definitions should be defined in css.phtml from the database, and appended to the file first, then the 
 * theme stylesheet should be loaded as per normal. This is so that any elements omitted from the stylesheet will still have 
 * a base definition and not look terrible, but the theme developer may still interact with the system through a standard 
 * css stylesheet as they are probably used to doing elsewhere. An interface for dynamic stylesheet generation should be 
 * added at a later date, so that the theme developer can take advantage of nested php functionality like the core system uses.
 * Extending the system to include support for Smarty templates (but not require them) would be ideal.
 * 
 * ----------------------------
 * 
 * Errors in css syntax and any resources that generate a 404 should be logged in css.log. If the theme developer has an account
 * within the system, they should also generate a flag in their notifications so they can address the problem promptly.
 * 
 * ----------------------------
 * 
 * Care should be taken throughout development to insure that standard css editing is not impeded, only extended with further 
 * development options. The goal is to insure that designers with no coding experience do not need to tinker with php in order
 * to develop themes if they do not know how to do so, and to insure that using the backend GUI is optional, not mandatory. This
 * is not going to be a WYSIWYG system, it is only going to extend the theme developers ability to create a very good theme easily 
 * using whatever toolset they are comfortable with, internal or otherwise.
 * 
 * ----------------------------
 * 
 * Longer timeline developments: built in css validation, browser extention that allows firebug edits to be saved directly to the 
 * system, remote source inheritance, theme syncing across remote notes
 * 
 */


//Style base definitions
define('_C_THEME_B', '#000'); //color base
define('_C_THEME_B_C', '#F1F1F1'); //color-base-content
define('_C_THEME_P','#C3C3C3'); //color-primary
define('_C_THEME_S','blue'); //color-secondary
define('_C_THEME_T','#FFA503'); //color-tertiary
define('_C_THEME_BGI',_THEME_IMG.'bg_f.png'); //background-image
define('_C_THEME_BGI_DRAWER',_THEME_IMG.'tweed.png'); //drawer background-image
define('_C_HTML_P','black'); //color-text
define('_C_HTML_H1','red'); //color-title
define('_C_HTML_H2','orange'); //color-subtitle
define('_C_HTML_H3','brown'); //color-heading
define('_C_HTML_A','cyan'); //color-links
define('_C_HTML_A_RO','blue'); //color-links-rollover
define('_C_HTML_A_V','cyan'); //color-links-visited


require_once __DIR__.'/../../../index.php';
?>
