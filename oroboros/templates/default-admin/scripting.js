/* 
 * this is the javascript file for the default-admin template.
 */

//functionality that loads on page load goes here



    <?php
    //php server side prerender functionality goes here
    require _CORE . _VIEWS . ucfirst(_DOC) . '/' . 'baseline.js';
    require _CORE . _VIEWS . ucfirst(_DOC) . '/libs/backend/' . 'baseline.js';
?>
jQuery(document).ready(function(){
    //functionality that loads after the dom has resolved goes here
});

jQuery(window).load(function(){
    //functionality that loads after the page has rendered goes here
});