<?php

namespace core\testmodule\lib;

/**
 * Description of Bootstrap
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Bootstrap extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function init() {
        echo 'testmodule was successfully loaded<br>';
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}