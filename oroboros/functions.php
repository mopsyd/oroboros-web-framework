<?php
//loader function to default to core file if local copy does not exist. If entry is a directory, recursively loads all files in the directory
function loader($path, $file) {
    $file = ltrim($file, '_');
    if (file_exists(_LOCAL . $path . $file)) {
        if(is_dir(_LOCAL . $path . $file)) {
            $dir = _LOCAL . $path . $file;
            $subfiles = scandir($dir);
            foreach($subfiles as $i) {
                if ($i != '.' && $i != '..') {
                    loader($dir,$i);
                }
            }
            unset($dir);
            unset($subfiles);
        } else {
            require_once _LOCAL . $path . $file;
        }
    } else {
        if(is_dir(_CORE . $path . $file)) {
            $dir = _CORE . $path . $file;
            $subfiles = scandir($dir);
            foreach($subfiles as $i) {
                if ($i != '.' && $i != '..') {
                    loader($dir,$i);
                }
            }
            unset($dir);
            unset($subfiles);
        } else {
            require_once _CORE . $path . $file;
        }
    }
}
?>
