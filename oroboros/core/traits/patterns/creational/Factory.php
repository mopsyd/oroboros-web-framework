<?php

/**
 * Factory design pattern generic implementation
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
trait Factory {
    
    public static function build($object) {
        
        if (class_exists($object)) {
            return new $object();
        }
        else {
            throw new Exception("Requested object: " . $object . " is invalid.");
        }
    }
}

?>
