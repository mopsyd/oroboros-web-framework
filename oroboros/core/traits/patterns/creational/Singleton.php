<?php

/**
 * Singleton design pattern generic implementation
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
trait Singleton {
    
    private static $instance;
    
    public static function getInstance() {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self;
        }
        return self::$instance;
    }
    
}

?>
