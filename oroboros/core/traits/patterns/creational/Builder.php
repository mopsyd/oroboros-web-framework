<?php

/**
 * Builder design pattern generic implementation
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
trait Builder {
    
    abstract function getObject();
    abstract private function defineObjectType();
    abstract private function checkObject();
    abstract private function returnObject();
    abstract private function returnException();
    
}

?>
