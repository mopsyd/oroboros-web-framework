<?php

/**
 * AbstractFactory design pattern generic implementation
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
trait AbstractFactory {
    
    abstract public function build();
    abstract protected function defineType();
    abstract private function checkObject();
    abstract private function returnObject();
    abstract private function returnException();
}

?>
