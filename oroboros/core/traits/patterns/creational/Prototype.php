<?php

/**
 * Prototype design pattern generic implementation
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
trait Prototype {
    
    static $instances;
    public $instance;
    private $class;
    private $path;
    
    abstract function __clone();
    public function logInstance() {
        $this->instance = ++self::$instances;
    }
    
}

?>
