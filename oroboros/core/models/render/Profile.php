<?php

namespace oroboros\core\models\render;

class Profile extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
        $this->init();
    }

    protected function init() {
        $type = isset($_SESSION['user']['device']['type']) ? $_SESSION['user']['device']['type'] : 'browser';
        $class = '\\oroboros\\core\\views\\Site\\Content\\' . ucfirst($type) . '\\Form';
        $file = _BASE . 'oroboros/core/views/Site/Content/' . ucfirst($type) . '/Form.php';
        if (is_readable($file)) {
            //instantiate the form object
            require $file;
            $this->form = new $class($this->package);
        } else {
            //form class not readable, throw error
            echo 'file not found at ' . $file . '<br>';
        }
    }

    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'User Profile',
                'permission' => 'registered',
                'content' => array()
            )
        );
    }
    
    public function notifications() {
        
    }
    
    public function settings() {
        
    }
    
    public function messages() {
        
    }
    
    public function privacy() {
        
    }

    public function __destruct() {
        parent::__destruct();
    }

}