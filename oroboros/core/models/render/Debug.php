<?php

namespace oroboros\core\models\render;

class Debug extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }

    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Site Debug Settings',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Server Debug Settings',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Javascript Debug Settings',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Database Debug Settings',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Stylesheet Debug Settings',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }

}