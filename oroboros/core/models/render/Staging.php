<?php

namespace oroboros\core\models\render;

class Staging extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }

    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Staging Settings',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Approve Staged Changes',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Stage PHP Class',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Stage Javascript',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Stage Database Changes',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Stage Site',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Stage Subdomain',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Stage Subsection',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Stage Page',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Stage Template',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Stage Theme',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Stage Component',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Stage Module',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }

    public function __destruct() {
        parent::__destruct();
    }

}