<?php

/**
 * Description of Server
 *
 * @author briandayhoff
 */

namespace oroboros\core\models\render;

class Server extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    const VIEWSECURITY = 'serv_view_sec';
    const VIEWSERVERENV = 'serv_view_env';
    const VIEWCODEENV = 'view_code';

    public function __construct($package) {
        parent::__construct($package);
        $this->server = new \oroboros\core\libs\Server\Server($this->package);
    }

    public function index() {
        $this->controllerContent['dashwidget'] = array(
            array(
                'type' => 'dashwidget',
                'title' => 'Server Specs',
                'permission' => self::VIEWSERVERENV,
                'content' => array(
                    'body' => array(
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>Server Type: </b>' . $this->server->checkServerType()
                        ),
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>HTTP: </b>' . $this->server->checkHttp()
                        ),
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>CGI: </b>' . $this->server->checkCgi()
                        ),
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>Address: </b>' . $this->server->checkServerAddress()
                        ),
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>Port: </b>' . $this->server->getPort()
                        ),
                    ),
                    'footer' => NULL
                ),
                'id' => 'server-type-widget'
            ),
            array(
                'type' => 'dashwidget',
                'title' => 'Server Info',
                'permission' => self::VIEWSERVERENV,
                'content' => array(
                    'body' => array(
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>Server Name: </b>' . $this->server->checkServerName()
                        ),
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>Admin: </b>' . $this->server->checkServerAdmin()
                        ),
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>Webserver ID: </b>' . $this->server->checkWebserverUser()
                        ),
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>Signature: </b>' . $this->checkServSig()
                        ),
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>SSL: </b>' . $this->checkSsl()
                        ),
                    ),
                    'footer' => NULL
                ),
                'id' => 'server-status-widget'
            ),
            array(
                'type' => 'dashwidget',
                'title' => 'Include Path',
                'permission' => self::VIEWCODEENV,
                'content' => array(
                    'body' => array(
                        array(
                            'type' => 'sub',
                            'content' => array(
                                array(
                                    'type' => 'sub',
                                    'class' => array('info'),
                                    'content' => array(
                                        $this->checkIncludePath()
                                    )
                                )
                            )
                        )
                    ),
                    'footer' => NULL
                ),
                'id' => 'path-info-widget'
            ),
            array(
                'type' => 'dashwidget',
                'title' => 'Server Load',
                'permission' => self::VIEWSERVERENV,
                'content' => array(
                    'body' => array(
                        array(
                            'type' => 'sub',
                            'content' => 'unimplemented'
                        )
                    ),
                    'footer' => NULL
                ),
                'id' => 'path-info-widget'
            )
        );
        $this->controllerContent['content'] = array();
        $this->serverStats();
        $this->serverSecurity();
        $this->codeEnv();
        $this->controllerContent['dashmod'] = array(
            array(
                'type' => 'dashmod',
                'title' => 'Server Details',
                'content' => array(),
                'id' => 'server-details'
            ),
            array(
                'type' => 'dashmod',
                'title' => 'Code Support',
                'content' => array(),
                'id' => 'code-support'
            ),
        );
    }

    protected function codeEnv() {
        $render = array();
        $render['title'] = 'Development Environment';
        $render['permission'] = self::VIEWCODEENV;
        $render['content'] = array(
            'body' => $this->getCodeEnv(),
            'footer' => array()
        );
        $render['id'] = 'code-environment-subsection';

        $this->controllerContent['content'][] = $render;
    }

    protected function serverStats() {
        $render = array();
        $render['title'] = 'Server Analysis';
        $render['permission'] = self::VIEWSERVERENV;
        $render['content'] = array(
            'body' => array(
                array(),
            ),
            'footer' => array()
        );
        $render['id'] = 'server-environment-subsection';
        $this->controllerContent['content'][] = $render;
    }

    protected function serverSecurity() {
        $render = array();
        $render['title'] = 'Server Security';
        $render['permission'] = self::VIEWSECURITY;
        $render['content'] = array(
            'body' => array(),
            'footer' => array()
        );
        $render['id'] = 'server-security-subsection';
        $this->controllerContent['content'][] = $render;
    }

    protected function checkSsl() {
        $class = $this->server->checkSsl() == 'enabled' ? 'decent' : 'poor';
        return '<span class="' . $class . '">' . $this->server->checkSsl() . '</span>';
    }

    protected function checkServSig() {
        $class = $this->server->checkServerSig() == 'disabled' ? 'decent' : 'poor';
        return '<span class="' . $class . '">' . $this->server->checkServerSig() . '</span>';
    }

    protected function checkIncludePath() {
        $path = $this->server->getIncludePath();
        $render = array(
            'type' => 'ul',
            'class' => array('delist'),
            'content' => array()
        );
        foreach ($path as $item) {
            $render['content'][] = array(
                'type' => 'li',
                'content' => $item
            );
        }
        return $render;
    }

    protected function getCodeEnv() {
        $codeinf = array(
            'type' => 'ul',
            'class' => array('delist'),
            'method' => 'tag',
            'content' => array(
                array(
                    'type' => 'li',
                    'content' => $this->php_v()
                ),
                array(
                    'type' => 'li',
                    'content' => $this->mysql_v()
                ),
                array(
                    'type' => 'li',
                    'content' => $this->ruby_v()
                ),
                array(
                    'type' => 'li',
                    'content' => $this->git_v()
                ),
                array(
                    'type' => 'li',
                    'content' => $this->perl_v()
                ),
                array(
                    'type' => 'li',
                    'content' => $this->cplusplus_v()
                ),
                array(
                    'type' => 'li',
                    'content' => $this->python_v()
                ),
            )
        );
        return $codeinf;
    }

    public function php_v() {
        $x = shell_exec("php --version");
        return $x;
    }

    public function mysql_v() { //not working
        $x = shell_exec("mysql --version");
        return $x;
    }

    public function ruby_v() {
        $x = shell_exec("ruby --version");
        return $x;
    }

    public function git_v() { //not working
        $x = shell_exec("git --version");
        return $x;
    }

    public function perl_v() {
        $x = shell_exec("perl --version");
        return $x;
    }

    public function cplusplus_v() { //not working
        $x = shell_exec("c++ --version");
        return $x;
    }

    public function python_v() { //not working
        $x = shell_exec("python --version");
        return $x;
    }

    public function __destruct() {
        parent::__destruct();
    }

}

?>
