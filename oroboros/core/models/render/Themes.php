<?php
namespace oroboros\core\models\render;

class Themes extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Create Theme',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Modify Theme',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Import Theme',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Export Theme',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}