<?php

namespace oroboros\core\models\render;

class Dashboard extends \oroboros\core\libs\Abstracts\Site\Model\Model implements \oroboros\core\interfaces\model\Render {
    
    //permissions
    const GLOBALDASH = 'g_view_dash'; //user may globally view the dashboard section of any site in the network
    const PRIVATEDASH = 'p_view_dash'; //user may view the dashboard while it is set to private
    const DASH = 'view_dash'; //user may view the dashboard of this site

    public function __construct($package) {
        parent::__construct($package);
        $this->_package = $package;
        $this->registerBaselineAssets();
    }
    
    private function registerBaselineAssets() {
        $this->registerBaselineStylesheets();
        $this->registerBaselineScripts();
    }
    
    private function registerBaselineStylesheets() {

    }
    
    private function registerBaselineScripts() {
        
    }

    public function index() {
        $this->controllerContent['dashwidget'] = array(
            array(
                'type' => 'dashwidget',
                'title' => 'Site Info',
                'permission' => 'admin',
                'content' => array(
                    'body' => array(
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>Site Name: </b>' . $this->_site['title']
                        ),
                        array(
                            'type' => 'br',
                            'method' => 'short'
                        ),
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>Visitors: </b> unimplemented'
                        ),
                    ),
                    'footer' => NULL
                ),
                'id' => 'site-info-widget'
            ),
            array(
                'type' => 'dashwidget',
                'title' => 'User Statistics',
                'permission' => 'admin',
                'content' => array(
                    'body' => array(
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>Accounts: </b> unimplemented'
                        ),
                        array(
                            'type' => 'br',
                            'method' => 'short'
                        ),
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>Online: </b> unimplemented'
                        ),
                        array(
                            'type' => 'br',
                            'method' => 'short'
                        ),
                        array(
                            'type' => 'a',
                            'class' => array('info'),
                            'content' => '<sub>View List (unimplemented)</sub>',
                            'params' => array(
                                'href' => '#'
                            )
                        ),
                    ),
                    'footer' => NULL
                ),
                'id' => 'user-stats-widget'
            ),
        );
        $this->controllerContent['content'] = array(
            array(
                'content' => '<p>This is test site content.</p>'
            )
        );
        $this->controllerContent['dashmod'] = array(
            array(
                'type' => 'dashmod',
                'permission' => 'architect',
                'title' => 'Architect Scratchbox',
                'content' => array(
                    'body' => NULL,
                    'footer' => array(
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => 'This is an experimental area for architecture development. It is only visible to the core architecture team.'
                        )
                    )
                ),
                'id' => 'architect-scratchbox'
            ),
            array(
                'type' => 'dashmod',
                'permission' => 'architect',
                'title' => 'Oroboros News',
                'class' => array(
                    'half'
                ),
                'content' => array(
                    'body' => array(
                        array(
                            'content' => 'This is where the news feed will go when it is completed.'
                        ),
                    ),
                    'footer' => array()
                ),
                'id' => 'oroboros-news'
            ),
        );
        //$this->setContent($this->_page['slug']);
    }

    public function news() {
        return '<p>This is where the news feed will go when it is completed.</p>';
    }

    public function sidebar() {
        $sidebar = new \oroboros\core\libs\Render\Sidebar($this->package);
        return $sidebar->getSidebar('dashboard');
    }

    function logout() {
        $_SESSION['user']['loggedIn'] = FALSE;
        $this->user->handleLogin();
        header('location: ' . _URL . 'login');
        exit;
    }

}