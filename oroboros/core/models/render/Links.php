<?php

namespace oroboros\core\models\render;

class Links extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }

    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'Links Overview',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Linkset Scope',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Linkset Hooks',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Linkset Access Rights',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
        );
    }

    public function add() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'Add Linkset',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
        );
    }

    public function modify() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'Modify Linkset',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
        );
    }

    public function remove() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'Remove Linkset',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
        );
    }

    public function archive() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'Archive Linkset',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
        );
    }

    public function restore() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'Restore Linkset',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
        );
    }

    public function import() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'Import Linksets',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
        );
    }

    public function export() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'Export Linksets',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
        );
    }

    public function cloneLinks() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'Clone Linkset',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
        );
    }

    public function __destruct() {
        parent::__destruct();
    }

}