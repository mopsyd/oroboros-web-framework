<?php
namespace oroboros\core\models\render;

class Sites extends \oroboros\core\libs\Abstracts\Site\Model\Model {
    
    const EDITSITE = '';
    const EDITGLOBAL = 'g_site_edit';
    const ADDSITE = '';
    const VIEWDETAILS = '';
    const VIEWGLOBALDETAILS = '';
    const REMOVESITE = '';
    const PACKAGESITE = '';
    const GLOBALIMPORTSITE = 'g_site_import';
    const IMPORTSITE = '';
    const GLOBALEXPORTSITE = 'g_site_export';
    const EXPORTSITE = '';
    const CLONESITE = '';
    const ARCHIVESITE = '';
    const SHARESITE = '';
    const MIRRORSITE = '';
    const SETSITEPRIVACY = '';

    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'Sites Overview',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Site Settings',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Create Site',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Edit Site',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Remove Site',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Backup Site',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Package Site',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Import Site',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Export Site',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Mirror Site',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
        );
    }
    
    public function settings() {
        
    }
    
    public function create() {
        
    }
    
    public function edit() {
        
    }
    
    public function remove() {
        
    }
    
    public function import() {
        
    }
    
    public function export() {
        
    }
    
    public function package() {
        
    }
    
    public function lockdown() {
        
    }
    
    public function archive() {
        
    }
    
    public function share() {
        
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}