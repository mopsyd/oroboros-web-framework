<?php

namespace oroboros\core\models\render;

class Templates extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }

    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Create Template',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Modify Template',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Import Template',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Export Template',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }

    public function __destruct() {
        parent::__destruct();
    }

}