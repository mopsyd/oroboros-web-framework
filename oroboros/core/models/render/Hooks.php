<?php
namespace oroboros\core\models\render;

class Hooks extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'Manage Hooks',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Define Hook Scope',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Define Hook Access Rights',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Create Hook',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Remove Hook',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Edit Hook',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
        );
    }
    
    public function register() {
        
    }
    
    public function modify() {
        
    }
    
    public function remove() {
        
    }
    
    public function import() {
        
    }
    
    public function export() {
        
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}