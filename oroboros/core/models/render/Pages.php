<?php

namespace oroboros\core\models\render;

class Pages extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
        $this->init();
    }

    protected function init() {
        $type = isset($_SESSION['user']['device']['type']) ? $_SESSION['user']['device']['type'] : 'browser';
        $class = '\\oroboros\\core\\views\\Site\\Content\\' . ucfirst($type) . '\\Form';
        $file = _BASE . 'oroboros/core/views/Site/Content/' . ucfirst($type) . '/Form.php';
        if (is_readable($file)) {
            //instantiate the form object
            require $file;
            $this->form = new $class($this->package);
        } else {
            //form class not readable, throw error
            echo 'file not found at ' . $file . '<br>';
        }
    }

    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'Pages Overview',
                'permission' => 'admin',
                'content' => '<p>Test site content</p>'
            )
        );
    }
    
    public function add() {
        $this->controllerContent['content'][] = array(
            'title' => 'Add New Page',
            'content' => $this->createPageForm($_SESSION['device']['mode'])
        );
    }

    public function create() {

    }

    public function modify() {
        $this->controllerContent['content'][] = array(
            'title' => 'Modify Page',
            'content' => $this->buildCreateAccountForm($_SESSION['device']['mode'])
        );
    }

    public function edit() {

    }

    public function import() {
        $this->controllerContent['content'][] = array(
            'title' => 'Import Pages',
            'content' => $this->buildCreateAccountForm($_SESSION['device']['mode'])
        );
    }

    public function export() {
        $this->controllerContent['content'][] = array(
            'title' => 'Export Pages',
            'content' => $this->buildCreateAccountForm($_SESSION['device']['mode'])
        );
    }

    public function archive() {
        $this->controllerContent['content'][] = array(
            'title' => 'Archive Page',
            'content' => $this->buildCreateAccountForm($_SESSION['device']['mode'])
        );
    }

    public function restore() {
        $this->controllerContent['content'][] = array(
            'title' => 'Restore Page',
            'content' => $this->buildCreateAccountForm($_SESSION['device']['mode'])
        );
    }

    public function remote() {
        $this->controllerContent['content'][] = array(
            'title' => 'Remote Pages',
            'content' => $this->buildCreateAccountForm($_SESSION['device']['mode'])
        );
    }

    public function delete() {
        $this->controllerContent['content'][] = array(
            'title' => 'Delete Page',
            'content' => $this->buildCreateAccountForm($_SESSION['device']['mode'])
        );
    }

    public function clonePage() {
        $this->controllerContent['content'][] = array(
            'title' => 'Clone Page',
            'content' => $this->buildCreateAccountForm($_SESSION['device']['mode'])
        );
    }

    public function createPageForm($type = 'browser') {
        //build the data array
        $fields = array(
            array(
                'classes' => array('formsection'),
                'formItems' => array(
                    array(
                        'fieldtype' => 'input',
                        'title' => 'page-slug',
                        'name' => 'page-slug',
                        'label' => array(
                            'value' => 'Page Slug'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'page-title',
                        'name' => 'page-title',
                        'label' => array(
                            'value' => 'Page Title'
                        ),
                        'type' => 'text'
                    )
                )
            ),
            array(
                'classes' => array('formsection'),
                'formItems' => array(
                    array(
                        'fieldtype' => 'select',
                        'title' => 'page-parent',
                        'name' => 'page-parent',
                        'label' => array(
                            'value' => 'Page Parent'
                        ),
                        'options' => $this->getSitePages($this->site['slug'], 1)
                    ),
                    array(
                        'fieldtype' => 'select',
                        'title' => 'page-subdomain',
                        'name' => 'page-subdomain',
                        'label' => array(
                            'value' => 'Subdomain'
                        ),
                        'options' => $this->getSiteSubdomains($this->site['slug'], 1)
                    ),
                    array(
                        'fieldtype' => 'select',
                        'title' => 'page-template',
                        'name' => 'page-template',
                        'label' => array(
                            'value' => 'Page Template'
                        ),
                        'options' => $this->getTemplates(1)
                    ),
                    array(
                        'fieldtype' => 'select',
                        'title' => 'page-theme',
                        'name' => 'page-theme',
                        'label' => array(
                            'value' => 'Page Theme'
                        ),
                        'options' => $this->getThemes(1)
                    ),
                )
            ),
            array(
                'classes' => array('formsection'),
                'formItems' => array(
                    array(
                        'fieldtype' => 'select',
                        'title' => 'site-section',
                        'name' => 'site-section',
                        'label' => array(
                            'value' => 'Site Section'
                        ),
                        'options' => $this->getSiteSections($this->site['slug'], 1)
                    ),
                    array(
                        'fieldtype' => 'select',
                        'title' => 'visibility',
                        'name' => 'visibility',
                        'label' => array(
                            'value' => 'Visibility'
                        ),
                        'options' => $this->getUserGroups($this->site['slug'], 1)
                    ),
                )
            ),
            array(
                'classes' => array('bottom', 'formFooter'),
                'formItems' => array(
                    array(
                        'fieldtype' => 'input',
                        'name' => 'global',
                        'class' => array('left'),
                        'text' => 'Global Page',
                        'type' => 'checkbox'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'name' => 'enabled',
                        'class' => array('left'),
                        'checked' => TRUE,
                        'text' => 'Page Enabled',
                        'type' => 'checkbox'
                    ),
                    array(
                        'fieldtype' => 'button',
                        'value' => 'Preview Page',
                    ),
                    array(
                        'fieldtype' => 'input',
                        'value' => 'Create Page',
                        'type' => 'submit'
                    )
                )
            )
        );

        //create the form
        $render = $this->form->makeForm('create-page', 'pages/create', 'post', $fields, 1, array('create-page-box')) . PHP_EOL;
        //var_dump($render);
        return $render;
    }

    protected function getUserGroups($site, $format = FALSE) {
        $query = "SELECT `slug`,`display` FROM `user_groups` WHERE `site` IN ('$site','global') AND `show`='1' AND `slug` IN ('" . implode("','", $_SESSION['user']['permissions']) . "') AND `slug` NOT IN ('banned', 'restricted', 'SYSTEM', 'self') ORDER BY `hierarchy` ASC;";
        $results = $this->db->select($query);
        if ($format == TRUE) {
            //formatting for form input
            $options = array();
            foreach ($results as $group) {
                $options[] = array(
                    'value' => $group['slug'],
                    'text' => $group['display']
                );
            }
            return $options;
        } else {
            //returning raw user group results
            return $results;
        }
    }
    
    protected function getSitePages($site, $format = FALSE) {
        $query = "SELECT `slug`,`display` FROM `sites_subsections` WHERE `active`=1 AND `site` IN ('$site', 'global') ORDER BY `display` ASC;";
        $results = $this->db->select($query);
        if ($format == TRUE) {
            //formatting for form input
            $options = array();
            foreach ($results as $group) {
                $options[] = array(
                    'value' => $group['slug'],
                    'text' => $group['display']
                );
            }
            return $options;
        } else {
            //returning raw user group results
            return $results;
        }
    }
    
    protected function getSiteSubdomains($site, $format = FALSE) {
        $query = "SELECT `slug`,`title` FROM `sites_subdomains` WHERE `active`=1 AND `site` IN ('$site', 'global') ORDER BY `title` ASC;";
        $results = $this->db->select($query);
        if ($format == TRUE) {
            //formatting for form input
            $options = array();
            foreach ($results as $group) {
                $options[] = array(
                    'value' => $group['slug'],
                    'text' => $group['title']
                );
            }
            return $options;
        } else {
            //returning raw user group results
            return $results;
        }
    }
    
    protected function getSiteSections($site, $format = FALSE) {
        $query = "SELECT `section`,`display` FROM `sites_subsections` WHERE `active`=1 AND `site` IN ('$site', 'global') ORDER BY `display` ASC;";
        $results = $this->db->select($query);
        if ($format == TRUE) {
            //formatting for form input
            $options = array();
            foreach ($results as $group) {
                $options[] = array(
                    'value' => $group['section'],
                    'text' => $group['display']
                );
            }
            return $options;
        } else {
            //returning raw user group results
            return $results;
        }
    }
    
    protected function getTemplates($format = FALSE) {
        $query = "SELECT `slug`,`title`,`version` FROM `templates` WHERE `active`='1' AND `show`='1' ORDER BY `title` ASC;";
        $results = $this->db->select($query);
        if ($format == TRUE) {
            //formatting for form input
            $options = array();
            foreach ($results as $group) {
                $options[] = array(
                    'value' => $group['slug'],
                    'text' => $group['title'] . ' - ' .$group['version']
                );
            }
            return $options;
        } else {
            //returning raw user group results
            return $results;
        }
    }
    
    protected function getThemes($format = FALSE) {
        $query = "SELECT `slug`,`title`,`version` FROM `themes` WHERE `active`='1' AND `show`='1' ORDER BY `title` ASC;";
        $results = $this->db->select($query);
        if ($format == TRUE) {
            //formatting for form input
            $options = array();
            foreach ($results as $group) {
                $options[] = array(
                    'value' => $group['slug'],
                    'text' => $group['title'] . ' - ' .$group['version']
                );
            }
            return $options;
        } else {
            //returning raw user group results
            return $results;
        }
    }

    public function __destruct() {
        parent::__destruct();
    }

}