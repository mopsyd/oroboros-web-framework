<?php
namespace oroboros\core\models\render;

class Maintenance extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function index() {
        $this->controllerContent['content'] = array();
        if (isset($this->_site['global']) && $this->_site['global'] == TRUE) {
            $this->controllerContent['content'][] = array(
                'title' => 'Global Maintenance Settings',
                'content' => '<p>This is test site content.</p>'
            );
        }
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Site Maintenance Settings',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
}