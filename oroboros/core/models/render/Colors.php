<?php
namespace oroboros\core\models\render;

class Colors extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Color Schemes',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Palette Builder',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}