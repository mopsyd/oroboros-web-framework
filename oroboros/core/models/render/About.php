<?php
namespace oroboros\core\models\render;

class About extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'About',
                'content' => '<p>This is test site content.</p>'
            )
        );
    }
    
    public function development() {
        
    }
    
    public function design() {
        
    }
    
    public function networking() {
        
    }
    
    public function extensibility() {
        
    }
    
    public function display() {
        
    }
    
    public function inheritance() {
        
    }
    
    public function security() {
        
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}