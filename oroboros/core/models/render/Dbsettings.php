<?php
namespace oroboros\core\models\render;

class DbSettings extends \oroboros\core\libs\Abstracts\Site\Model\Model {
    
    const VIEWCOREDB = 'db_view_core';
    const VIEWDB = 'db_view';
    const EDITCOREDB = 'db_update_core';
    const EDITDB = 'db_modify';
    const CREATEDB = 'db_create';
    const REMOVEDB = 'db_remove';
    const CREATEREMOTEDB = 'db_create_rm';
    const VIEWREMOTEDB = 'db_view_rm';
    const EDITREMOTEDB = 'db_edit_rm';
    const REMOVEREMOTEDB = 'db_remove_rm';
    const QUERYDB = 'db_query_exec';
    const VIEWQUERY = 'db_query_view';
    const QUERYDBREMOTE = 'db_query_ex_rm';
    const VIEWQUERYREMOTE = 'db_view_rm';
    const BACKUPDB = 'db_backup';
    const BACKUPDBREMOTE = 'db_backup_rm';
    const DBSQLEDITOR = 'db_sql_editor';
    const UPLOADSQL = 'file_up_sql';

    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function index() {
        $this->controllerContent = array();
        //widgets
        $this->coreDatabaseWidget();
        $this->databaseConnectionsWidget();
        
        //content
        $this->coreDatabaseSection();
        $this->addDatabaseSection();
        $this->modifyDatabaseSection();
        $this->importDatabaseSection();
        $this->exportDatabaseSection();
        $this->backupDatabaseSection();
        
        //dashmods
        
    }
    
    protected function coreDatabaseSection() {
        $this->controllerContent['content'][] = array(
            'title' => 'Core Database',
            'permission' => self::EDITCOREDB,
            'content' => array(),
            'id' => 'core-database-section'
        );
    }
    
    protected function addDatabaseSection() {
        $this->controllerContent['content'][] = array(
            'title' => 'Add New Database Connection',
            'permission' => self::CREATEDB,
            'content' => array(),
            'id' => 'add-database-section'
        );
    }
    
    protected function modifyDatabaseSection() {
        $this->controllerContent['content'][] = array(
            'title' => 'Modify Database Connection',
            'permission' => self::EDITDB,
            'content' => array(),
            'id' => 'modify-database-section'
        );
    }
    
    protected function importDatabaseSection() {
        $this->controllerContent['content'][] = array(
            'title' => 'Import Database',
            'permission' => self::CREATEDB,
            'content' => array(),
            'id' => 'import-database-section'
        );
    }
    
    protected function exportDatabaseSection() {
        $this->controllerContent['content'][] = array(
            'title' => 'Export Database',
            'permission' => self::BACKUPDB,
            'content' => array(),
            'id' => 'export-database-section'
        );
    }
    
    protected function backupDatabaseSection() {
        $this->controllerContent['content'][] = array(
            'title' => 'Backup Database',
            'permission' => self::BACKUPDB,
            'content' => array(),
            'id' => 'backup-database-section'
        );
    }
    
    protected function coreDatabaseWidget() {
        $this->controllerContent['dashwidget'][] = array(
            'type' => 'dashwidget',
            'title' => 'Core Database Info',
            'permission' => self::VIEWCOREDB,
            'content' => array(
                'body' => $this->showDbDetails($this->db),
                'footer' => array()
            ),
            'id' => 'core-database-widget'
        );
    }
    
    protected function databaseConnectionsWidget() {
        $this->controllerContent['dashwidget'][] = array(
            'type' => 'dashwidget',
            'title' => 'Database Connections',
            'permission' => self::VIEWDB,
            'content' => array(
                'body' => array(),
                'footer' => array()
            ),
            'id' => 'database-connections-widget'
        );
    }
    
    private function showDbDetails($connection) {
        $render = array(
            array(
                'type' => 'ul',
                'method' => 'tag',
                'class' => array('delist'),
                'content' => array(
                    array(
                        'type' => 'li',
                        'content' => array(
                            array(
                                'type' => 'sub',
                                'class' => array('info'),
                                'content' => '<b>Name: </b>' . $this->getName($connection)
                            ),
                        ),
                    ),
                    array(
                        'type' => 'li',
                        'content' => array(
                            array(
                                'type' => 'sub',
                                'class' => array('info'),
                                'content' => '<b>Instance: </b>' . $this->getInstance($connection)
                            ),
                        ),
                    ),
                    array(
                        'type' => 'li',
                        'content' => array(
                            array(
                                'type' => 'sub',
                                'class' => array('info'),
                                'content' => '<b>Type: </b>' . $this->getType($connection)
                            ),
                        ),
                    ),
                    array(
                        'type' => 'li',
                        'content' => array(
                            array(
                                'type' => 'sub',
                                'class' => array('info'),
                                'content' => '<b>Method: </b>' . $this->getMethod($connection)
                            ),
                        ),
                    ),
                    array(
                        'type' => 'li',
                        'content' => array(
                            array(
                                'type' => 'sub',
                                'class' => array('info'),
                                'content' => '<b>User: </b>' . $this->getUser($connection)
                            ),
                        ),
                    ),
                    array(
                        'type' => 'li',
                        'content' => array(
                            array(
                                'type' => 'sub',
                                'class' => array('info'),
                                'content' => '<b>Pass: </b>' . $this->getPass($connection)
                            ),
                        ),
                    ),
                    array(
                        'type' => 'li',
                        'content' => array(
                            array(
                                'type' => 'sub',
                                'class' => array('info'),
                                'content' => '<b>Port: </b>' . $this->getPort($connection)
                            ),
                        ),
                    ),
                ),
            ),
        );
        
        return $render;
    }
    
    private function getInstance($connection) {
        return $connection->instance();
    }
    
    private function getName($connection) {
        return $connection->name();
    }
    
    private function getUser($connection) {
        return $connection->user();
    }
    
    private function getPass($connection) {
        return $connection->pass();
    }
    
    private function getMethod($connection) {
        return $connection->method();
    }
    
    private function getPort($connection) {
        return $connection->port();
    }
    
    private function getType($connection) {
        return $connection->type();
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}