<?php
namespace oroboros\core\models\render;

class Groups extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'User Groups',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Create Group',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Modify Group',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function add() {
        $this->controllerContent['content'][] = array(
            'title' => 'Create User Group',
            'content' => $this->buildCreateAccountForm($_SESSION['device']['mode'])
        );
    }

    public function modify() {
        $this->controllerContent['content'][] = array(
            'title' => 'Modify User Group',
            'content' => $this->buildEditAccountForm($_SESSION['device']['mode'])
        );
    }

    public function import() {
        $this->controllerContent['content'][] = array(
            'title' => 'Import User Group',
            'content' => 'unimplemented'
        );
    }

    public function export() {
        $this->controllerContent['content'][] = array(
            'title' => 'Export User Group',
            'content' => 'unimplemented'
        );
    }

    public function archive() {
        $this->controllerContent['content'][] = array(
            'title' => 'Archive User Group',
            'content' => 'unimplemented'
        );
    }

    public function restore() {
        $this->controllerContent['content'][] = array(
            'title' => 'Restore User Group',
            'content' => 'unimplemented'
        );
    }

    public function remote() {
        $this->controllerContent['content'][] = array(
            'title' => 'Manage Remote Groups',
            'content' => 'unimplemented'
        );
    }

    public function delete() {
        $this->controllerContent['content'][] = array(
            'title' => 'Delete User Group',
            'content' => 'unimplemented'
        );
    }

    public function cloneGroup() {
        $this->controllerContent['content'][] = array(
            'title' => 'Clone User Group',
            'content' => 'unimplemented'
        );
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}