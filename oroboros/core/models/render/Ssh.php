<?php
namespace oroboros\core\models\render;

class Ssh extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Manage SSH Connections',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Set SSH Connection Task',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Register SSH Connection',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Deauthorize SSH Connection',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Import SSH Connection',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Export SSH Connection',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}