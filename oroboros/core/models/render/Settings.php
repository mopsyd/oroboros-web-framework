<?php

namespace oroboros\core\models\render;

class Settings extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }

    public function index() {
        $this->controllerContent = array();
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Settings Overview',
            'permission' => 'webmaster',
            'content' => '<p>This is test content</p>'
        );
    }

    public function globalSettings() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Global Settings',
            'permission' => 'webmaster',
            'content' => '<p>This is test content</p>'
        );
    }

    public function site() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Site Settings',
            'permission' => 'webmaster',
            'content' => '<p>This is test content</p>'
        );
    }

    public function page() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Page Settings',
            'permission' => 'webmaster',
            'content' => '<p>This is test content</p>'
        );
    }

    public function content() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Content Settings',
            'permission' => 'webmaster',
            'content' => '<p>This is test content</p>'
        );
    }

    public function forum() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Forum Settings',
            'permission' => 'webmaster',
            'content' => '<p>This is test content</p>'
        );
    }

    public function comments() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Comment &amp; Discussion Settings',
            'permission' => 'webmaster',
            'content' => '<p>This is test content</p>'
        );
    }

    public function users() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'User Settings',
            'permission' => 'webmaster',
            'content' => '<p>This is test content</p>'
        );
    }

    public function registration() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Registration Settings',
            'permission' => 'webmaster',
            'content' => '<p>This is test content</p>'
        );
    }

    public function modules() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Module Settings',
            'permission' => 'webmaster',
            'content' => '<p>This is test content</p>'
        );
    }

    public function components() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Component Settings',
            'permission' => 'webmaster',
            'content' => '<p>This is test content</p>'
        );
    }

    public function themes() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Theme Settings',
            'permission' => 'webmaster',
            'content' => '<p>This is test content</p>'
        );
    }

    public function templates() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Template Settings',
            'permission' => 'webmaster',
            'content' => '<p>This is test content</p>'
        );
    }

    public function access() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Access Settings',
            'permission' => 'webmaster',
            'content' => '<p>This is test content</p>'
        );
    }

    public function nodes() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Settings',
            'permission' => 'webmaster',
            'content' => '<p>This is test content</p>'
        );
    }

    public function __destruct() {
        parent::__destruct();
    }

}