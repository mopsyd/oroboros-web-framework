<?php

namespace oroboros\core\models\render;

class Dns extends \oroboros\core\libs\Abstracts\Site\Model\Model {
    
    //permissions
    const ADDDNS = 'dns_add';
    const EDITDNS = 'dns_edit';
    const DEFINEDNS = 'dns_define';
    const REMOVEDNS = 'dns_remove';

    public function __construct($package) {
        parent::__construct($package);
    }

    public function index() {
        $default = $this->siteInfo(NULL, 1);
        $defaultnode = isset($default['node']) ? '<span class="poor">' . $default['node'] . '</span>' : '<span class="decent">Local</span>';
        $defaultglobal = $default['global'] == TRUE ? '<span class="decent">TRUE</span>' : '<span class="poor">FALSE</span>';
        $domains = '<span class="decent">' . $this->domains() . '</span>';
        $orphans = $this->domains(0, 1);
        $orphans = $orphans > 0 ? '<span class="poor">' . $orphans . '</span>' : '<span class="decent">' . $orphans . '</span>';
        $globals = '<span class="decent">' . $this->domains(0, 0, 1) . '</span>';
        $active = $this->domains(1) === $this->domains() ? '<span class="decent">' . $this->domains(1) . '</span>' : '<span class="poor">' . $this->domains(1) . '</span>';
        $this->controllerContent['dashwidget'] = array(
            array(
                'type' => 'dashwidget',
                'title' => 'DNS Info',
                'permission' => 'webmaster',
                'content' => array(
                    'body' => array(
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>Domains: </b>' . $domains
                        ),
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>Active: </b>' . $active
                        ),
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>Global Sites: </b>' . $globals
                        ),
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>Parked: </b>' . $orphans
                        ),
                    ),
                    'footer' => NULL
                ),
                'id' => 'dns-info-widget'
            ),
            array(
                'type' => 'dashwidget',
                'title' => 'Default Domain',
                'permission' => 'webmaster',
                'content' => array(
                    'body' => array(
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>DNS: </b><span class="decent">' . $default['dns'] . '</span>'
                        ),
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>Node: </b>' . $defaultnode
                        ),
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => '<b>Global Access: </b>' . $defaultglobal
                        ),
                    ),
                    'footer' => NULL
                ),
                'id' => 'default-domain-widget'
            ),
        );
        $this->controllerContent['content'] = array(
            array(
                'title' => 'DNS Overview',
                'permission' => self::ADDDNS,
                'content' => '<p>This is test site content.</p>'
            ),
        );
        if ($this->domains(0, 1) > 0) {
            $this->parkedDomains();
        }
    }
    
    
    function register() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Register Domain',
                'permission' => self::ADDDNS,
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    function modify() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Modify Domain',
                'permission' => self::ADDDNS,
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    function deauthorize() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Deauthorize Domain',
                'permission' => self::ADDDNS,
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    function assign() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Assign Domain',
                'permission' => self::ADDDNS,
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    function redirect() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Redirect Domain',
                'permission' => self::ADDDNS,
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    function suspend() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Suspend Domain',
                'permission' => self::ADDDNS,
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }

    protected function domains($active = FALSE, $orphans = FALSE, $global = FALSE) {
        if ($active == TRUE) {
            //select active domains
            $query = "SELECT `dns` FROM `dns` WHERE `show`='1' AND `active`='1';";
        } elseif ($orphans == TRUE) {
            //select orphan domains
            $query = "SELECT `dns` FROM `dns` WHERE `show`='1' AND `site` IS NULL;";
        } elseif ($global == TRUE) {
            //select global domains
            $query = "SELECT `dns` FROM `dns` WHERE `show`='1' AND `global`='1';";
        } else {
            //select all domains
            $query = "SELECT `dns` FROM `dns` WHERE `show`='1';";
        }
        $result = count($this->db->select($query));
        return $result;
    }
    
    protected function parkedDomains() {
        $this->controllerContent['dashmod'][] = array(
                'type' => 'dashmod',
                'title' => 'Parked Domains',
                'permission' => self::DEFINEDNS,
                'content' => array(
                    'body' => array(
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => 'Orphan domains (unimplemented)'
                        ),
                    ),
                    'footer' => NULL
                ),
                'id' => 'parked-domains-dashmod'
            );
    }

    protected function siteInfo($site = NULL, $default = FALSE) {
        $where = isset($site) ? " WHERE `site`='$site' " : NULL;
        $where = $default == TRUE ? " WHERE `default`='1' LIMIT 1 " : $where;
        $query = "SELECT `dns`,`site`,`node`,`active`,`show`,`default`,`global` FROM `dns` $where;";
        $result = $this->db->select($query);
        return count($result) > 1 ? $result : $result[0];
    }

}