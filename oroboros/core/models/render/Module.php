<?php

namespace oroboros\core\models\render;

class Module extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }

    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Module Overview',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function manage() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Manage Modules',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function edit() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Edit Modules',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function add() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Add Modules',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function remove() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Remove Modules',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function validate() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Validate Module Package',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function history() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Module History',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function permissions() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Module Permissions',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function import() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Import Modules',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function export() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Export Modules',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function package() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Package Modules',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function create() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Create Modules',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function inherit() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Module Inheritance',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function archive() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Archive Modules',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function restore() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Restore Modules',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }

    public function __destruct() {
        parent::__destruct();
    }

}