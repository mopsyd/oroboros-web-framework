<?php

namespace oroboros\core\models\render;

class Accounts extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
        $this->init();
    }

    protected function init() {
        $type = isset($_SESSION['user']['device']['type']) ? $_SESSION['user']['device']['type'] : 'browser';
        $class = '\\oroboros\\core\\views\\Site\\Content\\' . ucfirst($type) . '\\Form';
        $file = _BASE . 'oroboros/core/views/Site/Content/' . ucfirst($type) . '/Form.php';
        if (is_readable($file)) {
            //instantiate the form object
            require $file;
            $this->form = new $class($this->package);
        } else {
            //form class not readable, throw error
            echo 'file not found at ' . $file . '<br>';
        }
    }

    public function index() {
        $this->controllerContent = array();
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Accounts Overview',
                'content' => 'unimplemented'
            ),
        );
    }

    public function add() {
        $this->controllerContent['content'][] = array(
            'title' => 'Create User Account',
            'content' => $this->buildCreateAccountForm($_SESSION['device']['mode'])
        );
    }

    public function modify() {
        $this->controllerContent['content'][] = array(
            'title' => 'Modify User Account',
            'content' => $this->buildEditAccountForm($_SESSION['device']['mode'])
        );
    }

    public function import() {
        $this->controllerContent['content'][] = array(
            'title' => 'Import User Account',
            'content' => 'unimplemented'
        );
    }

    public function export() {
        $this->controllerContent['content'][] = array(
            'title' => 'Export User Account',
            'content' => 'unimplemented'
        );
    }

    public function archive() {
        $this->controllerContent['content'][] = array(
            'title' => 'Archive User Account',
            'content' => 'unimplemented'
        );
    }

    public function restore() {
        $this->controllerContent['content'][] = array(
            'title' => 'Restore User Account',
            'content' => 'unimplemented'
        );
    }

    public function remote() {
        $this->controllerContent['content'][] = array(
            'title' => 'Manage Remote Accounts',
            'content' => 'unimplemented'
        );
    }

    public function delete() {
        $this->controllerContent['content'][] = array(
            'title' => 'Delete User Account',
            'content' => 'unimplemented'
        );
    }

    public function cloneAccount() {
        $this->controllerContent['content'][] = array(
            'title' => 'Clone User Account',
            'content' => 'unimplemented'
        );
    }

    public function buildCreateAccountForm($type = 'browser') {
        //build the data array
        $groups = $this->getUserGroups($this->site['slug'], 1);
        $fields = array(
            array(
                'classes' => array('formsection'),
                'formItems' => array(
                    array(
                        'fieldtype' => 'input',
                        'title' => 'name-first',
                        'name' => 'name-first',
                        'label' => array(
                            'value' => 'First Name'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'name-last',
                        'name' => 'name-last',
                        'label' => array(
                            'value' => 'Last Name'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'phone',
                        'name' => 'phone',
                        'label' => array(
                            'value' => 'Telephone'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'email',
                        'name' => 'email',
                        'label' => array(
                            'value' => 'Email'
                        ),
                        'type' => 'text'
                    )
                )
            ),
            array(
                'classes' => array('formsection'),
                'formItems' => array(
                    array(
                        'fieldtype' => 'input',
                        'title' => 'username',
                        'name' => 'username',
                        'label' => array(
                            'value' => 'Username'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'password',
                        'name' => 'password',
                        'label' => array(
                            'value' => 'Password'
                        ),
                        'type' => 'password'
                    ),
                    array(
                        'fieldtype' => 'select',
                        'title' => 'group',
                        'name' => 'group',
                        'label' => array(
                            'value' => 'Group'
                        ),
                        'options' => $groups
                    ),
                    array(
                        'fieldtype' => 'select',
                        'title' => 'image',
                        'name' => 'image',
                        'label' => array(
                            'value' => 'User Icon'
                        ),
                        'options' => array(
                            array(
                                'value' => 'site',
                                'text' => 'Use Site Default'
                            ),
                            array(
                                'value' => 'group',
                                'text' => 'Use Group Default'
                            )
                        )
                    )
                )
            ),
            array(
                'classes' => array('formsection'),
                'formItems' => array(
                    array(
                        'fieldtype' => 'input',
                        'title' => 'addr_1',
                        'name' => 'addr_1',
                        'label' => array(
                            'value' => 'Street Address'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'addr_2',
                        'name' => 'addr_2',
                        'label' => array(
                            'value' => 'Unit Number'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'addr_city',
                        'name' => 'addr_city',
                        'label' => array(
                            'value' => 'City'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'addr_state',
                        'name' => 'addr_state',
                        'label' => array(
                            'value' => 'State'
                        ),
                        'type' => 'text'
                    )
                )
            ),
            array(
                'classes' => array('formsection'),
                'formItems' => array(
                    array(
                        'fieldtype' => 'input',
                        'title' => 'addr_zip',
                        'name' => 'addr_zip',
                        'text' => ' - ',
                        'label' => array(
                            'value' => 'Zip'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'addr_zip_ext',
                        'name' => 'addr_zip_ext',
                        'label' => array(
                            'value' => 'Zip Extension'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'addr_country',
                        'name' => 'addr_country',
                        'label' => array(
                            'value' => 'Country'
                        ),
                        'type' => 'text'
                    )
                )
            ),
            array(
                'classes' => array('formFooter'),
                'formItems' => array(
                    array(
                        'fieldtype' => 'input',
                        'name' => 'global',
                        'text' => 'Global user',
                        'type' => 'checkbox'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'name' => 'enabled',
                        'checked' => TRUE,
                        'text' => 'User enabled',
                        'type' => 'checkbox'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'value' => 'Create User',
                        'type' => 'submit'
                    )
                )
            )
        );

        //create the form
        $render = '<div class="login-form">' . $this->form->makeForm('create-user', 'accounts/create', 'post', $fields, 1, array('create-user-box')) . '</div>' . PHP_EOL;
        return $render;
    }

    public function buildEditAccountForm($type = 'browser') {
        //build the data array
        $groups = $this->getUserGroups($this->site['slug'], 1);
        $fields = array(
            array(
                'classes' => array('formsection'),
                'formItems' => array(
                    array(
                        'fieldtype' => 'input',
                        'title' => 'name-first',
                        'name' => 'name-first',
                        'label' => array(
                            'value' => 'First Name'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'name-last',
                        'name' => 'name-last',
                        'label' => array(
                            'value' => 'Last Name'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'phone',
                        'name' => 'phone',
                        'label' => array(
                            'value' => 'Telephone'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'email',
                        'name' => 'email',
                        'label' => array(
                            'value' => 'Email'
                        ),
                        'type' => 'text'
                    )
                )
            ),
            array(
                'classes' => array('formsection'),
                'formItems' => array(
                    array(
                        'fieldtype' => 'input',
                        'title' => 'username',
                        'name' => 'username',
                        'label' => array(
                            'value' => 'Username'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'password',
                        'name' => 'password',
                        'label' => array(
                            'value' => 'Password'
                        ),
                        'type' => 'password'
                    ),
                    array(
                        'fieldtype' => 'select',
                        'title' => 'group',
                        'name' => 'group',
                        'label' => array(
                            'value' => 'Group'
                        ),
                        'options' => $groups
                    )
                )
            ),
            array(
                'classes' => array('formsection'),
                'formItems' => array(
                    array(
                        'fieldtype' => 'input',
                        'title' => 'addr_1',
                        'name' => 'addr_1',
                        'label' => array(
                            'value' => 'Street Address'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'addr_2',
                        'name' => 'addr_2',
                        'label' => array(
                            'value' => 'Unit Number'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'addr_city',
                        'name' => 'addr_city',
                        'label' => array(
                            'value' => 'City'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'addr_state',
                        'name' => 'addr_state',
                        'label' => array(
                            'value' => 'State'
                        ),
                        'type' => 'text'
                    )
                )
            ),
            array(
                'classes' => array('formsection'),
                'formItems' => array(
                    array(
                        'fieldtype' => 'input',
                        'title' => 'addr_zip',
                        'name' => 'addr_zip',
                        'text' => ' - ',
                        'label' => array(
                            'value' => 'Zip'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'addr_zip_ext',
                        'name' => 'addr_zip_ext',
                        'label' => array(
                            'value' => 'Zip Extension'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'addr_country',
                        'name' => 'addr_country',
                        'label' => array(
                            'value' => 'Country'
                        ),
                        'type' => 'text'
                    )
                )
            ),
            array(
                'classes' => array('formFooter'),
                'formItems' => array(
                    array(
                        'fieldtype' => 'input',
                        'name' => 'global',
                        'text' => 'Global user',
                        'type' => 'checkbox'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'name' => 'enabled',
                        'checked' => TRUE,
                        'text' => 'User enabled',
                        'type' => 'checkbox'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'value' => 'Update User',
                        'type' => 'submit'
                    )
                )
            )
        );

        //create the form
        $render = '<div class="login-form">' . $this->form->makeForm('login', 'accounts/edit', 'post', $fields, 1, array('edit-user-box')) . '</div>' . PHP_EOL;
        return $render;
    }

    protected function getUsers($site, $format = FALSE) {
        $query = "SELECT `login`,`email`,`group`,`suspended`,`banned`,`disabled`,`global` FROM `users` WHERE `site` IN ('$site','global') AND `active`='1' AND `show`='1' ORDER BY `hierarchy` ASC;";
        $results = $this->db->select($query);
        if ($format == TRUE) {
            //formatting for form input
            $options = array();
            foreach ($results as $group) {
                $options[] = array(
                    'value' => $group['slug'],
                    'text' => $group['display']
                );
            }
            return $options;
        } else {
            //returning raw user group results
            return $results;
        }
    }

    public function getOnlineUsers($site) {
        
    }

    public function getAccountsOverview($site) {
        
    }

    protected function getUserGroups($site, $format = FALSE) {
        $query = "SELECT `slug`,`display` FROM `user_groups` WHERE `site` IN ('$site','global') AND `show`='1' AND `slug` IN ('" . implode("','", $_SESSION['user']['permissions']) . "') AND `slug` NOT IN ('default', 'banned', 'restricted', 'SYSTEM', 'self', '" . $_SESSION['user']['group'] . "') ORDER BY `hierarchy` ASC;";
        $results = $this->db->select($query);
        if ($format == TRUE) {
            //formatting for form input
            $options = array();
            foreach ($results as $group) {
                $options[] = array(
                    'value' => $group['slug'],
                    'text' => $group['display']
                );
            }
            return $options;
        } else {
            //returning raw user group results
            return $results;
        }
    }

    protected function getUserData($user, $site) {
        
    }

    protected function getUserInfo($user, $site) {
        
    }

    public function create() {
//        if ($this->user->security->validateSessionNonce($_POST['nonce']) == TRUE) {
//            //valid user session
//            echo 'The user session is valid<br>';
//        } else {
//            //invalid user session, log out user and record session jacking attempt to security log
//            echo 'The user session does not match!<br>';
//        }
        if ($this->user->checkForExistingUser($_POST['username'], $this->site['slug']) == FALSE) {
            //account does not exist, proceed with creation
            $valid = $this->user->validateUserData($_POST);
            if (!is_array($valid)) {
                //listed account creation information is valid, proceed with account creation
                if ($this->user->makeUser($_POST, $this->site['slug']) == TRUE) {
                    //user creation successful
                    $_SESSION['form'] = array(
                        'formid' => 'create-user',
                        'server-message-type' => 'success',
                        'server-message-response' => 'User ' . $_POST['username'] . ' has been successfully created'
                    );
                    $details = $this->user->makeUserDetails($_POST, $this->site['slug']);
                    if (is_array($details)) {
                        $_SESSION['form']['server-message-response'] .= ' with the following account profile information:<ul>' . PHP_EOL;
                        foreach ($details as $item) {
                            $_SESSION['form']['server-message-response'] .= '<li>' . ucwords(str_replace('_', ' ', key($item))) . ': ' . $item[key($item)] . '</li>' . PHP_EOL;
                        }
                        $_SESSION['form']['server-message-response'] .= '</ul>' . PHP_EOL;
                    } else {
                        $_SESSION['form']['server-message-response'] .= '.' . PHP_EOL;
                    }
                    header('Location: /accounts');
                } else {
                    //user creation failed
                    header('Location: /accounts');
                }
            } else {
                //listed account creation is not valid, return errors
                $_SESSION['form']['server-message-type'] = 'fail';
                $_SESSION['form']['server-message-response'] = 'Error: The following information is required:<br><ul>' . PHP_EOL;
                foreach ($valid as $requirement) {
                    $_SESSION['form']['server-message-response'] .= '<li><sub>' . $requirement . '</sub></li>' . PHP_EOL;
                }
                $_SESSION['form']['server-message-response'] .= '</ul>' . PHP_EOL;
                header('Location: /accounts');
            }
        } else {
            //account already exists
            $_SESSION['form'] = array(
                'formid' => 'create-user',
                'server-message-type' => 'fail',
                'server-message-response' => 'This user already exists',
            );
            header('Location: /accounts');
        }
    }

}