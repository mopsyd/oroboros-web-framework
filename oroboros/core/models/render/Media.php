<?php
namespace oroboros\core\models\render;

class Media extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Manage Images',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Manage Audio',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Manage Video',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Manage Flash Content',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}