<?php

namespace oroboros\core\models\render;

class Nodes extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }

    public function index() {
        $this->controllerContent = array();
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Overview',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function local() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Local Node',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function connections() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Connections',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function requests() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Requests',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function accounts() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Remote Accounts',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function access() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Access Rights',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function sites() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Site Sharing',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function mirrors() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Mirror Instances',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function data() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Data Archives',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function content() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Content Archives',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function assets() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Assets',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function repository() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Repositories',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function cdn() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Content Delivery Networks',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function distribution() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Distribution Network',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function bridge() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Bridge Instances',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function network() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Network',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function community() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Instance Community',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function flag() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Flags',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function authorize() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Authorize Node Access',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function restrict() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Restrict Node Access',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function deauthorize() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Deauthorize Node',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function validation() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Validate Node',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function map() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Map',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function hierarchy() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Hierarchy',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function scope() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Access Scope',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function settings() {
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Node Settings',
            'permission' => 'operator',
            'content' => '<p>This is test content</p>'
        );
    }

    public function __destruct() {
        parent::__destruct();
    }

}
