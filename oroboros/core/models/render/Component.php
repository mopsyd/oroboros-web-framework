<?php

namespace oroboros\core\models\render;

class Component extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }

    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Component Overview',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function manage() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Manage Components',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function edit() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Edit Components',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function add() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Add Components',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function remove() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Remove Components',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function validate() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Validate Component Package',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function history() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Component History',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function permissions() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Component Permissions',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function import() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Import Components',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function export() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Export Components',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function package() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Package Components',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function create() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Create Components',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function inherit() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Component Inheritance',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function archive() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Archive Components',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function restore() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Restore Components',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }

    public function __destruct() {
        parent::__destruct();
    }

}