<?php
namespace oroboros\core\models\render;

class Docs extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Oroboros Documentation',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    
    function owner() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Ownership Documentation',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    function developer() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Developer Documentation',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    function webmaster() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Webmaster Documentation',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    function designer() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Designer Documentation',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    function operator() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Operator Documentation',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    function admin() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Admin Documentation',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    function moderator() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Moderator Documentation',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    function author() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Author Documentation',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    function registered() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Registered User Documentation',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    function user() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'User Documentation',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    function nodes() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Node Documentation',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    function navigation() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'System Navigation Documentation',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    function setup() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'System Setup Documentation',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    function import() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Asset Import Documentation',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    function export() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Asset Export Documentation',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    function troubleshooting() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'System Troubleshooting Documentation',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}