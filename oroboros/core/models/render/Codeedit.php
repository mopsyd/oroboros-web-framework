<?php

namespace oroboros\core\models\render;

class Codeedit extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }

    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Code Editor',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }

}