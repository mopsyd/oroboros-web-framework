<?php
namespace oroboros\core\models\render;

class Ftp extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'FTP Settings',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'FTP Connections',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'FTP Access Rights',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'FTP Whitelist',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'FTP Greylist',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'FTP Blacklist',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
}