<?php
namespace oroboros\core\models\render;

class Seo extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'SEO Overview',
                'permission' => 'webmaster',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'SEO Settings',
                'permission' => 'webmaster',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Robots.txt',
                'permission' => 'webmaster',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Sitemap',
                'permission' => 'webmaster',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Keywords',
                'permission' => 'webmaster',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'User Tracking',
                'permission' => 'webmaster',
                'content' => '<p>This is test content</p>'
            ),
        );
    }
    
    public function keywords() {
        
    }
    
    public function content() {
        
    }
    
    public function robots() {
        
    }
    
    public function crawlers() {
        
    }
    
    public function sitemap() {
        
    }
    
    public function tracking() {
        
    }
    
    public function backlinks() {
        
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}