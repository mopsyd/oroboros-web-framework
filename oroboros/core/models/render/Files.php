<?php
namespace oroboros\core\models\render;

class Files extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function index() {
        
    }
    
    public function download() {
        
    }
    
    public function upload() {
        
    }
    
    public function archive() {
        
    }
    
    public function restore() {
        
    }
    
    public function browse() {
        
    }
    
    public function filemap() {
        
    }
    
    public function import() {
        
    }
    
    public function export() {
        
    }
}