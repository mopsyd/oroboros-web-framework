<?php
namespace oroboros\core\models\render;

class Syndication extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function index() {
        $this->controllerContent = array();
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'Syndication Overview',
                'permission' => 'webmaster',
                'content' => '<p>This is test content</p>'
            ),
        );
    }
    
    public function email() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'Email Syndication',
                'permission' => 'webmaster',
                'content' => '<p>This is test content</p>'
            ),
        );
    }
    
    public function rss() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'RSS Syndication',
                'permission' => 'webmaster',
                'content' => '<p>This is test content</p>'
            ),
        );
    }
    
    public function calendar() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'Calendar Syndication',
                'permission' => 'webmaster',
                'content' => '<p>This is test content</p>'
            ),
        );
    }
    
    public function announcements() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'Announcement Syndication',
                'permission' => 'webmaster',
                'content' => '<p>This is test content</p>'
            ),
        );
    }
    
    public function node() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'Node Content Syndication',
                'permission' => 'webmaster',
                'content' => '<p>This is test content</p>'
            ),
        );
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}