<?php
namespace oroboros\core\models\render;

class Ssl extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'SSL Certificates',
                'permission' => 'webmaster',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Register SSL Certificate',
                'permission' => 'webmaster',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Deauthorize SSL Certificate',
                'permission' => 'webmaster',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Assign SSL Certificate',
                'permission' => 'webmaster',
                'content' => '<p>This is test content</p>'
            ),
        );
    }
}