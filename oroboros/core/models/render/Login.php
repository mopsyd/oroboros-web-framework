<?php
namespace oroboros\core\models\render;

class Login extends \oroboros\core\libs\Abstracts\Site\Model\Model {
    
    //permissions
    const GLOBALLOGIN = 'g_login'; //user may login to sites globally
    const LOGIN = 'login'; //user may login to the site
    const NODELOGIN = 'node_login'; //user may login from a remote node
    const GLOBALLOCKDOWNLOGIN = 'g_lock_login'; //user has global permission to login to sites while lockdown enabled
    const LOCKDOWNLOGIN = 's_lock_login'; //user has permission to login to this site while lockdown is enabled
    const GLOBALMAINTENANCELOGIN = 'g_maint_login'; //user has global permission to login to sites while maintenance mode is enabled
    const MAINTENANCELOGIN = 's_maint_login'; //user has permission to login to this site during maintenance mode
    const NODEPRIVATEFRONTEND = 'p_node_frontend'; //user may view the frontend of the site while set to private from remote nodes
    const PRIVATEFRONTEND = 'p_view_frontend'; //user may view the frontend while set to private
    const NODEPRIVATEBACKEND = 'p_node_dash'; //user may view the backend if the site while set to private from remote nodes
    const PRIVATEBACKEND = 'p_view_dash'; //user may view the backend while set to private
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function buildForm($type='browser') {
        $class = '\\oroboros\\core\\views\\Site\\Content\\' . ucfirst($type) . '\\Form';
        $file = _BASE . 'oroboros/core/views/Site/Content/' . ucfirst($type) . '/Form.php';
        if (is_readable($file)) {
            //instantiate the form object
            require $file;
            $form = new $class($this->package);
            
            //build the data array
            $fields = array(
                array(
                    'fieldtype' => 'input',
                    'title' => 'login',
                    'name' => 'login',
                    'label' => array(
                        'value' => 'Username'
                    ),
                    'type' => 'text'
                ),
                array(
                    'fieldtype' => 'input',
                    'title' => 'password',
                    'name' => 'password',
                    'label' => array(
                        'value' => 'Password'
                    ),
                    'type' => 'password'
                ),
                array(
                    'fieldtype' => 'input',
                    'value' => 'Login',
                    'type' => 'submit'
                ),
                array(
                    'fieldtype' => 'input',
                    'value' => 'Login',
                    'checked' => FALSE,
                    'text' => 'Lost password?',
                    'type' => 'checkbox'
                ),
                array(
                    'fieldtype' => 'input',
                    'value' => 'Login',
                    'checked' => FALSE,
                    'text' => 'Forgot username?',
                    'type' => 'checkbox'
                ),
            );
            
            //create the form
            $render = '<div class="login-form">' . $form->makeForm('login', 'login/run', 'post', $fields, 0, array('login-box')) . '</div>' . PHP_EOL;
            return $render;
            
        } else {
            //form class not readable, throw error
            echo 'file not found at ' . $file . '<br>';
            return FALSE;
        }
    }
    
    public function socialLogin($type) {
        
    }

    public function run() {
        $sth = $this->db->prepare("SELECT `login` FROM `user` WHERE 
                `login` = :login AND `password` = :password AND `site` IN ('" . $this->_site['slug'] . "', 'global') LIMIT 1;");
        $sth->execute(array(
            ':login' => $_POST['login'],
            ':password' => \oroboros\core\libs\Encryption\Hash::create('sha256', $_POST['password'], HASH_PASSWORD_KEY)
        ));

        $data = $sth->fetch();
        $count = $sth->rowCount();
        if ($count > 0) {
            // login
            $_SESSION['user']['loggedIn'] = TRUE;
            $this->user->site = $this->_site;
            $this->user->page = $this->_page;
            $this->user->subdomain = $this->_subdomain;
            $this->user->updateUser($data['login']);
            header('location: ../dashboard');
        } else {
            $_SESSION['form']['server-message-type'] = "fail";
            $_SESSION['form']['server-message-response'] = "Your username or password was incorrect, Please try again.";
            header('location: ../login');
        }
    }
    
    protected function setPerms($user) {
        $this->perms->initUser($user);
        $this->perms->checkPerms($_SESSION['user']['login'],$_SESSION['user']['group']['slug']);
    }

}