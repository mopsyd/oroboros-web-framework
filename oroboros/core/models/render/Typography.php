<?php
namespace oroboros\core\models\render;

class Typography extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Manage Typefaces',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Package Typefaces',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Import Fonts',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Export Fonts',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
}