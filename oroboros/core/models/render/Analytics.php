<?php
namespace oroboros\core\models\render;

class Analytics extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Site Analytics',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function users() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'User Analysis',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function google() {
                $this->controllerContent['content'] = array(
            array(
                'title' => 'Google Analytics',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function yahoo() {
                $this->controllerContent['content'] = array(
            array(
                'title' => 'Yahoo Web Analytics',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}