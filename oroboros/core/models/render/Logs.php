<?php
namespace oroboros\core\models\render;

class Logs extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'type' => 'content',
                'title' => 'Review Logs',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Clear Logs',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Archive Logs',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
            array(
                'type' => 'content',
                'title' => 'Log Settings',
                'permission' => 'admin',
                'content' => '<p>This is test content</p>'
            ),
        );
    }
    
    public function renderLogs() {
        $dashmod = new \oroboros\core\libs\Render\dashboard\Dashmod($this->package);
        $logs = $this->getLogs();
        $render = '<div class="submenu">' . PHP_EOL .
            '<div class="nav">' . PHP_EOL .
            '<ul>' . PHP_EOL;
         foreach ($logs as $logtitle) {
             $render .='<li>' . PHP_EOL . '<a href="#' . str_replace('.log', NULL, $logtitle) . '-log">' . ucfirst(str_replace('.log', NULL, $logtitle)) . '</a></li>';
         }
         $render .='</ul><div class="content">';
         $logs = $this->getLogContents($logs);
         foreach ($logs as $logcontent) {
             $render .= $this->renderLog(preg_replace('@[\r\n]@','<br>',  $logcontent)) . PHP_EOL;
         }
        
        $render .= '</div>' . PHP_EOL . '</div>' . PHP_EOL;
        return $render;
    }
    
    private function getLogs() {
        $logs = glob(_LOG . '*.log');
        $logfiles = array();
        foreach ($logs as $log) {
            $logfiles[] = str_replace(_LOG, NULL, $log);
        }
        return $logfiles;
    }
    
    private function getLogContents($logs) {
        foreach ($logs as $log) {
            $logfiles[] = $this->renderLog($this->fetchLog(_LOG . $log));
        }
        return $logfiles;
    }
    
    private function fetchLog($log) {
        if (is_readable($log)) {
            $logfile = file_get_contents($log);
            return $logfile;
        } else return false;
    }
    
    private function renderLog($log) {
        $logfile = '<span id="' . preg_match('@^[a-zA-Z0-9]{1,}@', $log) . '" class="logfile"><p>' . $log . '</p></span>';
        return $logfile;
    }
    private function getLogName($log) {
        $name = preg_match('@^[a-zA-Z0-9]{1,}@', $log);
        return $name;
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}