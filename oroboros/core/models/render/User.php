<?php

namespace oroboros\core\models\render;

class User extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }

    public function index() {
        $this->controllerContent = array();
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'User Overview',
            'permission' => 'admin',
            'content' => '<p>This is test content</p>'
        );
    }

    public function manage() {
        $this->controllerContent = array();
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'User Management',
            'permission' => 'admin',
            'content' => '<p>This is test content</p>'
        );
    }

    public function groups() {
        $this->controllerContent = array();
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'User Groups',
            'permission' => 'admin',
            'content' => '<p>This is test content</p>'
        );
    }

    public function security() {
        $this->controllerContent = array();
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'User Security',
            'permission' => 'admin',
            'content' => '<p>This is test content</p>'
        );
    }

    public function mute() {
        $this->controllerContent = array();
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Mute Users',
            'permission' => 'admin',
            'content' => '<p>This is test content</p>'
        );
    }

    public function restrict() {
        $this->controllerContent = array();
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Restrict Users',
            'permission' => 'admin',
            'content' => '<p>This is test content</p>'
        );
    }

    public function ban() {
        $this->controllerContent = array();
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Ban Users',
            'permission' => 'admin',
            'content' => '<p>This is test content</p>'
        );
    }

    public function suspend() {
        $this->controllerContent = array();
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Suspend Users',
            'permission' => 'admin',
            'content' => '<p>This is test content</p>'
        );
    }

    public function flag() {
        $this->controllerContent = array();
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Monitor Users',
            'permission' => 'admin',
            'content' => '<p>This is test content</p>'
        );
    }

    public function monitor() {
        $this->controllerContent = array();
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'User Overview',
            'permission' => 'admin',
            'content' => '<p>This is test content</p>'
        );
    }

    public function blacklist() {
        $this->controllerContent = array();
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Blacklist Settings',
            'permission' => 'admin',
            'content' => '<p>This is test content</p>'
        );
    }

    public function whitelist() {
        $this->controllerContent = array();
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Whitelist Settings',
            'permission' => 'admin',
            'content' => '<p>This is test content</p>'
        );
    }

    public function greylist() {
        $this->controllerContent = array();
        $this->controllerContent['content'][] = array(
            'type' => 'content',
            'title' => 'Greylist Settings',
            'permission' => 'admin',
            'content' => '<p>This is test content</p>'
        );
    }

    public function userList() {

        return $this->db->select('SELECT userid, login, role FROM user');
    }

    public function userSingleList($userid) {

        return $this->db->select('SELECT userid, login, role FROM user WHERE userid = :userid', array(':userid' => $userid));
    }

    public function create($data) {

        $this->db->insert('user', array(
            'login' => $data['login'],
            'password' => \oroboros\core\libs\Encryption\Hash::create('sha256', $data['password'], HASH_PASSWORD_KEY),
            'role' => $data['role']
        ));
    }

    public function editSave($data) {
        $postData = array(
            'login' => $data['login'],
            'password' => _Hash::create('sha256', $data['password'], HASH_PASSWORD_KEY),
            'role' => $data['role']
        );

        $this->db->update('user', $postData, "`userid` = {$data['userid']}");
    }

    public function delete($userid) {

        $result = $this->db->select('SELECT `role` FROM `user` WHERE `userid` = :userid', array(':userid' => $userid));

        if ($result[0]['role'] == 'owner')
            return false;

        $this->db->delete('user', "userid = '$userid'");
    }

    protected function checkUser($user, $site) {

        $x = $this->db->select('SELECT `login`,`group`,`hierarchy`,`active` FROM `user` WHERE `login`="' . $user . '" AND `site` IN ("' . $site . '", "global")');

        return $x;
    }

    public function checkParent($group) {

        $x = $this->db->select('SELECT `inherit` FROM `user_groups` WHERE `slug`="' . $group . '"');

        return $x[0]['inherit'];
    }

    protected function validateUser($user, $password) {
        //check login password
    }

    protected function checkGroup($user) {

        $group = $this->db->select('SELECT `group` FROM `user` WHERE `login`="' . $user . '"');
        $val = $this->db->select('SELECT `slug`,`display`,`inherit`,`site`,`hierarchy` FROM `user_groups` WHERE `slug`="' . $group[0]['group'] . '"');

        return $val[0];
    }

    protected function checkSite() {

        $x = $this->db->select('SELECT `slug` FROM `sites` WHERE `url`="' . $_SERVER['HTTP_HOST'] . '" LIMIT 1');

        return $x[0]['slug'];
    }

}