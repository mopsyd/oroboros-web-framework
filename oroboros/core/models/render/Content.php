<?php

namespace oroboros\core\models\render;

class Content extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }

    public function index() {
        $this->controllerContent = array();
        $this->controllerContent['content'][] = array(
            'title' => 'Content Overview',
            'content' => '<p>This is test site content.</p>'
        );
    }

    public function create() {
        $this->controllerContent['content'][] = array(
            'title' => 'Create Content',
            'content' => '<p>This is test site content.</p>'
        );
    }

    public function access() {
        $this->controllerContent['content'][] = array(
            'title' => 'Content Access',
            'content' => '<p>This is test site content.</p>'
        );
    }

    public function edit() {
        $this->controllerContent['content'][] = array(
            'title' => 'Edit Content',
            'content' => '<p>This is test site content.</p>'
        );
    }

    public function remove() {
        $this->controllerContent['content'][] = array(
            'title' => 'Remove Content',
            'content' => '<p>This is test site content.</p>'
        );
    }

    public function schedule() {
        $this->controllerContent['content'][] = array(
            'title' => 'Schedule Content',
            'content' => '<p>This is test site content.</p>'
        );
    }

    public function archive() {
        $this->controllerContent['content'][] = array(
            'title' => 'Archive Content',
            'content' => '<p>This is test site content.</p>'
        );
    }

    public function restore() {
        $this->controllerContent['content'][] = array(
            'title' => 'Restore Content',
            'content' => '<p>This is test site content.</p>'
        );
    }

    public function import() {
        $this->controllerContent['content'][] = array(
            'title' => 'Import Content',
            'content' => '<p>This is test site content.</p>'
        );
    }

    public function export() {
        $this->controllerContent['content'][] = array(
            'title' => 'Export Content',
            'content' => '<p>This is test site content.</p>'
        );
    }

    public function share() {
        $this->controllerContent['content'][] = array(
            'title' => 'Content Sharing',
            'content' => '<p>This is test site content.</p>'
        );
    }

    public function subscribe() {
        $this->controllerContent['content'][] = array(
            'title' => 'Content Subscriptions',
            'content' => '<p>This is test site content.</p>'
        );
    }

    public function package() {
        $this->controllerContent['content'][] = array(
            'title' => 'Package Content',
            'content' => '<p>This is test site content.</p>'
        );
    }

    public function __destruct() {
        parent::__destruct();
    }

}