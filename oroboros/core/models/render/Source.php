<?php

namespace oroboros\core\models\render;

class Source extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }

    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Source Control Settings',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Source Control Access Rights',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Initialize Repository',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Clone Repository',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Pull Repository Changes',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Commit Repository Changes',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Push Repository Changes',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Checkout Branch',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Create Branch',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Compare Branches',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Set Remote',
                'content' => '<p>This is test site content.</p>'
            ),
            array(
                'title' => 'Modify Remote',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }

    public function __destruct() {
        parent::__destruct();
    }

}