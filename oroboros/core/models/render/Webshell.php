<?php

namespace oroboros\core\models\render;

class Webshell extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }

    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Webshell Terminal',
                'content' => '<p>This is test site content.</p>'
            ),
        );
    }

}