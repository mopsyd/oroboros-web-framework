<?php

namespace oroboros\core\models\render;

class Permissions extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    const SYSTEMNODES = 'sys_display_all';
    const SYSTEMGROUP = 'sys_group_all';
    const GLOBALSITES = 'g_sites_edit';
    const GLOBALNODES = 'g_node_edit';
    const GLOBALGROUPS = 'g_group_edit';
    const GLOBALGROUPPERMS = 'g_perms_group';
    const GLOBALUSERPERMS = 'g_perms_user';
    const ADDNODES = 'developer';
    const EDITNODES = 'admin';
    const EDITGROUPS = 'admin';
    const EDITGROUPPERMS = 'admin';
    const EDITUSERPERMS = 'admin';

    public function __construct($package) {
        parent::__construct($package);
        $this->init();
    }

    protected function init() {
        $this->_user = $_SESSION['user']['login'];
        $type = isset($_SESSION['user']['device']['type']) ? $_SESSION['user']['device']['type'] : 'browser';
        $class = '\\oroboros\\core\\views\\Site\\Content\\' . ucfirst($type) . '\\Form';
        $file = _BASE . 'oroboros/core/views/Site/Content/' . ucfirst($type) . '/Form.php';
        if (is_readable($file)) {
            //instantiate the form object
            require $file;
            $this->form = new $class();
        } else {
            //form class not readable, throw error
            echo 'file not found at ' . $file . '<br>';
        }
    }

    public function index() {
        $this->controllerContent['content'] = array(
            array(
                'title' => 'Register Permission Node',
                'permission' => self::ADDNODES,
                'content' => $this->addPermNode()
            ),
            array(
                'title' => 'Manage Permissions Nodes',
                'permission' => self::EDITNODES,
                'content' => $this->permSets($this->perms->checkPermission($this->_user, self::SYSTEMNODES))
            ),
            array(
                'title' => 'Manage Permissions Groups',
                'permission' => self::EDITGROUPS,
                'content' => $this->permGroups($this->perms->checkPermission($this->_user, self::SYSTEMGROUP))
            ),
            array(
                'title' => 'Manage Group Permissions',
                'permission' => self::EDITGROUPPERMS,
                'content' => $this->groupPerms($this->perms->checkPermission($this->_user, self::SYSTEMNODES))
            ),
            array(
                'title' => 'Manage User Permissions',
                'permission' => self::EDITUSERPERMS,
                'content' => $this->userPerms($this->perms->checkPermission($this->_user, self::SYSTEMNODES))
            ),
        );
    }

    protected function addPermNode() {
        $fields = array(
            array(
                'classes' => array('formsection'),
                'formItems' => array(
                    array(
                        'fieldtype' => 'input',
                        'title' => 'perm-slug',
                        'name' => 'perm-slug',
                        'label' => array(
                            'value' => 'Permission Slug'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'perm-title',
                        'name' => 'perm-title',
                        'label' => array(
                            'value' => 'Permission Title'
                        ),
                        'type' => 'text'
                    ),
                    array(
                        'fieldtype' => 'input',
                        'title' => 'perm-description',
                        'name' => 'perm-description',
                        'label' => array(
                            'value' => 'Permission Description'
                        ),
                        'type' => 'text'
                    ),
                )
            ),
            array(
                'classes' => array('formsection'),
                'formItems' => array(
                    array(
                        'fieldtype' => 'select',
                        'title' => 'permission-group',
                        'name' => 'permission-group',
                        'label' => array(
                            'value' => 'Permission Group'
                        ),
                        'options' => $this->getPermissionGroups(1, 1, $this->perms->checkPermission($this->_user, self::SYSTEMNODES))
                    ),
                    array(
                        'fieldtype' => 'select',
                        'title' => 'permission-site',
                        'name' => 'permission-site',
                        'label' => array(
                            'value' => 'Site Scope'
                        ),
                        'options' => $this->getSites($this->perms->checkPermission($this->_user, self::GLOBALSITES))
                    ),
                )
            ),
            array(
                'classes' => array('bottom', 'formFooter'),
                'formItems' => array(
                    array(
                        'fieldtype' => 'input',
                        'value' => 'Create Permission',
                        'type' => 'submit'
                    )
                )
            )
        );

        //create the form
        $render = $this->form->makeForm('create-permission', 'permissions/create', 'post', $fields, 1, array('create-permission-box')) . PHP_EOL;
        return $render;
    }

    protected function permGroups($showAll = FALSE) {
        if ($showAll == TRUE) {
            //get extended permissions groups
            $groups = $this->getPermissionGroups(1, 0, 1);
        } else {
            //get standard permissions groups
            $groups = $this->getPermissionGroups(1, 0);
        }
    }

    protected function permSets($showAll = FALSE) {
        $permGroups = $this->getPermissionGroups(1, 0, $showAll);
        $perms = $this->getPermissionNodes(0, 0, $showAll);
        $permParents = array();
        foreach ($perms as $permission) {
            $permParents[] = $permission['group'];
        }
        $groups = $this->getPermissionGroups(1, 1, $this->perms->checkPermission($this->_user, self::SYSTEMNODES), 0, 1);
        $bulkGroups = array();
        foreach ($groups as $item) {
            $bulkGroups[] = array(
                'type' => 'option',
                'content' => $item['text'],
                'params' => array(
                    'value' => $item['value']
                )
            );
        }



        $render = array(
            'type' => 'ul',
            'class' => array('form-list'),
            'content' => array(
                'formhead' => array(
                    'type' => 'li',
                    'class' => array('formhead', 'full', 'clear'),
                    'content' => array(
                        array(
                            'type' => 'span',
                            'class' => array('form-item', 'left', 'pad-right'),
                            'content' => array(
                                array(
                                    'type' => 'label',
                                    'content' => 'Bulk Options',
                                    'params' => array(
                                        'for' => 'bulk-options'
                                    )
                                ),
                                array(
                                    'type' => 'br',
                                    'method' => 'short'
                                ),
                                array(
                                    'type' => 'select',
                                    'class' => NULL,
                                    'content' => array(
                                        array(
                                            'type' => 'option',
                                            'content' => 'No Action',
                                            'params' => array(
                                                'value' => 'no-op'
                                            )
                                        ),
                                        array(
                                            'type' => 'option',
                                            'content' => 'Edit',
                                            'params' => array(
                                                'value' => 'edit'
                                            )
                                        ),
                                        array(
                                            'type' => 'option',
                                            'content' => 'Schedule',
                                            'params' => array(
                                                'value' => 'schedule'
                                            )
                                        ),
                                        array(
                                            'type' => 'option',
                                            'content' => 'Enable',
                                            'params' => array(
                                                'value' => 'enable'
                                            )
                                        ),
                                        array(
                                            'type' => 'option',
                                            'content' => 'Disable',
                                            'params' => array(
                                                'value' => 'disable'
                                            )
                                        ),
                                        array(
                                            'type' => 'option',
                                            'content' => 'Delete',
                                            'params' => array(
                                                'value' => 'delete'
                                            )
                                        ),
                                    ),
                                    'params' => array(
                                        'name' => 'bulk-options',
                                    )
                                ),
                            )
                        ),
                        array(
                            'type' => 'span',
                            'class' => array('form-item', 'left', 'pad-right'),
                            'content' => array(
                                array(
                                    'type' => 'label',
                                    'content' => 'Group Assignment',
                                    'params' => array(
                                        'for' => 'group-options'
                                    )
                                ),
                                array(
                                    'type' => 'br',
                                    'method' => 'short'
                                ),
                                array(
                                    'type' => 'select',
                                    'class' => NULL,
                                    'content' => $bulkGroups,
                                    'params' => array(
                                        'name' => 'group-options',
                                    )
                                ),
                            )
                        ),
                        array(
                            'type' => 'span',
                            'class' => array('form-item', 'left'),
                            'content' => array(
                                array(
                                    'type' => 'label',
                                    'content' => 'Process Bulk Request',
                                    'params' => array(
                                        'for' => 'submit'
                                    )
                                ),
                                array(
                                    'type' => 'br',
                                    'method' => 'short'
                                ),
                                array(
                                    'type' => 'input',
                                    'params' => array(
                                        'type' => 'submit',
                                        'value' => 'Submit Changes',
                                        'name' => 'submit'
                                    )
                                ),
                            )
                        ),
                    )
                )
            )
        );
        foreach ($permGroups as $key => $value) {
            if (in_array($value['id'], $permParents)) {
                $render['content'][$key] = array(
                    'type' => 'li',
                    'class' => array('form-list-section-head'),
                    'content' => array(
                        array(
                            'type' => 'h3',
                            'id' => 'permgroup-' . $value['id'],
                            'content' => $value['title']
                        ),
                        array(
                            'type' => 'sub',
                            'class' => array('info'),
                            'content' => $value['description']
                        ),
                        'form-items' => array(
                            'type' => 'ul',
                            'class' => array('form-list-section-content'),
                            'content' => array()
                        )
                    ),
                );
                foreach ($perms as $permission) {
                    if ($permission['group'] == $value['id']) {
                        $render['content'][$key]['content']['form-items']['content'][] =
                                array(
                                    'type' => 'li',
                                    'class' => array('form-list-section-element'),
                                    'content' => array(
                                        array(
                                            'type' => 'span',
                                            'class' => array('form-list-input'),
                                            'content' => array(
                                                array(
                                                    'type' => 'input',
                                                    'class' => array('toggle'),
                                                    'params' => array(
                                                        'type' => 'checkbox',
                                                        'name' => 'permission-' . $permission['slug']
                                                    )
                                                ),
                                                array(
                                                    'type' => 'span',
                                                    'class' => array('form-list-subsection'),
                                                    'content' => array(
                                                        array(
                                                            'type' => 'p',
                                                            'class' => array('subheading'),
                                                            'content' => $permission['display']
                                                        ),
                                                        array(
                                                            'type' => 'sub',
                                                            'class' => array('description'),
                                                            'content' => $permission['description']
                                                        ),
                                                    )
                                                ),
                                                array(
                                                    'type' => 'span',
                                                    'class' => array('control-options'),
                                                    'content' => array(
                                                        array(
                                                            'type' => 'span',
                                                            'id' => $permission['slug'] . '-details',
                                                            'class' => array('ui', 'details')
                                                        ),
                                                        array(
                                                            'type' => 'span',
                                                            'id' => $permission['slug'] . '-edit',
                                                            'class' => array('ui', 'edit')
                                                        ),
                                                        array(
                                                            'type' => 'span',
                                                            'id' => $permission['slug'] . '-delete',
                                                            'class' => array('ui', 'delete')
                                                        ),
                                                    )
                                                ),
                                            )
                                        ),
                                    ),
                        );
                    }
                }
            }
        }
        $return = array(
            'type' => 'form',
            'class' => array('form-list'),
            'content' => array(
                'nonce' => $this->form->getNonceField(),
                'formlist' => array()
            ),
            'params' => array(
                'name' => 'update-perm-nodes',
                'action' => 'permissions/update',
                'method' => 'post'
            )
        );
        if (isset($_SESSION['form'])) {
            $return['content']['response'] = $this->form->getResponseField();
        }
        $return['content']['formlist'] = $render;
        return array($return);
    }

    protected function groupPerms($showAll = FALSE) {
        if ($showAll == TRUE) {
            //get extended group permissions
        } else {
            //get standard standard group permissions
        }
    }

    protected function userPerms($user = NULL, $showAll = FALSE) {
        $user = isset($user) ? $user : $this->_user;
        if ($showAll == TRUE) {
            //get extended user permissions
        } else {
            //get standard user permissions
        }
    }

    protected function getAccountPerms($site, $user, $type = 'ALLOW') {
        
    }

    protected function getGroupPerms($site, $user, $type = 'ALLOW') {
        
    }

    protected function getPermissionNodes($render = FALSE, $rendertype = FALSE, $showAll = FALSE) {
        $groups = $this->getPermissionGroups(0, 0, $showAll, 1);
        $groupArray = array();
        foreach ($groups as $item) {
            $groupArray[] = $item['slug'];
        }
        $query = $showAll == TRUE ? "SELECT `slug`,`display`,`description`,`active`,`group` FROM `permissions` WHERE `site` IN ('" . $this->site['slug'] . "', 'global') AND `group` IN ('" . implode("','", $groupArray) . "') ORDER BY `hierarchy` ASC;" : "SELECT `slug`,`display`,`description`,`active`,`group` FROM `permissions` WHERE `site` IN ('" . $this->site['slug'] . "', 'global') AND `group` IN ('" . implode("','", $groupArray) . "') AND `show`='1' ORDER BY `hierarchy` ASC;";
        $result = $this->db->select($query);
        return $render == TRUE ? $this->formatPermArray($result, $rendertype) : $result;
    }

    protected function getPermissionGroups($render = FALSE, $rendertype = FALSE, $showAll = FALSE, $slugOnly = FALSE, $no_op = FALSE) {
        if ($showAll == FALSE) {
            //display normal permissions
            $query = "SELECT `slug`,`display`,`description`,`active` FROM `permissions_groups` WHERE `show`='1' AND `slug` NOT IN ('SYSTEM') ORDER BY `hierarchy` ASC;";
        } elseif ($slugOnly == TRUE) {
            $query = $showAll == TRUE ? "SELECT `slug` FROM `permissions_groups` ORDER BY `hierarchy` ASC;" : "SELECT `slug` FROM `permissions_groups` WHERE `show`='1' ORDER BY `hierarchy` ASC;";
        } else {
            //show extended permissions
            $query = "SELECT `slug`,`display`,`description`,`active` FROM `permissions_groups` ORDER BY `hierarchy` ASC;";
        }
        $result = $this->db->select($query);
        if ($no_op == TRUE) {
            $results = array(
                array(
                    'slug' => 'no-op',
                    'display' => 'No Action',
                    'description' => '',
                    'active' => 1
                )
            );
            foreach ($result as $item) {
                $results[] = $item;
            }
            return $render == TRUE ? $this->formatPermArray($results, $rendertype) : $results;
        }
        return $render == TRUE ? $this->formatPermArray($result, $rendertype) : $result;
    }

    protected function getOrphans($count = FALSE, $global = FALSE) {
        $query = $global == TRUE ? "SELECT `slug`,`display`,`description`,`active` FROM `permissions` WHERE `group` IS NULL ORDER BY `hierarchy` ASC;" : "SELECT `slug`,`display`,`description`,`active` FROM `permissions` WHERE `group` IS NULL AND `site`='" . $this->_site['slug'] . "' ORDER BY `hierarchy` ASC;";
        $result = $this->db->select($query);
        return $count == TRUE ? count($result) : $result;
    }

    protected function formatPermArray($array, $optionList = FALSE) {
        $render = array();
        if ($optionList == TRUE) {
            //return option list
            foreach ($array as $item) {
                $render[] = array(
                    'value' => $item['slug'],
                    'text' => $item['display']
                );
            }
        } else {
            //return display list
            foreach ($array as $item) {
                $render[] = array(
                    'id' => $item['slug'],
                    'title' => $item['display'],
                    'description' => $item['description']
                );
            }
        }
        return $render;
    }

    protected function getSites($showAll = FALSE, $slugOnly = FALSE) {
        $query = $showAll = TRUE ? "SELECT `slug`,`display` FROM `sites` WHERE `slug` NOT IN ('SYSTEM','self') ORDER BY `id` ASC;" : "SELECT `slug`,`display` FROM `sites` WHERE `slug` NOT IN ('SYSTEM','self') AND `show`='1' ORDER BY `id` ASC;";
        if ($slugOnly == TRUE) {
            $query = $showAll = TRUE ? "SELECT `slug` FROM `sites` WHERE `slug` NOT IN ('SYSTEM','self') ORDER BY `id` ASC;" : "SELECT `slug` FROM `sites` WHERE `slug` NOT IN ('SYSTEM','self') AND `show`='1' ORDER BY `id` ASC;";
            return $this->db->select($query);
        }
        $result = $this->db->select($query);
        return $this->formatPermArray($result, 1);
    }

    public function create() {
        //        if ($this->user->security->validateSessionNonce($_POST['nonce']) == TRUE) {
//            //valid user session
//            echo 'The user session is valid<br>';
//        } else {
//            //invalid user session, log out user and record session jacking attempt to security log
//            echo 'The user session does not match!<br>';
//        }
        if ($this->checkForExistingPermission($_POST['perm-slug'], $this->_site['slug']) == FALSE) {
            //account does not exist, proceed with creation
            $valid = $this->validatePermissionData($_POST);
            if (!is_array($valid)) {
                //listed account creation information is valid, proceed with account creation
                if ($this->makePerm($_POST) == TRUE) {
                    //user creation successful
                    $_SESSION['form'] = array(
                        'formid' => 'create-permission',
                        'server-message-type' => 'success',
                        'server-message-response' => 'Permission: ' . $_POST['perm-title'] . ' has been successfully created'
                    );
                    header('Location: /permissions');
                } else {
                    //user creation failed
                    $_SESSION['form'] = array(
                        'formid' => 'create-permission',
                        'server-message-type' => 'fail',
                        'server-message-response' => 'Failure: unknown reason for failure. Please contact your system administrator.',
                    );
                    header('Location: /permissions');
                }
            } else {
                //listed account creation is not valid, return errors
                $_SESSION['form']['server-message-type'] = 'fail';
                $_SESSION['form']['server-message-response'] = 'Error: The following information is required:<br><ul>' . PHP_EOL;
                foreach ($valid as $requirement) {
                    $_SESSION['form']['server-message-response'] .= '<li><sub>' . $requirement . '</sub></li>' . PHP_EOL;
                }
                $_SESSION['form']['server-message-response'] .= '</ul>' . PHP_EOL;
                header('Location: /permissions');
            }
        } else {
            //account already exists
            $_SESSION['form'] = array(
                'formid' => 'create-permission',
                'server-message-type' => 'fail',
                'server-message-response' => 'This permission already exists',
            );
            header('Location: /permissions');
        }
    }

    protected function checkForExistingPermission($slug, $site) {
        $query = "SELECT `slug` FROM `permissions` WHERE `slug`='$slug' AND `site` IN ('$site','global');";
        $result = $this->db->select($query);
        return empty($result) ? FALSE : TRUE;
    }

    protected function validatePermissionData($data) {
        $valid = array();
        if (!isset($data['perm-slug']) || $data['perm-slug'] == '') {
            $valid[] = 'No permission slug provided';
        }
        if (strlen($data['perm-slug']) > 16) {
            $valid[] = 'Permission slug was longer than the required 16 characters';
        }
        if (!isset($data['perm-title']) || $data['perm-title'] == '') {
            $valid[] = 'No permission title was provided';
        }
        $groups = array();
        foreach ($this->getPermissionGroups(0, 0, $this->perms->checkPermission($this->_user, self::SYSTEMNODES, 1), 1) as $item) {
            $groups[] = $item['slug'];
        }
        if (!isset($data['permission-group']) || !in_array($data['permission-group'], $groups)) {
            //flag this for hacking attempts
            $valid[] = 'Invalid permission group provided';
        }
        $sites = array();
        foreach ($this->getSites($this->perms->checkPermission($this->_user, self::GLOBALSITES, 1)) as $item) {
            $sites[] = $item['value'];
        }

        if (!isset($data['permission-site']) || !in_array($data['permission-site'], $sites)) {
            //flag this for hacking attempts
            $valid[] = 'Invalid site scope provided';
        }
        return !empty($valid) ? $valid : TRUE;
    }

    protected function makePerm($data) {
        try {
            $result = $this->getPermissionNodes(0, 0, 1);
            $count = count($result) + 1;
            $insert = array(
                'slug' => $data['perm-slug'],
                'display' => $data['perm-title'],
                'active' => 1,
                'group' => $data['permission-group'],
                'site' => $data['permission-site'],
                'description' => isset($data['perm-description']) ? $data['perm-description'] : 'No description provided. Added on ' . $this->time->now('clock', '-') . ' by ' . $this->_user . '.',
                'show' => 1,
                'hierarchy' => $count
            );
            $result = $this->db->insert('permissions', $insert);
            return TRUE;
        } catch (Exception $e) {
            //log database error
            return FALSE;
        }
    }

    public function update() {
        
    }

    public function __destruct() {
        parent::__destruct();
    }

}