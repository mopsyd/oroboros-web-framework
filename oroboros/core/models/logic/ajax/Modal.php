<?php

namespace oroboros\core\models\logic\ajax;

class Modal extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    public function __construct($package) {
        parent::__construct($package);
    }

    public function renderModal($page, $source, $content) {
        //echo 'renderModal model fetching content for page: ' . $page . ', source: ' . $source . ', with content: ' . $content . PHP_EOL;
        return FALSE;
    }

    public function renderModalError() {
        $title = $this->makeTitle('Error', 'error');
        $content = $this->makeContent(array(array('content' => 'The server could not load the requested content', 'type' => 'h3', 'class' => 'error'), array('content' => 'Please try again later.', 'type' => 'p')), 'message');
        $footer = $this->makeFooter('');
        return $this->data->package_json(array('title' => $title, 'content' => $content, 'footer' => $footer));
    }

    protected function makeTitle($content, $class = NULL) {
        $classes = $this->checkClasses($class);
        $render = '<h2' . $classes . '>' . $content . '</h2>' . PHP_EOL;
        return $render;
    }

    protected function makeContent($content, $type, $class = NULL) {
        $render = NULL;
        switch ($type) {
            case 'message':
                $render = $this->makeMessage($content, $class);
                break;
            case 'form':
                $render = $this->makeForm($content, $class);
                break;
            case 'video':
                $render = $this->makeVideo($content, $class);
                break;
            case 'gallery':
                $render = $this->makeGallery($content, $class);
                break;
            case 'table':
                $render = $this->makeTable($content, $class);
                break;
            case 'list':
                $render = $this->makeList($content, $class);
                break;
        }
        return isset($render) ? $render : FALSE;
    }

    protected function checkClasses($class) {
        $classes = isset($class) && is_array($class) ? ' class="' . implode(' ', $class) . '"' : NULL;
        $classes = isset($class) && is_string($class) ? ' class="' . $class . '"' : $classes;
        return $classes;
    }

    protected function makeFooter($content, $class = NULL) {
        $classes = $this->checkClasses($class);
        $render = '<span' . $classes . '>';
        if (is_array($content)) {
            //render multisection footer
            $render .= '<span>';
            $type = isset($content['type']) ? $content['type'] : 'message';
            $subclass = isset($content['class']) ? $content['class'] : NULL;
            $render .= $this->makeContent($content, $type, $subclass);
            $render .= '</span>';
        } else {
            //render single footer content
            $render .= $content;
        }
        $render .= '</span>';
        return $render;
    }

    protected function makeMenu($content, $class = NULL) {
        $classes = $this->checkClasses($class);
    }

    protected function makeForm($content, $class = NULL) {
        $classes = $this->checkClasses($class);
    }

    protected function makeList($content, $class = NULL) {
        $classes = $this->checkClasses($class);
    }

    protected function makeTable($content, $class = NULL) {
        $classes = $this->checkClasses($class);
    }

    protected function makeMessage($content, $class = NULL) {
        $classes = $this->checkClasses($class);
        $type = isset($content['type']) ? $content['type'] : 'p';
        $render = '<' . $type . $classes . '>';
        if (isset($content['content'])) {
            if (is_array($content['content'])) {
                $render .= PHP_EOL;
                foreach ($content['content'] as $item) {
                    $subclass = isset($content['class']) ? $content['class'] : NULL;
                    $render .= $this->makeMessage($content, $subclass) . PHP_EOL;
                }
            } else {
                $render .= $content['content'];
            }
        } else {
            $render .= 'No content available.';
        }
        $render .= '</' . $type . '>' . PHP_EOL;
        return $render;
    }

    protected function makeVideo($content, $class = NULL) {
        $classes = $this->checkClasses($class);
    }

    protected function makeGallery($content, $class = NULL) {
        $classes = $this->checkClasses($class);
    }

    protected function makeThumbs($content, $class = NULL) {
        $classes = $this->checkClasses($class);
    }

    protected function makeUI($content, $class = NULL) {
        $classes = $this->checkClasses($class);
    }

    public function __destruct() {
        parent::__destruct();
    }

}