<?php

namespace oroboros\core\controllers\render;

class Module extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'Module';
    const MODELTYPE = 'render';

    public function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    function index() {
        $this->model->index();
        $this->render();
    }
    
    function manage() {
        $this->model->manage();
        $this->render();
    }
    
    function edit() {
        $this->model->index();
        $this->render();
    }
    
    function add() {
        $this->model->add();
        $this->render();
    }
    
    function remove() {
        $this->model->remove();
        $this->render();
    }
    
    function validate() {
        $this->model->validate();
        $this->render();
    }
    
    function history() {
        $this->model->history();
        $this->render();
    }
    
    function permissions() {
        $this->model->permissions();
        $this->render();
    }
    
    function import() {
        $this->model->import();
        $this->render();
    }
    
    function export() {
        $this->model->export();
        $this->render();
    }
    
    function package() {
        $this->model->package();
        $this->render();
    }
    
    function create() {
        $this->model->create();
        $this->render();
    }
    
    function inherit() {
        $this->model->inherit();
        $this->render();
    }
    
    function archive() {
        $this->model->archive();
        $this->render();
    }
    
    function restore() {
        $this->model->restore();
        $this->render();
    }
    
    public function __destruct() {
        parent::__destruct();
    }

}