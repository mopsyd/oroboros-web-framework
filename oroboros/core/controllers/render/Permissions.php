<?php
namespace oroboros\core\controllers\render;

class Permissions extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {
    
    const MODEL = 'Permissions';
    const MODELTYPE = 'render';

    public function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }
    
    function index() {
        $this->model->index();
        $this->render();
    }
    
    function create() {
        $this->model->create();
    }
    
    function update() {
        $this->model->update();
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}