<?php

namespace oroboros\core\controllers\render;

class Login extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'Login';
    const MODELTYPE = 'render';

    function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    function index() {
        $this->render();
    }

    public function loginForm() {
        echo '<div class="header"><h2>Please login to continue.</h2></div><div class="body">';
        echo $this->model->buildForm('browser');
        echo $this->model->socialLogin('browser');
        echo '</div><div class="footer"><a href="#">Need an account?</a></div>';
    }

    function run() {
        $this->model->run();
    }

}