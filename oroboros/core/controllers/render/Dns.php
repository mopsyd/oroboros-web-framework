<?php

namespace oroboros\core\controllers\render;

class Dns extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'Dns';
    const MODELTYPE = 'render';

    function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    function index() {
        $this->model->index();
        $this->render();
    }
    
    function register() {
        $this->model->register();
        $this->render();
    }
    
    function modify() {
        $this->model->modify();
        $this->render();
    }
    
    function deauthorize() {
        $this->model->deauthorize();
        $this->render();
    }
    
    function assign() {
        $this->model->assign();
        $this->render();
    }
    
    function redirect() {
        $this->model->redirect();
        $this->render();
    }
    
    function suspend() {
        $this->model->suspend();
        $this->render();
    }
    
    public function __destruct() {
        parent::__destruct();
    }

}