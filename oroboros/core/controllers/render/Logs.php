<?php
namespace oroboros\core\controllers\render;

class Logs extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {
    
    const MODEL = 'Logs';
    const MODELTYPE = 'render';

    function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }
    
    function index() 
    {   
        $this->model->index();
        $this->render();
    }
    
    public function renderLogs() {
        echo $this->model->renderLogs();
    }
}