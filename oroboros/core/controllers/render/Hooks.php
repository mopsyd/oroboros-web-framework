<?php

namespace oroboros\core\controllers\render;

class Hooks extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'Hooks';
    const MODELTYPE = 'render';

    public function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    function index() {
        $this->model->index();
        $this->render();
    }
    
    
    function register() {
        $this->model->register();
        $this->render();
    }
    
    function modify() {
        $this->model->modify();
        $this->render();
    }
    
    function remove() {
        $this->model->remove();
        $this->render();
    }
    
    function import() {
        $this->model->import();
        $this->render();
    }
    
    function export() {
        $this->model->export();
        $this->render();
    }

    public function __destruct() {
        parent::__destruct();
    }

}