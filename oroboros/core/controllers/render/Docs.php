<?php

namespace oroboros\core\controllers\render;

class Docs extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {
    
    const MODEL = 'Docs';
    const MODELTYPE = 'render';

    public function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    function index() {
        $this->model->index();
        $this->render();
    }
    
    function owner() {
        $this->model->owner();
        $this->render();
    }
    
    function developer() {
        $this->model->developer();
        $this->render();
    }
    
    function webmaster() {
        $this->model->webmaster();
        $this->render();
    }
    
    function designer() {
        $this->model->designer();
        $this->render();
    }
    
    function operator() {
        $this->model->operator();
        $this->render();
    }
    
    function admin() {
        $this->model->admin();
        $this->render();
    }
    
    function moderator() {
        $this->model->moderator();
        $this->render();
    }
    
    function author() {
        $this->model->author();
        $this->render();
    }
    
    function registered() {
        $this->model->registered();
        $this->render();
    }
    
    function user() {
        $this->model->user();
        $this->render();
    }
    
    function nodes() {
        $this->model->nodes();
        $this->render();
    }
    
    function navigation() {
        $this->model->navigation();
        $this->render();
    }
    
    function setup() {
        $this->model->setup();
        $this->render();
    }
    
    function import() {
        $this->model->import();
        $this->render();
    }
    
    function export() {
        $this->model->export();
        $this->render();
    }
    
    function troubleshooting() {
        $this->model->troubleshooting();
        $this->render();
    }
    
    public function __destruct() {
        parent::__destruct();
    }

}