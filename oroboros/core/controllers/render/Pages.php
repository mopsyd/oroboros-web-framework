<?php

namespace oroboros\core\controllers\render;

class Pages extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'Pages';
    const MODELTYPE = 'render';

    function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    function index() {
        $this->model->index();
        $this->render();
    }
    
    function add() {
        $this->model->add();
        $this->render();
    }

    function create() {
        $this->model->create();
    }

    function modify() {
        $this->model->modify();
        $this->render();
    }

    function edit() {
        $this->model->edit();
    }

    function import() {
        $this->model->import();
        $this->render();
    }

    function export() {
        $this->model->export();
        $this->render();
    }

    function archive() {
        $this->model->archive();
        $this->render();
    }

    function restore() {
        $this->model->restore();
        $this->render();
    }

    function remote() {
        $this->model->remote();
        $this->render();
    }

    function delete() {
        $this->model->delete();
        $this->render();
    }

    function clonePage() {
        $this->model->clonePage();
        $this->render();
    }

}