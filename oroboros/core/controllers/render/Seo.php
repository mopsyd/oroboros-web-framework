<?php

namespace oroboros\core\controllers\render;

class Seo extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'Seo';
    const MODELTYPE = 'render';

    public function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    function index() {
        $this->model->index();
        $this->render();
    }
    
    function keywords() {
        $this->model->keywords();
        $this->render();
    }
    
    function content() {
        $this->model->content();
        $this->render();
    }
    
    function robots() {
        $this->model->robots();
        $this->render();
    }
    
    function crawlers() {
        $this->model->crawlers();
        $this->render();
    }
    
    function sitemap() {
        $this->model->sitemap();
        $this->render();
    }
    
    function tracking() {
        $this->model->tracking();
        $this->render();
    }
    
    function backlinks() {
        $this->model->backlinks();
        $this->render();
    }

    public function __destruct() {
        parent::__destruct();
    }

}