<?php

namespace oroboros\core\controllers\render;

class Profile extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'Profile';
    const MODELTYPE = 'render';

    function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    function index() {
        $this->model->index();
        $this->render();
    }
    
    function notifications() {
        $this->model->notifications();
        $this->render();
    }
    
    function settings() {
        $this->model->settings();
        $this->render();
    }
    
    function messages() {
        $this->model->messages();
        $this->render();
    }
    
    function privacy() {
        $this->model->privacy();
        $this->render();
    }

}