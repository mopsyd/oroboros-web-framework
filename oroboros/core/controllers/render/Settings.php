<?php

namespace oroboros\core\controllers\render;

class Settings extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'Settings';
    const MODELTYPE = 'render';

    public function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    function index() {
        $this->model->index();
        $this->render();
    }

    public function globalSettings() {
        $this->model->index();
        $this->render();
    }

    public function site() {
        $this->model->site();
        $this->render();
    }

    public function page() {
        $this->model->page();
        $this->render();
    }

    public function content() {
        $this->model->content();
        $this->render();
    }

    public function forum() {
        $this->model->forum();
        $this->render();
    }

    public function comments() {
        $this->model->comments();
        $this->render();
    }

    public function users() {
        $this->model->users();
        $this->render();
    }

    public function registration() {
        $this->model->registration();
        $this->render();
    }

    public function modules() {
        $this->model->modules();
        $this->render();
    }

    public function components() {
        $this->model->components();
        $this->render();
    }

    public function themes() {
        $this->model->themes();
        $this->render();
    }

    public function templates() {
        $this->model->templates();
        $this->render();
    }

    public function access() {
        $this->model->access();
        $this->render();
    }

    public function nodes() {
        $this->model->nodes();
        $this->render();
    }

    public function __destruct() {
        parent::__destruct();
    }

}