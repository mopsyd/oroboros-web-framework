<?php

namespace oroboros\core\controllers\render;

class Files extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'Files';
    const MODELTYPE = 'render';

    public function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    function index() {
        $this->model->index();
        $this->render();
    }
    
    function download() {
        $this->model->download();
        $this->render();
    }
    
    function upload() {
        $this->model->upload();
        $this->render();
    }
    
    function archive() {
        $this->model->archive();
        $this->render();
    }
    
    function restore() {
        $this->model->restore();
        $this->render();
    }
    
    function browse() {
        $this->model->browse();
        $this->render();
    }
    
    function filemap() {
        $this->model->filemap();
        $this->render();
    }
    
    function import() {
        $this->model->import();
        $this->render();
    }
    
    function export() {
        $this->model->export();
        $this->render();
    }
    
    public function __destruct() {
        parent::__destruct();
    }

}