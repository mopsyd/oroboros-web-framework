<?php

/**
 * Description of pagerender
 *
 * @author Your Name <brian@mopsyd.me>
 */
namespace oroboros\core\controllers\render;

class Pagerender extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {
    
    const MODEL = 'Pagerender';
    const MODELTYPE = 'render';

    function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
        $this->init();
    }
    
    private function init() {
        
    }
    
    public function index($method=NULL) {
        $content = NULL;
        $this->render();
    }
}
