<?php

class Syndication extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'Syndication';
    const MODELTYPE = 'render';

    public function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    function index() {
        $this->model->index();
        $this->render();
    }

    function email() {
        $this->model->email();
        $this->render();
    }

    function rss() {
        $this->model->rss();
        $this->render();
    }

    function calendar() {
        $this->model->calendar();
        $this->render();
    }

    function announcements() {
        $this->model->announcements();
        $this->render();
    }

    function node() {
        $this->model->node();
        $this->render();
    }

    public function __destruct() {
        parent::__destruct();
    }

}