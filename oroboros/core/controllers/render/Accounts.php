<?php

namespace oroboros\core\controllers\render;

class Accounts extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'Accounts';
    const MODELTYPE = 'render';

    function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    //page call methods

    function index() {
        $this->model->index();
        $this->render();
    }

    function add() {
        $this->model->add();
        $this->render();
    }

    function create() {
        $this->model->create();
    }

    function modify() {
        $this->model->modify();
        $this->render();
    }

    function edit() {
        $this->model->edit();
    }

    function import() {
        $this->model->import();
        $this->render();
    }

    function export() {
        $this->model->export();
        $this->render();
    }

    function archive() {
        $this->model->archive();
        $this->render();
    }

    function restore() {
        $this->model->restore();
        $this->render();
    }

    function remote() {
        $this->model->remote();
        $this->render();
    }

    function delete() {
        $this->model->delete();
        $this->render();
    }

    function cloneAccount() {
        $this->model->cloneAccount();
        $this->render();
    }

    //dashmod call methods

    function createUserForm($type = 'browser') {
        echo $this->model->buildCreateAccountForm($type);
    }

    function editUserForm($type = 'browser') {
        echo $this->model->buildEditAccountForm($type);
    }

}