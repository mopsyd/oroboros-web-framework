<?php

namespace oroboros\core\controllers\render;

class Dashboard extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'Dashboard';
    const MODELTYPE = 'render';

    function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    function index() {
        $this->model->index();
        $this->render();
    }

    public function news() {
        echo $this->model->news();
    }

    public function sidebar() {
        echo $this->model->sidebar();
    }

    function logout() {
        $this->model->logout();
    }

}