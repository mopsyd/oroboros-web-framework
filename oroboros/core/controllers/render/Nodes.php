<?php

namespace oroboros\core\controllers\render;

class Nodes extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'Nodes';
    const MODELTYPE = 'render';

    public function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    function index() {
        $this->model->index();
        $this->render();
    }

    function local() {
        $this->model->local();
        $this->render();
    }

    function connections() {
        $this->model->connections();
        $this->render();
    }

    function requests() {
        $this->model->requests();
        $this->render();
    }

    function accounts() {
        $this->model->accounts();
        $this->render();
    }

    function access() {
        $this->model->access();
        $this->render();
    }

    function sites() {
        $this->model->sites();
        $this->render();
    }

    function mirrors() {
        $this->model->mirrors();
        $this->render();
    }

    function data() {
        $this->model->data();
        $this->render();
    }

    function content() {
        $this->model->content();
        $this->render();
    }

    function assets() {
        $this->model->assets();
        $this->render();
    }

    function repository() {
        $this->model->repository();
        $this->render();
    }

    function cdn() {
        $this->model->cdn();
        $this->render();
    }

    function distribution() {
        $this->model->distribution();
        $this->render();
    }

    function bridge() {
        $this->model->bridge();
        $this->render();
    }

    function network() {
        $this->model->network();
        $this->render();
    }

    function community() {
        $this->model->community();
        $this->render();
    }

    function flag() {
        $this->model->flag();
        $this->render();
    }

    function authorize() {
        $this->model->authorize();
        $this->render();
    }

    function restrict() {
        $this->model->restrict();
        $this->render();
    }

    function deauthorize() {
        $this->model->deauthorize();
        $this->render();
    }

    function validation() {
        $this->model->validation();
        $this->render();
    }

    function map() {
        $this->model->map();
        $this->render();
    }

    function hierarchy() {
        $this->model->hierarchy();
        $this->render();
    }

    function scope() {
        $this->model->scope();
        $this->render();
    }

    function settings() {
        $this->model->settings();
        $this->render();
    }

    public function __destruct() {
        parent::__destruct();
    }

}