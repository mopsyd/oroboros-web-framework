<?php
namespace oroboros\core\controllers\render;

class Index extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {
    
    const MODEL = 'Index';
    const MODELTYPE = 'render';

    function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }
    
    function index() {
        $this->render();
    }
}