<?php

namespace oroboros\core\controllers\render;

class Sites extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'Sites';
    const MODELTYPE = 'render';

    public function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    function index() {
        $this->model->index();
        $this->render();
    }
    
     function settings() {
        $this->model->settings();
        $this->render();
    }
    
     function create() {
        $this->model->create();
        $this->render();
    }
    
     function edit() {
        $this->model->edit();
        $this->render();
    }
    
     function remove() {
        $this->model->remove();
        $this->render();
    }
    
     function import() {
        $this->model->import();
        $this->render();
    }
    
     function export() {
        $this->model->export();
        $this->render();
    }
    
     function package() {
        $this->model->package();
        $this->render();
    }
    
     function lockdown() {
        $this->model->lockdown();
        $this->render();
    }
    
     function archive() {
        $this->model->archive();
        $this->render();
    }
    
     function share() {
        $this->model->share();
        $this->render();
    }

    public function __destruct() {
        parent::__destruct();
    }

}