<?php

namespace oroboros\core\controllers\render;

class User extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'User';
    const MODELTYPE = 'render';

    public function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    function index() {
        $this->model->index();
        $this->render();
    }
    
    function groups() {
        $this->model->groups();
        $this->render();
    }
    
    function security() {
        $this->model->security();
        $this->render();
    }
    
    function manage() {
        $this->model->manage();
        $this->render();
    }
    
    function mute() {
        $this->model->mute();
        $this->render();
    }
    
    function restrict() {
        $this->model->restrict();
        $this->render();
    }
    
    function ban() {
        $this->model->ban();
        $this->render();
    }
    
    function suspend() {
        $this->model->suspend();
        $this->render();
    }
    
    function flag() {
        $this->model->flag();
        $this->render();
    }
    
    function monitor() {
        $this->model->monitor();
        $this->render();
    }
    
    function blacklist() {
        $this->model->blacklist();
        $this->render();
    }
    
    function whitelist() {
        $this->model->whitelist();
        $this->render();
    }
    
    function greylist() {
        $this->model->greylist();
        $this->render();
    }

    public function create() {
        $data = array();
        $data['login'] = $_POST['login'];
        $data['password'] = $_POST['password'];
        $data['role'] = $_POST['role'];

        // @TODO: Do your error checking!

        $this->model->create($data);
        header('location: ' . _URL . 'user');
    }

    public function edit($id) {
        $this->view->user = $this->model->userSingleList($id);
        $this->render();
    }

    public function editSave($id) {
        $data = array();
        $data['id'] = $id;
        $data['login'] = $_POST['login'];
        $data['password'] = $_POST['password'];

        // @TODO: Do your error checking!

        $this->model->editSave($data);
        header('location: ' . _URL . 'user');
    }

    public function delete($id) {
        $this->model->delete($id);
        header('location: ' . _URL . 'user');
    }

}