<?php

namespace oroboros\core\controllers\render;

class Error extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'Error';
    const MODELTYPE = 'render';
    
    protected $errImg = NULL;

    function __construct($package, $code, $title, $message) {
        parent::__construct($package);
        $this->view->addController($this);
        $this->view->pageslug = 'error';
        //$this->init($code, $title, $message);
    }

    public function init($code, $title = NULL, $message = NULL) {
        $this->errImg = _IMAGES . 'error/' . $code . '.png';
        switch ($code) {
            case 301:
                //permanent redirect. Forward user to correct page.
                header("HTTP/1.1 301 Moved Permanently");

                //need to complete new location query
                //header("Location: http://www.New-Website.com");
                break;
            case 302:
                //temporary redirect. Forward user to correct page.
                header("HTTP/1.1 302 Moved Temporarily");

                //need to complete new location query
                //header("Location: http://www.New-Website.com");
                break;
            case 403:
                //Forbidden. Send viewer to error page.
                header($_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden");
                header('Status: 403 Forbidden');
                header('HTTP/1.0 403 Forbidden');
                $_SERVER['REDIRECT_STATUS'] = 403;
                $this->error403($title, $message);
                break;
            case 404:
                //not found. Send viewer to error page.
                header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
                header("Status: 404 Not Found");
                header("HTTP/1.0 404 Not Found");
                $_SERVER['REDIRECT_STATUS'] = 404;
                $this->error404($title, $message);
                break;
            case 500:
                //internal server error. Log incident and send viewer to error page.
                header('HTTP/1.1 500 Internal Server Error', true, 500);
                $this->error500($title, $message);
                break;
            case 503:
                //service temporarily unavailable. Try again later
                header('HTTP/1.1 503 Service Temporarily Unavailable');
                header('Status: 503 Service Temporarily Unavailable');
                header('Retry-After: 300'); //300 seconds
                $this->error503($title, $message);
                break;
            case 'maintenance':
                //the dns for the requested site is pointed to the installation, but has not yet been set up
                //set server message for the webmaster that the dns entry is parked and recieving external traffic, and should be assigned to a site
                header('HTTP/1.1 503 Service Temporarily Unavailable');
                header('Status: 503 Service Temporarily Unavailable');
                header('Retry-After: 300'); //300 seconds
                $this->errorMaintenance($title, $message);
                break;
            case 'lockdown':
                //the dns for the requested site is pointed to the installation, but has not yet been set up
                //set server message for the webmaster that the dns entry is parked and recieving external traffic, and should be assigned to a site
                header('HTTP/1.1 503 Service Temporarily Unavailable');
                header('Status: 503 Service Temporarily Unavailable');
                header('Retry-After: 300'); //300 seconds
                $this->errorLockdown($title, $message);
                break;
            case 'nosite':
                //the dns for the requested site is pointed to the installation, but has not yet been set up
                //set server message for the webmaster that the dns entry is parked and recieving external traffic, and should be assigned to a site
                header('HTTP/1.1 503 Service Temporarily Unavailable');
                header('Status: 503 Service Temporarily Unavailable');
                header('Retry-After: 300'); //300 seconds
                $this->errorNoSite($title, $message);
                break;
            case 'disabled':
                //the dns for the requested site is pointed to the installation, but has not yet been set up
                //set server message for the webmaster that the dns entry is parked and recieving external traffic, and should be assigned to a site
                header('HTTP/1.1 503 Service Temporarily Unavailable');
                header('Status: 503 Service Temporarily Unavailable');
                header('Retry-After: 300'); //300 seconds
                $this->errorDisabled($title, $message);
                break;
        }
        $this->render();
    }

    private function error403($title = NULL, $message = NULL) {
        if (isset($title)) {
            $this->view->title = $title;
        } else {
            $this->view->title = 'Access denied!';
        }
        if (isset($message)) {
            $this->view->msg = $message . '<br>';
        } else {
            $this->view->msg = 'You do not have permission to view this page.<br>';
        }
        $this->view->errImg = $this->errImg;
    }

    private function error404($title = NULL, $message = NULL) {
        if (isset($title)) {
            $this->view->title = $title;
        } else {
            $this->view->title = 'Page not found!';
        }
        if (isset($message)) {
            $this->view->msg = $message . '<br>';
        } else {
            $this->view->msg = 'This page doesn\'t exist on this server.<br>';
        }
        $this->view->errImg = $this->errImg;
    }

    private function error500($title = NULL, $message = NULL) {
        if (isset($title)) {
            $this->view->title = $title;
        } else {
            $this->view->title = 'Oops!';
        }
        if (isset($message)) {
            $this->view->msg = $message . '<br>';
        } else {
            $this->view->msg = 'Looks like we broke something...<br>';
        }
        $this->view->errImg = $this->errImg;
    }
    
    private function error503($title = NULL, $message = NULL) {
        if (isset($title)) {
            $this->view->title = $title;
        } else {
            $this->view->title = 'Oops!';
        }
        if (isset($message)) {
            $this->view->msg = $message . '<br>';
        } else {
            $this->view->msg = 'Looks like we broke something...<br>';
        }
        $this->view->errImg = $this->errImg;
    }

    private function errorNoSite($title = NULL, $message = NULL) {
        if (isset($title)) {
            $this->view->title = $title;
        } else {
            $this->view->title = 'This site has not yet been configured';
        }
        if (isset($message)) {
            $this->view->msg = $message . '<br>';
        } else {
            $this->view->msg = 'We\'re working on building something awesome. Please check back later!<br>';
        }
        $this->view->errImg = $this->errImg;
    }

    private function errorDisabled($title = NULL, $message = NULL) {
        if (isset($title)) {
            $this->view->title = $title;
        } else {
            $this->view->title = 'Unavailable';
        }
        if (isset($message)) {
            $this->view->msg = $message . '<br>';
        } else {
            $this->view->msg = 'This site is currently disabled. Please try again later.<br>';
        }
        $this->view->errImg = $this->errImg;
    }

    private function errorMaintenance($title = NULL, $message = NULL) {
        if (isset($title)) {
            $this->view->title = $title;
        } else {
            $this->view->title = 'We\'ll Be Right Back';
        }
        if (isset($message)) {
            $this->view->msg = $message . '<br>';
        } else {
            $this->view->msg = 'This site is currently undergoing maintenance. Please try again later.<br>';
        }
        $this->view->errImg = $this->errImg;
    }

    private function errorLockdown($title = NULL, $message = NULL) {
        if (isset($title)) {
            $this->view->title = $title;
        } else {
            $this->view->title = 'Access Denied';
        }
        if (isset($message)) {
            $this->view->msg = $message . '<br>';
        } else {
            $this->view->msg = 'This Site is currently in lockdown. Please try again later.<br>';
        }
        $this->view->errImg = $this->errImg;
    }

}