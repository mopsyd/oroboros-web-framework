<?php

namespace oroboros\core\controllers\render;

class Links extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'Links';
    const MODELTYPE = 'render';

    public function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    function index() {
        $this->model->index();
        $this->render();
    }
    
    function add() {
        $this->model->add();
        $this->render();
    }
    
    function modify() {
        $this->model->modify();
        $this->render();
    }
    
    function remove() {
        $this->model->remove();
        $this->render();
    }
    
    function archive() {
        $this->model->archive();
        $this->render();
    }
    
    function restore() {
        $this->model->restore();
        $this->render();
    }
    
    function import() {
        $this->model->import();
        $this->render();
    }
    
    function export() {
        $this->model->export();
        $this->render();
    }
    
    function cloneLinks() {
        $this->model->cloneLinks();
        $this->render();
    }
    
    public function __destruct() {
        parent::__destruct();
    }

}