<?php

namespace oroboros\core\controllers\render;

class Themes extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'Themes';
    const MODELTYPE = 'render';

    public function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    function index() {
        $this->model->index();
        $this->render();
    }
    
    public function __destruct() {
        parent::__destruct();
    }

}