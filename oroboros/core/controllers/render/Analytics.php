<?php

namespace oroboros\core\controllers\render;

class Analytics extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'Analytics';
    const MODELTYPE = 'render';

    public function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    function index() {
        $this->model->index();
        $this->render();
    }
    
    function users() {
        $this->model->users();
        $this->render();
    }
    
    function google() {
        $this->model->google();
        $this->render();
    }
    
    function yahoo() {
        $this->model->yahoo();
        $this->render();
    }

    public function __destruct() {
        parent::__destruct();
    }

}