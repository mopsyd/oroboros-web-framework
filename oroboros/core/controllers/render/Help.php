<?php
namespace oroboros\core\controllers\render;

class Help extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {
    
    const MODEL = 'Help';
    const MODELTYPE = 'render';

    function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }
    
    function index() {
        $this->render();
    }

    public function other($arg = false) {
        
        require 'models/help_model.php';
        $model = new \oroboros\core\models\Help_Model($this->package);
        $this->view->blah = $model->blah();
        
    }

}