<?php

namespace oroboros\core\controllers\render;

class About extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'About';
    const MODELTYPE = 'render';

    public function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    function index() {
        $this->model->index();
        $this->render();
    }
    
    function development() {
        $this->model->development();
        $this->render();
    }
    
    function design() {
        $this->model->design();
        $this->render();
    }
    
    function networking() {
        $this->model->networking();
        $this->render();
    }
    
    function extensibility() {
        $this->model->extensibility();
        $this->render();
    }
    
    function display() {
        $this->model->display();
        $this->render();
    }
    
    function inheritance() {
        $this->model->inheritance();
        $this->render();
    }
    
    function security() {
        $this->model->security();
        $this->render();
    }

    public function __destruct() {
        parent::__destruct();
    }

}