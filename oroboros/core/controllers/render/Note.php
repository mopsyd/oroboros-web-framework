<?php
namespace oroboros\core\controllers\render;

class Note extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {
    
    const MODEL = 'Note';
    const MODELTYPE = 'render';

    public function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    public function index() {
        $this->view->noteList = $this->model->noteList();
        $this->render();
    }

    public function create() {
        $data = array(
            'title' => $_POST['title'],
            'content' => $_POST['content']
        );
        $this->model->create($data);
        header('location: ' . _URL . 'note');
    }

    public function edit($id) {
        $this->view->note = $this->model->noteSingleList($id);
        $this->render();

        if (empty($this->view->note)) {
            die('This is an invalid note!');
        }
    }

    public function editSave($noteid) {
        $data = array(
            'noteid' => $noteid,
            'title' => $_POST['title'],
            'content' => $_POST['content']
        );

        // @TODO: Do your error checking!

        $this->model->editSave($data);
        header('location: ' . _URL . 'note');
    }

    public function delete($id) {
        $this->model->delete($id);
        header('location: ' . _URL . 'note');
    }

}