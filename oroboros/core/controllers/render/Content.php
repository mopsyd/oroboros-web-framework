<?php

namespace oroboros\core\controllers\render;

class Content extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {

    const MODEL = 'Content';
    const MODELTYPE = 'render';

    public function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }

    function index() {
        $this->model->index();
        $this->render();
    }
    
    public function create() {
        $this->model->create();
        $this->render();
    }
    
    public function access() {
        $this->model->access();
        $this->render();
    }
    
    public function edit() {
        $this->model->edit();
        $this->render();
    }
    
    public function remove() {
        $this->model->index();
        $this->render();
    }
    
    public function schedule() {
        $this->model->schedule();
        $this->render();
    }
    
    public function archive() {
        $this->model->archive();
        $this->render();
    }
    
    public function restore() {
        $this->model->restore();
        $this->render();
    }
    
    public function import() {
        $this->model->import();
        $this->render();
    }
    
    public function export() {
        $this->model->export();
        $this->render();
    }
    
    public function share() {
        $this->model->share();
        $this->render();
    }
    
    public function subscribe() {
        $this->model->subscribe();
        $this->render();
    }
    
    public function package() {
        $this->model->package();
        $this->render();
    }

    public function __destruct() {
        parent::__destruct();
    }

}