<?php
namespace oroboros\core\controllers\logic\ajax;

class Modal extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {
    
    const MODEL = 'Modal';
    const MODELTYPE = 'logic/ajax';

    function __construct($package) {
        parent::__construct($package);
        $this->view->addController($this);
    }
    
    public function renderModal($param) {
        $params = array();
        $count = 0;
        foreach($param as $parameter) {
            $params[$count] = $parameter;
            $count++;
        }
        $render = $this->model->renderModal($params[0], $params[1], $params[2]);
        if ($render != FALSE) {
            //return modal content
            return $render;
        } else {
            //modal content not found, return modal error message
            $render = $this->model->renderModalError();
            return $render;
        }
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}