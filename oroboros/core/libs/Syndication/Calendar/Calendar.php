<?php
/**
 * Description of _CalendarSyndication
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Syndication\Calendar;

class Calendar extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    function __construct($package=NULL) {
        parent::__construct($package);
    }
    
    public function init() {
        
    }
    
    public function register_calendar($calendar, $site, $group, $share=FALSE) {
        
    }
    
    public function unregister_calendar($calendar, $site) {
        
    }
    
    public function set_access($calendar, $site, $group=NULL, $user=NULL, $ip=NULL) {
        
    }
    
    function __destruct() {
        parent::__destruct();
    }
}

?>
