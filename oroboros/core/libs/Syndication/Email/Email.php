<?php
/**
 * Description of EmailSyndication
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Syndication\Email;

class Email extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    function __construct($package=NULL) {
        parent::__construct($package);
    }
    
    public function init() {
        
    }
    
    public function push_broadcast($email, $group, $site) {
        
    }
    
    public function schedule_broadcast($email, $group, $schedule, $recurrance=NULL) {
        
    }
    
    public function register_group($group, $site) {
        
    }
    
    public function unregister_group($group, $site) {
        
    }
    
    function __destruct() {
        parent::__destruct();
    }
}

?>
