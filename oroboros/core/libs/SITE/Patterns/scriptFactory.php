<?php

namespace oroboros\core\libs\SITE\Patterns;

/**
 * Loads javascript requirements for dynamic scripting allocation
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class scriptFactory extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    protected function checkRegistry() {
        
    }
    
    protected function checkCdn() {
        
    }
    
    protected function unsetRegistryEntry($item) {
        
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}