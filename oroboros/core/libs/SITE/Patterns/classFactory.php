<?php

namespace oroboros\core\libs\SITE\Patterns;

/**
 * Description of classFactory
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class classFactory extends \oroboros\core\libs\Abstracts\Patterns\Factory {

    private static $register = array();
    protected static $autoload;

    public function __construct() {
        
    }

    public function loadObject($location, $class, $params = NULL) {
        if ($this->checkObject($location . '/' . $class . '.php') != FALSE) {
            $val = parent::__construct($location, $class, $params);
            parent::__destruct();
            return $val;
        } else {
            $val = parent::__construct($location, $class, $params, TRUE);
        }
        parent::__destruct();
        return $val;
    }

    public function checkObject($class) {
        if (!in_array($class, self::$register)) {
            self::$register[] = $class;
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function __destruct() {
        
    }

}