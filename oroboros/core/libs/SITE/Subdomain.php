<?php

namespace oroboros\core\libs\SITE;

/**
 * Description of Subdomain
 * this class handles the initial site loading settings, determines
 * access rights, dependencies, node redirection, and most other aspects
 * of the initial page load based on the subdomain settings.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */



/**
 * NOTES:
 * A valid subdomain may be 1-63 characters
 * It may contain hyphens, but may not begin or end with them
 * Double hyphens should validate on external domains, but should not be allowed in this system
 * A site may not have more than 127 subdomains
 * 
 * All sites should resolve normally with the "www" subdomain, but should be
 * redirected to their root dns based on site settings
 */
class Subdomain extends \oroboros\core\libs\Abstracts\Site\Lib {

    public function __construct($package = NULL) {
        parent::__construct($package);
    }

    public function init() {
        
    }

    public function checkSubdomain($subdomain = NULL, $site = NULL) {
        if (!isset($subdomain)) {
            //checks the subdomain for the current site
            $subdomain = explode('.', $_SERVER['HTTP_HOST']);
            if ($subdomain[0] == 'www' || count($subdomain) == 2) {
                //default subdomain
                $this->_subdomain = 'default';
                if (count($subdomain) == 3) {
                    $this->_domain = $subdomain[1] . '.' . $subdomain[2];
                } else {
                    $this->_domain = $subdomain[0] . '.' . $subdomain[1];
                }
                return array(
                    'subdomain' => $this->_subdomain,
                    'domain' => $this->_domain
                );
            } else {
                //non-default subdomain, continue processing
                $this->_subdomain = $subdomain[0];
                $this->_domain = $subdomain[1] . '.' . $subdomain[2];
                //check subdomain table, if not listed for site, 404 page. If webmaster access, also ask if you want to add the subdomain to the database.
                return array(
                    'subdomain' => $this->_subdomain,
                    'domain' => $this->_domain
                );
            }
            //echo 'Subdomain: ' . $this->_subdomain . '<br>';
            //echo 'Domain: ' . $this->_domain . '<br>';
        } else {
            //checks the subdomain for the specified site
        }
    }

    public function checkAccess($site, $subdomain) {
        
    }

    public function getInfo($site, $subdomain) {
        
    }

    public function registerSubdomain($site, $subdomain) {
        
    }

    public function importSubdomain($site, $subdomain, $layout) {
        
    }

    public function exportSubdomain($site, $subdomain, $location) {
        
    }

    public function addSubdomain($site, $subdomain) {
        
    }

    public function deleteSubdomain($site, $subdomain) {
        
    }

    public function suspendSubdomain($site, $subdomain) {
        
    }

    public function enableSubdomain($site, $subdomain) {
        
    }

    public function archiveSubdomain($site, $subdomain) {
        
    }

    public function restoreSubdomain($site, $subdomain) {
        
    }

    public function packageSubdomain($site, $subdomain, $target) {
        
    }

    public function unpackageSubdomain($site, $subdomain, $file) {
        
    }

    public function __destruct() {
        parent::__destruct();
    }

}