<?php

namespace oroboros\core\libs\SITE;

/**
 * This class handles node resolution for web page loads
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Node extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    public function __construct($package) {
        parent::__construct($package);
        $this->init();
    }
    
    protected function init() {

    }
    
    public function __destruct() {
        parent::__destruct();
    }
}