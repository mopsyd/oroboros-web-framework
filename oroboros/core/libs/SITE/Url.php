<?php

namespace oroboros\core\libs\SITE;

/**
 * This class parses the url string and determines the page hierarchy,
 * which method to call, and which params to pass.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Url extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    public function __construct($package) {
        parent::__construct($package);
        $this->init();
    }
    
    protected function init() {
        $this->url = explode('.', $_SERVER['HTTP_HOST']);
        if ($this->url[0] == 'www') {
            $dnsfix = array();
            unset($this->url[0]);
            foreach ($this->url as $item) {
                $dnsfix[] = $item;
            }
            $this->url = $dnsfix;
        }
        if (count($this->url) > 2) {
            $domain = $this->url[0];
            $dnsfix = array();
            unset($this->url[0]);
            $suffix = implode('.', $this->url);
            $this->url = array($domain, $suffix);
        }
        $this->request = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
        $this->request[1] = isset($this->request[1]) ? $this->request[1] : NULL;
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}