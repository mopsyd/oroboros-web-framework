<?php

namespace oroboros\core\libs\SITE;

/**
 * This class handles the creation, deletion, validation, and updating of dns entries
 * for the system.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Dns extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    protected $node = NULL;

    public function __construct($package) {
        parent::__construct($package);
        $this->init();
    }

    protected function init($domain = NULL) {
        $domain = isset($domain) ? $domain : $_SERVER['HTTP_HOST'];
        $this->url = is_array($domain) ? $domain : explode('.', $domain);
        if ($this->url[0] == 'www') {
            $dnsfix = array();
            unset($this->url[0]);
            foreach ($this->url as $item) {
                $dnsfix[] = $item;
            }
            $this->url = $dnsfix;
        }
        if (count($this->url) > 2) {
            $domain = $this->url[0];
            $dnsfix = array();
            unset($this->url[0]);
            $suffix = implode('.', $this->url);
            $this->url = array($domain, $suffix);
        }
    }

    public function handleDns($dns = NULL) {
        $dns = isset($dns) ? $dns : $_SERVER['HTTP_HOST'];
        $check = explode('.', $dns);
        switch (count($check)) {
            case 1:
                //malformed dns. This error should never happen outside a local development environment.
                //throw error and log instance of malformed dns
                return 'malformed';
                break;
            case 2:
                //dns is standard root level domain, check dns table for request
                $check = $this->checkWildcardDomain($dns);
                if ($check !== FALSE) {
                    //dns entry found
                    $type = $this->checkAccessLevel($check);
                    return $type === FALSE ? 'unregistered' : TRUE;
                } else {
                    return 'unregistered';
                }
                break;
            default:
                //dns may contain subdomain, may have multi-part domain, or may have multi-level suffix
                //perform all checks to isolate instance
                //attempt to check full domain
                $check = $this->checkWildcardDomain($dns);
                if ($check !== FALSE) {
                    //dns entry found
                    $type = $this->checkAccessLevel($check);
                    return $type === FALSE ? 'unregistered' : $type;
                } else {
                    //dns not found
                    return 'unregistered';
                }

                break;
        }
    }

    protected function isolateSubdomain($dns) {
        
    }

    protected function isolateSuffix($dns) {
        
    }

    protected function rootDomain($dns) {
        
    }
    
    protected function checkWildcardDomain($dns, $recursive=TRUE) {
        $array = explode('.', $dns);
        if ($recursive == TRUE) {
            //remove the first element of the dns and check again
            $sub = array_shift($array);
            $dns = str_replace($sub . '.', NULL, implode('.', $array));
        }
        $query = "SELECT `dns`,`site`,`node`,`active`,`show`,`default`,`global`,`private`,`staging`,`lockdown`,`maintenance`,`redirect`,`redirect_location` FROM `dns` WHERE `dns` LIKE '%" . $dns . "'";
        $result = $this->db->select($query);
        if(empty($result)) {
            return count($array) <= 2 ? false : $this->checkWildcardDomain($dns, TRUE);
        } else return $result;
    }

    public function checkDomain($dns = NULL) {
        $this->init($dns);
        $query = "SELECT `dns`,`site`,`node`,`active`,`show`,`default`,`global`,`private`,`staging`,`lockdown`,`maintenance`,`redirect`,`redirect_location` FROM `dns` WHERE `dns`='" . $this->url[0] . '.' . $this->url[1] . "' LIMIT 1;";
        $result = $this->db->select($query);
        if (!empty($result)) {
            //site exists, proceed
            $return = $result[0];
        } else {
            //site does not exist, handle missing site
            $return = FALSE;
        }
        return $return;
    }

    protected function checkAccessLevel($dns_record) {
        if ($this->checkShow($dns_record) == FALSE) {
            return 'sys-only';
        }
        $this->checkRedirect($dns_record);
        if ($this->checkOrphan($dns_record) == FALSE) {
            return 'orphan';
        }
        $this->node = $this->checkNode($dns_record);
        if ($this->checkGlobal($dns_record) == FALSE) {
            return 'global';
        }
        if ($this->checkPrivate($dns_record) == FALSE) {
            return 'private';
        }
        if ($this->checkStaging($dns_record) == FALSE) {
            return 'staging';
        }
        if ($this->checkLockdown($dns_record) == FALSE) {
            return 'lockdown';
        }
        if ($this->checkMaintenance($dns_record) == FALSE) {
            return 'maintenance';
        }
        if ($this->checkActive($dns_record) == FALSE) {
            return 'disabled';
        }
    }

    protected function checkRedirect($record) {
        if (isset($record['redirect']) && $record['redirect'] == 1) {
            //check redirection
            if (isset($record['redirect_location'])) {
                //redirect to other site
                header('Location: http://' . $record['redirect_location']);
            } else {
                //malformed redirection, log instance, set as orphan and proceed as orphan instance
                $record['redirect_location'] = NULL;
                $record['redirect'] = FALSE;
                $this->db->update('dns', array('redirect' => $record['redirect'], 'redirect_location' => $record['redirect_location']), '`dns`="' .$record['dns']. '"');
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    protected function checkOrphan($record) {
        return isset($record[0]['site']) && $record[0]['redirect'] !== 1 ? TRUE : FALSE;
    }

    protected function checkNode($record) {
        return isset($record[0]['node']) ? $node : 'local';
    }

    protected function checkActive($record) {
        return isset($record[0]['active']) && $record[0]['active'] == 1 ? TRUE : FALSE;
    }

    protected function checkShow($record) {
        return isset($record[0]['show']) && $record[0]['show'] == 1 ? TRUE : FALSE;
    }

    protected function checkDefault($record) {
        return isset($record[0]['default']) && $record[0]['default'] == 1 ? TRUE : FALSE;
    }

    protected function checkGlobal($record) {
        return isset($record[0]['global']) && $record[0]['global'] == 1 ? TRUE : FALSE;
    }

    protected function checkPrivate($record) {
        return isset($record['private']) && $record['private'] == 1 ? TRUE : FALSE;
    }

    protected function checkStaging($record) {
        return isset($record['staging']) && $record['staging'] == 1 ? TRUE : FALSE;
    }

    protected function checkLockdown($record) {
        return isset($record['lockdown']) && $record['lockdown'] == 1 ? TRUE : FALSE;
    }

    protected function checkMaintenance($record) {
        return isset($record['maintenance']) && $record['maintenance'] == 1 ? TRUE : FALSE;
    }
    
    public function addDns($dns) {
        //adds a new dns record to the database and sets a system alert for webmasters that a new dns entry is registered
        $insert = array(
                        'dns' => $this->sanitizeDns($dns),
                        'active' => 0,
                    );
        $this->db->insert('dns', $insert, 1);
    }
    
    protected function sanitizeDns($dns) {
        //removes the subdomain and and www prefix if they exist, and returns a correctly formatted dns entry
        if (is_array($dns)) {
            $dns = implode('.', $dns);
        }
        $dns = str_replace('www.', NULL, $dns);
        return $dns;
    }
    
    public function updateDns($dns, $params=NULL) {
        $check = $this->db->select("SELECT `id` FROM `dns` WHERE `dns`='$dns' LIMIT 1;");
        if (!empty($check)) {
            //record exists, proceed with update statement
            $this->db->update(`dns`, $params, '`dns` ="' . $dns . '"');
            return TRUE;
        } else return FALSE;
    }
    
    public function deleteDns($dns) {
        $check = $this->db->select("SELECT `id` FROM `dns` WHERE `dns`='$dns' LIMIT 1;");
        if (!empty($check)) {
            //record exists, proceed with delete statement
            $this->db->delete(`dns`, '`dns` ="' . $dns . '"', 1);
            return TRUE;
        } else return FALSE;
    }

    public function __destruct() {
        parent::__destruct();
    }

}