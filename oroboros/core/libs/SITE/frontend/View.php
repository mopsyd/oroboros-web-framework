<?php

namespace oroboros\core\libs\SITE\frontend;

/**
 * Description of View
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class View extends \oroboros\core\libs\Abstracts\Site\View\View {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    protected function buildControllerContent() {
        foreach ($this->controllerContent as $item) {
            switch ($item['type']) {
                case 'linkset':
                    //render a linkset
                    print 'rendering a linkset<br>';
                    break;
                case 'media':
                    //render media
                    print 'rendering media<br>';
                    break;
                case 'form':
                    //render a form
                    print 'rendering form<br>';
                    break;
                case 'post':
                    //render a post
                    print 'rendering post content<br>';
                    break;
                case 'api':
                    //render api content
                    print 'rendering api content<br>';
                    break;
                case 'custom':
                    //render custom content
                    print 'rendering custom content<br>';
                    break;
            }
        }
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}