<?php

namespace oroboros\core\libs\SITE\frontend;

/**
 * Description of Controller
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Controller extends \oroboros\core\libs\Abstracts\Site\Controller\Controller {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}