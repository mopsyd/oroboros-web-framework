<?php

namespace oroboros\core\libs\SITE\frontend;

/**
 * Description of Model
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Model extends \oroboros\core\libs\Abstracts\Site\Model\Model {
    
    public function __construct($package = NULL, $error = NULL) {
        parent::__construct($package, $error);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}