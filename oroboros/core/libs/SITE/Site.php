<?php

namespace oroboros\core\libs\SITE;

/**
 * Description of Site
 * this class handles the initial site loading settings, determines
 * access rights, dependencies, node redirection, and most other aspects
 * of the initial page load based on the site settings.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */

/**
 * NOTES
 * Total DNS length cannot exceed 255 chars, including subdomain, domain, suffix and separators
 * Most domain registrars limit to 253 characters
 * Top level domain may not be all numeric
 */
class Site extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    //permissions
    const NODELOGIN = 'node_login';
    const GLOBALLOCKDOWNLOGIN = 'g_lock_login';
    const LOCKDOWNLOGIN = 's_lock_login';
    const GLOBALMAINTENANCELOGIN = 'g_maint_login';
    const MAINTENANCELOGIN = 's_maint_login';
    const NODEPRIVATEFRONTEND = 'p_node_frontend';
    const PRIVATEFRONTEND = 'p_view_frontend';
    const NODEPRIVATEBACKEND = 'p_node_dash';
    const PRIVATEBACKEND = 'p_view_dash';

    public function __construct($package = NULL) {
        parent::__construct($package);
        $this->init();
    }

    public function init() {
        $this->security = new \oroboros\core\libs\SITE\Security($this->package);
        $this->dns = new \oroboros\core\libs\SITE\Dns($this->package);
        $this->url = new \oroboros\core\libs\SITE\Url($this->package);
        $this->node = new \oroboros\core\libs\SITE\Node($this->package);
        $this->user = new \oroboros\core\libs\User\User($this->package);
    }

    public function checkRequest($domain = NULL, $url_string = NULL) {
        $check = $this->checkSecurity($domain);
        if ($check === TRUE) {
            $check = $this->checkDns($domain);
            if ($check === TRUE) {
                $check = $this->checkUrl($url_string);
            }
        }
        return $check === TRUE ? 200 : $check;
    }

    public function checkSecurity($request = NULL) {
        $request = isset($request) ? $request : $_SERVER['REQUEST_URI'];
        $check = $this->security->checkUri($request);
        if ($check !== TRUE) {
            $mitigate = NULL;
            foreach ($check as $key => $value) {
                switch ($key) {
                    case 'hack':
                        //log hack attempts, mitigate further hacks according to site security definitions, add attempt
                        //to hack counter, if threshhold is exceeded perform temporary security lockout on ip address and
                        //throw security flag for administrator
                        echo 'mitigating hack attempt...<br>';
                        $mitigate = 'hack';
                        break;
                    case 'malformed':
                        //log malformed errors with request uri for future repair, throw 500 error
                        echo 'a malformed request was detected<br>';
                        $mitigate = 'malformed';
                        break;
                    case 'unregistered':
                        //log unregistered result for future attention, throw 404 error
                        echo 'an unregistered resource was detected<br>';
                        $mitigate = 'unregistered';
                        break;
                    case 'unauthorized':
                        //user does not have permission to access request. Throw 403 error, add attempt to
                        //forbidden counter, if unauthorized requests exceed threshold perform temporary
                        //lockout on ip address, log usage pattern in the security log, throw security flag for administrator
                        echo 'an unauthorized request was detected<br>';
                        $mitigate = 'unauthorized';
                        break;
                    case 'missing':
                        //log missing asset and throw 404 error
                        echo 'a missing asset was detected<br>';
                        $mitigate = 'missing';
                        break;
                }
            }
            return $mitigate;
        } else {
            //request is valid, proceed
            return TRUE;
        }
    }

    public function checkDns($dns = NULL) {
        $check = $this->dns->handleDns();
        if ($check !== TRUE) {
            //malformed dns
            $mitigate = NULL;
            switch ($check) {
                case 'disabled':
                    //This dns entry is disabled. 
                    echo 'site not enabled within the system<br>';
                    $mitigate = 'disabled';
                    break;
                case 'sys-only':
                    //This is a system only site entry. Attempts to access may be hack attempts. Throw 404 error, log attempt
                    echo 'system only<br>';
                    $mitigate = 'system';
                    break;
                case 'unregistered':
                    //dns is not registered in the system. Throw site not found, add dns to dns table as disabled orphan, set
                    //flag for webmaster to notify them that there has been a new dns entry made avaliable for use
                    $this->dns->addDns($_SERVER['HTTP_HOST']);
                    $mitigate = 'unregistered';
                    break;
                case 'orphan':
                    //dns exists but is not assigned to a site. Send user to "Coming soon" page.
                    //echo 'dns not assigned to a site<br>';
                    $mitigate = 'orphan';
                    break;
                case 'private':
                    //the site has been set to private. If the logged in user matches, allow access. Otherwise throw 403 error.
                    echo 'an unregistered resource was detected<br>';
                    $mitigate = 'private';
                    break;
                case 'lockdown':
                    //the site is in lockdown mode. If the user has access rights to bypass lockdown, allow standard access. Otherwise
                    //display lockdown page
                    echo 'this site is currently in lockdown<br>';
                    $mitigate = 'lockdown';
                    break;
                case 'maintenance':
                    //the site is currently in maintenance mode. Allow users with bypass access rights to enter normally, otherwise 
                    //display maintenance message
                    echo 'the site is currently in maintenence mode<br>';
                    $mitigate = 'maintenance';
                    break;
            }
            return $mitigate;
        } else {
            //resource found and validated, proceed with request
            return 200;
        }
    }

    public function checkUrl($request = NULL) {
        $request = isset($request) ? $request : $_SERVER['REQUEST_URI'];
        $check = $this->url->checkRequest($request);
    }
    
    protected function sanitizeDns($dns) {
        //removes the subdomain and and www prefix if they exist, and returns a correctly formatted dns entry
        $dns = str_replace('www.', NULL, $dns);
        return $dns;
    }

    public function __destruct() {
        parent::__destruct();
    }

}