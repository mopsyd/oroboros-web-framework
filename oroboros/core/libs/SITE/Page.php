<?php

namespace oroboros\core\libs\SITE;

/**
 * Description of Page
 * this class handles the initial site loading settings, determines
 * access rights, dependencies, node redirection, and most other aspects
 * of the initial page load based on the page settings.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Page extends \oroboros\core\libs\Abstracts\Site\Lib {

    public function __construct($package = NULL) {
        parent::__construct($package);
    }

    public function init() {
        
    }
    
    public function resolveUri($uri, $subdomain, $site) {
        $uri = trim($uri, '/');
        $pages = explode('/', $uri);
        $query = "SELECT `slug`,`display`,`description`,`parent`,`controller`,`method`,`stylesheet`,`theme`,`template`,`template_file`,`access_level` FROM `pages` WHERE `active`=1 AND `show`=1 AND `site` IN ('" . $site . "','global') AND `subdomain`='" . $subdomain . "' AND `slug` IN (\"" . implode('\",\"', $pages) . "\") ORDER BY `hierarchy` ASC;";
        $results = $this->db->select($query);
        $path = NULL;
        $num = 0;
        $prev = NULL;
        foreach ($results as $page) {
            $path .= $page['slug'] . '/';
            //iterate through results to create page path, build correct hierarchy, etc

        }
    }
    
    public function isolateMethod($pages, $results) {
        
    }

    public function get_page($slug, $site, $subdomain) {
        
    }

    public function create_page($slug, $site = NULL, $subdomain = NULL, $data = array()) {
        if (is_null($site)) {
            $site = $_SESSION['site'];
        }
        if (is_null($subdomain)) {
            $subdomain = $_SESSION['subdomain'];
        }
        $data['slug'] = $slug;
        $data['site'] = $site;
        $data['subdomain'] = $subdomain;
        $data = $this->checkPageData($data);
        if ($this->checkPagePreexisting($slug, $site, $subdomain) === TRUE) {
            //preexisting page, return false
            echo 'the page already exists<br>';
            return FALSE;
        } else {
            //page does not exist, proceed with checks
            if ($this->checkSitePreexisting($site) === TRUE) {
                //site exists, proceed
                if ($this->checkSubdomainPreexisting($site, $subdomain) === TRUE) {
                    //proceed with creation step
                    $this->db->insert('pages', $data);
                } else {
                    //subdomain does not exist, throw exception
                    echo 'the subdomain does not exist<br>';
                    return FALSE;
                }
            } else {
                //site does not exist, throw exception
                echo 'the site does not exist<br>';
                return FALSE;
            }
        }
    }
    
    protected function checkPagePreexisting($page, $site, $subdomain) {
        $test = $this->db->select("SELECT `slug` FROM `pages` WHERE `slug`='" . $page . "' AND `site` = '" . $site . "' AND `subdomain`='" . $subdomain . "' LIMIT 1;");
        if (!empty($test)) {
            return TRUE;
        } else return FALSE;
    }
    
    protected function checkSitePreexisting($site) {
        $test = $this->db->select("SELECT `slug` FROM `sites` WHERE `slug` = '" . $site . "' LIMIT 1;");
        if (!empty($test)) {
            return TRUE;
        } else return FALSE;
    }
    
    protected function checkSubdomainPreexisting($site, $subdomain) {
        $test = $this->db->select("SELECT `slug` FROM `sites_subdomains` WHERE `slug`='" . $subdomain . "' AND `site` IN ('" . $site . "','global') LIMIT 1;");
        if (!empty($test)) {
            return TRUE;
        } else return FALSE;
    }

    protected function checkPageData($data) {
        if (!isset($data['description'])) {
            $data['description'] = NULL;
        }
        if (!isset($data['parent'])) {
            $data['parent'] = 'index';
        }
        if (!isset($data['method'])) {
            $data['method'] = $this->getParentMethod($data['parent'], $data['site'], $data['subdomain']);
        }
        if (!isset($data['controller'])) {
            $data['controller'] = $this->getParentController($data['parent'], $data['site'], $data['subdomain']);
        }
        if (!isset($data['stylesheet'])) {
            $data['stylesheet'] = NULL;
        }
        if (!isset($data['theme'])) {
            $data['theme'] = $this->getParentTheme($data['parent'], $data['site'], $data['subdomain']);
        }
        if (!isset($data['template'])) {
            $data['template'] = $this->getParentTemplate($data['parent'], $data['site'], $data['subdomain']);
        }
        if (!isset($data['template_file'])) {
            $data['template_file'] = $this->getParentTemplateFile($data['parent'], $data['site'], $data['subdomain']);
        }
        if (!isset($data['access_level'])) {
            $data['access_level'] = $this->getParentAccessLevel($data['parent'], $data['site'], $data['subdomain']);
        }
        if (!isset($data['permission'])) {
            $data['permission'] = $this->getParentPermission($data['parent'], $data['site'], $data['subdomain']);
        }
        if (!isset($data['hierarchy'])) {
            $data['hierarchy'] = 999;
        }
        if (!isset($data['active'])) {
            $data['active'] = 1;
        }
        if (!isset($data['show'])) {
            $data['show'] = 1;
        }
        return $data;
    }
    
    private function getParentDetails($page, $site, $subdomain) {
        $result = $this->db->select("SELECT * FROM `pages` WHERE `slug`='" . $page . "' AND `site`='" . $site . "' AND `subdomain` = '" . $subdomain . "' LIMIT 1;");
        return $result[0];
    }
    
    public function getParentPermission($page, $site, $subdomain) {
        $permission = $this->getParentDetails($page, $site, $subdomain);
        return $permission['permission'];
    }
    
    private function getParentTemplate($page, $site, $subdomain) {
        $template = $this->getParentDetails($page, $site, $subdomain);
        return $template['template'];
    }
    
    private function getParentTemplateFile($page, $site, $subdomain) {
        $file = $this->getParentDetails($page, $site, $subdomain);
        return $file['template_file'];
    }
    
    private function getParentTheme($page, $site, $subdomain) {
        $theme = $this->getParentDetails($page, $site, $subdomain);
        return $theme['theme'];
    }
    
    private function getParentController($page, $site, $subdomain) {
        $controller = $this->getParentDetails($page, $site, $subdomain);
        return $controller['controller'];
    }
    
    private function getParentAccessLevel($page, $site, $subdomain) {
        $controller = $this->getParentDetails($page, $site, $subdomain);
        return $controller['access_level'];
    }
    
    private function getParentMethod($page, $site, $subdomain) {
        $method = $this->getParentDetails($page, $site, $subdomain);
        return $method['method'];
    }

    private function getDefaultPageController() {
        $controller = $this->db->select("SELECT `id`,`subtype` FROM `codebase` WHERE `type`='CONTROLLER' AND `subtype` IN ('CORE','LOCAL','STAGING') AND `default`=TRUE;");
        foreach ($controller as $key => $value) {
            switch ($value['subtype']) {
                case 'STAGING':
                    //check if the user has staging access rights, create page if they do
                    if (isset($_SESSION['sandbox']['staging'])) {
                        return $controller[$key]['id'];
                    }
                    break;
                case 'LOCAL':
                    //local copy exists, return that instance instead of core
                    return $controller[$key]['id'];
                    break;
                case 'CORE':
                    //only core file exists, return that instance as default
                    return $controller[$key]['id'];
                    break;
            }
        }
    }

    public function __destruct() {
        parent::__destruct();
    }

}