<?php

namespace oroboros\core\libs\SITE\backend;

/**
 * Description of View
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class View extends \oroboros\core\libs\Abstracts\Site\View\View {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    protected function buildControllerContent() {
        foreach ($this->controllerContent as $item) {
            switch ($item['type']) {
                case 'linkset':
                    //render a linkset
                    print 'rendering a linkset<br>';
                    break;
                case 'dashwidget':
                    //render a dashwidget
                    if (!isset($this->dashwidget)) {
                        //instantiate the dashwidget class
                        //$this->autoload->addNamespace();
                        $this->dashwidget = new \oroboros\core\views\Site\Content\Browser\Dashwidget();
                    }
                    
                    //create the dashwidget
                    $id = isset($item['id']) ? $item['id'] : NULL;
                    $class = isset($item['content']['classes']) && is_array($item['content']['classes']) ? $item['content']['classes'] : NULL;
                    $class = isset($item['classes']) && is_string($item['classes']) ? array($item['classes']) : $class;
                    print $this->dashwidget->render($item['content']['title'], $item['content'], $id, $class);
                    
                    break;
                case 'dashmod':
                    //render a dashwidget
                    print 'rendering a dashmod<br>';
                    if (!isset($this->dashwidget)) {
                        //instantiate the dashwidget class
                        //$this->autoload->addNamespace();
                        $this->dashmod = new \oroboros\core\views\Site\Content\Browser\Dashmod();
                    }
                    break;
                case 'dashtable':
                    //render a dashtable
                    print 'rendering a dashtable<br>';
                    break;
                case 'dashlist':
                    //render a dashlist
                    print 'rendering a dashlist<br>';
                    break;
                case 'media':
                    //render media
                    print 'rendering media<br>';
                    break;
                case 'form':
                    //render a form
                    print 'rendering form<br>';
                    break;
                case 'api':
                    //render api content
                    print 'rendering api content<br>';
                    break;
                case 'custom':
                    //render custom content
                    print 'rendering custom content<br>';
                    break;
            }
        }
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}