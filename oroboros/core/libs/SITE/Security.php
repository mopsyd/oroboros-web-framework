<?php

namespace oroboros\core\libs\SITE;

/**
 * This class handles site security based on the url request. It insures
 * that the requester has proper access rights, has made a valid request, 
 * and has not passed any subversive data in the request string
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Security extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    protected $base = NULL;
    protected $path = NULL;
    protected $badStrings = NULL;
    
    public function __construct($package) {
        parent::__construct($package);
        $this->init();
    }
    
    protected function init() {
        $this->initBadStrings();
        $this->base = $_SERVER['HTTP_HOST'];
        $this->path = _BASE;
    }
    
    public function checkUri($uri) {
        $check = array();
        foreach ($this->badStrings as $index => $section) {
            foreach ($section as $item) {
                if (strpos($uri, $item) !== FALSE) {
                    //a bad parameter was found, add to validation array
                    if(!isset($check[$index])) {
                        $check[$index] = array();
                    }
                    $check[$index][] = $item;
                }
            }
        }
        if (!empty($check)) {
            //malformed request, do not process
            return $check;
        } else {
            //proceed with request
            return TRUE;
        }
    }
    
    protected function initBadStrings() {
        $this->badStrings = array(
            'hack' => array(
               '../',
               '..\\',
                '~/',
                '~\\',
                './',
                '.\\',
                '..',
                '\\',
                '\0',
                '\\ftp',
                '\\ssh',
                '%00',
                'bin',
                'bash',
            ),
            'malformed' => array(),
            'unregistered' => array(),
            'unauthorized' => array(),
            'missing' => array()
        );
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}