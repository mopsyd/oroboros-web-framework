<?php
/**
 * Description of _Connection
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Connection;

class Connection {
    
    public function __construct() {

    }
    
    public function init() {
        
    }
    
    public function open_ssl_tunnel($destination, $user, $password=NULL, $key=NULL, $keepalive=FALSE) {
        
    }
    
    public function close_ssl_tunnel($tunnel) {
        
    }
    
    public function ftp_connect($server, $user, $password, $directory = '/', $keepalive=FALSE) {
        
    }
    
    public function ftp_close($connection) {
        
    }
    
    public function sftp_connect($server, $user, $password, $directory = '/', $keepalive=FALSE) {
        
    }
    
    public function push($connection, $content, $destination) {
        
    }
    
    public function pull($connection, $content, $location) {
        
    }
    
    public function register_ssh($connection, $user, $password, $key=NULL) {
        
    }
    
    public function unregister_ssh($connection) {
        
    }
    
    public function register_ftp($connection, $user, $password, $directory, $passive=FALSE) {
        
    }
    
    public function unregister_ftp($connection) {
        
    }
    
    public function register_sftp($connection, $user, $password, $directory, $passive=FALSE) {
        
    }
    
    public function unregister_sftp($connection) {
        
    }
     
    function __destruct() {

    }
}

?>
