<?php
namespace oroboros\core\libs\Database;

class Database extends \PDO {

    private $details = array();

    public function __construct($con = 'core', $method = 'connect', $params = NULL) {
        $this->con = $con;
        $this->mode = $method;
        $this->params = $params;
        $this->connected = FALSE;
        $this->init();
    }

    private function init() {
        $this->data = new \oroboros\core\libs\Dataflow\Dataflow();
        switch ($this->con) {
            //determines which database to affect
            case 'core':
                //loads the core database
                $this->settingsfile = _DATA . 'core.ini';
                $this->details = $this->data->fetch_ini($this->settingsfile);
                $this->details = $this->details[$this->con];
                $this->details['instance'] = $this->con;
                break;
            default:
                //loads another database connection
                $this->settingsfile = _DATA . 'db.ini';
                $this->details = $this->data->fetch_ini($this->settingsfile);
                $this->details = $this->details[$this->con];
                $this->details['instance'] = $this->con;
                break;
        }
        if (!isset($this->details['location']) || $this->details['location'] == '') {
            $this->details['location'] = 'local';
        }
        if (!isset($this->details['method']) || $this->details['method'] == '') {
            $this->details['method'] = 'TCP/IP';
        }
        if ($this->details['method'] == 'TCP/IP' && $this->details['server'] == '127.0.0.1') {
            $this->details['server'] = 'localhost';
        }
        switch ($this->mode) {
            //determines what to do to the database
            case 'new':
                //adds a new database connection to the registry
                echo '<sub class="info">The add database feature has not yet been implemented</sub>';
                break;
            case 'edit':
                //edits an existing database connection in the registry
                echo '<sub class="info">The edit database feature has not yet been implemented</sub>';
                break;
            case 'delete':
                //deletes an unneccessary database connection from the registry
                if ($con = 'core') {
                    //attempt to delete core database, log instance and disable the code that called it, flag for owner attention as critical security issue
                    echo '<sub class="critical">YOU MAY NOT DELETE THE CORE DATABASE!</sub>';
                } else {
                    echo '<sub class="info">The delete database feature has not yet been implemented</sub>';
                }
                break;
            case 'info':
                //returns info on the database from the registry if it exists

                break;
            default:
                //loads the database as a new object;
                $this->connect();
                break;
        }
    }
    
    //connection

    private function connect($attributes = array()) {
        $this->connected = TRUE;
        //$attributes[PDO::ATTR_PERSISTENT] = TRUE;
        $attributes[\PDO::ATTR_ERRMODE] = \PDO::ERRMODE_EXCEPTION;
        parent::__construct($this->details['type'] . ':host=' . $this->details['server'] . ';dbname=' . $this->details['dbname'], $this->details['user'], $this->details['password'], $attributes);
    }
    
    public function attributes($attributes) {
        foreach ($attributes as $attribute => $value) {
            parent::setAttribute($attribute, $value);
        }
    }

    public function instance() {
        return $this->details['instance'];
    }

    public function name() {
        return $this->details['dbname'];
    }

    public function user() {
        return $this->details['user'];
    }

    public function pass() {
        return $this->details['password'];
    }

    public function method() {
        return $this->details['method'];
    }

    public function port() {
        return $this->details['port'];
    }

    public function type() {
        return $this->details['type'];
    }

    public function server() {
        return $this->details['server'];
    }

    public function location() {
        return $this->details['location'];
    }
    
    //queries

    /**
     * select
     * @param string $sql An SQL string
     * @param array $array Paramters to bind
     * @param constant $fetchMode A PDO Fetch mode
     * @return mixed
     */
    public function select($sql, $array = array(), $fetchMode = \PDO::FETCH_ASSOC) {
        if ($this->connected == TRUE) {
            $sth = $this->prepare($sql);
            foreach ($array as $key => $value) {
                $sth->bindValue("$key", $value);
            }

            $sth->execute();
            return $sth->fetchAll($fetchMode);
        } else {
            //need to log instance of unconnected database query
            echo '<sub class="critical">This database connection has not been initialized! (Error at line ' . __LINE__ . ' of file ' . __FILE__ . ')</sub>' . PHP_EOL;
            return false;
        }
    }

    /**
     * insert
     * @param string $table A name of table to insert into
     * @param string $data An associative array
     */
    public function insert($table, $data, $not_exists=FALSE) {
        $noexist = $not_exists == TRUE ? ' IF NOT EXISTS' : NULL;
        if ($this->connected == TRUE) {
            ksort($data);

            $fieldNames = implode('`, `', array_keys($data));
            $fieldValues = ':' . implode(', :', array_keys($data));
            $ignore = $not_exists == TRUE ? ' IGNORE' : NULL;
            $sth = $this->prepare("INSERT" . $ignore . " INTO $table (`$fieldNames`) VALUES ($fieldValues)");

            foreach ($data as $key => $value) {
                $sth->bindValue(":$key", $value);
            }

            $sth->execute();
        } else {
            //need to log instance of unconnected database query
            echo '<sub class="critical">This database connection has not been initialized! (Error at line ' . __LINE__ . ' of file ' . __FILE__ . ')</sub>' . PHP_EOL;
            return false;
        }
    }

    /**
     * update
     * @param string $table A name of table to insert into
     * @param array $data An associative array
     * @param string $where the WHERE query part
     */
    public function update($table, $data, $where) {
        if ($this->connected == TRUE) {
            ksort($data);

            $fieldDetails = NULL;
            foreach ($data as $key => $value) {
                $fieldDetails .= "`$key`=:$key,";
            }
            $fieldDetails = rtrim($fieldDetails, ',');

            $sth = $this->prepare("UPDATE $table SET $fieldDetails WHERE $where");

            foreach ($data as $key => $value) {
                $sth->bindValue(":$key", $value);
            }

            $sth->execute();
        } else {
            //need to log instance of unconnected database query
            echo '<sub class="critical">This database connection has not been initialized! (Error at line ' . __LINE__ . ' of file ' . __FILE__ . ')</sub>' . PHP_EOL;
            return false;
        }
    }

    /**
     * delete
     * 
     * @param string $table
     * @param string $where
     * @param integer $limit
     * @return integer Affected Rows
     */
    public function delete($table, $where, $limit = 1) {
        if ($this->connected == TRUE) {
            return $this->exec("DELETE FROM $table WHERE $where LIMIT $limit");
        } else {
            //need to log instance of unconnected database query
            echo '<sub class="critical">This database connection has not been initialized! (Error at line ' . __LINE__ . ' of file ' . __FILE__ . ')</sub>' . PHP_EOL;
            return false;
        }
    }

    /**
     * inner join - select
     * 
     * @param string $target_table
     * @param array $select
     * @param string $join_table
     * @param array $on
     * @param array $where (optional)
     * @param type $order (optional)
     * @param type $sort (optional)
     */
    public function join($target_table, $select, $join_table, $on_conditions, $where = NULL, $order = NULL, $sort = 'ASC') {
        if ($this->connected == TRUE) {
            $show = NULL;
            $x = 0;
            foreach ($select as $s) {
                $x++;
                $show .= $target_table . '.' . $s;
                if (!$x == count($select)) {
                    $show .= ',';
                }
            }
            $on = NULL;
            $x = 0;
            foreach ($on_conditions as $i => $ii) {
                $x++;
                $on .= $i . '=' . $ii;
                if (!$x == count($on_conditions)) {
                    $on .= ',';
                }
            }
            $q = "SELECT $show FROM `$target_table` INNER JOIN `$join_table` ON $on";
            $x = 0;
            if (isset($where)) {
                foreach ($where as $i => $ii) {
                    if ($x == 0) {
                        $q .= " WHERE $i='$ii'";
                        $x++;
                    } else {
                        $q .= " AND $i='$ii'";
                    }
                }
            }

            if (isset($order)) {
                $q .= " ORDER BY $order $sort";
            }
            //echo $q;
            $sth = $this->query($q);
            $sth->setFetchMode(\PDO::FETCH_ASSOC);
            $result = $sth->fetchAll();
            return $result;
        } else {
            //need to log instance of unconnected database query
            echo '<sub class="critical">This database connection has not been initialized! (Error at line ' . __LINE__ . ' of file ' . __FILE__ . ')</sub>' . PHP_EOL;
            return false;
        }
    }
    
    //table info

    private function metaStats($val) {
        if ($this->connected == TRUE) {
            $sth = $this->prepare("set global innodb_stats_on_metadata=$val;");
            $sth->execute();
        } else {
            //need to log instance of unconnected database query
            echo '<sub class="critical">This database connection has not been initialized! (Error at line ' . __LINE__ . ' of file ' . __FILE__ . ')</sub>' . PHP_EOL;
            return false;
        }
    }

    public function getTableColumns($database, $table) {
        if ($this->connected == TRUE) {
            $this->metaStats(0);
            $result = $this->select("SELECT `COLUMN_NAME` FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA`='$database' AND `TABLE_NAME`='$table';");
            $columns = array();
            foreach ($result as $item) {
                $columns[] = $item['COLUMN_NAME'];
            }
            $this->metaStats(1);
            return $columns;
        } else {
            //need to log instance of unconnected database query
            echo '<sub class="critical">This database connection has not been initialized! (Error at line ' . __LINE__ . ' of file ' . __FILE__ . ')</sub>' . PHP_EOL;
            return false;
        }
    }
    
    //databases
    
    public function backupDatabase($database, $filename, $location, $format, $compression) {
        
    }
    
    //tables
    
    public function backupTable($table, $filename, $location, $format, $compression) {
        
    }
    
    public function makeTable($table, $cols, $info, $not_exists=FALSE) {
        $noexist = $not_exists == TRUE ? ' IF NOT EXISTS' : NULL;
        $query = "CREATE TABLE" . $noexist . " `$table` (
                " .$info['primary'] . " INT( " . $info['primary-length'] . " ) AUTO_INCREMENT PRIMARY KEY";
        if (!empty($cols)) {
            $query .= ", ";
        }
        foreach ($cols as $key => $value) {
            $query .= $this->makeColumn($table, $key, $value) . ',';
        }
        if (isset($info['index'])) {
            foreach ($info['index'] as $index) {
                $index['value'] = isset($index['value']) ? $index['value'] : $index['name'];
                $query .= $this->makeIndex($index['name'], $index['type'], $index['value']) . ', ';
            }
        }
        $query = rtrim($query, ', ') . ' ';
        
        $charset = isset($info['charset']) ? $info['charset'] : "'utf8'";
        $collate = isset($info['collate']) ? $info['collate'] : "'utf8_general_ci'";
        $comment = isset($info['comment']) ? " COMMENT='" . $info['comment'] . "'" : NULL;
        $query .= ") ENGINE=" . $info['engine'] . " CHARACTER SET $charset COLLATE $collate" . $comment . ";";
        $this->exec($query);
    }
    
    public function dropTable($table) {
        $query = "DROP TABLE `$table`;";
        $this->exec($query);
    }
    
    //columns
    
    protected function makeColumn($table, $colname, $details) {
        $null = isset($details['null']) ? ' NOT NULL' : NULL;
        $render = "`$colname` " . $details['type'] . "( " . $details['length'] . " )" . $null . " default " . $details['default'];
        return $render;
    }
    
    public function addColumn($table, $colname, $details) {
        $query = "ALTER TABLE `$table` ADD COLUMN `$colname`;";
        $this->exec($query);
    }
    
    public function alterColumn($table, $colname, $details) {
        switch ($this->details['type']) {
            case 'mysql':
                //MySQL database
                $mod = "MODIFY COLUMN";
                $wrap = '`';
                break;
            case 'mssql':
                //MS SQL Server database
                $mod = "ALTER COLUMN";
                $wrap = '"';
                break;
            case 'OCI':
                //Oracle database
                $mod = "MODIFY COLUMN";
                $wrap = '"';
                break;
            case 'pgsql':
                //PostGreSQL database
                $mod = "MODIFY COLUMN";
                $wrap = '"';
                break;
            
        }
        $query = "ALTER TABLE " . $wrap .$table .$wrap ." $mod " . $wrap . $colname . $wrap;
        
    }
    
    public function dropColumn($table, $colname, $details) {
        $query = "ALTER TABLE `$table` DROP COLUMN `$colname`;";
        $this->exec($query);
    }
    
    protected function makeIndex($index, $type, $value) {
        $render = $type=='UNIQUE' ? 'UNIQUE ' : NULL;
        $render .= "KEY `$index` (`$value`)";
        return $render;
    }
    
    //keys
    
    public function addIndex($table, $name, $cols, $type=NULL) {
        $index = isset($type) ? ' ' .$type : NULL;
        $query = "CREATE" . $index . " `$name` ON `$table` (`" . implode('`, `', $cols) . "`)";
        $this->exec($query);
    }
    
    public function removeIndex($table, $index) {
        $query = "ALTER TABLE `$table` DROP INDEX `$index`;";
        $this->exec($query);
    }
    
    public function checkIndexes($table) {
        
    }
    
    //relations
    
    public function addRelation($table, $cols, $key, $foreign_table, $foreign_cols, $foreign_key) {
        
    }
    
    public function removeRelation($table, $relation) {
        
    }
    
    public function checkRelations($table) {
        
    }
    
    //users
    
    public function backupUser($user, $file, $location, $format, $compression) {
        
    }
    
    public function makeUser($user, $perms, $not_exists=FALSE) {
        
    }
    
    public function editUser($user, $perms) {
        
    }
    
    public function removeUser($user) {
        
    }

    public function __destruct() {

    }
    
    //procedures
    
    public function createProcedure() {
        
    }
    
    public function editProcedure() {
        
    }
    
    public function deleteProcedure() {
        
    }
    
    public function execProcedure() {
        
    }
    
    //triggers
    
    public function createTrigger() {
        
    }
    
    public function editTrigger() {
        
    }
    
    public function checkTriggers() {
        
    }
    
    public function deleteTrigger() {
        
    }

}