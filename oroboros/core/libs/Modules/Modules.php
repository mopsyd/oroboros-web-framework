<?php
/**
 * Description of Modules
 * This class handles the loading, validation, activation, and packaging of modules
 * for the Oroboros core system, as well as the registration of their dependencies.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Modules;

class Modules extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    const PATH = 'oroboros/modules/';
    const CORE = 'core/';
    const LOCAL = 'local/';
    const STAGING = 'staging/';
    const THIRDPARTY = 'third-party/';
    
    private $modules = array();
    
    function __construct($package) {
        parent::__construct($package);
        $this->init();
    }
    
    private function init() {
        $this->settings = new \oroboros\core\libs\Settings\Module($this->package);
        $this->settings->init();
    }
    
    public function get_modules($site, $subdomain, $page) {
        $query = "SELECT `module`,`type`,`version`,`permission` FROM `sites_modules` WHERE `active`=1";
        $result = $this->db->select($query);
        if (!empty($result)) {
            //var_dump($result);
            foreach ($result as $module) {
                $package=array();
                $query = "SELECT `display`,`settings` FROM `modules` WHERE `slug`='" . $module['module'] . "' AND `type`='" . $module['type'] . "' AND `version`='" . $module['version'] . "' LIMIT 1;";
                $details = $this->db->select($query);
                $package['title'] = $details[0]['display'];
                $package['version'] = $module['version'];
                $package['type'] = $module['type'];
                $package['slug'] = $module['module'];
                $package['permission'] = $module['permission'];
                $package['settings'] = $details[0]['settings'];
                $package['path'] = _BASE . self::PATH . constant('self::' . strtoupper($module['type'])) . '/' . $module['module'] . '/';
                $this->modules[$module['module']] = $package;
            }
        }
    }
    
    public function get_module_settings($module) {
        
    }
    
    public function get_module_layout($module) {
        
    }
    
    public function disable_module($module, $site, $subdomain=NULL, $page=NULL) {
        
    }
    
    public function delete_module($module) {
        
    }
    
    public function archive_module($module) {
        
    }
    
    public function restore_module($module, $version) {
        
    }
    
    public function register_module($module) {
        
    }
    
    public function unregister_module($module) {
        
    }
    
    public function update_module($module) {
        
    }
    
    public function fetch_module_dependencies($module) {
        
    }
    
    public function get_module_conflicts($module, $target) {
        
    }
    
    function __destruct() {
        parent::__destruct();
    }
}

?>
