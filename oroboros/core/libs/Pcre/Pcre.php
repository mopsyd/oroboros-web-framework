<?php
/**
 * This file handles perl compatible regular expressions for the system
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Pcre;

class Pcre {
    
    function __construct() {
        $this->init();
    }
    
    public function init() {
    }
    
    public function match_email($content, $method='check') {
        //this method matches a potentially valid email address, using ICANN specifications
        //this method does not match currently valid top level domains, only whether or not
        //the address formulation is potentially valid. This method can also be used to remove an email
        //from a string.
        $regex = '[a-zA-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}';
    }
    
    public function match_phone($content, $region='usa', $method='check') {
        //this method will match a valid phone number by region (defaults to USA). If the region
        //does not exist in this method, it will return 'region unavailable', rather than FALSE so 
        //you are aware that the passed phone number is out of scope for this method. Instances of
        //this should be handled by the calling function or method. This method can also be used to
        //remove a phone number from a string.
        switch ($region) {
            case 'usa':
                //matches a united states valid telephone number
                
                break;
            case 'canada':
                //matches a canadian valid phone number
                
                break;
            case 'mexico':
                //matches a mexican valid phone number
                
                break;
            default:
                return 'region unavailable';
                break;
        }
    }
    
    public function match_potential_credit_card($content, $method='check') {
        //this method will check if a string potentially contains credit card numbers,
        //and will run the actual match_credit_card method on any positives for further
        //validation. It can also be used to remove card numbers from a string, or obfuscate
        //their numerical values.
        $regex = '(?:\d[ -]*?){13,16}';
    }
    
    public function match_credit_card($content, $method='check') {
        //this method will accept a credit card string which may include spaces or dashes,
        //then validate whether the card is a valid number, and which type of card it is. It can also
        //be used to remove credit cards from a string.
        $content = str_replace(' ', NULL, $content);
        $content = str_replace('-', NULL, $content);
        $regex = array(
            'visa' => '^4[0-9]{12}(?:[0-9]{3})?$',
            'mastercard' => '^4[0-9]{12}(?:[0-9]{3})?$',
            'amex' => '^3[47][0-9]{13}$',
            'dinersclub' => '^3(?:0[0-5]|[68][0-9])[0-9]{11}$',
            'discover' => '^6(?:011|5[0-9]{2})[0-9]{12}$',
            'jcb' => '^(?:2131|1800|35\d{3})\d{11}$'
            
        );

        
    }
    
    public function match_address($content, $region='usa', $method='check') {
        //this method will validate a potential address string based on the
        //valid address criteria by region (defaults to USA). If the specified region
        //is out of scope for this method, it will return 'region unavailable' to alert
        //the calling function or method that it cannot validate the address. These instances
        //should be handled by the calling function or method. It can also be used to remove an
        //address from a string.
        
    }
    
    public function match_css($content, $method='check', $tags=FALSE) {
        //this method will validate whether a css string contains errors. It does not validate whether
        //the css string represents a valid css handle or vendor extension, only whether tags are correctly
        //closed, and values are properly separated from their handle, and that the referral string is well formed.
        //It can also be used to remove css from a string. Ths method can also be instructed to ignore css that is
        //wrapped in <code> or <pre> tags, or alternately to wrap css in <code> or <pre> tags so it does not execute.
        
    }
    
    public function match_script($content, $method='check', $tags=FALSE) {
        //this method will determine whether a string contains javascript. It does not validate the javascript, only
        //determines whether or not a string potentially contains it. This is primarily to be used to remove potentially
        //hazardous scripts from user-submitted content, and should not be used to determine if your script works correctly.
        //it can also be used to remove scripts from a string. Ths method can also be instructed to ignore scripts that are
        //wrapped in <code> or <pre> tags, or alternately to wrap scripts in <code> or <pre> tags so they do not execute.
        
    }
    
    public function match_url($content, $method='check') {
        //this method will determine whether a string contains a well formed URL based on w3c specification. It does not
        //validate whether or not the URL leads to an active link. It can also be used to remove links from a string.
        
    }
    
    public function match_html($content, $method='check') {
        //this method determines if a passed string is composed of valid html markup. It can also be used to remove html from
        //a string.
        
    }
    
    public function match_php($content, $method='check') {
        //this method determines the presence of php scripting in a string passed to it. It does not validate the php, only
        //determines whether a string contains php or not. It can also be used to remove php from a string.
        
    }
    
    public function match_query($content, $method='check') {
        //this method will determine whether a string contains a valid sql query. It does not determine what the query does, only
        //whether it is present. It can also be used to strip queries from a string.
        
    }
    
    public function match_content_hook($content, $method='check') {
        //this method matches Oroboros content hooks, and can be used to find, replace, delete, or count them.
        $regex = '\[\][a-zA-Z0-9\-]{1,32}\[\/\]';
    }
    
    public function match_function_hook($content, $method='check') {
        //this method matches Oroboros function hooks, and can be used to find, replace, delete, or count them.
        $regex = '\[\[[a-zA-Z0-9\-]{1,32}\]\][[a-zA-Z0-9\-, ]{1,256}]{0,}\[\/\]';
    }
    
    public function predefined($content, $regex, $method='check') {
        //this method allows a function or method to pass a predefined regular expression, which will evaluate
        //against the defined content using the specified method. This method should be used to run custom
        //regular expressions that do not exist within the system already.
        
    }
    
    public function test($content, $regex) {
        //this method allows a function or method to test a regular expression, and determine whether or not it 
        //correctly validates against the provided content. It will not alter the content. This method should be
        //passed a string of content that should generate a positive result, and a regular expression that should
        //generate a positive match against the content. This method will return true if the regex generates a match, 
        //and false otherwise.
        
    }
    
    public function check($content, $regex) {
        //this method checks for the existence of the pattern in the content, and returns true if it finds it.
        
    }
    
    public function match($content, $regex) {
        //this method checks for the existence of the pattern in the content, and determines the location(s) within the string.
        
    }
    
    public function replace($content, $regex, $replacement) {
        //this method checks for the existence of the pattern in the content, and replaces it with the specified value.
        
    }
    
    public function count($content, $regex) {
        //this method counts the instances of the pattern in the string, and returns an integer representing this value.
        
    }
    
    public function remove($content, $regex) {
        //this method removes any instances of the pattern from the string, and returns the sanitized string.
        
    }
    
    function __destruct() {
    }
}

?>
