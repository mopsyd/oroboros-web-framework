<?php

namespace oroboros\core\libs\Autoload;

/**
 * This class autoloads phtml templates for the view.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Template {
    
    const TEMPLATEDIR = 'templates/';
    const DEFAULTSDIR = 'templates/DEFAULTS/';
    const SUFFIX = '.phtml';
    
    public function __construct() {
        ;
    }
    
    protected function init() {
        
    }
    
    public function __destruct() {
        ;
    }
}