<?php

namespace oroboros\core\libs\Autoload;

/**
 * This class autoloads javascript assets for the javascript engine.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Javascript {
    
    const COREPATH = 'views/Js/';
    const PUBLICPATH = 'public/js/lib/';
    
    public function __construct() {
        ;
    }
    
    protected function init() {
        
    }
    
    public function __destruct() {
        ;
    }
}