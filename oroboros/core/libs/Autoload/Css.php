<?php

namespace oroboros\core\libs\Autoload;

/**
 * This class autoloads css assets for the stylesheet engine.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Css {
    
    const COREPATH = 'views/Css/';
    const PUBLICPATH = 'public/css/stylesheets/';
    
    public function __construct() {
        ;
    }
    
    protected function init() {
        
    }
    
    public function __destruct() {
        ;
    }
}