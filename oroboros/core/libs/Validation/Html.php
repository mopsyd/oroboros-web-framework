<?php

namespace oroboros\core\libs\Validation;
/**
 * Description of Html
 * This file performs validation on Html strings or pages, then returns
 * a corrected string if required.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Html extends \oroboros\core\libs\Validation\Validator {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function init() {
        
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
