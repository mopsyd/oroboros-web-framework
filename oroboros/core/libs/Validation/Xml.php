<?php

namespace oroboros\core\libs\Validation;
/**
 * Description of Xml
 * This file performs validation on xml files and content, then returns
 * a corrected string if required.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Xml extends \oroboros\core\libs\Validation\Validator {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function init() {
        
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
