<?php

namespace oroboros\core\libs\Validation;
/**
 * Description of Javascript
 * This file performs validation on javascript files and scripts, then returns
 * a corrected string if required.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Javascript extends \oroboros\core\libs\Validation\Validator {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function init() {
        
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
