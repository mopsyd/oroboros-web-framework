<?php

namespace oroboros\core\libs\Validation;
/**
 * Description of Ajax
 * This file performs validation on Ajax calls to insure that they are correctly
 * formatted, sent from the correct user, ip and device, contain a valid nonce,
 * and are requesting a valid operation that the user has permission to request.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Ajax extends \oroboros\core\libs\Validation\Validator {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function init() {
        
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
