<?php

namespace oroboros\core\libs\Validation;
/**
 * Description of Css
 * This file performs validation on css stylesheets and content, then returns
 * a corrected string if required.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Css extends \oroboros\core\libs\Validation\Validator {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function init() {
        
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
