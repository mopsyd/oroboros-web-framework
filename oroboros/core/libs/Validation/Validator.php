<?php

namespace oroboros\core\libs\Validation;

/**
 * Description of Validator
 * This is the abstract class used as the template for all other validation classes
 * 
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
abstract class Validator extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    const AUTOLOAD_FILE = '/libs/Autoload/Autoload.php';
    const AUTOLOAD_REGISTER = '/libs/Validation/';
    
    public function __construct($package) {
        parent::__construct($package);
        $this->init();
    }
    
    private function init() {
        
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}