<?php
/**
 * Loads the debug settings for the system.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Settings;

class Debug {
    
    const SETTINGS = 'debug.ini';
    
    public function __construct() {
        $this->init();
    }
    
    private function init() {
        $this->settingsfile = _DATA . 'debug.ini';
        $this->data = new \oroboros\core\libs\Dataflow\Dataflow();
        $this->_settings = $this->data->fetch_ini($this->settingsfile);
        
    }
    
    public function handleSettings() {
        $settings = array();
        $settings['SYSTEM']['logfile'] = $this->checkSystemLogFile();
        $settings['MODULES']['logfile'] = $this->checkModuleLogFile();
        $settings['COMPONENTS']['logfile'] = $this->checkComponentLogFile();
        $settings['TEMPLATES']['logfile'] = $this->checkTemplateLogFile();
        $settings['THEMES']['logfile'] = $this->checkThemeLogFile();
        $settings['DATABASE']['logfile'] = $this->checkDatabaseLogFile();
        $settings['NODES']['logfile'] = $this->checkNodeLogFile();
        $settings['SYSTEM']['logindex'] = $this->checkSystemLogIndex();
        $settings['MODULES']['logindex'] = $this->checkModuleLogIndex();
        $settings['COMPONENTS']['logindex'] = $this->checkComponentLogIndex();
        $settings['TEMPLATES']['logindex'] = $this->checkTemplateLogIndex();
        $settings['THEMES']['logindex'] = $this->checkSystemLogIndex();
        $settings['DATABASE']['logindex'] = $this->checkDatabaseLogIndex();
        $settings['NODES']['logindex'] = $this->checkNodeLogIndex();
        $settings['MODULES']['enabled'] = $this->checkModuleEnabled();
        $settings['COMPONENTS']['enabled'] = $this->checkComponentEnabled();
        $settings['SYSTEM']['debug'] = $this->checkSystemDebugMode();
        $settings['MODULES']['debug'] = $this->checkModuleDebugMode();
        $settings['COMPONENTS']['debug'] = $this->checkComponentDebugMode();
        $settings['SYSTEM']['localprefix'] = $this->checkSystemLocalPrefix();
        $settings['SYSTEM']['stagingprefix'] = $this->checkSystemStagingPrefix();
        $settings['SYSTEM']['subprefixes'] = $this->checkSystemSubprefix();
        $settings['MODULES']['subprefixes'] = $this->checkModuleSubprefix();
        $settings['COMPONENTS']['subprefixes'] = $this->checkComponentSubprefix();
        $settings['SYSTEM']['subprefix-class'] = $this->checkSystemSubprefixClass();
        $settings['MODULES']['subprefix-class'] = $this->checkModuleSubprefixClass();
        $settings['COMPONENTS']['subprefix-class'] = $this->checkComponentSubprefixClass();
        $settings['SYSTEM']['subprefix-classtype'] = $this->checkSystemSubprefixClasstype();
        $settings['SYSTEM']['subprefix-namespace'] = $this->checkSystemSubprefixNamespace();
        $settings['SYSTEM']['subprefix-method'] = $this->checkSystemSubprefixMethod();
        $settings['MODULES']['subprefix-method'] = $this->checkModuleSubprefixMethod();
        $settings['COMPONENTS']['subprefix-method'] = $this->checkComponentSubprefixMethod();
        $settings['SYSTEM']['subprefix-file'] = $this->checkSystemSubprefixFile();
        $settings['MODULES']['subprefix-file'] = $this->checkModuleSubprefixFile();
        $settings['COMPONENTS']['subprefix-file'] = $this->checkComponentSubprefixFile();
        $settings['SYSTEM']['subprefix-subsites'] = $this->checkSystemSubprefixSubsites();
        $settings['DATABASE']['logerrors'] = $this->checkDatabaseLogErrors();
        $settings['DATABASE']['logwarnings'] = $this->checkDatabaseLogWarnings();
        $settings['DATABASE']['lognull'] = $this->checkDatabaseLogNull();
        $settings['NODES']['logip'] = $this->checkNodeLogIp();
        $settings['NODES']['logremoteid'] = $this->checkNodeLogRemoteId();
        $settings['NODES']['logremotekey'] = $this->checkNodeLogRemoteKey();
        $settings['NODES']['logaccessrights'] = $this->checkNodeLogAccessRights();
        $settings['NODES']['logcontype'] = $this->checkNodeLogConType();
        return $settings;
    }
    
    private function checkSystemLogFile() {
        if (isset($this->_settings['SYSTEM']['logfile'])) {
            return $this->_settings['SYSTEM']['logfile'];
        } else return 'system.log';
    }
    
    private function checkModuleLogFile() {
        if (isset($this->_settings['MODULE']['logfile'])) {
            return $this->_settings['MODULE']['logfile'];
        } else return 'system.log';
    }
    
    private function checkComponentLogFile() {
        if (isset($this->_settings['COMPONENT']['logfile'])) {
            return $this->_settings['COMPONENT']['logfile'];
        } else return 'system.log';
    }
    
    private function checkTemplateLogFile() {
        if (isset($this->_settings['TEMPLATES']['logfile'])) {
            return $this->_settings['TEMPLATES']['logfile'];
        } else return 'system.log';
    }
    
    private function checkThemeLogFile() {
        if (isset($this->_settings['TEMPLATES']['logfile'])) {
            return $this->_settings['TEMPLATES']['logfile'];
        } else return 'system.log';
    }
    
    private function checkDatabaseLogFile() {
        if (isset($this->_settings['DATABASE']['logfile'])) {
            return $this->_settings['DATABASE']['logfile'];
        } else return 'system.log';
    }
    
    private function checkNodeLogFile() {
        if (isset($this->_settings['NODES']['logfile'])) {
            return $this->_settings['NODES']['logfile'];
        } else return 'system.log';
    }
    
    private function checkSystemLogIndex() {
        if (isset($this->_settings['SYSTEM']['logindex'])) {
            return $this->_settings['SYSTEM']['logindex'];
        } else return '<<SYSTEM>>';
    }
    
    private function checkModuleLogIndex() {
        if (isset($this->_settings['MODULES']['logindex'])) {
            return $this->_settings['MODULES']['logindex'];
        } else return '<<SYSTEM>>';
    }
    
    private function checkComponentLogIndex() {
        if (isset($this->_settings['COMPONENTS']['logindex'])) {
            return $this->_settings['COMPONENTS']['logindex'];
        } else return '<<SYSTEM>>';
    }
    
    private function checkTemplateLogIndex() {
        if (isset($this->_settings['TEMPLATES']['logindex'])) {
            return $this->_settings['TEMPLATES']['logindex'];
        } else return '<<SYSTEM>>';
    }
    
    private function checkThemeLogIndex() {
        if (isset($this->_settings['THEMES']['logindex'])) {
            return $this->_settings['THEMES']['logindex'];
        } else return '<<SYSTEM>>';
    }
    
    private function checkDatabaseLogIndex() {
        if (isset($this->_settings['DATABASE']['logindex'])) {
            return $this->_settings['DATABASE']['logindex'];
        } else return '<<SYSTEM>>';
    }
    
    private function checkNodeLogIndex() {
        if (isset($this->_settings['NODES']['logindex'])) {
            return $this->_settings['NODES']['logindex'];
        } else return '<<SYSTEM>>';
    }
    
    private function checkSystemDebugMode() {
        if (isset($this->_settings['SYSTEM']['debug']) && $this->_settings['SYSTEM']['debug'] == TRUE) {
            return $this->_settings['SYSTEM']['debug'];
        } else return FALSE;
    }
    
    private function checkModuleDebugMode() {
        if (isset($this->_settings['MODULES']['debug']) && $this->_settings['MODULES']['debug'] == TRUE) {
            return $this->_settings['MODULES']['debug'];
        } else return FALSE;
    }
    
    private function checkComponentDebugMode() {
        if (isset($this->_settings['COMPONENTS']['debug']) && $this->_settings['COMPONENTS']['debug'] == TRUE) {
            return $this->_settings['COMPONENTS']['debug'];
        } else return FALSE;
    }
    
    private function checkModuleEnabled() {
        if (isset($this->_settings['MODULES']['enabled']) && $this->_settings['MODULES']['enabled'] == TRUE) {
            return $this->_settings['MODULES']['enabled'];
        } else return FALSE;
    }
    
    private function checkComponentEnabled() {
        if (isset($this->_settings['COMPONENTS']['enabled']) && $this->_settings['COMPONENTS']['enabled'] == TRUE) {
            return $this->_settings['COMPONENTS']['enabled'];
        } else return FALSE;
    }
    
    private function checkSystemLocalPrefix() {
        if (isset($this->_settings['SYSTEM']['localprefix'])) {
            return $this->_settings['SYSTEM']['localprefix'];
        } else return '<LOCAL>';
    }
    
    private function checkSystemStagingPrefix() {
        if (isset($this->_settings['SYSTEM']['stagingprefix'])) {
            return $this->_settings['SYSTEM']['stagingprefix'];
        } else return '<STAGING>';
    }
    
    private function checkSystemSubprefix() {
        if (isset($this->_settings['SYSTEM']['subprefix']) && $this->_settings['SYSTEM']['subprefix'] == TRUE) {
            return $this->_settings['SYSTEM']['subprefix'];
        } else return FALSE;
    }
    
    private function checkModuleSubprefix() {
        if (isset($this->_settings['MODULES']['subprefix']) && $this->_settings['MODULES']['subprefix'] == TRUE) {
            return $this->_settings['MODULES']['subprefix'];
        } else return FALSE;
    }
    
    private function checkComponentSubprefix() {
        if (isset($this->_settings['COMPONENTS']['subprefix']) && $this->_settings['COMPONENTS']['subprefix'] == TRUE) {
            return $this->_settings['COMPONENTS']['subprefix'];
        } else return FALSE;
    }
    
    private function checkSystemSubprefixClass() {
        if (isset($this->_settings['SYSTEM']['subprefix-class']) && $this->_settings['SYSTEM']['subprefix-class'] == TRUE) {
            return $this->_settings['SYSTEM']['subprefix-class'];
        } else return FALSE;
    }
    
    private function checkComponentSubprefixClass() {
        if (isset($this->_settings['COMPONENTS']['subprefix-class']) && $this->_settings['COMPONENTS']['subprefix-class'] == TRUE) {
            return $this->_settings['COMPONENTS']['subprefix-class'];
        } else return FALSE;
    }
    
    private function checkModuleSubprefixClass() {
        if (isset($this->_settings['MODULES']['subprefix-class']) && $this->_settings['MODULES']['subprefix-class'] == TRUE) {
            return $this->_settings['MODULES']['subprefix-class'];
        } else return FALSE;
    }
    
    private function checkSystemSubprefixClasstype() {
        if (isset($this->_settings['SYSTEM']['subprefix-classtype']) && $this->_settings['SYSTEM']['subprefix-classtype'] == TRUE) {
            return $this->_settings['SYSTEM']['subprefix-classtype'];
        } else return FALSE;
    }
    
    private function checkSystemSubprefixNamespace() {
        if (isset($this->_settings['SYSTEM']['subprefix-namespace']) && $this->_settings['SYSTEM']['subprefix-namespace'] == TRUE) {
            return $this->_settings['SYSTEM']['subprefix-namespace'];
        } else return FALSE;
    }
    
    private function checkSystemSubprefixMethod() {
        if (isset($this->_settings['SYSTEM']['subprefix-method']) && $this->_settings['SYSTEM']['subprefix-method'] == TRUE) {
            return $this->_settings['SYSTEM']['subprefix-method'];
        } else return FALSE;
    }
    
    private function checkModuleSubprefixMethod() {
        if (isset($this->_settings['MODULES']['subprefix-method']) && $this->_settings['MODULES']['subprefix-method'] == TRUE) {
            return $this->_settings['MODULES']['subprefix-method'];
        } else return FALSE;
    }
    
    private function checkComponentSubprefixMethod() {
        if (isset($this->_settings['COMPONENTS']['subprefix-method']) && $this->_settings['COMPONENTS']['subprefix-method'] == TRUE) {
            return $this->_settings['COMPONENTS']['subprefix-method'];
        } else return FALSE;
    }
    
    private function checkSystemSubprefixFile() {
        if (isset($this->_settings['SYSTEM']['subprefix-file']) && $this->_settings['SYSTEM']['subprefix-file'] == TRUE) {
            return $this->_settings['SYSTEM']['subprefix-file'];
        } else return FALSE;
    }
    
    private function checkModuleSubprefixFile() {
        if (isset($this->_settings['MODULES']['subprefix-file']) && $this->_settings['MODULES']['subprefix-file'] == TRUE) {
            return $this->_settings['MODULES']['subprefix-file'];
        } else return FALSE;
    }
    
    private function checkComponentSubprefixFile() {
        if (isset($this->_settings['COMPONENTS']['subprefix-file']) && $this->_settings['COMPONENTS']['subprefix-file'] == TRUE) {
            return $this->_settings['COMPONENTS']['subprefix-file'];
        } else return FALSE;
    }
    
    private function checkSystemSubprefixSubsites() {
        if (isset($this->_settings['SYSTEM']['subprefix-subsites']) && $this->_settings['SYSTEM']['subprefix-subsites'] == TRUE) {
            return $this->_settings['SYSTEM']['subprefix-subsites'];
        } else return FALSE;
    }
    
    private function checkDatabaseLogErrors() {
        if (isset($this->_settings['DATABASE']['logerrors']) && $this->_settings['DATABASE']['logerrors'] == TRUE) {
            return $this->_settings['DATABASE']['logerrors'];
        } else return FALSE;
    }
    
    private function checkDatabaseLogWarnings() {
        if (isset($this->_settings['DATABASE']['logwarnings']) && $this->_settings['DATABASE']['logwarnings'] == TRUE) {
            return $this->_settings['DATABASE']['logwarnings'];
        } else return FALSE;
    }
    
    private function checkDatabaseLogNull() {
        if (isset($this->_settings['DATABASE']['lognull']) && $this->_settings['DATABASE']['lognull'] == TRUE) {
            return $this->_settings['DATABASE']['lognull'];
        } else return FALSE;
    }
    
    private function checkNodeLogIp() {
        if (isset($this->_settings['NODES']['logip']) && $this->_settings['NODES']['logip'] == TRUE) {
            return $this->_settings['NODES']['logip'];
        } else return FALSE;
    }
    
    private function checkNodeLogRemoteId() {
        if (isset($this->_settings['NODES']['logremoteid']) && $this->_settings['NODES']['logremoteid'] == TRUE) {
            return $this->_settings['NODES']['logremoteid'];
        } else return FALSE;
    }
    
    private function checkNodeLogRemoteKey() {
        if (isset($this->_settings['NODES']['logremotekey']) && $this->_settings['NODES']['logremotekey'] == TRUE) {
            return $this->_settings['NODES']['logremotekey'];
        } else return FALSE;
    }
    
    private function checkNodeLogAccessRights() {
        if (isset($this->_settings['NODES']['logaccessrights']) && $this->_settings['NODES']['logaccessrights'] == TRUE) {
            return $this->_settings['NODES']['logaccessrights'];
        } else return FALSE;
    }
    
    private function checkNodeLogConType() {
        if (isset($this->_settings['NODES']['logcontype']) && $this->_settings['NODES']['logcontype'] == TRUE) {
            return $this->_settings['NODES']['logcontype'];
        } else return FALSE;
    }
    
    public function __destruct() {}
}

?>
