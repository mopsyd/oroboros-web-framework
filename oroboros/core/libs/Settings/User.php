<?php
/**
 * Loads the debug settings for the system.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Settings;

class User extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    function __construct($package=NULL) {
        parent::__construct($package);
        $this->init();
    }
    
    public function init() {
        
    }
    
    public function handleSettings($file) {
        if (is_readable($file)) {
            $results = $this->data->fetch_ini($file);
            
            return $results;
        } else {
            return FALSE;
        }
    }
    
    function __destruct() {
        parent::__destruct();
    }
}

?>
