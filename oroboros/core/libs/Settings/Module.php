<?php
/**
 * Description of ModuleSettings
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Settings;

class Module extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    function __construct($package=NULL) {
        parent::__construct($package);
    }
    
    public function init() {
        
    }
    
    function __destruct() {
        parent::__destruct();
    }
}

?>
