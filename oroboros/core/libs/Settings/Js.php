<?php
/**
 * Description of JsSettings
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Settings;

class Js {
    
    const SETTINGS = 'javascript.ini';
    
    public $_settings = NULL;
    
    function __construct($settings=NULL) {
        $this->_settings = $this->handleSettings();
        $this->init();
    }

    public function init() {
        $this->checkSettings();
    }
    
    public function handleSettings() {
        $this->settingsfile = _DATA . self::SETTINGS;
        $this->data = new \oroboros\core\libs\Dataflow\Dataflow();
        return $this->data->fetch_ini($this->settingsfile);
    }
    
    public function returnSettings() {
        return $this->settings;
    }
    
    public function checkSettings() {
        $settings = array(
        'core' => $this->_settings['Javascript']['loadCoreScripting'],
        'dependency' => $this->_settings['Javascript']['loadDependencyScripting'],
        'skeleton' => $this->_settings['Javascript']['loadSkeletonScripting'],
        'template' => $this->_settings['Javascript']['loadTemplateScripting'],
        'theme' => $this->_settings['Javascript']['loadThemeScripting'],
        'site' => $this->_settings['Javascript']['loadSiteScripting'],
        'page' => $this->_settings['Javascript']['loadPageScripting'],
        'module' => $this->_settings['Javascript']['loadModuleScripting'],
        'component' => $this->_settings['Javascript']['loadComponentScripting'],
        'minify' => $this->_settings['Javascript']['minify'],
        'debug' => $this->_settings['Javascript']['debug'],
        'cache' => $this->_settings['Javascript']['cache'],
        'validate' => $this->_settings['Javascript']['validate']
        );
        $this->settings = $settings;
    }
    
    function __destruct() {
        
    }
}

?>
