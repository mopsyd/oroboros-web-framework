<?php
/**
 * Loads the debug settings for the system.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Settings;

class Security {
    
    const SETTINGS = 'security.ini';
    
    public function __construct() {
        $this->init();
    }
    
    private function init() {
        $this->data = new \oroboros\core\libs\Dataflow\Dataflow();
        $this->settingsfile = _DATA . self::SETTINGS;
        $this->_settings = $this->data->fetch_ini($this->settingsfile);
        $settings = $this->handleSettings();
    }
    
    public function handleSettings() {
        $settings = array();
        $this->handleGeneral();
        //var_dump($this->_settings);
        return $this->_settings;
    }
    
    protected function handleGeneral() {
        $settings = $this->_settings['GENERAL'];
        //var_dump($settings);
        if (isset($settings['useSalt'])) {
            $this->_setSalt();
        }
        if (isset($settings['useIV'])) {
            $this->_setIV();
        }
        if (isset($settings['useHashTable'])) {
            $this->_setHashmode('table');
        }
        elseif (isset($settings['useHashTableFlat'])) {
            $this->_setHashmode('file');
        }
        if (isset($settings['useForFormNonce'])) {
            $this->_setNonceMode($settings['useForFormNonce']);
        }
        if (isset($settings['useForPasswords'])) {
            $this->_setPasswordHashMethod($settings['useForPasswords']);
        }
        if (isset($settings['useForSessionAuth'])) {
            $this->_setSessionAuthMode($settings['useForSessionAuth']);
        }
        if (isset($settings['useForFileHash'])) {
            $this->_setFileHashMode($settings['useForFileHash']);
        }
        if (isset($settings['SALT_GENERAL'])) {
            
        }
        if (isset($settings['SALT_PASSWORD'])) {
            
        }
        if (isset($settings['SALT_FILE_KEY'])) {
            
        }
        if (isset($settings['SALT_FILE_IV'])) {
            
        }
    }
    
    protected function _setSalt($salt=NULL) {
        
    }
    
    protected function _setIV($iv=NULL) {
        
    }
    
    protected function _setPasswordHashMethod($type) {
        
    }
    
    protected function _setSessionAuthMode($type) {
        
    }
    
    protected function _setFileHashMode($type) {
        
    }
    
    protected function _setHashmode($type) {
        switch ($type) {
            case 'table':
                //use a hash table in the database
                
                break;
            case 'file':
                //use a hash flatfile
                
                break;
            default:
                //undefined method, return error
                return FALSE;
                break;
        }
    }
    
    protected function _setNonceMode($type) {
        
    }
    
    protected function _setIVMode($type) {
        
    }
    
    public function __destruct() {

    }
}

?>
