<?php
/**
 * Loads the core settings for the system.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Settings;

class Core {
    
    const SETTINGS = 'settings.ini';

    private $_settings = NULL;
    private $db = NULL;

    public function __construct($db=null) {
        if(!is_null($db)) {
            $this->db = $db;
        } else {
            $this->db = new \oroboros\core\libs\Database\Database();
        }
        $this->init();
    }

    private function init() {
        $this->data = new \oroboros\core\libs\Dataflow\Dataflow();
        $this->settingsfile = _DATA . 'settings.ini';
        $this->_settings = $this->data->fetch_ini($this->settingsfile);
    }

    public function handleSettings() {
        $settings = array();
        $settings['SYSTEM']['scope'] = $this->checkSiteScope();
        $settings['SYSTEM']['reinstall'] = $this->checkInstallationRebase();
        $settings['SYSTEM']['rebaseDNS'] = $this->checkDnsRebase();
        $settings['SYSTEM']['useGIT'] = $this->checkGIT();
        $settings['SYSTEM']['useSSH'] = $this->checkSSH();
        $settings['SYSTEM']['useFTP'] = $this->checkFTP();
        $settings['SYSTEM']['forceSSL'] = $this->checkSSL();
        $settings['SYSTEM']['forceCoreClasses'] = $this->checkForceCoreClasses();
        $settings['SYSTEM']['useMirrors'] = $this->checkMirrors();
        $settings['SYSTEM']['useNodes'] = $this->checkNodes();
        $settings['SYSTEM']['oneWayNodeParent'] = $this->checkOneWayNodeParent();
        $settings['SYSTEM']['oneWayNodeChild'] = $this->checkOneWayNodeChild();
        $settings['SYSTEM']['nodeContentSync'] = $this->checkNodeContentSync();
        $settings['SYSTEM']['nodeSessionSync'] = $this->checkNodeSessionSync();
        $settings['SYSTEM']['webShell'] = $this->checkWebShell();
        $settings['DATABASE']['rebuildDatabase'] = $this->checkRebaseDatabase();
        $settings['DATABASE']['backupDatabase'] = $this->checkBackupDatabase();
        $settings['DATABASE']['backupTablesIndividually'] = $this->checkIndividualTableBackup();
        $settings['DATABASE']['deleteNotTruncate'] = $this->checkDeleteNotTruncate();
        $settings['DATABASE']['archiveBeforeTruncate'] = $this->checkArchiveBeforeTruncate();
        $settings['DATABASE']['databaseSync'] = $this->checkAllowDatabaseSyncing();
        $settings['DATABASE']['databaseBridge'] = $this->checkAllowDatabaseBridge();
        return $settings;
    }

    private function checkSiteScope() {
        if ($_SERVER['HTTP_HOST'] === $this->_settings['SYSTEM']['primaryInstallation']) {
            $scope = 'global';
        } else {
            //check database to see if site has global scope
            $result = $this->db->select("SELECT `scope` FROM `sites` WHERE `url` = '" . $_SERVER['HTTP_HOST'] . "' LIMIT 1;");
            $db = NULL;
            if(isset($result[0])) {
                $scope = $result[0];
            } else {
                $scope = 'local';
            }
        }
        return $scope;
    }
    
    private function checkInstallationRebase() {
        if (isset($this->_settings['SYSTEM']['rebaseInstallation']) && $this->_settings['SYSTEM']['rebaseInstallation'] == TRUE) {
            return $this->_settings['SYSTEM']['rebaseInstallation'];
        } else return FALSE;
    }
    
    private function checkDnsRebase() {
        if (isset($this->_settings['SYSTEM']['rebaseDNS']) && $this->_settings['SYSTEM']['rebaseDNS'] == TRUE) {
            return $this->_settings['SYSTEM']['rebaseDNS'];
        } else return FALSE;
    }
    
    private function checkGIT() {
        if (isset($this->_settings['SYSTEM']['enableGIT']) && $this->_settings['SYSTEM']['enableGIT'] == TRUE) {
            return $this->_settings['SYSTEM']['enableGIT'];
        } else return FALSE;
    }
    
    private function checkSSH() {
        if (isset($this->_settings['SYSTEM']['enableSSH']) && $this->_settings['SYSTEM']['enableSSH'] == TRUE) {
            return $this->_settings['SYSTEM']['enableSSH'];
        } else return FALSE;
    }
    
    private function checkFTP() {
        if (isset($this->_settings['SYSTEM']['enableFTP']) && $this->_settings['SYSTEM']['enableFTP'] == TRUE) {
            return $this->_settings['SYSTEM']['enableFTP'];
        } else return FALSE;
    }
    
    private function checkSSL() {
        if (isset($this->_settings['SYSTEM']['forceSSL']) && $this->_settings['SYSTEM']['forceSSL'] == TRUE) {
            return $this->_settings['SYSTEM']['forceSSL'];
        } else return FALSE;
    }
    
    private function checkForceCoreClasses() {
        if (isset($this->_settings['SYSTEM']['forceCoreClasses']) && $this->_settings['SYSTEM']['forceCoreClasses'] == TRUE) {
            return $this->_settings['SYSTEM']['forceCoreClasses'];
        } else return FALSE;
    }
    
    private function checkMirrors() {
        if (isset($this->_settings['SYSTEM']['enableMirrors']) && $this->_settings['SYSTEM']['enableMirrors'] == TRUE) {
            return $this->_settings['SYSTEM']['enableMirrors'];
        } else return FALSE;
    }
    
    private function checkNodes() {
        if (isset($this->_settings['SYSTEM']['enableNodes']) && $this->_settings['SYSTEM']['enableNodes'] == TRUE) {
            return $this->_settings['SYSTEM']['enableNodes'];
        } else return FALSE;
    }
    
    private function checkOneWayNodeParent() {
        if (isset($this->_settings['SYSTEM']['oneWayNodeParent']) && $this->_settings['SYSTEM']['oneWayNodeParent'] == TRUE) {
            return $this->_settings['SYSTEM']['oneWayNodeParent'];
        } else return FALSE;
    }
    
    private function checkOneWayNodeChild() {
        if (isset($this->_settings['SYSTEM']['oneWayNodeChild']) && $this->_settings['SYSTEM']['oneWayNodeChild'] == TRUE) {
            return $this->_settings['SYSTEM']['oneWayNodeChild'];
        } else return FALSE;
    }
    
    private function checkNodeContentSync() {
        if (isset($this->_settings['SYSTEM']['enableNodeContentSync']) && $this->_settings['SYSTEM']['enableNodeContentSync'] == TRUE) {
            return $this->_settings['SYSTEM']['enableNodeContentSync'];
        } else return FALSE;
    }
    
    private function checkNodeSessionSync() {
        if (isset($this->_settings['SYSTEM']['enableNodeSessionSync']) && $this->_settings['SYSTEM']['enableNodeSessionSync'] == TRUE) {
            return $this->_settings['SYSTEM']['enableNodeSessionSync'];
        } else return FALSE;
    }
    
    private function checkWebShell() {
        if (isset($this->_settings['SYSTEM']['enableWebShell']) && $this->_settings['SYSTEM']['enableWebShell'] == TRUE) {
            return $this->_settings['SYSTEM']['enableWebShell'];
        } else return FALSE;
    }
    
    private function checkRebaseDatabase() {
        if (isset($this->_settings['DATABASE']['rebuildDatabase']) && $this->_settings['DATABASE']['rebuildDatabase'] == TRUE) {
            return $this->_settings['DATABASE']['rebuildDatabase'];
        } else return FALSE;
    }
    
    private function checkBackupDatabase() {
        if (isset($this->_settings['DATABASE']['backupDatabase']) && $this->_settings['DATABASE']['backupDatabase'] == TRUE) {
            return $this->_settings['DATABASE']['backupDatabase'];
        } else return FALSE;
    }
    
    private function checkIndividualTableBackup() {
        if (isset($this->_settings['DATABASE']['backupTablesIndividually']) && $this->_settings['DATABASE']['backupTablesIndividually'] == TRUE) {
            return $this->_settings['DATABASE']['backupTablesIndividually'];
        } else return FALSE;
    }
    
    private function checkDeleteNotTruncate() {
        if (isset($this->_settings['DATABASE']['deleteNotTruncate']) && $this->_settings['DATABASE']['deleteNotTruncate'] == TRUE) {
            return $this->_settings['DATABASE']['deleteNotTruncate'];
        } else return FALSE;
    }
    
    private function checkArchiveBeforeTruncate() {
        if (isset($this->_settings['DATABASE']['archiveBeforeTruncate']) && $this->_settings['DATABASE']['archiveBeforeTruncate'] == TRUE) {
            return $this->_settings['DATABASE']['archiveBeforeTruncate'];
        } else return FALSE;
    }
    
    private function checkAllowDatabaseSyncing() {
        if (isset($this->_settings['DATABASE']['allowDatabaseSyncing']) && $this->_settings['DATABASE']['allowDatabaseSyncing'] == TRUE) {
            return $this->_settings['DATABASE']['allowDatabaseSyncing'];
        } else return FALSE;
    }
    
    private function checkAllowDatabaseBridge() {
        if (isset($this->_settings['DATABASE']['bridgeDatabases']) && $this->_settings['DATABASE']['bridgeDatabases'] == TRUE) {
            return $this->_settings['DATABASE']['bridgeDatabases'];
        } else return FALSE;
    }
    
    

    public function __destruct() {
        $this->db = NULL;
    }
}

?>
