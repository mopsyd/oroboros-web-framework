<?php

/**
 * Description of CssSettings
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */

namespace oroboros\core\libs\Settings;

class Css {

    const SETTINGS = 'css.ini';

    function __construct($settings = NULL) {
        $this->_settings = $this->handleSettings();
        $this->init();
    }

    public function init() {
        $this->checkSettings();
    }
    
    public function returnSettings() {
        return $this->settings;
    }

    public function handleSettings() {
        $this->settingsfile = _DATA . self::SETTINGS;
        $this->data = new \oroboros\core\libs\Dataflow\Dataflow();
        return $this->data->fetch_ini($this->settingsfile);
    }

    public function checkSettings() {
        $settings = array(
        'core' => $this->_settings['CSS']['loadCoreCss'],
        'dependency' => $this->_settings['CSS']['loadDependencyCss'],
        'skeleton' => $this->_settings['CSS']['loadSkeletonCss'],
        'template' => $this->_settings['CSS']['loadTemplateCss'],
        'theme' => $this->_settings['CSS']['loadThemeCss'],
        'site' => $this->_settings['CSS']['loadSiteCss'],
        'page' => $this->_settings['CSS']['loadPageCss'],
        'module' => $this->_settings['CSS']['loadModuleCss'],
        'component' => $this->_settings['CSS']['loadComponentCss'],
        'minify' => $this->_settings['CSS']['minify'],
        'debug' => $this->_settings['CSS']['debug'],
        'cache' => $this->_settings['CSS']['cache'],
        'validate' => $this->_settings['CSS']['validate']
        );
        $this->settings = $settings;
    }

    function __destruct() {
        
    }

}

?>
