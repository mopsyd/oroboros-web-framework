<?php

/**
 * Description of _Theme
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */

namespace oroboros\core\libs\Theme;

class Theme extends \oroboros\core\libs\Abstracts\Site\Lib {

    const DEFAULTS = '/oroboros/themes/';
    const DEFAULT_STYLESHEET = '/oroboros/themes/stylesheet.phtml';

    function __construct($package = NULL) {
        parent::__construct($package);
    }

    public function init() {
        $this->settings = new \oroboros\core\libs\Settings\Theme();
        $this->settings->init($this->package);
    }

    public function get_theme($page, $site, $subdomain) {
        $query = "SELECT `theme`,`parent` FROM `pages` WHERE `slug`='" . $page . "' AND `site`='" . $site . "' AND `subdomain`='" . $subdomain . "' AND `active` = 1 LIMIT 1;";
        $result = $this->db->select($query);
        if (!empty($result)) {
            if ($result[0]['theme'] == 'INHERIT') {
                //inherit theme details from page parent
                if ($result[0]['parent'] != NULL) {
                    $theme = $this->get_template($result[0]['slug'], $site, $subdomain);
                } else {
                    //theme inherits from site, check site theme
                    $theme = $this->get_site_theme($site);
                }
            } elseif ($result[0]['theme'] == 'RAW') {
                //do not render theme
                return FALSE;
            } else
                $theme = $result[0];
        } else {
            //no template to return
            return FALSE;
        }
        return $this->get_theme_details($theme['theme']);
    }

    public function get_site_theme($site) {
        $query = "SELECT `theme`,`parent` FROM `sites` WHERE `slug` = '" . $site . "' ;";
        $result = $this->db->select($query);
        switch ($result[0]['theme']) {
            case 'RAW':
                return FALSE;
                break;
            case 'INHERIT':
                return $this->get_site_theme($result[0]['parent']);
                break;
            default:
                return $result[0]['theme'];
                break;
        }
    }

    public function get_theme_details($theme) {
        $query = "SELECT `title`,`version`,`parent`,`html_type`,`type`,`skinnable`,`extendable`,`image_folder`,`config_file` FROM `themes` WHERE `slug`='" . $theme . "' AND `active`=1 LIMIT 1;";
        $result = $this->db->select($query);
        $themedata = $result[0];
        $themedata['slug'] = $theme;
        if (!isset($themedata['config_file'])) {
            $themedata['settings'] = $this->data->fetch_ini(_BASE . self::DEFAULTS . $theme . '/' . $themedata['config_file']);
        }
        unset($themedata['config_file']);
        $_SESSION['theme'] = $themedata;
        return $themedata;
    }
    
    public function set_session_identifier($data) {
        $_SESSION['theme'] = ($this->checkpackage($data) == TRUE) ? $data : $this->get_theme_details($data) ;
    }
    
    protected function checkpackage($data) {
        if (is_array($data)) {
            return (isset( $data['slug'] ) ) ? TRUE : FALSE ;
        } else return false;
    }

    public function get_theme_dependencies($theme) {
        
    }

    public function get_theme_stylesheet($theme) {
        
    }

    public function get_theme_scripts($theme) {
        
    }

    public function get_parent_theme($theme) {
        
    }

    public function package_theme_settings($theme) {
        
    }

    public function package_theme_layout($theme) {
        
    }

    public function package_theme_filebase($theme) {
        
    }

    public function package_theme($theme) {
        
    }

    public function archive_theme($theme) {
        
    }

    public function restore_theme($theme) {
        
    }

    public function fetch_remote_theme($theme) {
        
    }

    public function validate_theme($theme) {
        
    }

    public function register_theme($theme) {
        
    }

    public function disable_theme($theme) {
        
    }

    public function delete_theme($theme) {
        
    }

    public function clone_theme($theme) {
        
    }

    function __destruct() {
        parent::__destruct();
    }

}

?>
