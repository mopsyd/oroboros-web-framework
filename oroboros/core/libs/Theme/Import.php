<?php
/**
 * Description of Import
 *
 * @author Your Name <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Theme;

class Import extends oroboros\core\libs\Filebase\Import {

    private $doc = NULL;
    public $themedata = array();

    public function __construct($file, $root, $package=NULL) {
        parent::__construct($package);
        $this->themedata = $this->init($file, $root);
    }

    private function init($file, $root) {
        $this->parse_theme_xml($file);
        foreach ($this->doc->getElementsByTagName($root) as $i) {
            $x = $this->fetch_root_node($i);
        }
        return $x;
    }

    private function parse_theme_xml($file) { //loads the xml file
        //$file = THEME . $theme . '/layout.xml';
        $this->doc = new DOMDocument;
        $this->doc->preserveWhiteSpace = false;
        $this->doc->load($file);
    }

    private function fetch_root_node($origin, $array = array()) { //finds the xml root node and begins recursive process
        $array = $this->buildArray($origin);
        return $array;
    }

    private function buildArray($node, $array = array()) {
        foreach ($node->childNodes as $i) {
            if ($i->hasChildNodes()) {
                $array[$i->nodeName] = $this->buildArray($i);
                if ($i->hasAttributes()) {
                    $array[$i->nodeName]['attributes'] = $this->buildAttributes($i);
                }
            } else {
                if ($i->hasAttributes()) {
                    $array[$i->nodeName]['value'] = $i->nodeValue;
                    $array[$i->nodeName]['attributes'] = $this->buildAttributes($i);
                } else {
                    $array[$i->nodeName] = $i->nodeValue;
                }
            }
        }
        return $array;
    }

    private function buildAttributes($source, $array = array('attributes' => array())) {
        foreach ($source->attributes as $attr) {
            $name = $attr->nodeName;
            $value = $attr->nodeValue;
            $array['attributes'][$name] = $value;
        }
        return $array;
    }

    function __destruct() {
        unset($this->doc);
        unset($this->themedata);
        parent::__destruct();
    }

}
?>
