<?php
/**
 * Description of Validate
 * validates user submitted data to insure it conforms to the correct specifications;
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Dataflow;

class Validate extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    public function __construct($package=NULL) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
