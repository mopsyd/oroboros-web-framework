<?php

/**
 * Description of Dataflow
 * This library handles the conversion of data from one form to another. It can
 * read or create ini files, encode an array into xml either as a string or as a
 * file (which can create a new file, add to or overwrite an existing file, or insert into
 * a specific node of an existing file). It can also handle json strings, either creating them
 * or alternately returning an array from an existing string. Numerous other classes use this
 * library to handle the correct conversion of data throughout the system.
 *
 * @author briandayhoff
 */

namespace oroboros\core\libs\Dataflow;

class Dataflow {

    public function fetch_ini($path, $file = NULL, $section = NULL, $process_sections_boolean = TRUE) {
        $file_name = $path . $file;
        if (file_exists($file_name) && is_readable($file_name)) {
            $data = (parse_ini_file($file_name, $process_sections_boolean));
            return $data;
        } else {
            return false;
        }
    }

    public function package_ini($data, $destination, $filename = 'settings', $append = TRUE) {
        $filename = $filename . '.ini';
        $render = NULL;
        foreach ($data as $section => $settings) {
            //creates the section heading
            $render .= $this->create_ini_section_heading($section);
            //creates the settings and values
            foreach ($settings as $setting => $value) {
                $render .= $setting . ' = ';
                if (is_array($value)) {
                    foreach ($value as $val => $comment) {
                        $render .= $val . '    ; ' . $comment;
                    }
                    $render .= PHP_EOL; // adds a line break after the setting
                } else {
                    $render .= $value . PHP_EOL;
                }
            }
            $render .= PHP_EOL; //sets a one line break between sections
        }
        if (is_file($destination) && is_writable($destination)) {
            //file exists. Will overwrite or append to file
            if (is_writable($destination)) {
                if ($append == FALSE) {
                    //Will overwrite the file with the new data, if the file exists. Otherwise will create a new file.
                    $file = fopen($destination, 'w');
                    fwrite($file, $render);
                    fclose($file);
                    return true;
                } else {
                    //Will append the data to the existing file if the file exists, otherwise will create a new file.
                    if (is_file($destination) && is_readable($destination)) {
                        //file exists and is readable, will get the file contents and append the existing data with the new data
                        $file = fopen($destination, 'a');
                        fwrite($file, PHP_EOL . $render);
                        fclose($file);
                        return true;
                    }
                }
            } else {
                //the file exists but is not writeable. Log file permission error. If debug is enabled, alert user also.
                echo 'The file ' . $destination . ' exists, but cannot be modified due to a file permission error<br>';
                return false;
            }
        } elseif (is_dir($destination)) {
            //the file does not exist. Will attempt to generate a new ini file in the specified directory.
            if (is_writable($destination)) {
                //the specified directory can be written to. File will be created.
                $destination = $destination . $filename;
                $file = fopen($destination, 'w');
                fwrite($file, $render);
                fclose($file);
                return true;
            } else {
                //the specified directory is not writeable. Log file permission error. If debug is enabled, alert user also.
                echo 'The specified directory located at ' . $destination . ' is not writeable. Please correct the file permissions for this directory. <br>';
                return false;
            }
        } else {
            //could not find file. Will attempt to correct the path and try again. If still not valid, log an error and alert the user if debug mode is on.
            $path = explode('/', $destination);
            $file = str_replace('.ini', NULL, array_pop($path));
            $path = str_replace('//', '/', '/' . implode('/', $path) . '/');
            if (is_dir($path)) {
                //the corrected path exists, trying again with the corrected file string.
                $this->package_ini($data, $path, $file, $append);
            } else {
                //The specified destination is not a valid path. Log missing file path error. If debug is enabled, alert user also.
                echo 'The specified file destination at ' . $destination . ' is not a valid file path. Please correct the error in your codebase<br>';
                return false;
            }
        }
    }

    private function create_ini_section_heading($heading) {
        return '[' . $heading . ']' . PHP_EOL;
    }

    public function parse_json($data, $object = FALSE) {
        if ($object == TRUE) {
            //return the json string as an object
            return json_decode($data);
        } else {
            //return the json string as an array
            return json_decode($data, TRUE);
        }
    }

    public function package_json($data, $params = 0) {
        return json_encode($data, $params);
    }

    public function fetch_csv($location, $type = 'file', $delimiter = ',', $linebreak = ';', $columnheads = FALSE) {
        switch ($type) {
            case 'file':
                if (is_readable($location)) {
                    //file found, will process csv file.
                    $file = file_get_contents($location);
                    $result = $this->parse_csv($file, $delimiter, $linebreak, $columnheads);
                    return $result;
                } else {
                    //file is not readable or does not exist
                    if (file_exists($location)) {
                        //file has incorrect access rights. Log error, if debug mode is enabled also alert user.
                        echo 'The file located at ' . $location . ' could not be read due to incorrect file permissions<br>';
                        return false;
                    } else {
                        //file could not be found at the specified location. Log error, if debug mode is enabled also alert user.
                        echo 'CSV file not found at ' . $location . '. Please correct your access path and try again.<br>';
                    }
                }
                break;
            case 'url':

                break;
        }
    }

    public function parse_csv($csv, $delimiter = ',', $linebreak = ';', $columnheads = FALSE) {
        $result = array();
        $lines = explode($linebreak, $csv);
        if ($columnheads == TRUE) {
            $columns = explode($linebreak, $lines[0]);
            $row = 0;
            foreach ($lines as $line) {
                $column = 0;
                $result[$row] = array();
                foreach (explode($delimiter, $line) as $value) {
                    $result[$row][$columns[$column]] = trim($value, '"\'');
                    $column++;
                }
                $row++;
            }
        } else {
            foreach ($lines as $line) {
                $row = 0;
                $result[$row] = array();
                foreach ($lines as $line) {
                    $result[$row] = array();
                    foreach (explode($delimiter, $line) as $value) {
                        $result[$row][] = trim($value, '"\'');
                    }
                    $row++;
                }
            }
        }
        return $result;
    }

    public function package_csv($data, $delimiter = ',', $linebreak = ';') {
        $result = NULL;
        foreach ($data as $line => $values) {
            $result .= '"' . implode('"' . $delimiter . '"', $values) . '"' . $linebreak;
            if ($linebreak != PHP_EOL) {
                $result .= PHP_EOL;
            }
        }
        return $result;
    }

    public function parse_xml($source, $type = 'string', $args = NULL) {
        libxml_use_internal_errors(TRUE);
        switch ($type) {
            case 'string':
                //handles xml strings passed to the method
                $iterator = $this->load_xml_iterator($source, FALSE, $args);
                break;
            case 'file':
                //handles xml files passed to the document
                if (is_readable($source)) {
                    //the xml document is a readable file
                    $iterator = $this->load_xml_iterator($source, TRUE, $args);
                } else {
                    //the xml document is not a readable file, log error, if debug mode is enabled, also notify user.
                    echo 'The source file located at ' . $source . ' is not readable.<br>';
                    return false;
                }
                break;
            case 'url':
                //handles remote xml sources
                $iterator = $this->load_xml_iterator($source, TRUE, $args);
                break;
            default:
                //handles undefined xml sources

                break;
        }
        //handles the xml parsing
        return $this->xml_get_nodes($iterator);
    }

    public function parse_xml_node($source, $type = 'string', $xpath = NULL, $args = NULL) {
        libxml_use_internal_errors(TRUE);
        switch ($type) {
            case 'string':
                $iterator = $this->load_xml_iterator($source, FALSE, $args);
                break;
            case 'file':
                $iterator = $this->load_xml_iterator($source, TRUE, $args);
                break;
            case 'url':
                $iterator = $this->load_xml_iterator($source, TRUE, $args);
                break;
            default:

                break;
        }
    }

    public function xml_get_data($source, $xpath, $isURL = FALSE) {
        libxml_use_internal_errors(TRUE);
    }

    public function load_xml_document($file) {
        libxml_use_internal_errors(TRUE);
        try {
            $xml = simplexml_load_file($file);
            return $xml;
        } catch (\Exception $e) {
            throw new \Exception('The document is not a valid xml file: ' . $e);
            return false;
        }
    }

    public function load_xml_string($xml) {
        libxml_use_internal_errors(TRUE);
        return new \SimpleXMLElement($xml);
    }

    public function load_xml_iterator($source, $isURL = FALSE, $options = NULL) {
        libxml_use_internal_errors(TRUE);
        return new \SimpleXMLIterator($source, $options, $isURL);
    }

    private function xml_get_head_node($xml) {
        return $xml->getName();
    }

    private function xml_get_nodes($xml, $xpath = NULL) {
        libxml_use_internal_errors(TRUE);
        if (is_null($xpath)) {
            //the entire xml object will be indexed
            $xpath = '/' . $xml->getName();
            $path = '/';
            $headnode = array();
            $headnode['xpath'] = $xpath;
            $headnode['name'] = $xml->getName();
            $namespace = $this->xml_get_namespace($xml);
            if ($namespace != FALSE) {
                $headnode['namespace'] = $namespace;
            }
            $attributes = $this->xml_get_attributes($xml);
            if ($attributes != FALSE) {
                $headnode['attributes'] = $attributes;
            }

            $children = $this->xml_get_nodes($xml, $xpath);
            if ($children != FALSE) {
                $headnode['children'] = $children;
            }
            return $headnode;
        }
        $nodes = array();
        $elements = $xml->xpath($xpath);
        if (count($elements) == 1) {
            $elements = $elements[0];
        } elseif (count($elements) > 1) {
            
        } else
            return false;
        foreach ($elements as $key => $value) {
            $path = $xpath . '/' . $key;
            $nodes[$key]['xpath'] = $path;
            $nodes[$key]['name'] = $key;
            $prefix = $this->xml_get_prefix($xml);
            if ($prefix != FALSE) {
                $nodes[$key]['prefix'] = $prefix;
            }
            $value = $this->xml_get_value($xml, $path);
            if ($value != FALSE) {
                $nodes[$key]['value'] = $value;
            }
            $attributes = $this->xml_get_attributes($xml, rtrim($xpath, $key), $key);
            if ($attributes != FALSE) {
                $nodes[$key]['attributes'] = $attributes;
            }
            $children = $this->xml_get_nodes($xml, $path);
            if ($children != FALSE) {
                $nodes[$key]['children'] = $children;
            }
        }
        return $nodes;
    }

    private function xml_get_namespace($xml, $xpath = NULL) {
        libxml_use_internal_errors(TRUE);
        //this needs to be completed
        return FALSE;
    }

    private function xml_get_prefix($xml, $xpath = NULL) {
        libxml_use_internal_errors(TRUE);
        //this needs to be completed
        return FALSE;
    }

    private function xml_get_attributes($xml, $xpath = NULL, $node = NULL) {
        libxml_use_internal_errors(TRUE);
        if ($xpath == NULL) {
            $xpath = '/';
        } else {
            $xpath = $xpath . '/';
        }
        if ($node == NULL) {
            $nodeattr = $xml->getName() . '/@*';
        } else {
            $nodeattr = $node . '/@*';
        }
        $attributes = array();
        $query = $xpath . $nodeattr;
        //insert attribute values into $attributes array
        foreach ($xml->xpath($query) as $key => $values) {
            foreach ($values as $subkey => $value) {
                $attributes[$subkey] = (string) $value;
            }
        }
        if (empty($attributes)) {
            return FALSE;
        } else {
            return $attributes;
        }
    }

    private function xml_get_value($xml, $xpath) {
        libxml_use_internal_errors(TRUE);
        $values = array();
        $query = $xpath;
        foreach ((array) $xml->xpath($query) as $key => $value) {
            if (count((array) $value) > 1) {
                //contains multiple elements and is not a value
                return FALSE;
            } else {
                //contains one element, check if element is a child node
                if (substr((string) $value, 0) === '<' && substr((string) $value, -1) === '>') {
                    //element is a child node
                    return FALSE;
                } else {
                    //element is a value, return the value
                    return (string) $value;
                }
            }
        }
    }

    public function init_xml_writer($data, $version = '1.0', $charset = 'utf-8') {
        var_dump($data);
        @date_default_timezone_set("GMT");
        $xml = new \XMLWriter();
        $xml->openMemory();
        $xml->startDocument($version, $charset);
        $xml->setIndentString('&nbsp;&nbsp;&nbsp;&nbsp;');
        $xml->startElement($data['name']);
        if (isset($data['namespace'])) {
            
        }
        $xml = $this->write_xml($xml, $data['children']);
//
        $xml->endElement();
        $xml->endDocument();
        $xml = $xml->outputMemory();
        return $xml;
    }

    public function write_xml($xml, $data) {
        foreach ($data as $key => $value) {
            if (isset($value['value'])) {
                $xml->startElement($value['name']);
                if (isset($value['attributes'])) {
                    foreach ($value['attributes'] as $attr => $val) {
                        $xml->writeAttribute($attr, $val);
                    }
                }
                $xml->writeElement($key, $value['value']);
                //write node contents to node
                $xml->endElement(); 
               continue;
            } elseif (isset($value['children'])) {
                //recursively writes child nodes
                $xml->startElement($value['name']);
                $xml = $this->write_xml($xml, $value['children']);
                if (isset($value['attributes'])) {
                    foreach ($value['attributes'] as $key => $value) {
                        $xml->writeAttribute($key, $value);
                    }
                }
                $xml->endElement();
            }
        }
        return $xml;
    }

}

?>
