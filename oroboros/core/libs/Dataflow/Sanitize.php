<?php
/**
 * Description of Sanitize
 * Sanitizes user submitted data
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Dataflow;

class Sanitize extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    public function __construct($package=NULL) {
        parent::__construct($package);
    }
    
    public function strip_scripts($content) {
        
    }
    
    public function strip_styles($content) {
        
    }
    
    public function strip_queries($content) {
        
    }
    
    public function strip_escaped_chars($content) {
        
    }
    
    public function strip_links($content) {
        
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
