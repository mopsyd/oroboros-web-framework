<?php
namespace oroboros\core\libs\Encryption;

class Hash {
    
    const ALPHA = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const NUMERIC = '01234567890';
    const SYMBOLIC = '!@#$%^&*()_+-={}|[]:;<>?,./';

    /**
     *
     * @param string $algo The algorithm (md5, sha1, whirlpool, etc)
     * @param string $data The data to encode
     * @param string $salt The salt (This should be the same throughout the system probably)
     * @return string The hashed/salted data
     */
    public function __construct($settings) {
        $this->settings = $settings;
    }
    
    public static function create($algo, $data, $salt) {

        $context = hash_init($algo, HASH_HMAC, $salt);
        hash_update($context, $data);

        return hash_final($context);
    }
    
    protected function _loadHashClass($class, $settings) {
        $class = '\\oroboros\\core\\libs\\Encryption\\' . $class;
        $hash = new $class($settings);
        return $hash->makeHash();
    }
    
    public function random($length, $valid=NULL, $alpha=FALSE, $numeric=FALSE, $symbol=FALSE) {
        //construct valid character string
        $string = $alpha;
        $string .= ($alpha==TRUE) ? self::ALPHA : NULL ;
        $string .= ($numeric==TRUE) ? self::NUMERIC : NULL ;
        $string .= ($symbol==TRUE) ? self::SYMBOLIC : NULL ;
        $count = strlen($string);
        $rand = NULL;
        //generate string
        for ($i=0; $i< $length; $i++) {
            $rand_pick = mt_rand(1, $count);
            $rand .= $string{$rand_pick-1};
        }
        return $rand;
    }
    
    public function nonce() {
        return str_replace(' ', NULL, $this->random(32, NULL, 1, 1));
    }
    
    public function filehash($file) {
        
    }
    
    public function generate_salt($string) {
        
    }
    
    public function generate_IV($hash) {
        
    }
    
    public function generate_key($method) {
        
    }
    
    public function set_hash_method($purpose) {
        
    }
    
    public function set_salt($purpose) {
        
    }
    
    public function set_mode($type, $purpose) {
        
    }
    
    public function set_IV($type, $method) {
        
    }
    
    public function __destruct() {
        
    }

}