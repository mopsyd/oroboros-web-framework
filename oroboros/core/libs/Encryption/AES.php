<?php

/**
 * Rijndael 256 CBC encryption/decryption
 * @author briandayhoff
 * 
 * Usage:
 *      $pw = "Password";
        $cl = "cleartext";   
        $_aes = new encrypt($cl, $pw);
        $crypted=$_aes->encrypt($cl, $pw);
        $newClear=$_aes->decrypt($crypted, $pw);
 * 
 * This controller is not yet integrated into the system. It is dependent on errlog.php and
 * the ini handler class, and requires some refactoring to function correctly, as well as a 
 * model. It has been disabled until these steps are completed.
 */
namespace oroboros\core\libs\Encryption;

class AES {
    private $sval;
    private $skey;
    private $salt;
    private $_e;
    
    public function __construct($sval, $skey, $package=NULL) {
        parent::__construct($package);
        $this->skey=$skey;
        $this->sval=$sval;
        //$this->_e = new errlog(); update to use new system's log features. Disabled until configured
        $this->salt();
    }
    
    public function makeHash() {
        
    }
    
    public function encrypt($sval, $skey) {
        $sval.=$this->salt;
        //echo "key: ".$skey."<br>_DATA: ".$sval."<br>";
        return rtrim(
                        base64_encode(
                                mcrypt_encrypt(
                                        MCRYPT_RIJNDAEL_256, $skey, $sval, MCRYPT_MODE_ECB, mcrypt_create_iv(
                                                mcrypt_get_iv_size(
                                                        MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB
                                                ), MCRYPT_RAND)
                                )
                        ), "\0"
        );
    }

    public function decrypt($sval, $skey) {
        //echo "key: ".$skey."<br>_DATA: ".$sval."<br>";
        return str_replace($this->salt, "", rtrim(
                        mcrypt_decrypt(
                                MCRYPT_RIJNDAEL_256, $skey, base64_decode($sval), MCRYPT_MODE_ECB, mcrypt_create_iv(
                                        mcrypt_get_iv_size(
                                                MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB
                                        ), MCRYPT_RAND
                                )
                        ), "\0"
        ));
    }
    
    private function salt() {
        $file= _DATA .'/security.ini';
        try {
            $salt =(parse_ini_file($file, true));
            $this->salt = $salt['aes']['salt'];
        } catch (Exception $e) {
            //print __CLASS__.": Could not find security configuration file!<br>";
            //$this->_e->add($e);
        }
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
