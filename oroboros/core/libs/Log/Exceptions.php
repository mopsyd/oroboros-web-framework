<?php
/**
 * Description of _Exceptions
 * This is the custom error handler class
 *
 * @author Your Name <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Log;

class Exceptions extends \Exception {
    protected $log = NULL;
    protected $logpath = NULL;
    protected $destination = NULL;
    protected $settings = NULL;
    protected $prefix = NULL;
    protected $suffix = NULL;
    
    public function __construct($type) {
        $this->type = $type;
        $this->logpath = _LOG;
        $this->data = new \oroboros\core\libs\Dataflow\Dataflow();
        $this->settings = $this->data->fetch_ini(_DATA . 'debug.ini');
        var_dump($this->settings);
        switch ($type) {
            case 'system':
                //throws a system exception
                $this->log = $this->settings['SYSTEM']['logfile'];
                $this->prefix = $this->settings['SYSTEM']['logindex'];
                break;
            case 'module':
                //throws a module exception
                $this->log = $this->settings['MODULES']['logfile'];
                $this->prefix = $this->settings['MODULES']['logindex'];
                break;
            case 'component':
                //throws a component exception
                $this->log = $this->settings['COMPONENTS']['logfile'];
                $this->prefix = $this->settings['COMPONENTS']['logindex'];
                break;
            case 'template':
                //throws a template exception
                $this->log = $this->settings['TEMPLATES']['logfile'];
                $this->prefix = $this->settings['TEMPLATES']['logindex'];
                break;
            case 'theme':
                //throws a theme exception
                $this->log = $this->settings['THEMES']['logfile'];
                $this->prefix = $this->settings['THEMES']['logindex'];
                break;
            case 'database':
                //throws a database exception
                $this->log = $this->settings['DATABASE']['logfile'];
                $this->prefix = $this->settings['DATABASE']['logindex'];
                break;
            case 'node':
                //throws a node exception
                $this->log = $this->settings['NODES']['logfile'];
                $this->prefix = $this->settings['NODES']['logindex'];
                break;
            case 'security':
                //throws a security exception
                $this->log = $this->settings['SECURITY']['logfile'];
                $this->prefix = $this->settings['SECURITY']['logindex'];
                break;
            case 'framework':
                //throws a framework exception
                $this->log = $this->settings['FRAMEWORK']['logfile'];
                $this->prefix = $this->settings['FRAMEWORK']['logindex'];
                break;
            case 'wrapper':
                //throws a wrapper exception
                $this->log = $this->settings['WRAPPER']['logfile'];
                $this->prefix = $this->settings['WRAPPER']['logindex'];
                break;
            case 'cron':
                //throws a cron exception
                $this->log = $this->settings['CRON']['logfile'];
                $this->prefix = $this->settings['CRON']['logindex'];
                break;
            case 'adapter':
                //throws an adapter exception
                $this->log = $this->settings['ADAPTER']['logfile'];
                $this->prefix = $this->settings['ADAPTER']['logindex'];
                break;
            default:
                //throws an unknown type exception
                $this->log = $this->settings['UNKNOWN']['logfile'];
                $this->prefix = $this->settings['UNKNOWN']['logindex'];
                break;
        }
        $this->log = new \oroboros\core\libs\Log\Log($this->log, $this->settings['SYSTEM']['logmode'], $this->settings['SYSTEM']['debug']);
        $previous = $this->settings['SYSTEM']['previousException'];
        //parent::__construct($this->prefix . $this->suffix, $code=0, $previous);
    }
    
    public function getLog() {
        
    }
    
    public function __destruct() {
        ;
    }
}

?>
