<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Log
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Log;

class Log {
    
    const EMERGENCY = 'emergency';
    const ALERT     = 'alert';
    const CRITICAL  = 'critical';
    const ERROR     = 'error';
    const WARNING   = 'warning';
    const NOTICE    = 'notice';
    const INFO      = 'info';
    const DEBUG     = 'debug';
    
    public static $log;

    public function __construct($log, $method, $debug=NULL) {
        $this->init($log, $method, $debug);
    }
    
    private function init($log, $method, $debug=NULL) {
        $this->log = _LOG . $log;
        $this->method = $method;
        $this->checkLog($debug);
    }

    public function makeLog($type=_DOC) {
        //creates a new log for the specified module if one does not already exist
        try {
            date_default_timezone_set("UTC");
            $log = fopen($this->log, 'w') or die('Cannot create log file.');
            chmod($this->log, 0777);
            file_put_contents($this->log, 'Log file for '. $type . PHP_EOL .
                    'Generated on: ' . date('Y/m/d') . '-' . 'UTC: ' . time() . PHP_EOL . PHP_EOL .
                    '/* ---------------- BEGIN LOG ---------------- */' . PHP_EOL . PHP_EOL);
        } catch (Exception $e) {
            echo 'Caught exception: ' . $e;
        }
    }
    
    protected function checkLog($debug) {
        if(!file_exists($this->log) && $this->method == 'file|both') {
            $this->makeLog();
        }
    }
    
    public function updateLog() {
        
    }
    
    protected function addLogEntry($log) {
        
    }
    
    protected function deleteLog() {
        
    }
    
    protected function archiveLog() {
        
    }
    
    protected function formatEntry() {
        
    }
    
    public function __destruct() {
        ;
    }
}

?>
