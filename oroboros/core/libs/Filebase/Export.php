<?php
/**
 * Description of Export
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Filebase;

abstract class Export extends oroboros\core\libs\Lib {
    
    public function __construct($package=NULL) {
        parent::__construct($package);
    }
    
    public function init($package) {
        
    }
    
    public function package($files, $method) {
        
    }
    
    public function build_settings($location, $type) {
        
    }
    
    public function build_map($location, $type) {
        
    }
    
    public function ship($file, $target) {
        
    }
    
    public function share($file, $recipient, $method) {
        
    }
    
    public function validate_package($package, $type) {
        
    }
    
    public function register_package($package, $type) {
        
    }
    
    public function unregister_package($package, $type) {
        
    }
    
    public function delete_package($package, $location) {
        
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
