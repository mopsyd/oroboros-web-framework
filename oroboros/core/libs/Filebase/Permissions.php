<?php
/**
 * Description of Permissions
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Filebase;

class Permissions extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    function __construct($package=NULL) {
        parent::__construct($package);
    }
    
    public function init() {
        
    }
    
    public function chown($file, $user, $group=NULL) {
        
    }
    
    public function chmod($file, $permissions) {
        
    }
    
    public function check_rights($file) {
        
    }
    
    public function validate($file, $expected) {
        
    }
    
    public function map_permissions($directory, $recursive=FALSE) {
        
    }
    
    function __destruct() {
        parent::__destruct();
    }
}

?>
