<?php
/**
 * Description of _Import
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Filebase;

abstract class Import extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    function __construct($package=NULL) {
        parent::__construct($package);
    }
    
    public function init() {
        
    }
    
    public function check_settings($settingsfile, $type) {
        
    }
    
    public function check_layout($layoutfile, $type) {
        
    }
    
    public function compare_package($package, $reference) {
        
    }
    
    public function archive_package($package, $type) {
        
    }
    
    public function restore_package($package, $version) {
        
    }
    
    public function unpack_package($package) {
        
    }
    
    function __destruct() {
        parent::__destruct();
    }
}

?>
