<?php
/**
 * Description of _Archive
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Filebase;

class Archive {
    
    function __construct() {

    }
    
    public function init() {
        
    }
    
    public function zip($file, $destination) {
        
    }
    
    public function unzip($file, $destination) {
        
    }
    
    public function tar($file, $destination) {
        
    }
    
    public function untar($file, $destination) {
        
    }
    
    function __destruct() {

    }
}

?>
