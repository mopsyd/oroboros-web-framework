<?php
/**
 * This class loads dependencies for the core, class, adapter, node, site, page, content, module, component, stylesheet, script or media.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Filebase;

class Dependencies {
    
    function __construct($database, $datahandler) {
        $this->db = $database;
        $this->data = $datahandler;
        $this->init();
    }
    
    public function init() {
    }
    
    public function check_baseline_dependencies($type=_DOC) {
        $mode = ($type==_DOC) ? 'pageload' : 'definition' ;
        echo 'Mode: ' . $mode . '<br>';
    }
    
    public function _core($site, $subdomain, $page) {
        
    }
    
    public function _module($module) {
        
    }
    
    public function _component($component) {
        
    }
    
    public function _template($template) {
        
    }
    
    public function _theme($theme) {
        
    }
    
    public function _site($site) {
        
    }
    
    public function _page($page) {
        
    }
    
    public function _class($file) {
        
    }
    
    public function _javascript($page) {
        
    }
    
    public function _css($page) {
        
    }
    
    public function _adapters($adapter) {
        
    }
    
    public function _media($media, $type) {
        
    }
    
    public function _node($node) {
        
    }
    
    public function _register($dependency, $type, $location) {
        
    }
    
    public function _unregister($dependency, $type, $location) {
        
    }
    
    public function _load($dependency, $location) {
        
    }
    
    function __destruct() {

    }
}

?>
