<?php

/**
 * Description of _Filebase
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */

namespace oroboros\core\libs\Filebase;

class Filebase {

    function __construct($package = NULL) {
        $this->package = isset($package) ? $package : NULL;
        $this->init();
    }

    public function init() {
        $this->fileperms = new \oroboros\core\libs\Filebase\Permissions($this->package);
    }

    public function getFiles($path, $type = '*', $relative = FALSE, $pattern = NULL) {
        echo 'Path: ' . $path . $type . '<br>';
        $files = array();
        if ($relative == TRUE) {
            //relative directory checking
            if (!isset($pattern)) {
                //user defined pattern
                $pattern = array();
                if (isset($_SESSION['sandbox']['staging'])) {
                    $pattern[] = _STAGING;
                }
                $pattern[] = _LOCAL;
                $pattern[] = _CORE;
            }
            //preset pattern
            $pattern = array();
            if (isset($_SESSION['sandbox']['staging'])) {
                $pattern[] = _STAGING;
            }
            $pattern[] = _LOCAL;
            $pattern[] = _CORE;
            foreach ($pattern as $iterate) {
                echo 'searching ' . $iterate . $path . $type;
                $files[] = glob($iterate . $path . $type);
            }
        } else {
            //absolute pattern checking
            echo 'searching ' . $path . $type . '<br>';
            $files[] = glob($path . $type);
        }
        return !empty($files) ? $files : FALSE;
    }

    public function save($file, $path) {
        
    }

    public function copy($file, $path) {
        if (is_readable($file)) {
            if (is_writable($path)) {
                $tmp = array_pop(explode('/', $file));
                copy($file, $path . $tmp);
                return TRUE;
            } else
                return FALSE;
        } else {
            return FALSE;
        }
    }

    public function delete($file, $path, $archive = FALSE) {
        
    }

    public function load($file, $path) {
        
    }

    public function edit($file, $path) {
        
    }

    public function merge($file, $path, $targetfile, $targetpath) {
        
    }

    public function makedir($location, $recursive = FALSE) {
        if ($recursive == TRUE) {
            //recursively create directory structure
        } else {
            $path = explode('/', $location);
            array_pop($path);
            $path = implode('/', $path) . '/';
            //create single directory
            if (is_dir($location)) {
                //directory already exists
                return TRUE;
            } else {
                if (is_writable($path)) {
                    mkdir($location, 0775);
                    return TRUE;
                } else {
                    return FALSE;
                }
            }
        }
    }

    public function removedir($location) {
        //create single directory
        if (is_readable($location) && is_dir($location)) {
            //directory exists
            if (is_writable($location)) {
                $files = @scandir($location);
                if (count($files) > 2) {
                    $this->rrmdir($location);
                }
                rmdir($location);
                return TRUE;
            } else {
                //insufficient file permissions
                return FALSE;
            }
            return TRUE;
        } else {
            //directory does not exist
            return FALSE;
        }
    }

    protected function rrmdir($dir) {
        foreach (glob($dir . '/*') as $file) {
            if (is_dir($file)) {
                $files = @scandir($file);
                if (count($files) > 2) {
                    $this->rrmdir($file);
                } else {
                    rmdir($file);
                }
            } else {
                unlink($file);
            }
        }
    }

    public function lineforward($object) {
        
    }

    public function linebackward($object) {
        
    }

    public function rewind($object) {
        
    }

    public function end($object) {
        
    }

    function __destruct() {
        
    }

}

?>
