<?php

/**
 * This class handles rendering of html content
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */

namespace oroboros\core\libs\Render;

abstract class Html {

    const OFFSET = '&nbsp;&nbsp;&nbsp;&nbsp;';

    public function __construct() {
        $this->init();
    }

    private function init() {
        
    }

    public function build($type, $content, $method = 'tag', $id = NULL, $class = NULL, $params = NULL) {
        if ($method != 'short') {
            $render = $this->open($type, $method, $id, $class, $params);
            $render .= $this->body($content);
            $render .= $this->close($type);
        } else {
            $render = $this->open($type, $method, $id, $class, $params);
        }
        if ($type == 'a' && isset($params['href']) && $params['href'] == 'http://framework.local//content') {
                echo '<code>';
                echo ($render);
                echo '</code><hr>';
            }
        return $render;
    }
    
    public function buildNoWrap($content, $method = 'tag', $id = NULL, $class = NULL, $params = NULL) {
        if ($method != 'short') {
            $render = $this->open($content['type'], $method, $id, $class, $params);
            $render .= $this->body($content['content']);
            $render .= $this->close($content['type']);
        } else {
            $render = $this->open($type, $method, $id, $class, $params);
        }
        if ($type == 'a' && isset($params['href']) && $params['href'] == 'http://framework.local//content') {
                echo '<code>';
                echo ($render);
                echo '</code><hr>';
            }
        return $render;
    }

    protected function open($type, $method, $id = NULL, $class = NULL, $params = NULL) {
        switch ($method) {
            case 'tag':
                //render tag
                $render = $this->tag($type, $id, $class, $params);
                break;
            case 'short':
                //render short tag
                $render = $this->shortTag($type, $method, $id, $class, $params);
                break;
        }
        return $render;
    }

    protected function body($content) {
        $render = NULL;
        if (is_array($content)) {
            //recursively parse content
            foreach ($content as $item) {
                $type = isset($item['type']) ? $item['type'] : 'p';
                $method = isset($item['method']) ? $item['method'] : 'tag';
                $id = isset($item['id']) ? $item['id'] : NULL;
                $class = isset($item['class']) ? $item['class'] : NULL;
                $params = isset($item['params']) ? $item['params'] : NULL;
                $body = isset($item['content']) ? $item['content'] : NULL;
                $render .= $this->build($type, $body, $method, $id, $class, $params);
            }
        } else {
            //render content string
            $render = $content;
        }
        return $render;
    }

    protected function close($type) {
        $render = '</' . $type . '>' . PHP_EOL;
        return $render;
    }

    protected function tag($type, $id = NULL, $class = NULL, $params = NULL) {
        if ($type == 'a' && !isset($params)) {
            return NULL;
        }
        $render = '<' . $type . $this->makeId($id) . $this->makeClass($class) . $this->makeParam($params) . '>';
        return $render;
    }

    protected function shortTag($type, $tag, $id = NULL, $class = NULL, $params = NULL) {
        $render = '<' . $type . $this->makeId($id) . $this->makeClass($class) . $this->makeParam($params) . ' />' . PHP_EOL;
        return $render;
    }

    protected function makeId($id = NULL) {
        $render = isset($id) ? ' id="' . $id . '"' : NULL;
        return $render;
    }

    protected function makeClass($class = NULL) {
        $render = isset($class) && is_string($class) && $class != '' ? ' class="' . $class . '"' : NULL;
        $render = isset($class) && is_array($class) ? ' class="' . implode(' ', $class) . '"' : $class;
        return $render;
    }

    protected function makeParam($params = NULL) {
        if (isset($params)) {
            $render = NULL;
            foreach ($params as $key => $value) {
                $render .= ' ' . $key . '="' . $value . '"';
            }
            return $render;
        } else
            return NULL;
    }

    public function __destruct() {
        
    }

}

?>
