<?php
/**
 * This class handles sidebar content throughout the system
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Render;

class Sidebar extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    public function __construct($package=NULL) {
        parent::__construct($package);
        $this->init();
    }
    
    private function init() {

    }
    
    public function getSidebar($page) {
        
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
