<?php

namespace oroboros\core\libs\Render;

/**
 * Description of Headers
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Headers extends \oroboros\core\libs\Abstracts\Site\Lib {

    private $type = NULL;

    public function __construct($package, $type = _DOC) {
        parent::__construct($package);
        $this->init($type);
    }

    private function init($type) {
        $this->type = $type;
    }

    public function get_headers($type = NULL, $preformat = FALSE, $args = array(NULL, NULL)) {
        if (!isset($type)) {
            $type = $this->type;
        }
        if (isset($args[0])) {
            //handle response code
        }
        if (isset($args[1])) {
            //handle caching
        }
        switch ($type) {
            case 'site':
                //gets the page headers
                break;
            case 'js':
                //gets the javascript file headers, checks if caching is allowed
                header("Content-type: text/javascript; charset=utf-8");
                if(isset($args)) {
                    $this->checkCaching($args);
                }
                break;
            case 'css':
                //gets the css file headers, checks if caching is allowed
                header("Content-type: text/css; charset=utf-8");
                if(isset($args)) {
                    $this->checkCaching($args);
                }
                break;
            case 'xml':
                //gets the xml document headers
                header("Content-Type:text/xml");
                break;
            case 'text':
                //gets the text document headers
                header("Content-Type:text/plain");
                break;
            case 'errorpage':
                //gets the error page message headers
                switch ($args) {
                    case 301:
                        //returns resource permanently moved header
                        header("HTTP/1.1 301 Moved Permanently");
                        break;
                    case 302:
                        //returns resource temporarily moved header
                        //this header is used to determine internal redirection

                        break;
                    case 307:
                        //returns resource temporarily moved to another url header
                        //this header is used to determine node redirection

                        break;
                    case 400:
                        //returns bad request header

                        break;
                    case 403:
                        //returns access denied header
                        header($_SERVER["SERVER_PROTOCOL"] . " 403 Forbidden");
                        header('Status: 403 Forbidden');
                        header('HTTP/1.0 403 Forbidden');
                        $_SERVER['REDIRECT_STATUS'] = 403;
                        break;
                    case 404:
                        //returns resource not found header
                        header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
                        header("Status: 404 Not Found");
                        header("HTTP/1.0 404 Not Found");
                        $_SERVER['REDIRECT_STATUS'] = 404;
                        break;
                    case 405:
                        //returns not acceptable header
                        //this header is used when ajax requests lack the correct permissions,

                        break;
                    case 412:
                        //returns precondition failed header
                        //this header is used when form or ajax user key validation is unsuccessful
                        //instances of this header will be logged to the security log file

                        break;
                    case 414:
                        //returns resource too long header
                        //this header is used ajax json strings are too big, or when resources cannot
                        //be successfully sent over SOAP protocol

                        break;
                    case 500:
                        //returns internal server error header
                        //used when the codebase is broken
                        header('HTTP/1.1 500 Internal Server Error', true, 500);

                        break;
                    case 501:
                        //returns not implemented header
                        //used when core functionality exists, but is not yet functional for public use

                        break;
                    case 503:
                        //returns service unavailable header
                        //used when the process queue is too big, and the server cannot handle additional requests

                        break;
                    case 504:
                        //returns gateway timeout header
                        //used when upstream nodes do not respond within the request timeout window

                        break;
                }
                break;
            case 'sitemap':
                //gets the sitemap headers
                $this->get_headers('xml');
                break;
            case 'rss':
                //gets the rss feed headers
                header('Content-Type: application/rss+xml; charset=utf-8');
                break;
            case 'ajax':
                //gets headers relevant to the ajax request method
                switch ($args) {
                    case 200:
                        //the request was processed correctly
                        break;
                    case 201:
                        //the request has created a new resource
                        break;
                    case 202:
                        //the request was accepted but no action taken
                        //this status is used primarily in sandbox mode, to prevent core functionality from
                        //being accidentally or maliciously executed in design or development sandbox environments

                        break;
                    case 304:
                        //the request resulted in no modification
                        //this status is used in the same context as the previous, however pertains to resource or
                        //filebase changes

                        break;
                    default:
                        $this->get_headers('errorpage', $args);
                        break;
                }
                break;
            case 'framework':
                //gets headers relevant to the arguments passed
                switch ($args) {
                    case '200|201|202|304|':
                        $this->get_headers('ajax', $args);
                        break;
                    case '301|302|307|400|403|404|405|412|414|500|501|503|504':
                        $this->get_headers('errorpage', $args);
                        break;
                    default:
                        //log malformed header request
                        return false;
                        break;
                }
                break;
            case 'robots':
                //gets headers for the robots.txt document
                $this->get_headers('text');
                header("Cache-Control: max-age=0");
                break;
            case 'wrapper':
                //defers header definition to the subsystem
                return false;
                break;
            case 'cron':
                //returns headers relevant to the cron file operation
                break;
        }
    }

    private function checkCaching($args) {
        switch ($args) {
            case 'sandbox':
                //sets the sandbox mode css header protocol
                header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
                header('Pragma: no-cache'); // HTTP 1.0.
                header('Expires: 0'); // Proxies.
                break;
            case 'cache':
                //sets the cached css header protocol
                //CHANGE ME WHEN CACHING IS IMPLEMENTED
                header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
                header('Pragma: no-cache'); // HTTP 1.0.
                header('Expires: 0'); // Proxies.
                break;
            default:
                //sets the default css header protocol
                //CHANGE ME LATER
                header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
                header('Pragma: no-cache'); // HTTP 1.0.
                header('Expires: 0'); // Proxies.
                break;
        }
    }

    private function preformat($headers) {
        //returns headers as preformatted text, so they can be viewed on the screen without throwing an error
        $render = '<pre>' . PHP_EOL . $headers . PHP_EOL . '<pre>' . PHP_EOL;
        return $render;
    }

    public function __destruct() {
        parent::__destruct();
    }

}