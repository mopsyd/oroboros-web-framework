<?php

namespace oroboros\core\libs\Render;

/**
 * Description of Skeleton
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Skeleton extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    const TEMPLATEDIR = 'core/views/Skeleton/';
    const STYLESHEETDIR = '';
    const SCRIPTINGDIR = '';
    
    public function __construct($package) {
        parent::__construct($package);
        $this->init();
    }
    
    private function init() {
        
    }
    
    public function fetchSkeleton($method = 'site') {
        $skeleton = array();
        switch(_DOC) {
            case 'site':
                $skeleton['head'] = $this->renderSkeletonHead($method);
                $skeleton['head'] .= $this->checkComponentScaffold($method, 'head');
                $skeleton['foot'] = $this->checkComponentScaffold($method, 'foot');
                $skeleton['foot'] .= $this->renderSkeletonFoot($method);
                break;
            case 'css':
                
                break;
            case 'js':
                
                break;
            case 'wrapper':
                $skeleton['head'] = $this->renderSkeletonHead($method);
                $skeleton['foot'] = $this->renderSkeletonFoot($method);
                break;
            case 'framework':
                $skeleton['head'] = $this->renderSkeletonHead($method);
                $skeleton['foot'] = $this->renderSkeletonFoot($method);
                break;
            case 'ajax':
                return false;
                break;
            case 'rss':
                return false;
                break;
            case 'email':
                $skeleton['head'] = $this->renderSkeletonHead($method);
                $skeleton['foot'] = $this->renderSkeletonFoot($method);
                break;
            default:
                return false;
                break;
        }
        return $skeleton;
    }
    
    private function renderSkeletonHead($type) {
        require _APP . self::TEMPLATEDIR . $type . '/head.phtml';
    }
    
    private function renderSkeletonFoot($type) {
        require _APP . self::TEMPLATEDIR . $type . '/foot.phtml';
    }
    
    private function checkComponentScaffold($type, $section) {
        $scaffold = NULL;
        switch ($type) {
            case 'site':
                switch ($section) {
                case 'head':
                    //completes the skeleton head based on component settings
                    break;
                case 'foot':
                    //completes the skeleton foot based on component settings
                    break;
                }
                break;
            case 'wrapper':
                switch ($section) {
                case 'head':
                    //completes the skeleton head based on component settings
                    break;
                case 'foot':
                    //completes the skeleton foot based on component settings
                    break;
                }
                break;
            case 'framework':
                switch ($section) {
                case 'head':
                    //completes the skeleton head based on component settings
                    break;
                case 'foot':
                    //completes the skeleton foot based on component settings
                    break;
                }
                break;
            case 'email':
                switch ($section) {
                case 'head':
                    //completes the skeleton head based on component settings
                    break;
                case 'foot':
                    //completes the skeleton foot based on component settings
                    break;
                }
                break;
        }
        
        
        return $scaffold;
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}