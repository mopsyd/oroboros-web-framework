<?php
/**
 * Description of _Output
 * handles output buffering
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Render;

class Output {
    //put your code here
    public function _initBuffer() {
        while (ob_get_level() > 0) {
            ob_end_clean();
        }
        ob_start('ob_gzhandler', 0, PHP_OUTPUT_HANDLER_CLEANABLE | PHP_OUTPUT_HANDLER_FLUSHABLE | PHP_OUTPUT_HANDLER_REMOVABLE);
    }
    
    public function cleanGet() {
        return ob_get_clean();
    }
    
    public function fetch() {
        return ob_get_contents();
    }
    
    public function returnClear() {
        return ob_get_contents();
    }
    
    public function clear() {
        ob_clean();
    }
    
    public function destroy() {
        ob_end_clean();
    }
    
    public function level() {
        return ob_get_level();
    }
    
    public function length() {
        return ob_get_length();
    }
    
    public function status() {
        return ob_get_status();
    }
}

?>
