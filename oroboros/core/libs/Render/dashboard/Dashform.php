<?php
/**
 * Description of _Dashform
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Render\dashboard;

class Dashform extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    public function __construct($package=NULL) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}
?>
