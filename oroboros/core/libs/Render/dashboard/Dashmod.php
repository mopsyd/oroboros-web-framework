<?php
/**
 * Description of _Dashmod
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Render\dashboard;

class Dashmod extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    public function __construct($package=NULL) {
        parent::__construct($package);
    }
    
    public function dashmod($type, $title, $content, $id, $menu=NULL, $classes=NULL) {
        $this->dashmod = $this->dashmodShell($id, $classes=NULL);
        $this->dashmod .= $this->dashmodTitle($title);
        if (!is_null($menu)) {
            $this->dashmod .= $this->dashmodMenu($menu);
        }
        $this->dashmod .= $this->dashmodContent($type, $content);
        $this->dashmod .= $this->dashmodFinalize();
        return $this->dashmod;
    }
    
    private function dashmodTitle($title) {
        $title = '<div class="heading"><h2>' . $title . '</h2>';
        $title .= $this->dashmodUI();
        $title .= '</div>';
        return $title;
    }
    
    private function dashmodUI() {
        $ui ='<span class="menu"><span class="settings" /><span class="minimize" /><span class="close" /></span>';
        return $ui;
    }
    
    private function dashmodMenu($menu) {
        $menu = '<div class="menu"><ul class="nav">';
        foreach ($menu as $title => $item) {
            $menu .= '<li><a href="' .$item['link']. '">' . $title . '</a></li>';
        }
        $menu .= '</ul></div>';
        return $menu;
    }
    
    private function dashmodShell($id, $classes=NULL) {
        if (!is_null($classes)) {
            $classes = 'dashmod ' . $classes;
        } else {
            $classes = 'dashmod';
        }
        $shell = '<div id="' . $id . '" class="' . $classes . '">';
        return $shell;
    }
    
    private function dashmodContent($type, $content) {
        
    }
    
    private function dashmodInfo($content) {
        
    }
    
    private function dashmodForm($content) {
        
    }
    
    private function dashmodFormTable($content) {
        
    }
    
    private function dashmodMedia($content) {
        
    }
    
    private function dashmodFinalize() {
        return '</div>';
    }
    
    public function __destruct() {
        ;
    }
}

?>
