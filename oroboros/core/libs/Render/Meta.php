<?php
/**
 * Fetches the correct meta tags for the site and page.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Render;

class Meta extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    function __construct($package=NULL) {
        parent::__construct($package);
    }
    
    public function init() {
        
    }
    
    function __destruct() {
        parent::__destruct();
    }
}

?>
