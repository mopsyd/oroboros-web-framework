<?php

/**
 * This class injects relevant content into the page templates using the hook system.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Render;

class Hooks extends \oroboros\core\libs\Abstracts\Site\Lib {

    protected $hookremove = array();
    protected $unsetHooks = TRUE;
    protected $render = NULL;
    public $controller = NULL;
    //hook registration
    protected $hooks = array();

    public function __construct($package=NULL) {
        parent::__construct($package);
        $this->init($package);
    }

    private function init($package) {
        $this->page = $package['page'];
        $this->controller = isset($this->package['controller']) ? $this->package['controller'] : NULL;
        $this->template = $package['template'];
        //$this->theme = $package['theme'];
    }

    public function contentRender($content) {
        $render = $content;
        do {
            $hooks = $this->getPageHooks($render);
            foreach ($hooks as $hook) {
                $this->getHookScope($hook);
            }
            $results = $this->getContentHookContent($this->hooks, $hooks);
            $render = $this->removeUnusedHooks($this->hookremove, $render);
            $render = $this->renderContentHooks($this->hooks, $render, $results);
            $hooks = $this->getPageHooks($render);
            $this->hooks = array();
        } while ($this->unsetHooks != FALSE);
        return $render;
    }

    /**
     * Finds content hooks and returns an array
     * 
     * hook format: []hook[/]
     * hooks must be alphanumeric sequences up to 32 characters, and may include hyphens only
     * 
     * @param string $content
     * @return array
     */
    private function getPageHooks($content) {
        preg_match_all('@\[\][a-zA-Z0-9\-]{1,32}\[\/\]@', $content, $results);
        $hooks = array();
        foreach ($results as $hook) {
            $hooks[] = str_replace(array('[', ']', '/'), NULL, $hook);
        }
        if (empty($hooks[0])) {
            $this->unsetHooks = FALSE;
        }
        return $hooks[0];
    }

    /**
     * Finds function hooks in content and returns a multidimensional array
     * 
     * function hook format[[functionName]]param, param, param[/]
     * function hook names must be alphanumeric sequences up to 32 characters and may include hyphens only
     * params are optional. params may be alphanumeric strings separated by commas (spaces optional, they will be stripped at runtime)
     * any number of params may be passed, but the total param string cannot exceed 256 characters.
     * params should follow the correct format for the function they reference in the codebase.
     * 
     * @param string $content
     * @return array - key = function name, values in array = params
     */
    private function getPageFunctionHooks($content) {
        preg_match_all('@\[\[[a-zA-Z0-9\-]{1,32}\]\][[a-zA-Z0-9\-, ]{1,256}]{0,}\[\/\]@', $content, $results);
        $hooks = array();
        foreach ($results[0] as $hook) {
            preg_match('@\[\[[a-zA-Z0-9]{1,32}\]\]@', $hook, $x);
            $y = str_replace(array('[', '/', ']'), NULL, preg_replace('@\[\[[a-zA-Z0-9]{1,32}\]\]@', NULL, $hook));
            $hooks[str_replace(array('[', ']'), NULL, $x[0])] = explode(',', str_replace(' ', NULL, $y));
        }
        return $hooks;
    }

    private function getHookScope($hook) {
        $q = "SELECT `type`, `scope` FROM `hooks` WHERE `slug`='$hook' AND `permission` IN ('" . implode("','", $_SESSION['user']['permissions']) . "') AND `active`='1'";
        
        $hookdata = $this->db->select($q);
        
        if(isset($hookdata[0])) {
            $this->hooks[$hookdata[0]['scope']][$hookdata[0]['type']][] = $hook;
        } else {
            $this->hookremove[] = $hook;
        }
    }

    /**
     * Gets page content from the database based on the hook parameter passed to the method
     * 
     * @param array or string $hooks - the hook content to search the database for
     * @return boolean
     */
    private function getContentHookContent($hooksindex, $hooks) {
        foreach ($hooksindex as $hooksetkey => $hookset) {
            //loops through the hook sets
            switch ($hooksetkey) {
                case 'global':
                    //TODO create global content system
                    $this->hooks[$hooksetkey]['complete'] = FALSE;
                    $this->hooks[$hooksetkey]['query'] = 'INCOMPLETE QUERY DEFINITION (at line ' . __LINE__ . ' of file: ' . __FILE__ . ')';
                    $this->hooks[$hooksetkey]['renderPath'] = 'UNDEFINED RENDER PATH (at line ' . __LINE__ . ' of file: ' . __FILE__ . ')';
                    break;
                case 'system':
                    //TODO create system content system
                    $this->hooks[$hooksetkey]['complete'] = FALSE;
                    $this->hooks[$hooksetkey]['query'] = 'INCOMPLETE QUERY DEFINITION (at line ' . __LINE__ . ' of file: ' . __FILE__ . ')';
                    $this->hooks[$hooksetkey]['renderPath'] = 'UNDEFINED RENDER PATH (at line ' . __LINE__ . ' of file: ' . __FILE__ . ')';
                    break;
                case 'sites':
                    $this->hooks[$hooksetkey]['complete'] = TRUE;
                    $this->hooks[$hooksetkey]['query'] = "SELECT `id`,`type`,`content`,`permission`,`hook` FROM `sites_content` WHERE `hook` IN ('" . implode('\',\'', $hookset['content']) . "') AND `permission` IN ('" . implode("','", $_SESSION['user']['permissions']) . "') AND `site` IN ('" . $_SESSION['site'] . "','global') AND `active`=1 AND `show`=1 ORDER BY `hook` ASC, `hierarchy` ASC;";
                    $this->hooks[$hooksetkey]['renderPath'] = _SITES . $_SESSION['site'] . '/content/';
                    break;
                case 'pages':
                    $this->hooks[$hooksetkey]['complete'] = TRUE;
                    $this->hooks[$hooksetkey]['query'] = "SELECT `id`,`type`,`content`,`permission`,`hook` FROM `pages_content` WHERE `hook` IN ('" . implode('\',\'', $hookset['content']) . "') AND `permission` IN ('" . implode("','", $_SESSION['user']['permissions']) . "') AND `site` IN ('" . $_SESSION['site'] . "','global') AND `page`='" . $_SESSION['page'] . "' AND `subdomain`='" . $_SESSION['subdomain'] . "' AND `active`=1 AND `show`=1 ORDER BY `hook` ASC, `hierarchy` ASC;";
                    $this->hooks[$hooksetkey]['renderPath'] = _SITES . $_SESSION['site'] . '/' . $_SESSION['page'] . '/';
                    break;
                case 'templates':
                    $this->hooks[$hooksetkey]['complete'] = TRUE;
                    $this->hooks[$hooksetkey]['query'] = "SELECT `id`,`type`,`content`,`permission`,`hook` FROM `templates_content` WHERE `hook` IN ('" . implode('\',\'', $hookset['content']) . "') AND `permission` IN ('" . implode("','", $_SESSION['user']['permissions']) . "') AND `active`=1 AND `show`=1 ORDER BY `hook` ASC, `hierarchy` ASC;";
                    $this->hooks[$hooksetkey]['renderPath'] = _TEMPLATE . $this->template['template'] . '/content/';
                    break;
                case 'themes':
                    $this->hooks[$hooksetkey]['complete'] = FALSE;
                    $this->hooks[$hooksetkey]['query'] = "SELECT `id`,`type`,`content`,`permission`,`hook` FROM `themes_content` WHERE `hook` IN ('" . implode('\',\'', $hookset['content']) . "') AND `permission` IN ('" . implode("','", $_SESSION['user']['permissions']) . "') AND `active`=1 AND `show`=1 ORDER BY `hook` ASC, `hierarchy` ASC;";
                    $this->hooks[$hooksetkey]['renderPath'] = _THEME . $this->template['template'] . '/content/';
                    break;
                case 'modules':
                    $this->hooks[$hooksetkey]['complete'] = FALSE;
                    $this->hooks[$hooksetkey]['query'] = "SELECT `id`,`type`,`content`,`permission`,`hook` FROM `modules_content` WHERE `hook` IN ('" . implode('\',\'', $hookset['content']) . "') AND `permission` IN ('" . implode("','", $_SESSION['user']['permissions']) . "') AND `active`=1 AND `show`=1 ORDER BY `hook` ASC, `hierarchy` ASC;";
                    $this->hooks[$hooksetkey]['renderPath'] = 'UNDEFINED RENDER PATH (at line ' . __LINE__ . 'of file: ' . __FILE__ . ')';
                    break;
                case 'components':
                    $this->hooks[$hooksetkey]['complete'] = FALSE;
                    $this->hooks[$hooksetkey]['query'] = "SELECT `id`,`type`,`content`,`permission`,`hook` FROM `components_content` WHERE `hook` IN ('" . implode('\',\'', $hookset['content']) . "') AND `permission` IN ('" . implode("','", $_SESSION['user']['permissions']) . "') AND `active`=1 AND `show`=1 ORDER BY `hook` ASC, `hierarchy` ASC;";
                    $this->hooks[$hooksetkey]['renderPath'] = 'UNDEFINED RENDER PATH (at line ' . __LINE__ . 'of file: ' . __FILE__ . ')';
                    break;
            }

            //loop through hooks, push content to array, use $renderPath for content templates, then call hook replacement method

            if (!empty($hookset)) {
                switch (key($hookset)) {
                    case 'function':
                        //handles function hook rendering
                        break;
                    case 'content':
                        //handles content hook rendering
                        foreach ($hookset as $hooktype) {
                            if (!empty($hooktype)) {
                                //loops through the hook types
                                $content = array();
                                
                                $result = $this->db->select($this->hooks[$hooksetkey]['query']);
                                
                                foreach ($hooktype as $hook) {
                                    $empty = TRUE;
                                    foreach ($result as $i) {
                                        if ($i['hook'] == $hook) {
                                            $empty = FALSE;
                                        }
                                    }
                                    if ($empty == TRUE) {
                                        //flag hook for removal
                                        $this->hookremove[] = $hook;
                                    }
                                }
                                $this->hooks[$hooksetkey]['content'] = array();
                                foreach ($result as $i) {
                                    if (!isset($this->hooks[$hooksetkey]['content'][$i['hook']])) {
                                        $this->hooks[$hooksetkey]['content'][$i['hook']] = array(
                                            'hook' => $i['hook'],
                                            'items' => array(
                                                array(
                                                    'permission' => $i['permission'],
                                                    'content' => $i['content'],
                                                    'type' => $i['type']
                                                )
                                            )
                                        );
                                    } else {
                                        $this->hooks[$hooksetkey]['content'][$i['hook']]['items'][] = array(
                                            'permission' => $i['permission'],
                                            'content' => $i['content'],
                                            'type' => $i['type']
                                        );
                                    }
                                }
                            }
                        }
                        break;
                }
            }
        }
    }

    /**
     * Performs a preg_replace function on the content to insert page content where it is supposed to go.
     * 
     * @param string $source - the content block to insert the content into
     * @param array $content - array of the content to be rendered to the page
     * @return void
     */
    private function renderContentHooks($hooks, $source, $content) {
        foreach ($hooks as $hookname => $hookdata) {
            $result = NULL;
            foreach ($hookdata['content'] as $hook => $data) {
                foreach ($data['items'] as $item) {
                    $this->hooks[$hookname]['content'][$hook]['render'][] = $this->fetchContent($item, $hookdata['renderPath'], $hook) . PHP_EOL;
                }
                $source = $this->replaceContentHook($hook, $this->hooks[$hookname]['content'][$hook]['render'], $source);
            }
        }
        return $source;
    }

    private function fetchContent($item, $path, $hook) {
        //echo $hook . ' type is ' . $item['type'] . '<br>';
        switch ($item['type']) {
            case 'template':
                $result = $this->renderContentTemplate($item, $path);
                break;
            case 'html':
                //renders raw html to the page
                $result = $this->renderHtmlContent($item);
                break;
            case 'image':
                //renders images to the page
                //First create media proxy class
                break;
            case 'audio':
                //creates an audio wrapper and renders the controller to the page with the actual file non displaying
                //First create media proxy class
                break;
            case 'video':
                //creates a video element on the page using the defined system settings
                //First create media proxy class
                break;
            case 'component':
                //loads an activated component
                //First create component factory loader
                break;
            case 'linkset':
                //renders a linkset to the page using the links model
                $this->linkrender = !isset($this->links) ? $this->linkrender = new \oroboros\core\libs\Render\Links($this->package, $this->_site['slug'], $this->_subdomain, $this->_page['slug']) : $this->linkrender;
                $this->linkbuild = !isset($this->linkbuild) ? new \oroboros\core\views\Site\Content\Browser\Menu() : $this->linkbuild;
                $render = $this->linkrender->buildMenu($_SESSION['user']['login'], $this->_site['slug'], strtolower($hook), $item['permission'], '#', 'test-menu', 'test-menu');
                $result = $this->linkbuild->listMenuRender($render);
                //$menu = $this->linkrender->buildMenu($_SESSION['user']['login'],$this->_site['slug'],'dashhead','registered', '#', 'test-menu', 'test-menu');
                break;
            case 'flash':
                //renders a flash element to the page optimized for the viewers device settings. If no flash player, prompts the user to get flash.
                //First create media proxy class
                break;
            default:
                //log an error for an undefined content type and removes the hook from the page
                //First create error log class
                return false;
                break;
        }
        return $result;
    }

    /**
     * Renders content from a template located in the site subfolder
     * 
     * @param type $template
     * @return boolean
     */
    private function renderContentTemplate($template, $path) {
        $file = $path . $template['content'];
        //echo 'Supplied path: ' . $path . $template['content'] . '<br>';
        if (is_readable($file)) {
            ob_start();
            require $file;
            $result = ob_get_clean();
            return $result;
        } else {
            //log incident of missing file, notify site administrator, clear database record
            echo 'Could not find content template: ' . $file . '<br>';
            return false;
        }
    }

    /**
     * Renders static html content from the database
     * 
     * @param type $template
     * @return boolean
     */
    private function renderHtmlContent($item) {
        $render = $item['content'];
        return $render;
    }

    /**
     * Replaces the original content hook with the rendered content
     * 
     * @param string $hook - the hook to replace
     * @param string $content - the content to inject
     * @param string $target - the target to perform the replacement on
     * @return boolean
     */
    private function replaceContentHook($hook, $content, $target) {
        if (is_array($content)) {
            $contentString = NULL;
            foreach ($content as $item) {
                $contentString .= $item . PHP_EOL;
            }
            $content = $contentString;
            unset($contentString);
        }
        $result = preg_replace('@(\[\])' . preg_quote($hook) . '(\[\/\])@', $content, $target);
        return $result;
    }

    private function removeUnusedHooks($hooks, $target) {
        foreach ($hooks as $hook) {
            $target = preg_replace('@(\[\])' . preg_quote($hook) . '(\[\/\])@', NULL, $target);
        }
        return $target;
    }
    
    public function __destruct() {
        parent::__destruct();
    }

}

?>
