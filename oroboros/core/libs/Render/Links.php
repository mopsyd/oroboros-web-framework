<?php

/**
 * This class builds linksets for the system. If $userperms is passed as TRUE, 
 * it will build them based on the user's access rights. Otherwise it will build 
 * the entire linkset.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */

namespace oroboros\core\libs\Render;

class Links extends \oroboros\core\libs\Abstracts\Site\Lib {

    protected $db = NULL;
    protected $perms = NULL;
    protected $userperms = NULL;
    protected $page = NULL;
    protected $subdomain = NULL;
    protected $site = NULL;

    function __construct($package = NULL, $site, $subdomain = NULL, $page = NULL) {
        parent::__construct($package);
        $this->init($site, $subdomain, $page);
    }

    private function init($site, $subdomain, $page) {
        $this->site = $site;
        $this->subdomain = $subdomain;
        $this->page = $page;
    }

    public function getLinkset($site, $hook) {
        $index = $this->getLinksetIndex($site, $hook);
        $linkset = $this->getLinksetLinks($this->getChildLinksets($index, $site), $site, $index['slug']);
        $render = $this->makeLinks($linkset);
        return $render;
    }

    private function getLinksetIndex($site, $hook) {
        $q = "SELECT `slug`,`title` FROM `sites_links_sets` WHERE `permission` IN ('" . implode("','", $_SESSION['user']['permissions']) . "') AND `site` IN ('$site','global') AND `hook`='$hook' AND `active`='1' ORDER BY `hierarchy` ASC;";
        $index = $this->db->select($q);
        return $index[0];
    }

    private function getChildLinksets($linkset, $site) {
        $result = array(
            'title' => $linkset['title'],
            'identifier' => $linkset['slug'],
            'linksets' => array()
        );
        $query = "SELECT `slug`,`title` FROM `sites_links_sets` WHERE `permission` IN ('" . implode("','", $_SESSION['user']['permissions']) . "') AND `site` IN ('$site','global') AND `parent`='" . $result['identifier'] . "' AND `active`='1' ORDER BY `hierarchy` ASC;";

        $linksets = $this->db->select($query);

        $result['linksets'] = $linksets;
        return $result;
    }

    private function getLinksetLinks($linkset, $site, $origin) {
        for ($i = 0; $i < count($linkset['linksets']); $i++) {
            $query = "SELECT `slug`,`parent`,`link`,`display` FROM `sites_links` WHERE `permission` IN ('" . implode("','", $_SESSION['user']['permissions']) . "') AND `site` IN ('$this->site','global') AND `linkset`='" . $linkset['linksets'][$i]['slug'] . "' AND `active`='1' ORDER BY `hierarchy` ASC";
            $linkset['linksets'][$i]['links'] = $this->db->select($query);
        }

        return $linkset;
    }

    private function makeLinks($linkset) {
        switch ($linkset['identifier']) {
            case 'dashhead':
                //creates the dashboard header menu
                $links = '<ul class="nav dashhead">';
                foreach ($linkset['linksets'] as $indexes) {
                    $links .= '<li><span class="dashhead-index">' . PHP_EOL;
                    $links .= '<h4 id="dashhead-' . $indexes['slug'] . '">' . $indexes['title'] . '</h4>' . PHP_EOL . '<ul class="sublinks">' . PHP_EOL;
                    if (empty($indexes['links'])) {
                        $links .= '<li class="disabled"><p>No options available</p></li>' . PHP_EOL;
                    } else {
                        foreach ($indexes['links'] as $link) {
                            $links .= '<li class="sublink">' . PHP_EOL;
                            $links .= '<a href="' . $link['link'] . '"><p>' . $link['display'] . '</p></a>' . PHP_EOL;
                            $links .= '</li>' . PHP_EOL;
                        }
                    }
                    $links .= '</ul>' . PHP_EOL . '</span></li>' . PHP_EOL;
                }
                break;
            case 'dashindex':
                //creates the dashboard index menu
                $links = '<ul class="nav dashindex">';
                $links .= '<li class="title">' . PHP_EOL . '<h2>Dashboard Index</h2>' . PHP_EOL . '</li>' . PHP_EOL;
                foreach ($linkset['linksets'] as $indexes) {
                    $links .= '<li><a href="#"><h2>' . $indexes['title'] . '</h2></a><ul class="sublinks">' . PHP_EOL;
                    if (!empty($indexes['links'])) {
                        foreach ($indexes['links'] as $link) {
                            $links .= '<li class="sublink">' . PHP_EOL;
                            $links .= '<a href="' . $link['link'] . '"><h2>' . $link['display'] . '</h2></a>' . PHP_EOL;
                            $links .= '</li>' . PHP_EOL;
                        }
                    }
                    $links .= '</ul>' . PHP_EOL . '</li>' . PHP_EOL;
                }
                break;
            case 'userbox':
                //creates the dashboard index menu
                $links = '<ul class="nav dashindex">';
                $links .= '<li class="title">' . PHP_EOL . '<h2>Welcome ' . $_SESSION['user']['details']['first'] . '!</h2>' . PHP_EOL . '<p>Access Level: ' . ucfirst($_SESSION['user']['group']) . PHP_EOL . '</li>' . PHP_EOL;
                foreach ($linkset['linksets'] as $indexes) {
                    if (!empty($indexes['links'])) {
                        foreach ($indexes['links'] as $link) {
                            $links .= '<li class="sublink">' . PHP_EOL;
                            $links .= '<a href="' . $link['link'] . '"><h2>' . $link['display'] . '</h2></a>' . PHP_EOL;
                            $links .= '</li>' . PHP_EOL;
                        }
                    }
                    $links .= '</li>' . PHP_EOL;
                }
                break;
            case 'dashbar':
                $links = '<ul class="nav dashbar">';
                break;
            default:
                //creates a default multi-level or single-tier linkset
                $links = '<ul class="nav">';
                foreach ($linkset['linksets'] as $indexes) {
                    $links .= '<li class="sublink"><a href="' . $indexes['link'] . '"><p>' . $indexes['title'] . '</p></a><ul class="sublinks">' . PHP_EOL;
                    if (!empty($indexes['links'])) {
                        foreach ($indexes['links'] as $link) {
                            $links .= '<li class="sublink">' . PHP_EOL;
                            $links .= '<a href="' . $link['link'] . '"><h2>' . $link['display'] . '</h2></a>' . PHP_EOL;
                            $links .= '</li>' . PHP_EOL;
                        }
                    }
                    $links .= '</ul>' . PHP_EOL . '</li>' . PHP_EOL;
                }
                break;
        }
        $links .= '</ul>' . PHP_EOL;
        //echo $links;
        return $links;
    }

    public function siteLinks($site, $linkset, $userperms = FALSE) {
        $toplinks = $this->topLinks($site, $linkset, $userperms);
        $links = $toplinks;
        for ($i = 0; $i < count($links); $i++) {
            if (isset($links[$i]['slug'])) {
                $links[$i]['sublinks'] = $this->subLinks($site, $linkset, $links[$i]['slug'], $userperms);
            }
        }
        return $links;
    }

    protected function topLinks($site, $linkset, $userperms = FALSE) {

        if ($userperms == FALSE) {
            $links = $this->db->select("SELECT `slug`,`linkset`,`link`,`display` FROM `sites_links` WHERE `site` = '$site' AND `linkset`='$linkset' AND `parent` IS NULL ORDER BY `hierarchy`;");
        } else {
            if (isset($_SESSION['user']['login'])) {
                $user = $_SESSION['user']['login'];
            } else {
                $user = 'visitor';
            }
            $perms = ("'" . implode("','", $_SESSION['user']['permissions']) . "'");
            $links = $this->db->select("SELECT `slug`,`linkset`,`link`,`display` FROM `sites_links` WHERE `permission` IN ($perms) AND `site` = '$site' AND `linkset`='$linkset' AND `active`=1 AND `parent` IS NULL ORDER BY `hierarchy`;");
            $x = 0;
            foreach ($links as $i) {
                if ($i['slug'] == 'login' && $_SESSION['user']['loggedIn'] == TRUE) {
                    unset($links[$x]);
                } elseif ($i['slug'] == 'logout' && !isset($_SESSION['user']['loggedIn'])) {
                    unset($links[$x]);
                }
                $x++;
            }
        }

        return $links;
    }

    protected function subLinks($site, $linkset, $parent, $userperms = FALSE) {

        if ($userperms == FALSE) {
            $links = $this->db->select("SELECT `slug`,`linkset`,`link`,`display` FROM `sites_links` WHERE `site` = '$site' AND `linkset`='$linkset' AND `parent`='$parent' ORDER BY `hierarchy`;");
        } else {
            if (isset($_SESSION['user']['login'])) {
                $user = $_SESSION['user']['login'];
            } else {
                $user = 'visitor';
            }
            $perms = ("'" . implode("','", $_SESSION['user']['permissions']) . "'");
            $links = $this->db->select("SELECT `slug`,`linkset`,`link`,`display` FROM `sites_links` WHERE `permission` IN ($perms) AND `site` = '$site' AND `linkset`='$linkset' AND `active`=1 AND `parent`='$parent' ORDER BY `hierarchy`;");
        }
        for ($i = 0; $i < count($links); $i++) {
            if (isset($links[$i]['slug'])) {
                $links[$i]['sublinks'] = $this->subLinks($site, $linkset, $links[$i]['slug'], $userperms);
            }
        }
        return $links;
    }

    public function buildMenu($user, $site, $linkset, $permission, $link = NULL, $id = NULL, $class = NULL, $heading = NULL, $headertype = 'h3', $sectionhead = NULL) {
        $render = array();
        if (isset($heading)) {
            //build the menu header array
            $header = array(
                'permission' => $permission,
                'class' => 'title',
                'content' => array(
                    'heading' => array(
                        'type' => $headertype,
                        'content' => $heading,
                    )
                )
            );
        }
        if (isset($id)) {
            $render['id'] = $id;
        }
        if (isset($class)) {
            $render['class'] = is_array($class) ? $class : array($class);
        }
        $render['content']['linkset'] = $this->buildTopLevelLinks($site, $user, $linkset);
        return $render;
    }

    protected function buildLinkIcon($image, $alt = NULL) {
        $render = array(
            'image' => _IMAGES . 'icons/' . $image,
        );
        if (isset($alt)) {
            $render['alt'] = $alt;
        }
        return $render;
    }

    protected function buildLinkTitle($content, $type = NULL) {
        $render = array(
            'content' => $content,
        );
        if (isset($type)) {
            $render['type'] = $type;
        }
        return $render;
    }

    protected function buildTopLevelLinks($site, $user, $linkset) {
        $links = $this->fetchLinksets($site, $user, $linkset);
        if (empty($links)) {
            return FALSE;
        } else {
            $render = array();
            foreach ($links as $key => $value) {
                if ($this->perms->checkPermission($user, $value['permission']) == TRUE) {
                    $render[$key] = array(
                        'link' => isset($value['link']) ? $value['link'] : '#',
                        'content' => array(),
                    );
                    if (isset($value['link'])) {
                        $render[$key]['link'] = $value['icon'];
                    }
                    if (isset($value['icon'])) {
                        $render[$key]['content']['icon'] = $this->buildLinkIcon($value['icon']);
                    }
                    if (isset($value['title'])) {
                        $render[$key]['content']['title'] = $this->buildLinkTitle($value['title']);
                    }
                    $render[$key]['content']['linkset'] = $this->buildSubLinks($site, $value['slug']);
                }
            }
        }

        return $render;
    }

    protected function buildSubLinks($site, $linkset, $parent = NULL) {
        $parent = isset($parent) ? "AND `parent`='$parent' " : "AND `parent` IS NULL ";
        $query = "SELECT `slug`,`parent`,`linkset`,`site`,`link`,`link_type`,`script_namespace`,`icon`,`display`,`permission` FROM `sites_links` WHERE `active`='1' AND `linkset`='$linkset' AND `site` IN ('$site','global') $parent ORDER BY `hierarchy` ASC;";
        $result = $this->db->select($query);
        $render = array();
        if (!empty($result)) {
            foreach ($result as $key => $value) {
                $item = array(
                    'permission' => $value['permission'],
                    'link_type' => $value['link_type'],
                    'content' => array(),
                );
                if (isset($value['link'])) {
//                    if ($value['link'] == '/content') {
//                        var_dump($value);
//                    }
                    $item['link'] = $value['link'];
                }
                if ($value['link_type'] == 'script' && isset($value['script_namespace'])) {
                    $item['namespace'] = $value['script_namespace'];
                }
                if (isset($value['icon'])) {
                    $item['content']['icon'] = $this->buildLinkIcon($value['icon']);
                }
                if (isset($value['display'])) {
                    $item['content']['title'] = $this->buildLinkTitle($value['display']);
                }
                $children = $this->buildSubLinks($site, $linkset, $value['slug'], 1);
                if ($children != FALSE) {
                    $item['content']['linkset'] = $children;
                }
                $render[] = $item;
            }
            //var_dump($render);
            return $render;
        } else
            return FALSE;
    }

    protected function fetchLinksets($site, $user, $linkset=NULL) {
        $query = isset($linkset) ? "SELECT `slug`,`parent`,`hook`,`icon`,`link`,`title`,`site`,`permission` FROM `sites_links_sets` WHERE `site` IN('" . $site . "','global') AND `parent`='$linkset' AND `active`='1' ORDER BY `hierarchy` ASC;" : "SELECT `slug`,`parent`,`hook`,`icon`,`link`,`title`,`site`,`permission` FROM `sites_links_sets` WHERE `site` IN('" . $site . "','global') AND `active`='1' ORDER BY `hierarchy` ASC;";
        $result = $this->db->select($query);
        if (!empty($result)) {
            foreach ($result as $key => $item) {
                if ($this->perms->checkPermission($user, $item['permission']) == FALSE) {
                    unset($result[$key]);
                }
            }
            return !empty($result) ? $result : FALSE;
        } else
            return FALSE;
    }

    protected function fetchLinks($site, $linkset, $user, $parent = NULL) {
        $query = isset($parent) ? "SELECT `slug`,`parent`,`linkset`,`link`,`link_type`,`icon`,`display`,`permission` FROM `sites_links` WHERE `active`='1' AND `site` IN ('$site','global') AND `parent` IS NULL ORDER BY `hierarchy` ASC;" : "SELECT `slug`,`parent`,`link`,`link_type`,`icon`,`display`,`permission` FROM `sites_links` WHERE `active`='1' AND `site` IN ('$site','global') AND `parent`='$parent' ORDER BY `hierarchy` ASC;";
        $result = $this->db->select($query);
        if (!empty($result)) {
            foreach ($result as $key => $value) {
                if ($this->perms->checkPermission($user, $value['permission']) == FALSE) {
                    unset($result[$key]);
                }
            }
            return !empty($result) ? $result : FALSE;
        } else
            return FALSE;
    }

    function __destruct() {
        parent::__destruct();
    }

}

?>
