-- MySQL dump 10.13  Distrib 5.5.34, for osx10.6 (i386)
--
-- Host: localhost    Database: mvc
-- ------------------------------------------------------
-- Server version	5.5.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `access_level`
--

DROP TABLE IF EXISTS `access_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_level` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `display` varchar(64) NOT NULL DEFAULT 'Untitled',
  `description` varchar(256) NOT NULL DEFAULT 'No description provided',
  `permission` varchar(16) NOT NULL DEFAULT 'view',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `permission` (`permission`),
  KEY `id` (`id`,`slug`,`display`,`description`(255),`permission`,`active`),
  CONSTRAINT `access_level_ibfk_1` FOREIGN KEY (`permission`) REFERENCES `permissions` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_level`
--

LOCK TABLES `access_level` WRITE;
/*!40000 ALTER TABLE `access_level` DISABLE KEYS */;
INSERT INTO `access_level` VALUES (0,'SYSTEM','SYSTEM','DO NOT DELETE. This is the system access level. It allows the system to perform neccessary automated tasks unimpeded.','SYSTEM',1,0),(1,'open','Open Access','Access is not restricted and may be viewed by anyone','view',1,1),(2,'registered','Registered Users','Access is restricted to users with accounts registered with the site who are currently logged in','registered',1,1),(3,'internal','Internal Access','Access is restricted to a specific group of users defined in the groups table, and only on the parent node','group',1,1),(4,'development','Developer Access','Access is restricted to the development team, and only on the parent node','developer',1,1),(5,'node','Node Access','Access is restricted to registered nodes, and whatever group they each assign viewing rights to','node',1,1),(6,'node_group','Node Internal Access','Access is restricted to registered nodes, and group viewing rights are designated by the parent node, then pushed to remote nodes','node_internal',1,1),(7,'node_development','Node Developer Access','Access is restricted to registered nodes, and group viewing rights are granted only to developers registered with the parent node','node_developer',1,1),(8,'node_private','Node Private Access','Access is restricted to registered nodes, and remote nodes must be logged in under the usage account','view',1,1),(9,'private','Private Access','Only the specified account has access rights, and only on the parent node','self',1,1);
/*!40000 ALTER TABLE `access_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assets_items`
--

DROP TABLE IF EXISTS `assets_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assets_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `slug` varchar(16) NOT NULL DEFAULT '' COMMENT 'the system name for the asset',
  `version` varchar(32) DEFAULT NULL,
  `title` varchar(64) NOT NULL DEFAULT 'Untitled Asset' COMMENT 'the human readable name for the asset',
  `source` varchar(256) DEFAULT '' COMMENT 'the remote location to obtain the asset',
  `source_method` varchar(16) NOT NULL DEFAULT '' COMMENT 'the connection method required for obtaining the asset from it''s remote source (i.e. ssh, ftp, etc.)',
  `description` text COMMENT 'A brief description of the functionality of the asset',
  `local_source` varchar(256) DEFAULT NULL COMMENT 'The local resource location for the asset. If this is blank, the system will automatically attempt to obtain the asset from its fallback source, and then from its remote source when required for use.',
  `fallback` varchar(256) DEFAULT NULL COMMENT 'The fallback resource for an asset, if it''s primary source is not available for some reason. If both this field and local_source are null, the system will attempt to obtain the resource from it''s remote source, and throw an error if that is not possible.',
  `obtained` datetime NOT NULL COMMENT 'The date the asset was originally obtained',
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP COMMENT 'The date the asset was last modified by the system.',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Determines whether or not the asset has been activated within the system. If off, no other component can use the asset anywhere throughout the system.',
  `require` tinyint(1) NOT NULL DEFAULT '0',
  `hierarchy` int(4) unsigned zerofill NOT NULL DEFAULT '9999',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_2` (`slug`,`version`,`source_method`),
  KEY `slug` (`slug`),
  KEY `version` (`version`),
  KEY `title` (`title`),
  KEY `source` (`source`(255)),
  KEY `source_method` (`source_method`),
  KEY `local_source` (`local_source`(255)),
  KEY `fallback` (`fallback`(255)),
  CONSTRAINT `assets_items_ibfk_1` FOREIGN KEY (`source_method`) REFERENCES `assets_types_categories` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='Catalog of individual assets (i.e. jquery version x.x.x, etc.)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assets_items`
--

LOCK TABLES `assets_items` WRITE;
/*!40000 ALTER TABLE `assets_items` DISABLE KEYS */;
INSERT INTO `assets_items` VALUES (1,'jquery','1.11.1','jQuery v. 1.11.1','http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js','javascript','The jQuery javascript library.','oroboros/dependencies/javascript/jquery-1.11.1.js','http://code.jquery.com/jquery-1.11.1.min.js','2014-06-15 01:03:28','2014-08-25 21:18:58',1,1,0001),(2,'jquery','2.1.1','jQuery v. 2.1.1','http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js','javascript','The jQuery javascript library.','oroboros/dependencies/javascript/jquery-2.1.1.js','http://code.jquery.com/jquery-2.1.1.min.js','2014-06-15 01:06:35','2014-08-25 21:19:18',1,0,0001),(3,'jquery_ui','1.10.4','jQuery UI v. 1.10.4','http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js','javascript','The jQuery UI core library.','oroboros/dependencies/javascript/jquery_ui-1.10.4.js','http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js','2014-06-15 01:33:05','2014-08-25 21:27:42',1,0,0002),(4,'jquery_ui','lightness','jQuery UI Lightness Stylesheet','http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/lightness/jquery-ui.css','css','The Lightness theme stylesheet for jQuery UI','oroboros/dependencies/css/jquery_ui-lightness.css','http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/lightness/jquery-ui.css','2014-06-15 01:45:48','2014-08-25 21:27:34',1,0,0002),(5,'jquery_ui','lightness','jQuery UI Lightness Image Library','http://jquery-ui.googlecode.com/svn/tags/1.7.2/themes/ui-lightness/images/','img_dir','The image assets for jQuery UI lightness theme.','public/images/lib/ui_themes/ui_lightness/','http://jquery-ui.googlecode.com/svn/tags/1.7.2/themes/ui-lightness/images/','2014-06-15 01:46:30','2014-08-25 21:26:33',1,0,0002),(6,'dojo','1.9.3','Dojo v. 1.9.3','http://ajax.googleapis.com/ajax/libs/dojo/1.9.3/dojo/dojo.js','javascript','The Dojo javascript library, for creating rich functionality.','oroboros/dependencies/javascript/dojo-1.9.3.js','http://ajax.googleapis.com/ajax/libs/dojo/1.9.3/dojo/dojo.js','2014-06-15 02:57:12','2014-08-25 21:27:51',1,0,0002),(7,'jquery_mobile','1.4.2','jQuery Mobile v. 1.4.2','http://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.2/jquery.mobile.min.js','javascript','The jQuery mobile javascript library.','oroboros/dependencies/javascript/jquery.mobile-1.4.2.js','http://code.jquery.com/mobile/1.4.2/jquery.mobile-1.4.2.min.js','2014-06-15 03:30:01','2014-08-25 21:18:21',1,0,0002),(8,'jquery_mobile','1.4.2','jQuery Mobile v. 1.4.2 Default Theme','http://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.2/jquery.mobile.min.css','css','The jQuery mobile default theme css stylesheet.','oroboros/dependencies/css/jquery_mobile-1.4.2.css','http://code.jquery.com/mobile/1.4.2/jquery.mobile-1.4.2.min.css','2014-06-15 03:30:03','2014-08-25 21:20:48',1,0,0002),(10,'angular_js','1.2.17','Angular JS v. 1.2.17','http://ajax.googleapis.com/ajax/libs/angularjs/1.2.17/angular.min.js','javascript','Angular JS javascript library.','oroboros/dependencies/javascript/angular-js-1.2.17.js','http://ajax.googleapis.com/ajax/libs/angularjs/1.2.17/angular.min.js','2014-06-15 03:30:05','2014-08-25 21:28:00',1,0,0002),(11,'mootools','1.5.0','Mootools v. 1.5.0','http://ajax.googleapis.com/ajax/libs/mootools/1.5.0/mootools-yui-compressed.js','javascript','Mootools JS library.','oroboros/dependencies/javascript/mootools-1.5.0.js','http://ajax.googleapis.com/ajax/libs/mootools/1.5.0/mootools-yui-compressed.js','2014-06-15 03:30:32','2014-08-25 21:23:48',1,0,0002),(12,'normalize',NULL,'Normalize CSS Library','http://normalize-css.googlecode.com/svn/trunk/normalize.min.css','css','Normalizes css display across platforms.','public/css/stylesheets/normalize.css','http://normalize-css.googlecode.com/svn/trunk/normalize.min.css','0000-00-00 00:00:00','2014-08-25 21:30:24',1,1,0000),(14,'foundation',NULL,'Foundation CSS Library',NULL,'css','Sets the base level css display render for the page.','public/css/stylesheets/foundation.css',NULL,'0000-00-00 00:00:00','2014-08-25 21:24:29',1,1,0001),(15,'responsive',NULL,'Responsive CSS Library',NULL,'css','Base css definitions for responsive page functionality.','public/css/stylesheets/responsive.css',NULL,'0000-00-00 00:00:00','2014-08-25 21:24:32',1,1,0002),(16,'mousetrap','1.6.4','Mousetrap keyboard shortcuts','http://cdn.craig.is/js/mousetrap/mousetrap.min.js?9d308','javascript','Javascript library for mapping keybinds intuitively.','oroboros/dependencies/javascript/mousetrap-1.6.4.js','http://cdn.craig.is/js/mousetrap/mousetrap.min.js?9d308','0000-00-00 00:00:00','2014-08-25 21:14:44',1,1,9999);
/*!40000 ALTER TABLE `assets_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assets_types`
--

DROP TABLE IF EXISTS `assets_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assets_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `title` varchar(128) NOT NULL DEFAULT '',
  `description` text,
  `hierarchy` int(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `title` (`title`),
  KEY `hierarchy` (`hierarchy`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Categorizes assets to determine which part of the system uses them (i.e. MODEL, VIEW, CONTROLLER, CORE, etc.)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assets_types`
--

LOCK TABLES `assets_types` WRITE;
/*!40000 ALTER TABLE `assets_types` DISABLE KEYS */;
INSERT INTO `assets_types` VALUES (1,'CORE','Core Asset','This asset is required by the core system.',0),(2,'SYSTEM','System Asset','This asset adds functionality to the core, but is not required for basic operation.',0),(3,'MODEL','Model Asset','This asset extends the core model to add additional functionality.',1),(4,'CONTROLLER','Controller Asset','This asset extends one or more controllers, adding additional functionality.',2),(5,'VIEW','View Asset','This asset extends one or more views, adding additional functionality.',3),(6,'ADAPTER','CGI Asset','This is an asset loaded via cgi bin. This category allows the system to interface with other programming languages.',1);
/*!40000 ALTER TABLE `assets_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `assets_types_categories`
--

DROP TABLE IF EXISTS `assets_types_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `assets_types_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `title` varchar(64) NOT NULL DEFAULT 'Untitled Asset',
  `description` text,
  `type` varchar(16) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `title` (`title`),
  KEY `type` (`type`),
  CONSTRAINT `assets_types_categories_ibfk_1` FOREIGN KEY (`type`) REFERENCES `assets_types` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Subcategorizes assets by type (i.e. javascript, flash, etc. of asset_type VIEW)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `assets_types_categories`
--

LOCK TABLES `assets_types_categories` WRITE;
/*!40000 ALTER TABLE `assets_types_categories` DISABLE KEYS */;
INSERT INTO `assets_types_categories` VALUES (1,'javascript','Javascript Library','This is a library to extend the functionality of javascript.','VIEW',1),(2,'css','CSS Stylesheet','This is a css stylesheet that extends the page layout.','VIEW',1),(3,'img_dir','Image Directory','This is a directory of images to support another asset, extension or module.','VIEW',1);
/*!40000 ALTER TABLE `assets_types_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `codebase`
--

DROP TABLE IF EXISTS `codebase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codebase` (
  `id` int(11) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `class` varchar(128) DEFAULT NULL,
  `namespace` varchar(512) DEFAULT NULL,
  `file` varchar(64) NOT NULL DEFAULT '',
  `marker` varchar(32) NOT NULL DEFAULT '_BASE',
  `path` varchar(512) DEFAULT NULL,
  `type` varchar(16) NOT NULL DEFAULT '',
  `subtype` varchar(16) NOT NULL DEFAULT 'CORE',
  `parent` int(11) unsigned zerofill DEFAULT NULL,
  `require` tinyint(1) NOT NULL DEFAULT '0',
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `load_order` int(11) unsigned zerofill NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `class_2` (`class`,`file`,`marker`,`type`,`subtype`),
  UNIQUE KEY `class_3` (`class`,`type`,`subtype`,`default`),
  KEY `file` (`file`),
  KEY `type` (`type`),
  KEY `subtype` (`subtype`),
  KEY `path` (`path`(255)),
  KEY `parent` (`parent`),
  KEY `class` (`class`),
  KEY `marker` (`marker`),
  CONSTRAINT `codebase_ibfk_2` FOREIGN KEY (`subtype`) REFERENCES `codebase_subtypes` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `codebase_ibfk_3` FOREIGN KEY (`parent`) REFERENCES `codebase` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=230 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `codebase`
--

LOCK TABLES `codebase` WRITE;
/*!40000 ALTER TABLE `codebase` DISABLE KEYS */;
INSERT INTO `codebase` VALUES (00000000002,NULL,NULL,'index.php','_BASE','/','LANDING','CORE',NULL,1,0,00000000000,'This is the primary index file. It directs all other independent files how toward their specific configurations and bootstrap files.'),(00000000004,NULL,NULL,'config.php','_BASE','/','CONFIG','CORE',00000000002,1,0,00000000001,'This is the primary configuration file. It is responsible for defining all of the parameters required for the bootstrap process to initialize.'),(00000000005,NULL,NULL,'Functions.php','_BASE','/','CONFIG','CORE',00000000004,1,0,00000000000,'This file contains all core procedural functions that are called globally throughout the system.'),(00000000006,NULL,NULL,'.htaccess','_BASE','/','SERVER','CORE',NULL,0,0,00000000000,'This is the global server configuration file for Apache. If you are using an apache server, this file will direct your webserver how to initiate the system.'),(00000000007,NULL,NULL,'web.config','_BASE','/','SERVER','CORE',NULL,0,0,00000000000,'This is the global server configuration file for IIS. If you are using a Microsoft Internet Information Services server, this file will direct your webserver how to initiate the system.'),(00000000008,NULL,NULL,'.gitignore','_BASE','/','SERVER','CORE',NULL,0,0,00000000000,'This file is used to direct git repositories which files to exclude during commits. You do not need to modify this file unless you are doing development or creating a git repository for your specific installation.'),(00000000009,NULL,NULL,'.tarignore','_BASE','/','SERVER','CORE',NULL,0,0,00000000000,'This file indicates which files to exclude when creating a tar archive of the system. You do not need to modify this file unless you are creating a backup tar archive of your installation or moving the site to another location.'),(00000000010,NULL,NULL,'robots.php','_BASE','/','LANDING','CORE',NULL,0,0,00000000000,'This file provides directory access instructions to web crawlers. It is important for SEO purposes when Google, Bing, Yahoo, or other search engines crawl your directory structure.'),(00000000011,NULL,NULL,'sitemap.php','_BASE','/','LANDING','CORE',NULL,0,0,00000000000,'This file provides a sitemap for search engines. It will be automatically generated based on whatever pages you have marked as public within your network. This is important for SEO purposes when search engines crawl your site to insure that they are provided accurate page layouts and do not encounter broken links, which will reduce your search engine ranking. This file also excludes non-public pages, so they will not be crawled by standards compliant crawlers.'),(00000000012,NULL,NULL,'rss.php','_BASE','/','LANDING','CORE',NULL,0,0,00000000000,'This file handles rss and atom syndication of feeds in your network. Users subscribing to rss feeds (if you have them enabled) will access this file to generate the feed, and insure that only the correct information is passed to them.'),(00000000014,NULL,NULL,'core.ini','_DATA','/oroboros/data/','SETTINGS','CORE',00000000019,1,0,00000000000,'This file contains the database connection information for the core system. It exists separate of other database configuration data for any other databases you have registered for security purposes.'),(00000000015,NULL,NULL,'db.ini','_DATA','/oroboros/data/','SETTINGS','CORE',00000000019,1,0,00000000001,'This file contains database connection information for any other databases you have added to the system other than the core database. Your database connector will reference this file when attempting to connect to any other local or remote databases.'),(00000000016,NULL,NULL,'debug.ini','_DATA','/oroboros/data/','SETTINGS','CORE',00000000019,1,0,00000000002,'This file contains debug settings for troubleshooting the system. It will be fallen back upon if the core database becomes unavailable, so you will still be able to troubleshoot the system and will not become locked out completely. To prevent lockouts, debug settings are not stored in the database. If you edit them through the core system, this file will be updated directly.'),(00000000017,NULL,NULL,'security.ini','_DATA','/oroboros/data/','SETTINGS','CORE',00000000019,1,0,00000000003,'This file contains encryption data, such as salts, encryption keys, and other required information for various encryption means to take place. You should not ever edit this file directly unless explicitly instructed to do so by a valid authority, or you may permanently render some sort of data or functionality inaccessible. Please consult the security documentation for more information.'),(00000000018,NULL,NULL,'system.ini','_DATA','/oroboros/data/','SETTINGS','CORE',00000000019,1,0,00000000004,'This file contains settings for the core system. It will direct the bootstrap process which functionality to enable, and how to go about it. This is file is provided so that core settings can still be modified if you become locked out of the database for some reason, and may still troubleshoot the system or change it\'s performance without it becoming completely inoperable.'),(00000000019,'_Bootstrap',NULL,'Bootstrap.php','_LIBS','/oroboros/core/libs/','BOOTSTRAP','CORE',00000000002,1,0,00000000002,'This file bootstraps the core system. It should not be extended unless absolutely neccessary. All other subsystems will reference this file to determine how to initialize.'),(00000000020,'_Model',NULL,'Model.php','_LIBS','/oroboros/core/libs/','LIB','CORE',NULL,1,0,00000000008,'This file provides the core model template. It acts as the primary basis for all other models to extend.'),(00000000021,'_Controller',NULL,'Controller.php','_LIBS','/oroboros/core/libs/','LIB','CORE',NULL,1,0,00000000009,'This file provides the core controller template. It provides the base functionality for all other controllers to extend upon.'),(00000000022,'_Adapter',NULL,'Adapter.php','_LIBS','/oroboros/core/libs/','LIB','CORE',NULL,1,0,00000000010,'This file provides the core adapter template. It provides the base functionality for all other adapters that interface with other subsystems or programming languages to extend upon.'),(00000000023,'_View',NULL,'View.php','_LIBS','/oroboros/core/libs/','LIB','CORE',NULL,1,0,00000000011,'This file provides the core view template. It provides the base functionality for all other view files to extend upon so they may render output for the end user correctly.'),(00000000024,'_Firstrun',NULL,'Firstrun.php','_LIBS','/oroboros/core/libs/Firstrun/','LIB','CORE',NULL,0,0,00000000100,'This file will build the initial system setup if you have not run the system before, or alternately rebuild the system if the core \"rebaseInstallation\" setting is set to TRUE.'),(00000000028,NULL,NULL,'init.sql','_LIBS','/oroboros/core/libs/Firstrun/','SQL','CORE',00000000024,0,0,00000000000,'This is the core database architecture file. If you do not have an existing database, or wish to rebuild the database from scratch, this file will provide the core template to do so. It is generally only called when the system is initially configured, though it may also be called if the core \"rebaseInstallation\" setting is set to TRUE, or if the core \"rebuildDatabase\" setting is set to TRUE.'),(00000000030,'_Dataflow',NULL,'Dataflow.php','_LIBS','/oroboros/core/libs/Dataflow/','LIB','CORE',NULL,1,0,00000000001,'This file handles data parsing and conversion from one form to another (ie: parsing ini files, converting xml, json, php arrays, etc. from one form to another).'),(00000000031,'_Database',NULL,'Database.php','_LIBS','/oroboros/core/libs/Database/','LIB','CORE',NULL,1,0,00000000002,'This file provides the database abstraction layer. It handles SQL queries and provides the basis for communicating with the database to various other classes.'),(00000000032,'_Exceptions',NULL,'Exceptions.php','_LIBS','/oroboros/core/libs/Exceptions/','LIB','CORE',NULL,1,0,00000000005,'This file provides the basis for handling exceptions throughout the core system.'),(00000000034,'_Session',NULL,'Session.php','_LIBS','/oroboros/core/libs/Session/','LIB','CORE',NULL,1,0,00000000006,'This file provides the basis for user session authentication. It handles login attempts, acquires and sets user permissions from the permissions model, and determines what access level the user is allowed upon login or initial site visit.'),(00000000035,'_Ajax',NULL,'Ajax.php','_LIBS','/oroboros/core/libs/Ajax/','BOOTSTRAP','CORE',NULL,1,0,00000000000,'This file provides the bootstrap initialization process for the ajax handler.'),(00000000036,'_Css',NULL,'Css.php','_LIBS','/oroboros/core/libs/CSS/','BOOTSTRAP','CORE',NULL,1,0,00000000000,'This file provides the bootstrap initialization settings for the css landing file, and insures the correct stylesheet information is passed to the end user upon page load.'),(00000000037,'_Js',NULL,'Js.php','_LIBS','/oroboros/core/libs/Javascript/','BOOTSTRAP','CORE',NULL,1,0,00000000000,'This file provides the bootstrap initialization for the javascript landing file. It insures that the correct browser-side scripting is passed to the end user.'),(00000000038,'_Rss',NULL,'Rss.php','_LIBS','/oroboros/core/libs/RSS/','BOOTSTRAP','CORE',NULL,1,0,00000000000,'This file provides the bootstrap initialization for the rss landing file. It insures that users recieve the correct rss or atom feeds, and that they have correct permissions to view them.'),(00000000039,'_Sitemap',NULL,'Sitemap.php','_LIBS','/oroboros/core/libs/Sitemap/','BOOTSTRAP','CORE',NULL,1,0,00000000000,'This file provides the bootstrap initialization for the sitemap landing file. It insures that crawlers recieve a well-formed sitemap.xml document, and that only public facing pages are passed to the crawler.'),(00000000040,'_Robots',NULL,'Robots.php','_LIBS','/oroboros/core/libs/Robots/','BOOTSTRAP','CORE',NULL,1,0,00000000000,'This file provides the bootstrap initialization for the robots landing file. It insures that crawlers recieve a well-formed dynamic robots.txt document, so they can determine which files and directories to crawl correctly.'),(00000000041,'_Hash',NULL,'Hash.php','_LIBS','/oroboros/core/libs/Encryption/','LIB','CORE',NULL,1,0,00000000007,'This file handles the creation of encryption hashes through various means. It chooses the correct hashing method, applies a salt if required, checks hashes, and directs the storage of hashes and keys.'),(00000000042,'_AES',NULL,'AES.php','_LIBS','/oroboros/core/libs/Encryption/','LIB','CORE',NULL,0,0,00000000100,'This file handles the creation and decryption of AES cryptographic keys and hashes.'),(00000000043,'Pagerender',NULL,'Pagerender.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,1,1,00000000000,'This file handles the controller logic for all pages that do not have their own explicit controller. If a page only needs to display data and does not accept any user input, this controller will be used.'),(00000000045,'About',NULL,'about.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This file provides the controller for the about page.'),(00000000046,'Dashboard',NULL,'Dashboard.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This file provides the controller for the dashboard.'),(00000000047,'Docs',NULL,'Docs.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This file provides the controller for the docs page.'),(00000000048,'Error',NULL,'Error.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,1,0,00000000000,'This file provides the controller for all page errors. Any page that does not exist, has been moved, or the user does not have authorization to access will call upon this controller.'),(00000000049,'Help',NULL,'Help.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This file provides the controller for the help system. It is used in conjuction with the documentation controller, and also exists independently for use with the native help system.'),(00000000050,'Login',NULL,'Login.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This file provides the controller for the login process. It is used for all login pages within the system.'),(00000000051,'Note',NULL,'Note.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This file provides the controller for the note system. It is used to handle all journal entries, todo lists, and other means of recording private data or user-shared documentation (other than system documentation).'),(00000000057,'Permissions',NULL,'Permissions.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This file provides the controller for the permission system. It communicates with the permissions model to determine access rights for users, and also generates the correct output for permission management on the backend.'),(00000000069,'Note',NULL,'Note.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000051,0,0,00000000000,'This file determines the correct structure, storage, and access rights for user generated notes, journals, todo lists, and notifications, and insures that they are correctly archived and accessible to the right parties.'),(00000000075,'Dashboard',NULL,'Dashboard.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000046,0,0,00000000000,'This file provides the model for dashboard functionality for logged in users. Backend pages that do not have their own specific model use this model instead.'),(00000000076,'Login',NULL,'Login.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000050,0,0,00000000000,'This file provides the model for login attempts. It insures that the user exists within the system, and allocates their permissions, settings, and other such considerations upon login.'),(00000000077,'Server',NULL,'Server.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000086,0,0,00000000000,'This file provides the model for detecting the capabilities of the server, and returns them to the server managment pages that require them via the server info controller.'),(00000000079,NULL,NULL,'body.phtml','_VIEWS','/oroboros/core/views/','VIEW','CORE',NULL,0,0,00000000000,'This is the default body content template, if no other template is defined for the page.'),(00000000080,NULL,NULL,'content.phtml','_VIEWS','/oroboros/core/views/','VIEW','CORE',NULL,0,0,00000000000,'This is the default content template used by the system if no other template is defined for the page being viewed.'),(00000000081,NULL,NULL,'footer.phtml','_VIEWS','/oroboros/core/views/','VIEW','CORE',NULL,0,0,00000000000,'This is the default footer template used by the system if no other footer is defined for the page being viewed.'),(00000000082,NULL,NULL,'head.phtml','_VIEWS','/oroboros/core/views/','VIEW','CORE',NULL,0,0,00000000000,'This is the default document head template used by the system if no other document head is defined for the page being viewed.'),(00000000083,NULL,NULL,'header.phtml','_VIEWS','/oroboros/core/views/','VIEW','CORE',NULL,0,0,00000000000,'This is the default header template provided by the system if no other header is defined for the page being viewed.'),(00000000084,'Docs',NULL,'Docs.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000047,0,0,00000000000,'This file provides the model for the site documentation.'),(00000000085,'Pagerender',NULL,'Pagerender.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000043,1,1,00000000000,'This is the default model for page rendering.'),(00000000086,'Server',NULL,'Server.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This file is the controller for server management pages.'),(00000000087,NULL,NULL,'wrapper.php','_BASE','/','LANDING','CORE',NULL,0,0,00000000000,'This file loads the system as a wrapper for a subsystem.'),(00000000088,NULL,NULL,'framework.php','_BASE','/','LANDING','CORE',NULL,0,0,00000000000,'This file loads the system as a development framework.'),(00000000089,'Accounts',NULL,'Accounts.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the account management page.'),(00000000090,'Accounts',NULL,'Accounts.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000089,0,0,00000000000,'This is the model for the account management page.'),(00000000091,'Groups',NULL,'Groups.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the group management page.'),(00000000092,'Groups',NULL,'Groups.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000091,0,0,00000000000,'This is the model for the groups management page.'),(00000000093,'User',NULL,'User.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the user management page.'),(00000000094,'User',NULL,'User.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000093,0,0,00000000000,'This is the model for the user management page.'),(00000000095,'Templates',NULL,'Templates.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the templates page.'),(00000000096,'Templates',NULL,'Templates.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000095,0,0,00000000000,'This is the model for the templates page.'),(00000000097,'Themes',NULL,'Themes.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the themes page.'),(00000000098,'Themes',NULL,'Themes.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000097,0,0,00000000000,'This is the model for the themes page.'),(00000000099,'Media',NULL,'Media.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the media management page.'),(00000000100,'Media',NULL,'Media.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000099,0,0,00000000000,'This is the model for the media management page.'),(00000000101,'Typography',NULL,'Typography.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the typography page.'),(00000000102,'Typography',NULL,'Typography.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000101,0,0,00000000000,'This is the model for the typography page.'),(00000000103,'Colors',NULL,'Colors.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the color schemes page.'),(00000000104,'Colors',NULL,'Colors.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000103,0,0,00000000000,'This is the model for the color schemes page.'),(00000000105,'Maintenance',NULL,'Maintenance.php','_CONTROLLERS','/oroboros/core/models/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the site maintenance page.'),(00000000106,'Maintenance',NULL,'Maintenance.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000105,0,0,00000000000,'This is the model for the site maintenance page.'),(00000000107,'Debug',NULL,'Debug.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the debug page.'),(00000000108,'Debug',NULL,'Debug.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000107,0,0,00000000000,'This is the model for the debug page.'),(00000000109,'Module',NULL,'Module.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the module settings page.'),(00000000110,'Module',NULL,'Module.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000109,0,0,00000000000,'This is the model for the module settings page.'),(00000000111,'Component',NULL,'Component.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the component settings page.'),(00000000112,'Component',NULL,'Component.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000111,0,0,00000000000,'This is the model for the component settings page.'),(00000000113,'Staging',NULL,'Staging.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the staging system page.'),(00000000114,'Staging',NULL,'Staging.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000113,0,0,00000000000,'This is the model for the staging system page.'),(00000000115,'Webshell',NULL,'Webshell.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the webshell terminal page.'),(00000000116,'Webshell',NULL,'Webshell.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000115,0,0,00000000000,'This is the model for the webshell terminal page.'),(00000000119,'Logs',NULL,'Logs.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the system logs page.'),(00000000120,'Logs',NULL,'Logs.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000119,0,0,00000000000,'This is the model for the system log page.'),(00000000121,'DbSettings',NULL,'Dbsettings.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the database settings page'),(00000000122,'DbSettings',NULL,'Dbsettings.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000121,0,0,00000000000,'This is the model for the database settings page.'),(00000000123,'Source',NULL,'Source.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the source control page.'),(00000000124,'Source',NULL,'Source.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000123,0,0,00000000000,'This is the model for the source control page.'),(00000000125,'Cgi',NULL,'Cgi.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the cgi settings page.'),(00000000126,'Cgi',NULL,'Cgi.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000125,0,0,00000000000,'This is the model for the cgi settings page.'),(00000000127,'Codeedit',NULL,'Codeedit.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the code editor page.'),(00000000128,'Codeedit',NULL,'Codeedit.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000127,0,0,00000000000,'This is the model for the code editor page.'),(00000000129,'Ftp',NULL,'Ftp.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the ftp panel page.'),(00000000130,'Ftp',NULL,'Ftp.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000129,0,0,00000000000,'This is the model for the ftp panel page.'),(00000000131,'Ssh',NULL,'Ssh.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the ssh panel page.'),(00000000132,'Ssh',NULL,'Ssh.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000131,0,0,00000000000,'This is the model for the ssh panel page.'),(00000000133,'Seo',NULL,'Seo.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the seo configuration page.'),(00000000134,'Seo',NULL,'Seo.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000133,0,0,00000000000,'This is the model for the seo configuration page.'),(00000000135,'Nodes',NULL,'Nodes.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the nodes page.'),(00000000136,'Nodes',NULL,'Nodes.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000135,0,0,00000000000,'This is the model for the nodes page.'),(00000000137,'Analytics',NULL,'analytics.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the analytics page.'),(00000000138,'Analytics',NULL,'Analytics.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000137,0,0,00000000000,'This is the model for the analytics page.'),(00000000139,'Dns',NULL,'Dns.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the dns settings page.'),(00000000140,'Dns',NULL,'Dns.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000139,0,0,00000000000,'This is the model for the dns settings page.'),(00000000142,'Sites',NULL,'Sites.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the sites page.'),(00000000143,'Sites',NULL,'Sites.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000142,0,0,00000000000,'This is the model for the sites page.'),(00000000144,'Pages',NULL,'Pages.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the pages page.'),(00000000145,'Pages',NULL,'Pages.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000144,0,0,00000000000,'This is the model for the pages page.'),(00000000146,'Links',NULL,'Links.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the links page.'),(00000000147,'Links',NULL,'Links.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000146,0,0,00000000000,'This is the model for the links page.'),(00000000148,'Content',NULL,'Content.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the content page.'),(00000000149,'Content',NULL,'Content.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000148,0,0,00000000000,'This is the model for the content page.'),(00000000150,'Syndication',NULL,'Syndication.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the syndication page.'),(00000000151,'Syndication',NULL,'Syndication.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000150,0,0,00000000000,'This is the model for the syndication page.'),(00000000152,'Ssl',NULL,'Ssl.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the ssl certificate management page'),(00000000153,'Ssl',NULL,'Ssl.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000152,0,0,00000000000,'This is the model for the ssl certificate management page'),(00000000154,'Hooks',NULL,'Hooks.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the controller for the hooks page'),(00000000155,'Hooks',NULL,'Hooks.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000154,0,0,00000000000,'This is the model for the hooks page'),(00000000156,'_Ajax',NULL,'Ajax.php','_LIBS','/oroboros/core/libs/Ajax/','LIB','CORE',NULL,0,0,00000000100,'This is the Ajax library, used to direct ajax calls to the correct model and controller.'),(00000000157,'_Autoload',NULL,'Autoload.php','_LIBS','/oroboros/core/libs/Autoload/','LIB','CORE',NULL,1,0,00000000100,'This is the system autoloader class. It is used to load classes that are not registered with the system, and to handle autoloading when the system is loaded as a framework or wrapper so the database connection is not neccessary in those instances.'),(00000000158,'_Dependencies',NULL,'Dependencies.php','_LIBS','/oroboros/core/libs/Dependencies/','LIB','CORE',NULL,1,0,00000000100,'This is the dependency loader. It is used to determine which dependencies are required by various aspects of the system, and insure that they are loaded before they are called.'),(00000000159,'_Form',NULL,'Form.php','_LIBS','/oroboros/core/libs/Form/','LIB','CORE',NULL,1,0,00000000100,'This is the default form handler lib. It is used to handle form submission and validation throughout the system.'),(00000000160,'_Val',NULL,'Val.php','_LIBS','/oroboros/core/libs/Form/','LIB','CORE',00000000159,0,0,00000000100,'This is the form value library. It is used to return the correct message to the user when form submissions do not conform to the specified standards required.'),(00000000161,'_Links',NULL,'Links.php','_LIBS','/oroboros/core/libs/Links/','LIB','CORE',NULL,1,0,00000000015,'This file handles the creation of links and linksets. It is used throughout the system to insure that menus are correctly formed and rendered.'),(00000000162,'_Log',NULL,'Log.php','_LIBS','/oroboros/core/libs/Log/','LIB','CORE',NULL,1,0,00000000013,'This file is the log library. It handles the correct logging of errors into the logfile specified for the method being called.'),(00000000163,'_Modules',NULL,'Modules.php','_LIBS','/oroboros/core/libs/Modules/','LIB','CORE',NULL,1,0,00000000100,'This file is the Module library. It handles correctly loading modules required for a page view in the appropriate order, and according to the correct permissions.'),(00000000164,'_Email',NULL,'Email.php','_LIBS','/oroboros/core/libs/Email/','LIB','CORE',NULL,0,0,00000000100,'This file is the email library. It handles the correct formatting of emails, and insures that they are sent to the right parties using the correct mail template.'),(00000000165,'_Permissions',NULL,'Permissions.php','_LIBS','/oroboros/core/libs/Permissions/','LIB','CORE',NULL,1,0,00000000100,'This file is the permissions library. It handles the correct application of user permissions, and tells the system which content, pages, and sites to load, and which to restrict from use based on user access rights.'),(00000000166,'_Hooks',NULL,'Hooks.php','_LIBS','/oroboros/core/libs/Render/','LIB','CORE',NULL,1,0,00000000100,'This file is the hooks library. It handles content injection based on the hook system, to insure that all required page content displays in the correct place based on settings for the site, page, theme, template, modules, components, and access rights for the user.'),(00000000168,'_ComponentSettings',NULL,'ComponentSettings.php','_LIBS','/oroboros/core/libs/Settings/','LIB','CORE',NULL,1,0,00000000100,'This file is the component settings library. It handles system settings as they apply to components, and insures that included components conform to the correct system and component settings.'),(00000000169,'_CoreSettings',NULL,'CoreSettings.php','_LIBS','/oroboros/core/libs/Settings/','LIB','CORE',NULL,1,0,00000000100,'This file is the core settings library. It handles system settings as they pertain to the core system, and insures that all further loaded assets conform to these settings.'),(00000000170,'_CssSettings',NULL,'CssSettings.php','_LIBS','/oroboros/core/libs/Settings/','LIB','CORE',NULL,1,0,00000000100,'This file is the stylesheet settings library. It handles css settings, and insures that the stylesheet render engine follows the correct procedures.'),(00000000171,'_DebugSettings',NULL,'DebugSettings.php','_LIBS','/oroboros/core/libs/Settings/','LIB','CORE',NULL,1,0,00000000100,'This file is the debug settings library. It handles debugger settings throughout the system, and insures that debugging options and logging functions are correctly adhered to.'),(00000000172,'_JsSettings',NULL,'JsSettings.php','_LIBS','/oroboros/core/libs/Settings/','LIB','CORE',NULL,1,0,00000000100,'This file is the javascript settings library. It handles javascript settings for the script rendering engine, and insures that included script files conform to the correct specifications.'),(00000000173,'_ModuleSettings',NULL,'ModuleSettings.php','_LIBS','/oroboros/core/libs/Settings/','LIB','CORE',NULL,1,0,00000000100,'This file is the module settings library. It handles system settings as they apply to modules, and insures that included modules conform to the correct system and module settings.'),(00000000174,'_SecuritySettings',NULL,'SecuritySettings.php','_LIBS','/oroboros/core/libs/Settings/','LIB','CORE',NULL,1,0,00000000100,'This file is the security settings library. It handles security settings throughout the system, and insures that potential vulnerabilities and attack vectors are mitigated correctly.'),(00000000175,'_ThemeSettings',NULL,'ThemeSettings.php','_LIBS','/oroboros/core/libs/Settings/','LIB','CORE',NULL,1,0,00000000100,'This file is the theme settings library. It handles theme settings for the system, and insures that themes conform to the correct specifications for the site, page, and user.'),(00000000176,'_UserSettings',NULL,'UserSettings.php','_LIBS','/oroboros/core/libs/Settings/','LIB','CORE',NULL,1,0,00000000100,'This file is the user settings library. It handles user-defined settings throughout the system, and insures that any user-defined specifications are correctly adhered to, so long as they do not supercede the core system settings or violate access rights or privacy.'),(00000000177,'_RssSyndication',NULL,'Rss.php','_LIBS','/oroboros/core/libs/Syndication/RSS/','LIB','CORE',NULL,0,0,00000000100,'This file is the rss syndication library. It handles administrative definitions of rss feeds, and insures they are accessible to the end users who subscribe to those feeds.'),(00000000178,'_EmailSyndication',NULL,'Email.php','_LIBS','/oroboros/core/libs/Syndication/Email/','LIB','CORE',NULL,0,0,00000000100,'This file is the email syndication library. It allows administrative definition of mailing lists, and insures that cross-user emails are correctly handled.'),(00000000179,'_CalendarSyndication',NULL,'Calendar.php','_LIBS','/oroboros/core/libs/Syndication/Calendar/','LIB','CORE',NULL,0,0,00000000100,'This is the calendar syndication library. It insures that calendars defined within the system have the correct internal and external access rights, and that they are correctly available to parties allowed to view them.'),(00000000180,'_Template',NULL,'Template.php','_LIBS','/oroboros/core/libs/Template/','LIB','CORE',NULL,1,0,00000000100,'This is the template handler library. It insures that templates are correctly loaded, and that all assets and dependencies are accounted for according to settings and site or page specifications.'),(00000000181,'_Theme',NULL,'Theme.php','_LIBS','/oroboros/core/libs/Theme/','LIB','CORE',NULL,1,0,00000000100,'This is the theme library. It insures that themes are correctly loaded and applied based on settings or site, page, or user specification.'),(00000000182,'_ThemeImport',NULL,'Theme_Import.php','_LIBS','/oroboros/core/libs/Theme/','LIB','CORE',NULL,0,0,00000100100,'This is the theme importer library. It insures that imported themes are correctly validated and entered into the system.'),(00000000184,'_Browser',NULL,'Browser.php','_LIBS','/oroboros/core/libs/visitor/','LIB','CORE',NULL,1,0,00000000100,'This file is the browser definition library. It handles the detection of the end-user\'s browser, system, and capabilities, and insures that the page renders in the correct manner for their device and system.'),(00000000185,'_Archive',NULL,'Archive.php','_LIBS','/oroboros/core/libs/Archive/','LIB','CORE',NULL,1,0,00000000100,'This file is the archive library. It insures that files, media, sql, and other assets are correctly packaged and stored in the correct directory, and also handles the process of extraction from these archives, and subsequent replacement in the correct location.'),(00000000186,'_Import',NULL,'Import.php','_LIBS','/oroboros/core/libs/Import/','LIB','CORE',NULL,0,0,00000000100,'This file is the importer library. It handles the importation of files, sites, content, pages, themes, templates, components, modules, or any other resource, and subsequent validation and application of those resources.'),(00000000187,'_Export',NULL,'Export.php','_LIBS','/oroboros/core/libs/Export/','LIB','CORE',NULL,0,0,00000000100,'This is the exporter library. It handles the correct packaging and sending of sites, pages, content, SQL, modules, components, themes, templates, media, or other files or assets to the correct source.'),(00000000188,'_Connection',NULL,'Connection.php','_LIBS','/oroboros/core/libs/Connection/','LIB','CORE',NULL,1,0,00000000100,'This file is the connection library. It handles insuring that the correct means of connection within the system is applied, and that any security considerations and access rights for that connection type are adhered to.'),(00000000189,'_Source',NULL,'Source.php','_LIBS','/oroboros/core/libs/Source/','LIB','CORE',NULL,0,0,00000000100,'This file is the source control library. It insures that the source control is applied in the correct manner, and that version control systems such as git, svn, etc. are used in the correct manner.'),(00000000190,'_Design',NULL,'Design.php','_LIBS','/oroboros/core/libs/Design/','LIB','CORE',NULL,0,0,00000000100,'This is the design library. It insures that designers are provided the correct tools, and that their efforts are applied in the correct format to work in conjunction with the system.'),(00000000191,'_Develop',NULL,'Develop.php','_LIBS','/oroboros/core/libs/Develop/','LIB','CORE',NULL,0,0,00000000100,'This file is the developer library. It handles programmatic application to various components of the system, and insures that developers are provided with the correct tools to enhance or extend the system, and that their efforts are correctly applied.'),(00000000192,'_Author',NULL,'Author.php','_LIBS','/oroboros/core/libs/Author/','LIB','CORE',NULL,0,0,00000000100,'This is the content authoring library. It insures that content creation can be formatted in the correct manner and will be stored and rendered correctly, and that content authors are presented with the correct tools to create dynamic content and share it effectively.'),(00000000193,'_Filebase',NULL,'Filebase.php','_LIBS','/oroboros/core/libs/Filebase/','LIB','CORE',NULL,1,0,00000000003,'This file is the filebase library. It insures that files are registered within the system correctly, that they are stored in the correct location, and that changes to files are registered correctly.'),(00000000194,'_Fileperms',NULL,'Permissions.php','_LIBS','/oroboros/core/libs/Filebase/','LIB','CORE',NULL,1,0,00000000100,'This file is the file permission library. It insures that files have the correct ownership and file permissions applied to them, and that they are accessible to the system and the correct users based on access rights, and that others who should not have access are properly restricted.'),(00000000195,'_Schedule',NULL,'Schedule.php','_LIBS','/oroboros/core/libs/Schedule/','LIB','CORE',NULL,1,0,00000000100,'This file is the schedule library. It controls the system content schedule, and insures that tasks are completed when they are scheduled to do so.'),(00000000196,'_Clock',NULL,'Clock.php','_LIBS','/oroboros/core/libs/Schedule/','LIB','CORE',NULL,1,0,00000000100,'This file is the clock library. It insures that time displays correctly, and that time-based settings are correctly translated throughout the system.'),(00000000197,'_Media',NULL,'Media.php','_LIBS','/oroboros/core/libs/Media/','LIB','CORE',NULL,1,0,00000000014,'This file is the media library. It controls the access to various media types, acts as a media proxy if set up to prevent external linking, and distributes media required to the page based on the configuration of other aspects of the system.'),(00000000198,'_Device',NULL,'Device.php','_LIBS','/oroboros/core/libs/Visitor/','LIB','CORE',NULL,1,0,00000000100,'This file is the device library. It insures that the detected end user device is registered in the system, and instructs other aspects of the system to output content that will render correctly for their device.'),(00000000199,'Error',NULL,'Error.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000048,1,0,00000000000,'This is the error page model.'),(00000000200,'_User',NULL,'User.php','_LIBS','/oroboros/core/libs/User/','LIB','CORE',NULL,1,0,00000000012,'This file is the user library. It handles end-user defined settings.'),(00000000201,'_Logo',NULL,'Logo.php','_LIBS','/oroboros/core/libs/Content/','LIB','CORE',NULL,1,0,00000000100,'This file is the logo library. This file will fetch the correct logo for the site or asset based on parameters passed, user device, and site settings.'),(00000000202,'_Meta',NULL,'Meta.php','_LIBS','/oroboros/core/libs/Content/','LIB','CORE',NULL,1,0,00000000000,'This file is the meta library. It fetches the correct meta tags for the site and page.'),(00000000203,'_TemplateSettings',NULL,'TemplateSettings.php','_LIBS','/oroboros/core/libs/Settings/','LIB','CORE',NULL,1,0,00000000100,'This file is the template settings library. It handles the settings for the template, and insures that the template renders the correct assets in conjunction with the template library.'),(00000000204,'_Components',NULL,'Components.php','_LIBS','/oroboros/core/libs/Components/','LIB','CORE',NULL,1,0,00000000016,'This file is the components library. It handles loading components required for the page.'),(00000000205,'_Debug',NULL,'Debug.php','_LIBS','/oroboros/core/libs/Debug/','LIB','CORE',NULL,1,0,00000000013,'This is the debug library. It handles the application of various debugging operations within the system.'),(00000000206,'_Dashmod',NULL,'Dashmod.php','_LIBS','/oroboros/core/libs/Content/dashboard/','LIB','CORE',NULL,1,0,00000000100,'This file is the dashmod library. It controls the rendering of dashmods.'),(00000000207,'_Lib',NULL,'Lib.php','_LIBS','/oroboros/core/libs/','LIB','CORE',NULL,1,0,00000000100,'This file is the lib library. It is the file upon which all other libs extend.'),(00000000208,'_Dashwidget',NULL,'Dashwidget.php','_LIBS','/oroboros/core/libs/Content/dashboard/','LIB','CORE',NULL,1,0,00000000100,'This file is the dashboard widget library. It controls the creation of dashboard widgets.'),(00000000209,'_Dashform',NULL,'Dashform.php','_LIBS','/oroboros/core/libs/Content/dashboard/','LIB','CORE',NULL,1,0,00000000100,'This is the dashboard form library. It handles rendering forms for the backend.'),(00000000210,'_Dashtable',NULL,'Dashtable.php','_LIBS','/oroboros/core/libs/Content/dashboard/','LIB','CORE',NULL,1,0,00000000100,'This is the dashboard table library. It handles rendering of tables on the backend.'),(00000000211,'Browser',NULL,'Browser.php','_LIBS','/oroboros/core/libs/Utilities/','UTILITY','CORE',NULL,1,0,00000000000,'This is the third party browser and operating system detection utility used by the core system to direct the render engine to reference the correct setup for the end user request. License provided by the file:\n\n File: Browser.php\n Author: Chris Schuld (http://chrisschuld.com/)\n Last Modified: July 4th, 2014\n @version 1.9\n @package PegasusPHP\n \n Copyright (C) 2008-2010 Chris Schuld  (chris@chrisschuld.com)\n \n This program is free software; you can redistribute it and/or\n modify it under the terms of the GNU General Public License as\n published by the Free Software Foundation; either version 2 of\n the License, or (at your option) any later version.\n \n This program is distributed in the hope that it will be useful,\n but WITHOUT ANY WARRANTY; without even the implied warranty of\n MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n GNU General Public License for more details at:\n http://www.gnu.org/copyleft/gpl.html\n \n \n Typical Usage:\n<pre>\n <code>\n   $browser = new Browser();\n   if( $browser->getBrowser() == Browser::BROWSER_FIREFOX && $browser->getVersion() >= 2 ) {\n    echo \'You have FireFox version 2 or greater\';\n   }\n </code>\n</pre>\n User Agents Sampled from: http://www.useragentstring.com/\n \n This implementation is based on the original work from Gary White\n http://apptools.com/phptools/browser/'),(00000000212,'_Pcre',NULL,'Pcre.php','_LIBS','/oroboros/core/libs/Pcre/','LIB','CORE',NULL,1,0,00000000100,'This is the perl compatible regular expression library. It handles the regular expression needs of the system and is used by numerous libs, models, and other aspects of the system.'),(00000000213,'_Constants',NULL,'Constants.php','_LIBS','/oroboros/core/libs/Constants/','LIB','CORE',NULL,1,0,00000000100,'This file is the constant library. It handles the definition of constants for other libraries in the system.'),(00000000214,'_Sessionvars',NULL,'Sessionvars.php','_LIBS','/oroboros/core/libs/Session/','LIB','CORE',NULL,1,0,00000000000,'This file is the session variables library. It sets the base session variables used by the system to insure that various aspects of the system load correctly, acces rights are retained, and other parts of the system recieve the correct data, even between different landing pages.'),(00000000216,'_Sidebar',NULL,'Sidebar.php','_LIBS','/oroboros/core/libs/Content/','LIB','CORE',NULL,1,0,00000000000,'This is the sidebar library. It handles the correct rendering of sidebar content throughout the site.'),(00000000217,'_Output',NULL,'Output.php','_LIBS','/oroboros/core/libs/Render/','LIB','CORE',NULL,1,0,00000000000,'This file is the output buffer library. It handles the output buffering for the system when pages load.'),(00000000218,'_Validate',NULL,'Validate.php','_LIBS','/oroboros/core/libs/Dataflow/','LIB','CORE',NULL,0,0,00000000000,'This is the data validation library. It insures that data conforms to the correct specifications.'),(00000000219,'_Sanitize',NULL,'Sanitize.php','_LIBS','/oroboros/core/libs/Dataflow/','LIB','CORE',NULL,0,0,00000000000,'This is the data sanitization library. It insures that submitted data does not contain any subversive elements.'),(00000000220,'About',NULL,'About.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000045,0,0,00000000000,'This is the model for the about page.'),(00000000221,'Modal','\\oroboros\\core\\controllers\\logic\\ajax','Modal.php','_CONTROLLERS','/oroboros/core/controllers/logic/ajax/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the ajax modal controller. It renders post-pageload modal content in conjunction with the Modal model.'),(00000000222,'Modal','\\oroboros\\core\\models\\logic\\ajax','Modal.php','_MODELS','/oroboros/core/models/logic/ajax/','MODEL','CORE',00000000221,0,0,00000000000,'This is the ajax modal model. It recieves requests from the ajax modal controller and performs the correct logic for the request.'),(00000000223,'Permissions',NULL,'Permissions.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000057,0,0,00000000000,'This is the permissions model. It computes the logic for backend requests made to the server to affect user and group permissions.'),(00000000224,'Profile','\\oroboros\\core\\controllers\\render','Profile.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the profile controller. It handles user requests to edit their own account details.'),(00000000225,'Profile','\\oroboros\\core\\models\\render','Profile.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000224,0,0,00000000000,'This is the profile model. It handles the logic for requests from users to edit or display data about their account.'),(00000000226,'Files','\\oroboros\\core\\controllers\\render','Files.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the files controller. It handles the execution of user requests against the filebase.'),(00000000227,'Files','\\oroboros\\core\\models\\render','Files.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000226,0,0,00000000000,'This is the Files model. It handles the operation of changes to the filebase requested by users.'),(00000000228,'Settings','\\oroboros\\core\\controllers\\render','Settings.php','_CONTROLLERS','/oroboros/core/controllers/render/','CONTROLLER','CORE',NULL,0,0,00000000000,'This is the Settings controller. It handles user requests to modify the functionality of the installation.'),(00000000229,'Settings','\\oroboros\\core\\models\\render','Settings.php','_MODELS','/oroboros/core/models/render/','MODEL','CORE',00000000228,0,0,00000000000,'This is the Settings model. It handles performs system modifications based on user requests passed from the settings controller.');
/*!40000 ALTER TABLE `codebase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `codebase_ajax`
--

DROP TABLE IF EXISTS `codebase_ajax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codebase_ajax` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `controller` int(11) unsigned DEFAULT NULL,
  `method` varchar(32) NOT NULL DEFAULT '',
  `params` tinyint(1) NOT NULL DEFAULT '0',
  `permission` varchar(16) NOT NULL DEFAULT 'default',
  `type` varchar(4) NOT NULL DEFAULT 'get',
  PRIMARY KEY (`id`),
  UNIQUE KEY `controller_2` (`controller`,`method`),
  KEY `controller` (`controller`),
  KEY `method` (`method`),
  KEY `permission` (`permission`),
  KEY `type` (`type`),
  CONSTRAINT `codebase_ajax_ibfk_1` FOREIGN KEY (`controller`) REFERENCES `codebase` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `codebase_ajax_ibfk_2` FOREIGN KEY (`permission`) REFERENCES `permissions` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `codebase_ajax_ibfk_3` FOREIGN KEY (`type`) REFERENCES `codebase_ajax_types` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `codebase_ajax`
--

LOCK TABLES `codebase_ajax` WRITE;
/*!40000 ALTER TABLE `codebase_ajax` DISABLE KEYS */;
INSERT INTO `codebase_ajax` VALUES (1,221,'renderModal',3,'default','get');
/*!40000 ALTER TABLE `codebase_ajax` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `codebase_ajax_types`
--

DROP TABLE IF EXISTS `codebase_ajax_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codebase_ajax_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(4) NOT NULL DEFAULT '',
  `display` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `display` (`display`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `codebase_ajax_types`
--

LOCK TABLES `codebase_ajax_types` WRITE;
/*!40000 ALTER TABLE `codebase_ajax_types` DISABLE KEYS */;
INSERT INTO `codebase_ajax_types` VALUES (1,'get','Get Request: Non-destructive'),(2,'post','Post Request: Destructive');
/*!40000 ALTER TABLE `codebase_ajax_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `codebase_subtypes`
--

DROP TABLE IF EXISTS `codebase_subtypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codebase_subtypes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `display` varchar(64) NOT NULL DEFAULT '',
  `description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `codebase_subtypes`
--

LOCK TABLES `codebase_subtypes` WRITE;
/*!40000 ALTER TABLE `codebase_subtypes` DISABLE KEYS */;
INSERT INTO `codebase_subtypes` VALUES (1,'CORE','Core Fileset','This file is part of the Oroboros core system.'),(2,'LOCAL','Local Fileset','This file is a developer extension of the Oroboros core system.'),(3,'STAGING','Staging Fileset','This file is a work-in-progress extension of the Oroboros core system.'),(4,'COMPONENT','Component Fileset','This file is part of a component add-on.'),(5,'MODULE','Module Fileset','This file is part of a module add-on.'),(6,'TEMPLATE','Template Fileset','This file is part of a template.'),(7,'THEME','Theme Fileset','This file is part of a theme.'),(8,'SITE','Site Fileset','This file is part of an individual site package.'),(9,'SUBSYSTEM','Subsystem Fileset','This file is part of a subsystem functioning within the core.'),(10,'MEDIA','Media Fileset','This is a media file that renders for the end user (ie: image, audio, video, etc.).'),(11,'CGI','Common Gateway Interface Fileset','This file is of a language other than php, and is accessed using an adapter class specific to that file type.');
/*!40000 ALTER TABLE `codebase_subtypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `codebase_types`
--

DROP TABLE IF EXISTS `codebase_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `codebase_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `display` varchar(64) NOT NULL DEFAULT '',
  `description` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `codebase_types`
--

LOCK TABLES `codebase_types` WRITE;
/*!40000 ALTER TABLE `codebase_types` DISABLE KEYS */;
INSERT INTO `codebase_types` VALUES (1,'MODEL','Model','Computes logic for the system.'),(2,'VIEW','View','Renders data to the page for the end user.'),(3,'CONTROLLER','Controller','Accepts user input, filters data, and passes the end result to the model. Also takes output from the model and passes it to the view.'),(4,'LIB','Library','Code libraries that assist in building the core infrastructure and functionality. Works beneath the MVC layer.'),(5,'BOOTSTRAP','Bootstrap','Initializes the system.'),(6,'CONFIG','Configuration','Sets initial parameters that tell the bootstrap how to function correctly by parsing a settings file.'),(7,'ADAPTER','Adapter','Acts as a gateway between the core system and other systems. Handles the correct passage of data to and from subsystems and other code languages.'),(8,'SETTINGS','Settings','Defines configuration settings to be parsed by a configuration file.'),(9,'LAYOUT','Layout','Defines package presets and handles initial import or export of packages into the system.'),(10,'ARCHIVE','Archive','This is an archived version of an older file, package or folder. It is kept for rollback purposes or so older data can be accessed if needed, such as logs or database backups.'),(11,'SQL','SQL','This is a database backup, import or export file.'),(12,'PACKAGE','Package','This is a compressed series of files that can be imported into the system. It may include a theme, template, module, component, site, content, media, data, or any combination thereof.'),(13,'SERVER','Server File','This file is used by the server to determine how to load the system, or how it should function.'),(14,'LANDING','Landing File','This file may be accessed directly, and will initiate it\'s configuration through the primary index file when accessed.'),(15,'UTILITY','Utility','This is an open source third party code library, typically referenced by other libs, or occasionally by models, modules, or components. Licenses must remain intact at the head of the document, or be registered in the license table.');
/*!40000 ALTER TABLE `codebase_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `components`
--

DROP TABLE IF EXISTS `components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `components` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) NOT NULL DEFAULT '',
  `display` varchar(64) NOT NULL DEFAULT 'Untitled Component',
  `description` varchar(256) NOT NULL DEFAULT 'No description provided',
  `type` varchar(16) NOT NULL DEFAULT 'third-party',
  `folder` varchar(32) NOT NULL DEFAULT '',
  `settings` varchar(16) NOT NULL DEFAULT 'settings.ini',
  `stylesheet` varchar(32) DEFAULT 'styles.css',
  `template` varchar(32) NOT NULL DEFAULT 'template.phtml',
  `scripting` varchar(32) DEFAULT 'scripting.js',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `permission` varchar(16) NOT NULL DEFAULT 'add_components',
  `added` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `display` (`display`),
  KEY `type` (`type`),
  KEY `folder` (`folder`),
  KEY `permission` (`permission`),
  KEY `settings` (`settings`,`stylesheet`,`scripting`),
  KEY `template` (`template`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `components`
--

LOCK TABLES `components` WRITE;
/*!40000 ALTER TABLE `components` DISABLE KEYS */;
INSERT INTO `components` VALUES (1,'core_controller','Core Component Controller','This is the control panel to manipulate component display. It allows you to show or hide various components on the page, and may provide additional functionality depending on what each individual component has registered with the system.','core','component-controller','settings.ini','styles.css','component-controller.phtml','scripting.js',1,'add_components','2014-07-16 17:10:31','2014-07-16 23:22:14'),(2,'core_dashbar','Core Dashbar','This is the dashbar located at the top of the screen, which provides a quick means of navigation throughout the site. It may be manually configured by a site administrator.','core','dashbar','settings.ini','styles.css','dashbar.phtml','scripting.js',1,'add_components','2014-07-16 17:10:31','2014-07-16 23:22:28'),(3,'core_drawer','Core Drawer Utility','This is the drawer that pops out to the left of the page. It may provide links, content, or any other relevant information, settings or navigation depending on it\'s configuration.','core','drawer','settings.ini','styles.css','drawer.phtml','scripting.js',1,'add_components','2014-07-16 17:10:31','2014-07-16 23:22:35'),(4,'core_scroller','Core Page Scroller Utility','This is the page scrolling component. It allows viewers to jump from one post or section in a page to the next quickly and easily.','core','scroller','settings.ini','styles.css','scroller.phtml','scripting.js',0,'add_components','2014-07-16 17:10:31','2014-07-16 23:22:44');
/*!40000 ALTER TABLE `components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `components_content`
--

DROP TABLE IF EXISTS `components_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `components_content` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page` varchar(64) DEFAULT NULL,
  `site` varchar(16) NOT NULL DEFAULT 'default',
  `subdomain` varchar(16) NOT NULL DEFAULT 'default',
  `type` varchar(16) NOT NULL DEFAULT 'html',
  `content` text,
  `hook` varchar(32) NOT NULL DEFAULT 'BODY',
  `hierarchy` int(4) unsigned zerofill NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  `author` varchar(16) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `permission` varchar(16) NOT NULL DEFAULT 'view',
  PRIMARY KEY (`id`),
  KEY `page` (`page`),
  KEY `type` (`type`),
  KEY `author` (`author`),
  KEY `type_2` (`type`),
  KEY `hook` (`hook`),
  KEY `permission` (`permission`),
  KEY `site` (`site`),
  KEY `site_2` (`site`),
  KEY `subdomain` (`subdomain`),
  KEY `site_3` (`site`,`subdomain`,`page`),
  CONSTRAINT `components_content_ibfk_1` FOREIGN KEY (`site`) REFERENCES `sites` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `components_content_ibfk_2` FOREIGN KEY (`permission`) REFERENCES `permissions` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `components_content_ibfk_3` FOREIGN KEY (`subdomain`) REFERENCES `sites_subdomains` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `components_content_ibfk_4` FOREIGN KEY (`type`) REFERENCES `pages_content_types` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `components_content_ibfk_5` FOREIGN KEY (`hook`) REFERENCES `hooks` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `components_content_ibfk_6` FOREIGN KEY (`site`, `subdomain`, `page`) REFERENCES `pages` (`site`, `subdomain`, `slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `components_content`
--

LOCK TABLES `components_content` WRITE;
/*!40000 ALTER TABLE `components_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `components_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `connect_types`
--

DROP TABLE IF EXISTS `connect_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `connect_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `slug` varchar(16) NOT NULL DEFAULT '' COMMENT 'System usable name',
  `title` varchar(64) NOT NULL DEFAULT '' COMMENT 'Human readable name',
  `active` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Whether or not the connection type is currently available within this system environment',
  `show` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='This table manages the means of connection available throughout the site and system.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `connect_types`
--

LOCK TABLES `connect_types` WRITE;
/*!40000 ALTER TABLE `connect_types` DISABLE KEYS */;
INSERT INTO `connect_types` VALUES (0,'SYSTEM','DO NOT DELETE. System self access node.',1,0),(1,'http','Standard http connection',1,1),(2,'https','Secure http connection over ssl',0,1),(3,'ssh','SSH tunnel connection',0,1),(4,'ftp','FTP file transfer connection',0,1),(5,'sftp','Secure FTP file transfer connection',0,1);
/*!40000 ALTER TABLE `connect_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dependencies_assets`
--

DROP TABLE IF EXISTS `dependencies_assets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dependencies_assets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site` varchar(16) NOT NULL DEFAULT 'default',
  `device` varchar(16) DEFAULT NULL,
  `dependency` varchar(16) NOT NULL DEFAULT '',
  `version` varchar(32) NOT NULL DEFAULT '',
  `source_method` varchar(16) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `hierarchy` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `site` (`site`),
  KEY `dependency` (`dependency`),
  KEY `version` (`version`),
  KEY `source_method` (`source_method`),
  KEY `device` (`device`),
  CONSTRAINT `dependencies_assets_ibfk_1` FOREIGN KEY (`site`) REFERENCES `sites` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `dependencies_assets_ibfk_2` FOREIGN KEY (`dependency`) REFERENCES `assets_items` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `dependencies_assets_ibfk_3` FOREIGN KEY (`version`) REFERENCES `assets_items` (`version`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `dependencies_assets_ibfk_4` FOREIGN KEY (`source_method`) REFERENCES `assets_items` (`source_method`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `dependencies_assets_ibfk_5` FOREIGN KEY (`device`) REFERENCES `device_types` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dependencies_assets`
--

LOCK TABLES `dependencies_assets` WRITE;
/*!40000 ALTER TABLE `dependencies_assets` DISABLE KEYS */;
INSERT INTO `dependencies_assets` VALUES (2,'default',NULL,'jquery','2.1.1','javascript',1,0),(3,'default',NULL,'jquery_ui','1.10.4','javascript',1,1),(4,'default',NULL,'jquery_ui','lightness','css',1,1),(5,'default','mobile','jquery_mobile','1.4.2','javascript',1,2),(6,'default','mobile','jquery_mobile','1.4.2','css',1,2),(7,'default','tablet','jquery_mobile','1.4.2','javascript',1,2),(8,'default','tablet','jquery_mobile','1.4.2','css',1,2),(10,'default','desktop','mousetrap','1.6.4','javascript',1,3);
/*!40000 ALTER TABLE `dependencies_assets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_types`
--

DROP TABLE IF EXISTS `device_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `display` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_types`
--

LOCK TABLES `device_types` WRITE;
/*!40000 ALTER TABLE `device_types` DISABLE KEYS */;
INSERT INTO `device_types` VALUES (1,'desktop','Desktop based computers'),(2,'mobile','Mobile devices'),(3,'tablet','Tablet devices'),(4,'embedded','Embedded systems'),(5,'print','Printed documents'),(6,'crawler','Web crawlers and bots');
/*!40000 ALTER TABLE `device_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dns`
--

DROP TABLE IF EXISTS `dns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dns` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `dns` varchar(253) DEFAULT NULL,
  `site` varchar(16) DEFAULT NULL,
  `node` varchar(16) DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `show` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `global` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `private` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `staging` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lockdown` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `maintenance` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `redirect` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `redirect_location` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dns` (`dns`),
  UNIQUE KEY `site` (`site`),
  UNIQUE KEY `dns_2` (`dns`,`site`,`node`),
  UNIQUE KEY `site_2` (`site`,`dns`),
  KEY `node` (`node`),
  KEY `redirect_location` (`redirect_location`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dns`
--

LOCK TABLES `dns` WRITE;
/*!40000 ALTER TABLE `dns` DISABLE KEYS */;
INSERT INTO `dns` VALUES (0,'127.0.0.1','SYSTEM',NULL,1,0,0,1,1,0,0,0,0,NULL),(1,'framework.local','default',NULL,1,1,1,1,0,0,0,0,0,NULL),(2,'oroboros.local','global',NULL,1,1,0,0,0,0,0,0,0,NULL),(4,NULL,'self',NULL,0,0,0,0,1,0,0,0,0,NULL),(11,'framework.local.uk',NULL,NULL,0,1,0,0,0,0,0,0,0,NULL);
/*!40000 ALTER TABLE `dns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hooks`
--

DROP TABLE IF EXISTS `hooks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hooks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) NOT NULL DEFAULT '',
  `type` varchar(16) NOT NULL DEFAULT 'content',
  `scope` varchar(16) NOT NULL DEFAULT 'pages',
  `display` varchar(128) NOT NULL DEFAULT 'Untitled Hook',
  `permission` varchar(16) NOT NULL DEFAULT 'default',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_2` (`slug`),
  KEY `slug` (`slug`),
  KEY `display` (`display`),
  KEY `permission` (`permission`),
  KEY `type` (`type`),
  KEY `scope` (`scope`),
  CONSTRAINT `hooks_ibfk_1` FOREIGN KEY (`permission`) REFERENCES `permissions` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `hooks_ibfk_2` FOREIGN KEY (`type`) REFERENCES `hooks_types` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `hooks_ibfk_4` FOREIGN KEY (`scope`) REFERENCES `hooks_scope` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=72 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hooks`
--

LOCK TABLES `hooks` WRITE;
/*!40000 ALTER TABLE `hooks` DISABLE KEYS */;
INSERT INTO `hooks` VALUES (1,'TITLE','content','global','Page Title','default',1),(2,'BODY','content','pages','Body Content','default',1),(3,'USER','content','pages','User Name','default',1),(4,'HEAD','content','pages','Head Content','default',1),(5,'HEADER','content','pages','Header Content','default',1),(6,'FOOTER','content','pages','Footer Content','default',1),(7,'SIDEBAR','content','pages','Sidebar Content','default',1),(9,'DASHMODULES','content','pages','Dashboard Modules','registered',1),(10,'DASHINDEX','content','templates','Dashboard Index','registered',1),(11,'HEADERLINKS','content','templates','Frontend Header Linkset','default',1),(12,'DASHHEAD','content','templates','Dashboard Header Linkset','registered',1),(13,'PROFILELINKS','content','pages','User Profile Links','registered',1),(14,'DASHBAR','content','pages','Dashbar Linkset','default',1),(15,'DRAWER','content','pages','Drawer Linkset','default',1),(16,'DASHLINKS','content','templates','Dashboard Index Linkset','registered',1),(17,'ACTIVITY','content','pages','Dashboard Activity Panel','registered',1),(18,'DOCINDEX','content','pages','Documentation Page Index Menu','registered',1),(19,'SUBMENU','content','templates','Submenu Content','registered',1),(21,'DASHBARINDEX','content','components','Dashbar Index Menu','default',1),(22,'DASHBARUSERPANE','content','components','Dashbar User Panel','default',1),(23,'DRAWERINDEX','content','components','Drawer Index Menu','default',1),(24,'DRAWERTOOLS','content','components','Drawer Toolset','default',1),(25,'DRAWERCONTENT','content','components','Drawer Content','default',1),(26,'DRAWERTITLE','content','components','Drawer Title','default',1),(27,'DRAWERFOOTER','content','components','Drawer Footer','default',1),(28,'DASHCOMPONENTS','content','components','Dashbar Components','default',1),(30,'DOCCONTENT','content','pages','Documentation Content','registered',1),(31,'DASHWIDGETS','content','pages','Dashboard Widgets','registered',1),(32,'NAVIGATION','content','templates','Site Navigation Menu','registered',1),(34,'ARCHITECTDOCS','content','pages','Architect Documentation','architect',1),(36,'OWNERDOCS','content','pages','Owner Documentation','owner',1),(37,'DEVELOPERDOCS','content','pages','Developer Documentation','developer',1),(38,'DESIGNERDOCS','content','pages','Designer Documentation','designer',1),(39,'WEBMASTERDOCS','content','pages','Webmaster Documentation','webmaster',1),(40,'OPERATORDOCS','content','pages','Operator Documentation','operator',1),(41,'ADMINDOCS','content','pages','Admin Documentation','admin',1),(42,'AUTHORDOCS','content','pages','Author Documentation','author',1),(43,'MODERATORDOCS','content','pages','Moderator Documentation','moderator',1),(44,'USERDOCS','content','pages','User Documentation','registered',1),(45,'ARCHITECTDOCINDEX','content','pages','Architect Documentation Index','architect',1),(46,'OWNERDOCINDEX','content','pages','Owner Documentation Index','owner',1),(47,'DEVELOPERDOCINDEX','content','pages','Developer Documentation Index','developer',1),(48,'WEBMASTERDOCINDEX','content','pages','Webmaster Documentation Index','webmaster',1),(49,'OPERATORDOCINDEX','content','pages','Operator Documentation Index','operator',1),(50,'ADMINDOCINDEX','content','pages','Admin Documentation Index','admin',1),(51,'MODERATORDOCINDEX','content','pages','Moderator Documentation Index','moderator',1),(52,'AUTHORDOCINDEX','content','pages','Author Documentation Index','author',1),(53,'USERDOCINDEX','content','pages','User Documentation Index','registered',1),(54,'DEVTOOLS','content','pages','Developer Tools Documentation','developer',1),(55,'ARCHITECTTOOLS','content','pages','Architect Tools Documentation','architect',1),(56,'OWNERTOOLS','content','pages','Owner Tools Documentation','owner',1),(57,'DESIGNERTOOLS','content','pages','Designer Tools Documentation','designer',1),(58,'WEBMASTERTOOLS','content','pages','Webmaster Tools Documentation','webmaster',1),(59,'OPERATORTOOLS','content','pages','Operator Tools Documentation','operator',1),(60,'ADMINTOOLS','content','pages','Admin Tools Documentation','admin',1),(61,'MODERATORTOOLS','content','pages','Moderator Tools Documentation','moderator',1),(62,'AUTHORTOOLS','content','pages','Content Author Tools Documentation','author',1),(63,'USERTOOLS','content','pages','Registered User Tools Documentation','registered',1),(64,'ARCHITECTSCRATCHBOX','content','pages','Architect Scratchbox Test Hook','architect',1),(66,'USERBOX','content','templates','Userbox Linkset','registered',1),(67,'FOOTERTEMPLATE','content','templates','Footer Template Content','default',1),(68,'FOOTERTHEME','content','themes','Footer Theme Hook','default',1),(69,'FOOTERMODULE','content','modules','Footer Module Hook','default',1),(70,'FOOTERCOMPONENT','content','components','Footer Component Hook','default',1),(71,'FOOTERSITE','content','sites','Footer Site Hook','default',1);
/*!40000 ALTER TABLE `hooks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hooks_scope`
--

DROP TABLE IF EXISTS `hooks_scope`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hooks_scope` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `display` varchar(64) NOT NULL DEFAULT 'Untitled',
  `description` varchar(256) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  `permission` varchar(16) DEFAULT 'default',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `display` (`display`),
  KEY `permission` (`permission`),
  CONSTRAINT `hooks_scope_ibfk_1` FOREIGN KEY (`permission`) REFERENCES `permissions` (`slug`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hooks_scope`
--

LOCK TABLES `hooks_scope` WRITE;
/*!40000 ALTER TABLE `hooks_scope` DISABLE KEYS */;
INSERT INTO `hooks_scope` VALUES (1,'SYSTEM','System Hook','This hook will inject content into the page registered for the system. It should never be used for visual displaying content.',1,0,'SYSTEM'),(2,'nodes','Node Hook','This hook will inject content into the page registered to the current node connection.',1,1,'node'),(3,'sites','Site Hook','This hook will inject content into the page wherever that hook exists throughout the entire site.',1,1,'default'),(4,'pages','Page Hook','This hook will only inject content into pages where a specified record of content exists for that page.',1,1,'default'),(6,'components','Component Hook','This hook will inject components into the page wherever that component is registered for use.',1,1,'default'),(7,'modules','Module Hook','This hook will inject content into the page wherever the specified module is registered to function.',1,1,'default'),(8,'global','Global Hook','This hook will inject content into the page globally. Any entries for these hooks will be applied across your entire network.',1,1,'default'),(9,'templates','Template Hook','This hook will inject content into the page on any page that uses the specified template globally.',1,1,'default'),(10,'themes','Theme Hook','This hook will inject content into the page on any page that uses the specified theme globally.',1,1,'default'),(11,'groups','Group Hook','This hook will inject content into the page that is registered for all members of a specified group.',1,1,'default'),(12,'users','User Hook','This hook will inject content into the page that is registered for specified users.',1,1,'default'),(13,'permissions','Permission Hook','This hook will inject content into the page that is registered for all users with a specific permission.',1,1,'default');
/*!40000 ALTER TABLE `hooks_scope` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hooks_types`
--

DROP TABLE IF EXISTS `hooks_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hooks_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `display` varchar(64) NOT NULL DEFAULT 'Untitled Hook Type',
  `format` varchar(128) DEFAULT NULL,
  `permission` varchar(16) NOT NULL DEFAULT 'view',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `display` (`display`),
  KEY `format` (`format`),
  KEY `permission` (`permission`),
  CONSTRAINT `hooks_types_ibfk_1` FOREIGN KEY (`permission`) REFERENCES `permissions` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hooks_types`
--

LOCK TABLES `hooks_types` WRITE;
/*!40000 ALTER TABLE `hooks_types` DISABLE KEYS */;
INSERT INTO `hooks_types` VALUES (1,'content','Content Hook','[]hook[/]','view',1),(2,'function','Function Hook','[[function]]param, param, param...[/]','view',1);
/*!40000 ALTER TABLE `hooks_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inv_groups`
--

DROP TABLE IF EXISTS `inv_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inv_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `title` varchar(64) NOT NULL DEFAULT '',
  `description` varchar(512) NOT NULL DEFAULT 'No description provided',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `render` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `query_index` (`title`,`description`(255),`id`,`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inv_groups`
--

LOCK TABLES `inv_groups` WRITE;
/*!40000 ALTER TABLE `inv_groups` DISABLE KEYS */;
INSERT INTO `inv_groups` VALUES (1,'dry','Dry Goods','Food products that do not require refrigeration (i.e. spices, bread, flour, etc)',1,1),(2,'frozen','Frozen Goods','Food product that is stored frozen until use.',1,1),(3,'dairy','Dairy Products','Milk, cheese, eggs, other dairy product sensitive to spoilage.',1,1),(4,'meat','Meat Products','Animal protein, fish, etc.',1,1),(5,'produce','Produce','Fruit and vegetables',1,1),(6,'cleaning','Non-food Inventory','Cleaning supplies, paper, packaging, other non-edibles.',1,1),(7,'beverage','Beverage Inventory','Non-alcoholic beverages and cooking wine.',1,1),(8,'BASE','BASE GROUP OBJECT','This item is not an actual inventory entry, it is a database hook for basic unit conversions, do not delete.',1,0);
/*!40000 ALTER TABLE `inv_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inv_items`
--

DROP TABLE IF EXISTS `inv_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inv_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `group` varchar(16) NOT NULL DEFAULT '',
  `title` varchar(64) NOT NULL DEFAULT '',
  `description` varchar(512) NOT NULL DEFAULT 'No description provided',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `spoil_num` int(3) unsigned NOT NULL DEFAULT '1',
  `spoil_inc` varchar(1) NOT NULL DEFAULT 'd',
  `render` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `group` (`group`),
  KEY `title` (`title`),
  KEY `description` (`description`(255)),
  KEY `spoil_inc` (`spoil_inc`),
  CONSTRAINT `inv_items_ibfk_1` FOREIGN KEY (`group`) REFERENCES `inv_groups` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `inv_items_ibfk_2` FOREIGN KEY (`spoil_inc`) REFERENCES `inv_spoilage_inc` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inv_items`
--

LOCK TABLES `inv_items` WRITE;
/*!40000 ALTER TABLE `inv_items` DISABLE KEYS */;
INSERT INTO `inv_items` VALUES (0,'base','BASE','BASE OBJECT PARENT','This item is not an actual inventory entry, it is a database hook for basic unit conversions, do not delete.',1,999,'y',0),(1,'arugula','produce','Arugula','No description provided',1,1,'d',1),(2,'apples','produce','Apples, Granny Smith','No description provided',1,2,'w',1),(3,'basil','produce','Basil','No description provided',1,5,'d',1),(4,'beets','produce','Beets','BEEEEEETS!!!! :D !11!one!',1,2,'o',1),(5,'sprouts','produce','Brussels Sprouts','No description provided',1,10,'d',1),(6,'rcabbage','produce','Red Cabbage','No description provided',1,3,'w',1),(8,'gcabbage','produce','Green Cabbage','No description provided',1,3,'w',1),(9,'carrots','produce','Carrots','No description provided',1,3,'w',1),(10,'celery','produce','Celery','No description provided',1,2,'w',1),(12,'fennel','produce','Fennel','No description provided',1,2,'w',1),(13,'garlic','produce','Garlic Cloves','No description provided',1,4,'w',1),(14,'cilantro','produce','Herb, Cilantro','No description provided',1,5,'d',1),(15,'mint','produce','Herb, Mint','No description provided',1,7,'d',1),(16,'parsley','produce','Herb, Parsley','No description provided',1,5,'d',1),(17,'rosemary','produce','Herb, Rosemary','No description provided',1,10,'d',1),(18,'thyme','produce','Herb, Thyme','No description provided',1,2,'w',1),(19,'kale','produce','Kale','No description provided',1,2,'w',1),(20,'zucchini','produce','Squash, Zucchini','No description provided',1,7,'d',1),(21,'ssquash','produce','Squash, Summer','No description provided',1,7,'d',1),(22,'romaine','produce','Lettuce, Romaine','No description provided',1,5,'d',1),(23,'lemons','produce','Lemons','No description provided',1,10,'d',1),(24,'limes','produce','Limes','No description provided',1,10,'d',1),(25,'yonion','produce','Onions, Yellow','No description provided',1,3,'w',1),(26,'gonion','produce','Onions, Green','No description provided',1,2,'w',1),(27,'ronion','produce','Onions, Red','No description provided',1,3,'w',1),(28,'habanero','produce','Peppers, Habanero','No description provided',1,7,'d',1),(29,'rpepper','produce','Peppers, Red','No description provided',1,7,'d',1),(30,'jalapeno','produce','Peppers, Jalapeno','No description provided',1,2,'w',1),(31,'potato','produce','Potatoes, Russet','No description provided',1,2,'m',1),(32,'rpotato','produce','Potatoes, Red','No description provided',1,2,'m',1),(33,'spinach','produce','Spinach','No description provided',1,5,'d',1),(34,'tomatoes','produce','Tomatoes, 4x5','No description provided',1,7,'d',1),(35,'htomatoes','produce','Tomatoes, Heirloom','No description provided',1,5,'d',1),(36,'shallots','produce','Shallots','No description provided',1,2,'m',1),(37,'rapples','produce','Apples, Gala','No description provided',1,2,'w',1),(38,'grapefruit','produce','Grapefruit','No description provided',1,10,'d',1),(39,'milk','dairy','Milk, 2%','No description provided',1,7,'d',1),(40,'cream','dairy','Cream, Whipping','No description provided',1,10,'d',1),(41,'yolk','dairy','Eggs, Yolk','No description provided',1,3,'w',1),(42,'eggs','dairy','Eggs, Whole','No description provided',1,3,'w',1),(43,'round','meat','Eye of Round','No description provided',1,5,'d',1);
/*!40000 ALTER TABLE `inv_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inv_spoilage_inc`
--

DROP TABLE IF EXISTS `inv_spoilage_inc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inv_spoilage_inc` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(1) NOT NULL DEFAULT 'd',
  `title` varchar(32) NOT NULL DEFAULT 'Days',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inv_spoilage_inc`
--

LOCK TABLES `inv_spoilage_inc` WRITE;
/*!40000 ALTER TABLE `inv_spoilage_inc` DISABLE KEYS */;
INSERT INTO `inv_spoilage_inc` VALUES (1,'s','Seconds',0),(2,'m','Minutes',0),(3,'h','Hours',1),(4,'d','Days',1),(5,'w','Weeks',1),(6,'o','Months',1),(7,'y','Years',1);
/*!40000 ALTER TABLE `inv_spoilage_inc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inv_stock`
--

DROP TABLE IF EXISTS `inv_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inv_stock` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `title` varchar(64) NOT NULL DEFAULT '',
  `qty` int(4) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `order` datetime NOT NULL,
  `expire` datetime NOT NULL,
  `parent` varchar(16) NOT NULL DEFAULT 'BASE',
  `render` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parent` (`parent`),
  CONSTRAINT `inv_stock_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `inv_items` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inv_stock`
--

LOCK TABLES `inv_stock` WRITE;
/*!40000 ALTER TABLE `inv_stock` DISABLE KEYS */;
INSERT INTO `inv_stock` VALUES (1,'base','Basic Measurements',1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00','BASE',0),(2,'beets','Beets',1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00','BASE',1);
/*!40000 ALTER TABLE `inv_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inv_stock_costs`
--

DROP TABLE IF EXISTS `inv_stock_costs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inv_stock_costs` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT 'BASE',
  `item` varchar(16) NOT NULL DEFAULT '',
  `vendor` varchar(16) NOT NULL DEFAULT '',
  `cost` decimal(11,2) NOT NULL,
  `available` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `item` (`item`),
  KEY `vendor` (`vendor`),
  KEY `cost` (`cost`),
  CONSTRAINT `inv_stock_costs_ibfk_1` FOREIGN KEY (`item`) REFERENCES `inv_items` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `inv_stock_costs_ibfk_2` FOREIGN KEY (`vendor`) REFERENCES `vendors` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inv_stock_costs`
--

LOCK TABLES `inv_stock_costs` WRITE;
/*!40000 ALTER TABLE `inv_stock_costs` DISABLE KEYS */;
INSERT INTO `inv_stock_costs` VALUES (1,'beets_freshguys','beets','freshguys',24.81,1);
/*!40000 ALTER TABLE `inv_stock_costs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inv_stock_qty_inc`
--

DROP TABLE IF EXISTS `inv_stock_qty_inc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inv_stock_qty_inc` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `title` varchar(64) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inv_stock_qty_inc`
--

LOCK TABLES `inv_stock_qty_inc` WRITE;
/*!40000 ALTER TABLE `inv_stock_qty_inc` DISABLE KEYS */;
INSERT INTO `inv_stock_qty_inc` VALUES (1,'tsp','Teaspoons',1),(2,'tblsp','Tablespoons',1),(3,'pint','Pints',1),(4,'qt','Quarts',1),(5,'gal','Gallons',1),(6,'cup','Cups',1),(7,'lbs','Pounds',1),(8,'oz','Ounces',1),(9,'ea','Items',1),(10,'case','Cases',1),(11,'doz','Dozen',1),(12,'bag','Bags',1),(13,'bottle','Bottles',1),(14,'cans','Cans',1),(15,'jar','Jars',1),(16,'cartons','Cartons',1),(17,'bunch','Bunches',1),(18,'sleeves','Sleeves',1),(19,'box','Boxes',1);
/*!40000 ALTER TABLE `inv_stock_qty_inc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inv_stock_qty_inc_conv`
--

DROP TABLE IF EXISTS `inv_stock_qty_inc_conv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inv_stock_qty_inc_conv` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `item` varchar(16) NOT NULL DEFAULT '',
  `slug` varchar(16) NOT NULL DEFAULT '',
  `base_inc` varchar(16) NOT NULL DEFAULT '',
  `title` varchar(64) NOT NULL DEFAULT '',
  `count` int(4) unsigned NOT NULL DEFAULT '1',
  `increment` varchar(16) NOT NULL DEFAULT '',
  `result` varchar(16) NOT NULL DEFAULT 'tblsp',
  `render` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `item` (`item`),
  KEY `increment` (`increment`),
  KEY `slug_2` (`slug`,`item`,`id`,`title`,`count`,`increment`),
  KEY `base_inc` (`base_inc`),
  KEY `result` (`result`),
  CONSTRAINT `inv_stock_qty_inc_conv_ibfk_1` FOREIGN KEY (`item`) REFERENCES `inv_items` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `inv_stock_qty_inc_conv_ibfk_2` FOREIGN KEY (`base_inc`) REFERENCES `inv_stock_qty_inc` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `inv_stock_qty_inc_conv_ibfk_3` FOREIGN KEY (`result`) REFERENCES `inv_stock_qty_inc` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inv_stock_qty_inc_conv`
--

LOCK TABLES `inv_stock_qty_inc_conv` WRITE;
/*!40000 ALTER TABLE `inv_stock_qty_inc_conv` DISABLE KEYS */;
INSERT INTO `inv_stock_qty_inc_conv` VALUES (2,'BASE','tsp_tblsp','tsp','Teaspoons > Tablespoons',1,'3','tblsp',0);
/*!40000 ALTER TABLE `inv_stock_qty_inc_conv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `display` varchar(64) NOT NULL DEFAULT 'Untitled Module',
  `version` varchar(64) DEFAULT NULL,
  `type` varchar(16) NOT NULL DEFAULT 'third-party',
  `folder` varchar(32) NOT NULL DEFAULT '',
  `settings` varchar(32) NOT NULL DEFAULT 'settings.ini',
  `permission` varchar(16) NOT NULL DEFAULT 'default',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  `added` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  UNIQUE KEY `slug_2` (`slug`,`type`,`version`),
  KEY `permission` (`permission`),
  KEY `display` (`display`),
  KEY `type` (`type`),
  KEY `version` (`version`),
  KEY `slug_3` (`slug`,`type`),
  CONSTRAINT `modules_ibfk_2` FOREIGN KEY (`permission`) REFERENCES `permissions` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `modules_ibfk_3` FOREIGN KEY (`type`) REFERENCES `modules_types` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES (1,'testmodule','Test Module','0.0.1 alpha','core','testmodule','settings.ini','architect',1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules_content`
--

DROP TABLE IF EXISTS `modules_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules_content` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page` varchar(64) DEFAULT NULL,
  `site` varchar(16) NOT NULL DEFAULT 'default',
  `subdomain` varchar(16) NOT NULL DEFAULT 'default',
  `type` varchar(16) NOT NULL DEFAULT 'html',
  `content` text,
  `hook` varchar(32) NOT NULL DEFAULT 'BODY',
  `hierarchy` int(4) unsigned zerofill NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  `author` varchar(16) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `permission` varchar(16) NOT NULL DEFAULT 'view',
  PRIMARY KEY (`id`),
  KEY `page` (`page`),
  KEY `type` (`type`),
  KEY `author` (`author`),
  KEY `type_2` (`type`),
  KEY `hook` (`hook`),
  KEY `permission` (`permission`),
  KEY `site` (`site`),
  KEY `site_2` (`site`),
  KEY `subdomain` (`subdomain`),
  KEY `site_3` (`site`,`subdomain`,`page`),
  CONSTRAINT `modules_content_ibfk_1` FOREIGN KEY (`site`) REFERENCES `sites` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `modules_content_ibfk_2` FOREIGN KEY (`permission`) REFERENCES `permissions` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `modules_content_ibfk_3` FOREIGN KEY (`subdomain`) REFERENCES `sites_subdomains` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `modules_content_ibfk_4` FOREIGN KEY (`type`) REFERENCES `pages_content_types` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `modules_content_ibfk_5` FOREIGN KEY (`hook`) REFERENCES `hooks` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `modules_content_ibfk_6` FOREIGN KEY (`site`, `subdomain`, `page`) REFERENCES `pages` (`site`, `subdomain`, `slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules_content`
--

LOCK TABLES `modules_content` WRITE;
/*!40000 ALTER TABLE `modules_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `modules_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules_types`
--

DROP TABLE IF EXISTS `modules_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `display` varchar(64) NOT NULL DEFAULT '',
  `description` varchar(256) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules_types`
--

LOCK TABLES `modules_types` WRITE;
/*!40000 ALTER TABLE `modules_types` DISABLE KEYS */;
INSERT INTO `modules_types` VALUES (1,'core','Core Module','These are modules developed by the Oroboros core team. They have been tested for maximum compatibility.',1,1),(2,'third-party','Third Party Module','These are modules developed for the Oroboros platform by a third party. These may or may not function as intended, depending on the specific developer. Oroboros implies no guarantee as to the correct functionality of these modules.',1,1),(3,'staging','Staging Module','These are modules under development within this system. They can only be accessed from the sandbox development environment. When they are flagged as complete, they will be moved into the third-party directory and will be globally avaliable.',1,1);
/*!40000 ALTER TABLE `modules_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `note`
--

DROP TABLE IF EXISTS `note`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `note` (
  `noteid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userid` varchar(16) NOT NULL DEFAULT '',
  `title` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `date_added` datetime NOT NULL,
  PRIMARY KEY (`noteid`),
  KEY `userid` (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='This table contains user notes';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `note`
--

LOCK TABLES `note` WRITE;
/*!40000 ALTER TABLE `note` DISABLE KEYS */;
INSERT INTO `note` VALUES (5,'mopsyd','blah','blah','2014-07-03 16:32:15'),(6,'mopsyd','boogity','boogity','2014-07-05 11:44:10');
/*!40000 ALTER TABLE `note` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site_section` varchar(16) DEFAULT NULL,
  `path` varchar(256) DEFAULT NULL,
  `slug` varchar(64) NOT NULL DEFAULT 'index',
  `display` varchar(64) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `site` varchar(16) NOT NULL DEFAULT 'default',
  `subdomain` varchar(16) NOT NULL DEFAULT 'default',
  `parent` varchar(16) DEFAULT NULL,
  `controller` int(11) unsigned zerofill DEFAULT '00000000043',
  `method` varchar(128) DEFAULT NULL,
  `stylesheet` varchar(32) DEFAULT NULL,
  `theme` varchar(16) NOT NULL DEFAULT 'default',
  `template` varchar(16) NOT NULL DEFAULT 'default',
  `template_file` varchar(32) NOT NULL DEFAULT 'index',
  `access_level` varchar(16) NOT NULL DEFAULT 'open',
  `permission` varchar(16) NOT NULL DEFAULT 'view',
  `hierarchy` int(3) unsigned zerofill NOT NULL DEFAULT '999',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `site_2` (`site`,`subdomain`,`slug`),
  UNIQUE KEY `parent_2` (`parent`,`slug`),
  UNIQUE KEY `site_3` (`site`,`subdomain`,`site_section`,`slug`),
  KEY `slug` (`slug`),
  KEY `site` (`site`),
  KEY `parent` (`parent`),
  KEY `access_level` (`access_level`),
  KEY `subdomain` (`subdomain`),
  KEY `permission` (`permission`),
  KEY `stylesheet` (`stylesheet`),
  KEY `controller` (`controller`),
  KEY `site_section` (`site_section`),
  KEY `path` (`path`(255)),
  KEY `site_4` (`site`,`subdomain`,`site_section`),
  CONSTRAINT `pages_ibfk_1` FOREIGN KEY (`site`) REFERENCES `sites` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pages_ibfk_3` FOREIGN KEY (`access_level`) REFERENCES `access_level` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `pages_ibfk_4` FOREIGN KEY (`controller`) REFERENCES `codebase` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `pages_ibfk_5` FOREIGN KEY (`site_section`) REFERENCES `sites_subsections` (`section`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `subsection_index` FOREIGN KEY (`site`, `subdomain`, `site_section`) REFERENCES `sites_subsections` (`site`, `subdomain`, `section`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 COMMENT='This table contains individual pages that the system has been configured to display.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (0,NULL,NULL,'SITE',NULL,NULL,'self','default','RAW',NULL,NULL,NULL,'RAW','RAW','index','private','SYSTEM',000,1,0),(5,'frontend',NULL,'error','Oops! Something went awry!',NULL,'default','default','index',00000000048,NULL,NULL,'default','default','error','open','view',000,1,0),(6,'frontend',NULL,'index','Thank you for choosing Oroboros','The content management framework for developers and designers','default','default',NULL,00000000043,NULL,NULL,'default','default','index','open','view',000,1,1),(7,'backend',NULL,'dashboard','Global Dashboard',NULL,'default','default',NULL,00000000046,NULL,NULL,'default-admin','default-admin','dashboard','open','registered',000,1,1),(9,'backend','docs/','docs','Documentation','Site documentation','default','default','index',00000000047,NULL,'stylesheet.css','default-admin','default-admin','dashboard','open','registered',000,1,1),(10,'frontend','login/','login','Login','Please log in to continue','default','default','index',00000000050,NULL,NULL,'default','default','login','open','view',999,1,1),(11,'backend','users/','users','User Management','Manage user accounts from this page','default','default','index',00000000093,NULL,NULL,'default-admin','default-admin','dashboard','open','registered',000,1,1),(12,'backend','logout/','dashboard/logout','Logging out...',NULL,'default','default','dashboard',00000000046,NULL,NULL,'default','RAW','index','open','registered',999,1,1),(13,'frontend','login/run/','login/run','Logging in...',NULL,'default','default','login',00000000050,NULL,NULL,'default','RAW','index','open','login',999,1,1),(14,'backend','links/','links','Link Management','Manage site links for all sites in your network from this page','default','default','dashboard',00000000146,NULL,NULL,'default-admin','default-admin','dashboard','open','webmaster',005,1,1),(15,'frontend','about/','about','About Oroboros','Overview of system capabilities and features','default','default','index',00000000045,NULL,NULL,'default','default','index','open','view',900,1,1),(16,'backend','accounts/','accounts','Account Management','Create, edit and delete accounts from this page','default','default','dashboard',00000000089,NULL,NULL,'default-admin','default-admin','dashboard','open','admin',000,1,1),(17,'backend','admin/','admin','Administration Panel','Overview of administrative commands','default','default','dashboard',00000000046,NULL,NULL,'default-admin','default-admin','dashboard','open','registered',003,1,1),(18,'backend','permissions/','permissions','Access Rights','Manage access rights and restrictions from this page','default','default','dashboard',00000000057,NULL,NULL,'default-admin','default-admin','dashboard','open','registered',000,1,1),(19,'backend','webmaster/','webmaster','Webmaster Settings','Manage network-wide settings from this page','default','default','dashboard',00000000046,NULL,NULL,'default-admin','default-admin','dashboard','open','registered',001,1,1),(20,'backend','developer/','developer','Developer Settings','Manage site development efforts from this page','default','default','dashboard',00000000046,NULL,NULL,'default-admin','default-admin','dashboard','open','registered',002,1,1),(21,'backend','moderator/','moderator','Moderator Settings','Manage the site user-base from this page','default','default','dashboard',00000000046,NULL,NULL,'default-admin','default-admin','dashboard','open','registered',004,1,1),(22,'backend','site-statistics/','site-statistics','Website Statistics','Overview of this site\'s performance','default','default','dashboard',00000000043,NULL,NULL,'default-admin','default-admin','dashboard','open','developer',001,1,1),(23,'backend','server/','server','Server Information','Overview of the webserver capabilities','default','default','index',00000000086,NULL,NULL,'default-admin','default-admin','dashboard','open','operator',000,1,1),(25,'backend','user-groups/','user-groups','Group Management','Manage user groups from this page','default','default','dashboard',00000000091,NULL,NULL,'default-admin','default-admin','dashboard','open','admin',000,1,1),(26,'backend','user-management/','user-management','User Management','Moderate user behavior from this page','default','default','dashboard',00000000093,NULL,NULL,'default-admin','default-admin','dashboard','open','moderator',000,1,1),(27,'backend','templates/','templates','Templates','Modify templates from this page','default','default','dashboard',00000000095,NULL,NULL,'default-admin','default-admin','dashboard','open','designer',000,1,1),(28,'backend','themes/','themes','Themes','Modify themes from this page','default','default','dashboard',00000000097,NULL,NULL,'default-admin','default-admin','dashboard','open','designer',001,1,1),(29,'backend','media/','media','Media Management','Control media from this page','default','default','dashboard',00000000099,NULL,NULL,'default-admin','default-admin','dashboard','open','designer',002,1,1),(30,'backend','typography/','typography','Typography','Design typeface and typography from this page','default','default','dashboard',00000000101,NULL,NULL,'default-admin','default-admin','dashboard','open','designer',003,1,1),(31,'backend','palletes/','palletes','Color Schemes','Design color schemes from this page','default','default','dashboard',00000000103,NULL,NULL,'default-admin','default-admin','dashboard','open','designer',004,1,1),(32,'backend','maintenance/','maintenance','Site Maintenance','Perform site maintenance tasks from this page','default','default','dashboard',00000000105,NULL,NULL,'default-admin','default-admin','dashboard','open','developer',000,1,1),(33,'backend','debug/','debug','Debug Settings','Configure various settings to assist in site debugging from this page','default','default','dashboard',00000000107,NULL,NULL,'default-admin','default-admin','dashboard','open','developer',002,1,1),(34,'backend','modules/','modules','Module Settings','Configure module behavior from this page','default','default','dashboard',00000000109,NULL,NULL,'default-admin','default-admin','dashboard','open','developer',003,1,1),(35,'backend','components/','components','Component Settings','Configure component behavior from this page','default','default','dashboard',00000000111,NULL,NULL,'default-admin','default-admin','dashboard','open','developer',004,1,1),(36,'backend','staging/','staging','Staging System','Stage files for development from this page','default','default','dashboard',00000000113,NULL,NULL,'default-admin','default-admin','dashboard','open','developer',005,1,1),(37,'backend','webshell/','webshell','Web Shell Terminal','Execute command line functions from the web from this page','default','default','dashboard',00000000115,NULL,NULL,'default-admin','default-admin','dashboard','open','developer',006,1,1),(38,'backend','logs/','logs','System Logs','View system logs and determine their behavior from this page','default','default','dashboard',00000000119,NULL,NULL,'default-admin','default-admin','dashboard','open','developer',007,1,1),(39,'backend','database/','database','Database Settings','Configure databases from this page','default','default','dashboard',00000000121,NULL,NULL,'default-admin','default-admin','dashboard','open','developer',008,1,1),(40,'backend','source-control/','source-control','Source Control','Configure version control settings from this page','default','default','dashboard',00000000123,NULL,NULL,'default-admin','default-admin','dashboard','open','developer',009,1,1),(41,'backend','cgi/','cgi','CGI Settings','Configure CGI gateway settings from this page','default','default','dashboard',00000000125,NULL,NULL,'default-admin','default-admin','dashboard','open','developer',010,1,1),(42,'backend','ide/','ide','Code Editor','Handle code editing from this page','default','default','dashboard',00000000127,NULL,NULL,'default-admin','default-admin','dashboard','open','developer',011,1,1),(43,'backend','ftp/','ftp','FTP Panel','Handle FTP connections from this page','default','default','dashboard',00000000129,NULL,NULL,'default-admin','default-admin','dashboard','open','developer',012,1,1),(44,'backend','ssh/','ssh','SSH Panel','Handle SSH connections and tunnels from this page','default','default','dashboard',00000000131,NULL,NULL,'default-admin','default-admin','dashboard','open','developer',013,1,1),(45,'backend','seo/','seo','SEO Configuration','Optimize your site performance for SEO from this page','default','default','dashboard',00000000133,NULL,NULL,'default-admin','default-admin','dashboard','open','developer',014,1,1),(46,'backend','nodes/','nodes','Node Settings','Manage your local and remote node connections from this page','default','default','dashboard',00000000135,NULL,NULL,'default-admin','default-admin','dashboard','open','webmaster',000,1,1),(47,'backend','analytics/','analytics','Site Analytics','View sitewide analytics from this page','default','default','dashboard',00000000137,NULL,NULL,'default-admin','default-admin','dashboard','open','webmaster',001,1,1),(48,'backend','dns/','dns','DNS Settings','Manage DNS settings and domain names within the system from this page','default','default','dashboard',00000000139,NULL,NULL,'default-admin','default-admin','dashboard','open','webmaster',002,1,1),(49,'backend','sites/','sites','Sites','Manage websites within the system from this page','default','default','dashboard',00000000142,NULL,NULL,'default-admin','default-admin','dashboard','open','webmaster',003,1,1),(50,'backend','pages/','pages','Pages','Manage web pages within the system from this page','default','default','dashboard',00000000144,NULL,NULL,'default-admin','default-admin','dashboard','open','webmaster',004,1,1),(52,'backend','content/','content','Content Management','Manage content for all of your pages from this page','default','default','dashboard',00000000148,NULL,NULL,'default-admin','default-admin','dashboard','open','webmaster',006,1,1),(53,'backend','syndication/','syndication','Syndication Settings','Manage content syndication from this page','default','default','dashboard',00000000150,NULL,NULL,'default-admin','default-admin','dashboard','open','webmaster',007,1,1),(54,'backend','ssl/','ssl','SSL Certificates','Manage your ssl certificates for the system from this page','default','default','dashboard',00000000152,NULL,NULL,'default-admin','default-admin','dashboard','open','webmaster',008,1,1),(55,'backend','hooks/','hooks','Hooks','Manage content and function hooks for sites, pages, modules, components, themes, and templates from this page','default','default','dashboard',00000000154,NULL,NULL,'default-admin','default-admin','dashboard','open','webmaster',009,1,1),(56,'backend','accounts/create/','accounts/create','Create User Account','Creating user account...','default','default','accounts',00000000089,NULL,NULL,'default','RAW','index','open','admin',000,1,1),(57,'backend','accounts/edit/','accounts/edit','Edit User Account','Editing user account...','default','default','accounts',00000000089,NULL,NULL,'default','RAW','index','open','admin',000,1,1),(60,'frontend',NULL,'global-error','Oops! Something went awry!',NULL,'global','default','index',00000000048,NULL,NULL,'default','default','error','open','view',000,1,0),(61,'backend','sites/create/','Create Site','Create Website','Add a new website from this page','default','default','dashboard',00000000043,NULL,NULL,'default-admin','default-admin','dashboard','open','webmaster',999,1,1),(62,'backend','profile/','profile','User Profile','View an overview of your account from this page','default','default','dashboard',00000000224,NULL,NULL,'default-admin','default-admin','dashboard','open','registered',999,1,1),(63,'backend','files/','files','Filebase Manager','Upload, download, archive, restore, edit or delete files from this page','default','default','dashboard',00000000226,NULL,NULL,'default-admin','default-admin','dashboard','open','admin',999,1,1),(64,'backend','files/browse/','files/browse','File Browser','Browse the filebase from this page','default','default','dashboard',00000000226,NULL,NULL,'default-admin','default-admin','dashboard','open','admin',999,1,1),(66,'backend','settings/','settings','System Settings','Modify the functionality of your Oroboros installation from this page','default','default','dashboard',00000000228,NULL,NULL,'default-admin','default-admin','dashboard','open','owner',999,1,1);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_components`
--

DROP TABLE IF EXISTS `pages_components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_components` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `component` varchar(16) NOT NULL DEFAULT '',
  `site` varchar(16) NOT NULL DEFAULT 'default',
  `page` varchar(64) NOT NULL DEFAULT 'index',
  `subdomain` varchar(16) NOT NULL DEFAULT 'default',
  `permission` varchar(16) NOT NULL DEFAULT 'view',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `component_2` (`component`,`site`,`subdomain`,`page`),
  KEY `component` (`component`),
  KEY `site` (`site`),
  KEY `permission` (`permission`),
  KEY `subdomain` (`subdomain`),
  KEY `page` (`page`),
  CONSTRAINT `pages_components_ibfk_2` FOREIGN KEY (`permission`) REFERENCES `permissions` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `pages_components_ibfk_3` FOREIGN KEY (`site`) REFERENCES `sites` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_components`
--

LOCK TABLES `pages_components` WRITE;
/*!40000 ALTER TABLE `pages_components` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_content`
--

DROP TABLE IF EXISTS `pages_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_content` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `page` varchar(64) DEFAULT NULL,
  `site` varchar(16) NOT NULL DEFAULT 'default',
  `subdomain` varchar(16) NOT NULL DEFAULT 'default',
  `type` varchar(16) NOT NULL DEFAULT 'html',
  `content` text,
  `hook` varchar(32) NOT NULL DEFAULT 'BODY',
  `hierarchy` int(4) unsigned zerofill NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  `author` varchar(16) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `permission` varchar(16) NOT NULL DEFAULT 'view',
  PRIMARY KEY (`id`),
  KEY `page` (`page`),
  KEY `type` (`type`),
  KEY `author` (`author`),
  KEY `type_2` (`type`),
  KEY `hook` (`hook`),
  KEY `permission` (`permission`),
  KEY `site` (`site`),
  KEY `site_2` (`site`),
  KEY `subdomain` (`subdomain`),
  KEY `site_3` (`site`,`subdomain`,`page`),
  CONSTRAINT `pages_content_ibfk_2` FOREIGN KEY (`site`) REFERENCES `sites` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pages_content_ibfk_3` FOREIGN KEY (`permission`) REFERENCES `permissions` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pages_content_ibfk_4` FOREIGN KEY (`subdomain`) REFERENCES `sites_subdomains` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pages_content_ibfk_5` FOREIGN KEY (`type`) REFERENCES `pages_content_types` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pages_content_ibfk_6` FOREIGN KEY (`hook`) REFERENCES `hooks` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pages_content_ibfk_7` FOREIGN KEY (`site`, `subdomain`, `page`) REFERENCES `pages` (`site`, `subdomain`, `slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_content`
--

LOCK TABLES `pages_content` WRITE;
/*!40000 ALTER TABLE `pages_content` DISABLE KEYS */;
INSERT INTO `pages_content` VALUES (1,'login','default','default','template','index.phtml','BODY',0001,1,1,NULL,'2014-07-13 11:23:07','2014-07-18 02:24:51','view'),(3,'dashboard','default','default','template','newsbox.phtml','DASHMODULES',0001,1,1,NULL,'2014-07-13 11:23:07','2014-08-02 22:14:15','registered'),(4,'error','default','default','template','index.phtml','BODY',0001,1,1,NULL,'2014-07-13 11:23:07','2014-07-18 02:24:45','view'),(5,'docs','default','default','template','index.phtml','BODY',0001,1,1,NULL,'2014-07-13 11:23:07','2014-07-18 02:24:43','view'),(6,'index','default','default','template','index.phtml','BODY',0001,1,1,NULL,'2014-07-13 11:23:07','2014-07-18 02:24:40','view'),(13,'index','default','default','template','footer.phtml','FOOTER',0001,1,1,NULL,'2014-07-16 22:30:48','2014-07-18 01:39:04','view'),(14,'index','default','default','template','sidebar.phtml','SIDEBAR',0001,1,1,NULL,'2014-07-17 19:38:54','2014-08-02 06:35:07','view'),(15,'about','default','default','template','index.phtml','BODY',0001,1,1,NULL,'2014-07-17 20:23:32','2014-07-18 02:24:27','view'),(16,'dashboard','default','default','template','sidebar.phtml','SIDEBAR',0001,1,1,NULL,'2014-07-13 11:23:07','2014-08-02 06:34:43','view'),(17,'docs','default','default','template','sidebar.phtml','SIDEBAR',0001,1,1,NULL,'2014-07-13 11:23:07','2014-08-02 06:34:48','view'),(24,'permissions','default','default','template','sidebar.phtml','SIDEBAR',0001,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 06:34:57','view'),(27,'docs','default','default','html','<h3 id=\"developer\">Developer Documentation</h3><hr>','DEVELOPERDOCS',0001,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 19:48:28','developer'),(28,'docs','default','default','html','<h4 id=\"class-structure\">Class Structure</h4>\n<p>\n	Oroboros uses four primary classifications of php class: <a href=\"#docs-class-structure-models\">models</a>, <a href=\"#docs-class-structure-controllers\">controllers</a>, <a href=\"#docs-class-structure-adapters\">adapters</a>, and <a href=\"#docs-class-structure-libs\">libs</a>. In order to better understand the purpose of each, they are explained below in some detail.<br><br>\n<ul>\n<li id=\"docs-class-structure-models\">\n<h5>Models</h5>\n<p>Models perform the logical functionality of a page. They communicate with the database, handle most of the logic crunching, reference libs, and return the final output. Most of the heavy lifting in the codebase is performed by models, or is initiated by models and passed out to libs to perform functionality that will be reused throughout the site.</p>\n</li>\n<li id=\"docs-class-structure-controllers\">\n<h5>Controllers</h5>\n<p>Controllers act as an intermediary between the end user and the model. They accept user input and pass it to the model to be processed, and take the returned output from the model and send it to the viewer. The primary purpose of the controller within this system is to provide a filter between user-submitted input and the model, and to group large chains of code into logical groupings that can be addressed with a single point of reference.</p>\n</li>\n<li id=\"docs-class-structure-adapters\">\n<h5>Adapters</h5>\n<p>Adapters handle the system\'s needs to interface with other systems or programming languages. For example an adapter might handle the passage of data from a wordpress installation to the Oroboros core or vice versa, or they might handle other development efforts outside the scope of php, such as interfacing with perl, ruby, c++ or shell scripts. Adapters function similar to controllers, but at a deeper level than the controller. They likewise handle the passage of data back and forth in a logical manner, but are generally referenced by models or libs needing to extend their functionality in some way.</p>\n</li>\n<li id=\"docs-class-structure-libs\">\n<h5>Libs</h5>\n<p>Libs handle complex data operations much like models, but do so in a way that can be reused in numerous applications throughout the system rather than having a focused scope like a model would. Models often reference libs in order to handle generalized logic that requires a significant number of calculations, or functionality that will be needed in numerous places in an effort to keep the code base lighter and practice DRY (don\'t-repeat-yourself) programming habits. This makes bugs easier to address and keeps the system leaner than it would be if all of the data were handled by the models.</p>\n</li>\n</ul>\n</p>','DEVELOPERDOCS',0004,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 22:16:58','developer'),(29,'logs','default','default','template','logs.phtml','DASHMODULES',0000,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 09:41:46','operator'),(30,'docs','default','default','template','index.phtml','DASHMODULES',0000,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 19:47:32','registered'),(31,'docs','default','default','html','<h3 id=\"architect\">System Architect Documentation</h3><hr>','ARCHITECTDOCS',0001,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 22:13:57','architect'),(32,'docs','default','default','html','<h3 id=\"owner\">Site Owner Documentation</h3><hr>','OWNERDOCS',0001,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 22:13:21','owner'),(33,'docs','default','default','html','<h3 id=\"webmaster\">Webmaster Documentation</h3><hr>','WEBMASTERDOCS',0001,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 22:13:37','webmaster'),(34,'docs','default','default','html','<h3 id=\"operator\">Operator Documentation</h3><hr>','OPERATORDOCS',0001,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 22:13:15','operator'),(35,'docs','default','default','html','<h3 id=\"admin\">Admin Documentation</h3><hr>','ADMINDOCS',0001,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 22:14:05','admin'),(36,'docs','default','default','html','<h3 id=\"moderator\">Moderator Documentation</h3><hr>','MODERATORDOCS',0001,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 22:13:08','moderator'),(37,'docs','default','default','html','<h3 id=\"author\">Content Author Documentation</h3><hr>','AUTHORDOCS',0001,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 22:13:47','author'),(38,'docs','default','default','html','<h3 id=\"user\">Registered User Documentation</h3><hr>','USERDOCS',0001,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 22:13:30','registered'),(39,'docs','default','default','html','<span id=\"architect-account-scope\">\n<h4>System Architect Account Scope</h4>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;The purpose of the system architect account is to perform development on the core system architecture. As this is a critical task to system development, system architects are afforded all user permissions available in the system to insure that they are capable of troubleshooting any issue that may arise. Due to this, there is considerable responsibility that comes with access to this account. Great care must be taken not to tamper with active user accounts or sensitive data. Due to this concern, the installation shipment of this system does not include system architect accounts or permissions, and any code relating to system architect usage is stripped before the end product is sent for production and general usage.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;Your primary role as a system architect is to extend the core functionality of the system to make the overall package more usable to the full scope of needs of the end users. Most of your development work will occur directly on the core fileset, though occasional attention may be needed in the extension of modules, components, themes, templates, scripts, or other extensions that ship with the core package or are available as core components to the general public.\n<br><br>\n</p>\n</span>','ARCHITECTDOCS',0002,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 22:14:01','architect'),(40,'docs','default','default','html','<span id=\"owner-account-scope\">\n<h4>Owner Account Scope</h4>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;The purpose of the owner account is to manage the overall usage of the system. This is the account group that is set for the user registered upon initial installation of the system, and only this account may add others to this same group. As the owner of the installation, you are entirely liable for the responsible use of the system, and the actions of all subsidiary accounts registered within your installation. You may perform all administrative, development, design, moderation and authorship tasks directly, or you may delegate them out to other individuals and assign them group access rights in accordance with their roles. Permissions may also be granted or removed at both the individual and group level, so if you have a high performing member who you want to grant additional access to you may do so without changing their group, and similarly you may restrict individual access rights directly from individuals without fully demoting them if they are making poor use of one or two features but otherwise performing their role as expected. It is up to you to decide whom is eligible for any given role, and accord them the correct access rights and group assignment as needed. Developers, webmasters and site operators may also perform this task, though their ability to do so is restricted to their account level and below, and they may not elevate other users to the same group they are in, nor may they grant permissions that are set in the group hierarchy at their own group level, only those which are further down the chain if inheritance. Only the owner may grant permissions that are set at their own usage group, as there is no higher group in the system (the core development team uses a System Architect group that has further permissions, though this is used for core development and is not shipped with your installation, nor is any of the codebase that supports these permissions).\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;The ownership account role is quite diverse, and it may be easy to get overwhelmed by the vast variety of settings and options available to you if you are not yet familiar with the system. For this reason a means of artificially loading the site as any lower user group is provided, so that you can see the only the options available to that user set without being distracted by the full scope of features when performing specific tasks. Unlike other accounts (developer excluded), you may load the site as any lower group without affecting your primary access rights. This will remove options and links that are out of scope for the account group you are currently viewing as, though you will still have direct access to any pages that exceed that group\'s access rights by typing them directly into the url bar. You can also quickly toggle access into any other group to reveal options that would be available to that group, or return to the ownership view to see all of the options available.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;It is suggested that you make use of the workspaces feature in order to quickly perform tasks without getting distracted, and set up a workspace instance for each type of role that you need to perform. This will let you quickly cycle through various task sets without the need to refresh the page or keep changing your core access view. By default, all registered users have access to the workspaces functionality, but you can restrict this to a specific group if you wish by adjusting the permissions level if you wish.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;Please take some time to familiarize yourself with each of the other account usage sections in order to digest the full scope of options available to you. There are also specific owner-based options available, which are listed below. Keep in mind that when viewing the documentation for this system, only the entries that are applicable to the specific account access rights (by default, this can be changed if you wish) are viewable to the groups viewing this page (ie: developers will not see the ownership documentation, but will see all of the following sections, webmasters will not see owner or developer documentation, but will see the remaining sections, etc.).\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;Thank you for choosing to use the Oroboros system, we sincerely hope that it meets your needs as expected.\n<br><br>\n</p>\n</span>','OWNERDOCS',0002,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 22:13:26','owner'),(41,'docs','default','default','html','<span id=\"developer-account-scope\">\n<h4>Developer Account Scope</h4>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;The purpose of the developer account is primarily to extend the functionality of the core system as needed for your specific system needs, as the core provides a very broad scope of generalized functionality but is not going to be able to account for all of your specific needs out of the box. You may also use this account to build or extend modules, components, templates, themes, or other extensions by working directly with the codebase. Designers also have some access to these functions, but are generally separated from the codebase and will need to perform their duties by modifying css, html, and using the hook system to call upon core functionality without directly modifying the codebase. This provides a degree of separation between the development and design team, which prevents each from causing conflicts that may cause erratic or broken system behavior to arise. You as a developer are charged with the extension and troubleshooting of the overall system installation, and as such numerous tools for development, debugging, and modification of the core functionality are provided to you. Care must be taken not to damage critical data or ongoing user accounts, as you may often be working with data that is also being called upon by the user base. The system provides a number of features to create separation between the live site and development efforts, which you should familiarize yourself with so that you are not impeding the user base while performing system modifications. Please take a few moments to read over the documentation for the <i>integrated staging system</i>, as this is your primary means of working on the live site without disrupting live site functionality. Traditionally, you are probably familiar with setting up a completely separate staging site installation, or pulling a site down and doing development in a sandbox environment. Oroboros has this functionality built in, so that you can stage changes that do not affect the live site directly within the core installation, and then flag them as complete when they are functional and have been fully tested, at which point they immediately become available on the live site. This saves the time and energy required to reinstall an entire site, and also prevents the need to reconfigure the installation settings twice to match up with your staging directory, and then subsequently again when pushed back to live.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;The developer account also has access to the functionality of webmasters, operators, designers, admins, moderators and authors, in addition to all of the access rights of a standard registered user. In this manner you can test changes you have made as if you were performing from one of these roles to insure that changes that affect these roles are correctly functional and will not cause problems before being flagged as complete and applied to the primary system. You also have access to the account view module, which lets you preview any part of the site as if you were a member of any other group (or any other specific account also). This gives you a great degree of granular control over site development, and it is suggested that you employ ample use of this feature.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;You will also want to familiarize yourself with the workspaces component, which allows you to create multiple page instances from your dashboard and toggle through them at will without modifying the page. You may use this in conjunction with the account view module if you wish, and assign specific access rights to each workspace, or you may use separate workspaces to streamline your workflow, providing the toolset needed to specific tasks on each workspace (or both).\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;Please take some time to familiarize yourself with the developer documentation, and spend a bit of time reading over the core codebase and database structure. This will allow you to quickly get a better idea of how the system functions, and allow you to more efficiently modify the system as needed. Please also familiarize yourself with the debugging module, which can (when enabled) provide direct links back to relevant documentation so you can quickly get a better picture of what is going on in the event that an issue arises.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;Thank you for choosing to develop on the Oroboros platform. We welcome your efforts and expertise to our family of development professionals wholeheartedly, and we hope the functionality of the system meets (and exceeds) your expectations.\n</p>\n</span>','DEVELOPERDOCS',0002,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 22:15:22','developer'),(42,'docs','default','default','html','<span id=\"webmaster-account-scope\">\n<h4>Webmaster Account Scope</h4>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;The purpose of the webmaster account is to construct and manage the breadth of sites within the entire system installation. This account is not so much concerned with programming or extending core functionality, but more so as a top level manager of sites, content and functionality. You will primarily be concerned with setting up sites, analyzing traffic patterns, insuring seo is performed correctly, configuring connections to other nodes (remote installations of Oroboros that can be linked to in order to share content, accounts, assets, or act as remote mirrors to mitigate server load). There are a number of specific tasks of great importance assigned to this account type, such as managing DNS, SSL, configuring installation security, monitoring traffic patterns and insuring that the entire network of sites in your charge remains functional and performs optimally. You may also delegate a number of these responsibilities to site operators, who have most of the same permissions as the webmaster, but are restricted to single site installations. The webmaster account has access to the full breadth of sites within the entire network, and can manage settings that apply globally rather than only to a single website. Please keep in mind if you are delegating tasks out to site operators of this restriction, and insure that you do not assign them a task that must be performed in the global scope, as they will be unable to fulfill the request (unless you elevate their permissions, see the section on group and individual permission assignment below).\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;The webmaster account also has access to the functionality of operators, designers, admins, moderators and authors, in addition to all of the access rights of a standard registered user. This insures that you are able to modify the functionality of the system in any particular way needed if one of those accounts is not available to perform the task. You do not have direct access to the codebase, as modification of the codebase can break the entire network if it is not done responsibly, so that task is delegated to developers, who have elevated permissions above the webmaster level. Please consult your registered site owner if you require access to this functionality, and he can grant you permission as needed.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;You will want to familiarize yourself with the workspaces component, which allows you to create multiple page instances from your dashboard and toggle through them at will without modifying the page. You may use separate workspaces to streamline your workflow, providing the toolset needed to specific tasks on each workspace in order to organize your workflow in a concise and logical manner and work on specific tasks without being distracted by a number of other features and settings that are not relevant to the task at hand. Each workspace performs as an individual page load, each of which can be directed individually without refreshing the primary window, so you will not lose any changes in other workspaces when submitting a page in this manner.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;Please take some time to familiarize yourself with the webmaster documentation, which will walk you through the correct setup and application of the system, and how to customize functionality to meet your specific needs. This system can easily handle multiple websites, which is as simple as directing the DNS to this file and visiting it once while logged in as a webmaster to register the dns in the system, then creating a new site or assigning the domain to an existing site within the system. You can also forward sites to other nodes if you are implementing dynamic load balancing and have connected with remote nodes and granted them the correct access rights to inherit any content needed to mirror your site correctly. You can also use this feature to set up private networks, private CDN\'s, or any other specific application you can imagine. You will want to familiarize yourself thoroughly with Synapse (the node network system) to insure that you are correctly setting these connections up and have strong security. Ultimately it is at your discretion how much access to grant to any specific node, so please insure that you are not granting a high level of access to remote nodes unless they are coming from trusted sources.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;Thank you for choosing the the Oroboros platform to meet your site building needs. We welcome your efforts and expertise to our family of web professionals wholeheartedly, and we hope the functionality of the system meets (and exceeds) your expectations.\n</p>\n</span>','WEBMASTERDOCS',0002,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 22:13:42','webmaster'),(43,'docs','default','default','html','<span id=\"operator-account-scope\">\n<h4>Operator Account Scope</h4>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;The purpose of the operator account is to construct and manage the settings and overall performance of a single site installation (unless you are assigned access to additional sites by the webmaster, developer or owner). This account is primarily concerned with managing overall site performance and functionality. You will primarily be concerned with setting up the site, analyzing traffic patterns, and insuring seo is performed correctly. There are a number of specific tasks of great importance assigned to this account type, such as managing SSL, configuring site security, monitoring traffic patterns and insuring that the site in your charge remains functional and performs optimally. You may also delegate a number of these responsibilities to site admins and moderators, who have many of the same permissions as the operator, but are restricted from making changes that affect overall site performance. The operator account has access to the full breadth of settings that pertain to this specific site installation, and may create pages, content, users, subdomains, install modules, components, templates, themes, or other packages specific to this site installation. Please keep in mind if you are delegating tasks out to site admins or moderators that they do not have access to sitewide changes (beyond creating pages and content), and as a result of this restriction insure that you do not assign them a task that must be performed in the sitewide scope, as they will be unable to fulfill the request (unless you elevate their permissions, see the section on group and individual permission assignment below).\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;The operator account also has access to the functionality of designers, admins, moderators, authors, and regular registered users. This insures that you are able to modify the functionality of the system in any particular way needed if one of those accounts is not available to perform the task. You do not have direct access to the codebase, nor do you have access (by default) to other site installations in the network, as these permissions allow global changes that can adversely affect the entire site network if not used responsibly. Please consult your webmaster, developer, or registered site owner if you require access to this functionality, as they can grant you permission as needed.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;You will want to familiarize yourself with the workspaces component, which allows you to create multiple page instances from your dashboard and toggle through them at will without modifying the page. You may use separate workspaces to streamline your workflow, providing the toolset needed to specific tasks on each workspace in order to organize your workflow in a concise and logical manner and work on specific tasks without being distracted by a number of other features and settings that are not relevant to the task at hand. Each workspace performs as an individual page load, each of which can be directed individually without refreshing the primary window, so you will not lose any changes in other workspaces when submitting a page in this manner.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;Please take some time to familiarize yourself with the operator documentation, which will walk you through the correct setup and application of the system as it pertains to this site, and how to customize functionality to meet your specific needs. You will be able to place any part of the site (or the entire site itself) into maintenance mode if changes are required, manage the whitelist, blacklist and greylist, and can promote or demote other users to any account type up to the admin level. You can also assign any permissions below your account level to any other group or individual whose account level does not meet or exceed your own. Please review the documentation on permissions thoroughly, as a deep understanding of the permission system will greatly aid you in your delegation of tasks and setup of access rights for lower level accounts.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;Thank you for choosing the the Oroboros platform to meet your site building needs. We welcome your efforts and expertise to our family of web professionals wholeheartedly, and we hope the functionality of the system meets (and exceeds) your expectations.\n</p>\n</span>','OPERATORDOCS',0002,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 22:13:19','operator'),(44,'docs','default','default','html','<span id=\"admin-account-scope\">\n<h4>Admin Account Scope</h4>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;The purpose of the admin account is to manage content and users throughout the site, and act in a supervisory role to moderators and content authors. You will have the ability to directly create accounts, modify their access rights, create content, manage settings for various aspects of the site, and perform a number of other administrative tasks outlined below. You also have all of the permissions granted to moderators and content authors, and may perform any of their duties as needed if they are not available to do so. You do not have access to functionality that directly modifies content or functionality sitewide, though you may perform tasks that affect the usage of lower level accounts at the sitewide level. If you need to perform a task that exceeds your access rights, please consult the site operator, webmaster, developer or registered site owner, and they can grant you permission as needed.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;You will want to familiarize yourself with the workspaces component, which allows you to create multiple page instances from your dashboard and toggle through them at will without modifying the page. You may use separate workspaces to streamline your workflow, providing the toolset needed to specific tasks on each workspace in order to organize your workflow in a concise and logical manner and work on specific tasks without being distracted by a number of other features and settings that are not relevant to the task at hand. Each workspace performs as an individual page load, each of which can be directed individually without refreshing the primary window, so you will not lose any changes in other workspaces when submitting a page in this manner.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;Please take some time to familiarize yourself with the admin documentation, which will walk you through the configuration of settings and how they affect the site and user base, and how to customize functionality to meet your specific needs. You will be able to place any specific page of the site into maintenance mode if changes are required, manage the whitelist, blacklist and greylist, and can promote or demote other users to any account type up to the moderator level. You can also perform a number of administrative tasks that affect the access rights of lower level accounts (though you cannot directly assign permissions). Please review the documentation on admin functionality, as a thorough understanding of the options available to you will greatly aid your management of lower level accounts and site functionality.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;Thank you for choosing the the Oroboros platform to meet your site building needs. We welcome your efforts and expertise to our family of web professionals wholeheartedly, and we hope the functionality of the system meets (and exceeds) your expectations.\n</p>\n</span>','ADMINDOCS',0002,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 22:14:07','admin'),(45,'docs','default','default','html','<span id=\"moderator-account-scope\">\n<h4>Moderator Account Scope</h4>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;The purpose of the moderator account is to manage the actions, content, and usage of the site by registered users and content authors, insure that content and performance conform to the expected guidelines set by the ownership, and moderate user behavior and disputes, acting as an intermediary between users and higher level accounts who have numerous responsibilities regarding the overall functionality of the site and generally will not have time to monitor user behavior directly. You will also be acting in a supervisory role to content authors, and insure that they are submitting content that is appropriate to the site, and that it is well formed, grammatically correct, and structured in a way that displays properly. You have a number of tools at your disposal to manage lower level accounts, and can ban, suspend, or monitor or restrict lower level accounts, delete, modify or flag content for review, and may monitor the notes, messages, emails, content and other correspondence of lower level accounts (except when they are transmitted to accounts with access rights above your own).\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;You will want to familiarize yourself with the workspaces component, which allows you to create multiple page instances from your dashboard and toggle through them at will without modifying the page. You may use separate workspaces to streamline your workflow, providing the toolset needed to specific tasks on each workspace in order to organize your workflow in a concise and logical manner and work on specific tasks without being distracted by a number of other features and settings that are not relevant to the task at hand. Each workspace performs as an individual page load, each of which can be directed individually without refreshing the primary window, so you will not lose any changes in other workspaces when submitting a page in this manner.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;Please take some time to familiarize yourself with the moderator documentation, which will walk you through the proper management of content and the user base using the tools available to you, and how to customize functionality to meet your specific needs. You will also be able to promote or demote registered users content authors, or may demote content authors back to the registered user level. If your site has premium or VIP member settings, you will be able to manage those as well. You will also be able to monitor subversive behavior, and can flag accounts for administrative review when detected if they need attention that exceeds your access rights.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;Thank you for choosing the the Oroboros platform to meet your site building needs. We welcome your efforts and expertise to our family of web professionals wholeheartedly, and we hope the functionality of the system meets (and exceeds) your expectations.\n</p>\n</span>','MODERATORDOCS',0002,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 22:13:12','moderator'),(46,'docs','default','default','html','<span id=\"author-account-scope\">\n<h4>Content Author Account Scope</h4>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;The purpose of the content author account is to create dynamic and interesting content for the site. You have permissions that are elevated above the rights of standard users, in that you can contribute content that will render to the site directly beyond standard comments, calendar entries, and other inter-user correspondence. Your content will be reviewed by the moderator and administrative staff to insure that it conforms to the guidelines set for the site by the registered owner. If you abuse your usage rights, you may be banned, suspende or demoted back to standard registered user level, depending on the nature and frequency of the infraction(s).\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;You have a number of content creation tools available to you, listed under the content panel on the dashboard. Full documentation on how to effectively use these tools is displayed below, and you should spend a bit of time familiarizing yourself with the full range of functionality now available to you.  Please bear in mind that this is generalized documentation that ships with the original system installation, and some functionality may not be available to you if higher level accounts have changed configurations that restrict it or extend functionality in a way beyond the core components, and have omitted documenting their changes here.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;Please take some time to familiarize yourself with the content author documentation, which will walk you through the proper management of content and the user base using the tools available to you, and how to customize functionality to meet your specific needs. You will also be able to promote or demote registered users content authors, or may demote content authors back to the registered user level. If your site has premium or VIP member settings, you will be able to manage those as well. You will also be able to monitor subversive behavior, and can flag accounts for administrative review when detected if they need attention that exceeds your access rights.\n<br><br>\nWe would like to congradulate you on your acceptance as a quality content author within this site. We welcome your efforts and the rich content that you will inevitably bring to the user base.\n</p>\n</span>','AUTHORDOCS',0002,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 22:13:51','author'),(47,'docs','default','default','html','<span id=\"user-account-scope\">\n<h4>Registered User Account Scope</h4>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;Welcome to the dashboard for registered users! This site has a number of features that allow you to interact with the user base and user a number of features not available to non-registered users. Below you will find thorough documentation on the options available to you, and how to effectively use them to your advantage within the system. The default system installation provides a wide breadth of features to registered users, however these features may be extended or restricted at the discretion of the site staff, and there may be more or less available to you based on modifications made to the system and whether or not they have modified the documentation to reflect these changes. If you find undocumented options, or documentation on options that are not available to you, please notify the site staff so they can correct the documentation to reflect these changes. Thank you for joining our site, enjoy your stay!\n<br><br>\n</p>\n</span>','USERDOCS',0002,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 22:13:34','registered'),(48,'docs','default','default','html','<span id=\"developer-tools\">\n<h4>Developer Tools</h4>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;Below is a brief description of the toolset available to you as a developer within this system. You will find a detailed explanation as to the purpose and application of each tool available to you based on your own specific permissions level for your group and account. Please take a few minutes to review each tool at your disposal thoroughly.\n</p>\n<hr>\n[]DEVTOOLS[/]\n</span>','DEVELOPERDOCS',0003,1,1,NULL,'0000-00-00 00:00:00','2014-08-03 00:04:22','developer'),(49,'docs','default','default','html','<h3 id=\"designer\">Designer Documentation</h3><hr>','DESIGNERDOCS',0001,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 19:48:28','developer'),(50,'docs','default','default','html','<span id=\"designer-account-scope\">\n<h4>Developer Account Scope</h4>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;The purpose of the designer account is to create a rich and rewarding user experience for the user base, and to insure that content and functionality on both the front and backend of the site are presented in an intuitive and easy to use manner. The designer account has access to modifying the templates, themes, content, and scripting of the site, but is restricted from working with the server side codebase. If you need this functionality for your endeavors (such as building out ajax functionality or similar), please consult the registered site owner to be granted access to these features. You are provided with a design sandbox environment, in which you can test changes without applying them to the live site to insure that the UI/UX functionality is seamless before exposing your work to the broader user base. This can be done in two ways. For changes that only you will be viewing, these specifications will be loaded based on your individual login session, so only you will be able to see these changes, and only when you are logged in to the system. For group design efforts, a sandbox environment can be applied, in which case all members registered to that design group will be able to view and modify these changes, but they will still not apply to any other user unless they are granted preview mode access. Please consult the documentation on the <i>design sandbox environment</i> below for more information on this feature. This feature can be applied to themes, templates, content, scripts, and any other content that is not regulated by the server programmatically.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;The designer account also has access to the functionality of operators, admins, moderators and authors, in addition to all of the access rights of a standard registered user. In this manner you can test changes you have made as if you were performing from one of these roles to insure that your design efforts are stylish and seamless before being flagged as complete and applied to the primary system. You also have access to the account view module (though in the case of design accounts, functionality will be disabled for accounts that elevate your own, though you can still view the page layout to perform design tasks), which lets you preview any part of the site as if you were a member of any other group (or any other specific account also). This gives you a great degree of granular control over site design, and it is suggested that you employ ample use of this feature.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;You will also want to familiarize yourself with the workspaces component, which allows you to create multiple page instances from your dashboard and toggle through them at will without modifying the page. You may use this in conjunction with the account view module if you wish, and assign specific layouts to each workspace, or you may use separate workspaces to streamline your workflow, providing the toolset needed to specific tasks on each workspace (or both).\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;Please take some time to familiarize yourself with the design documentation, and spend a bit of time reading over the core codebase and database structure. This will allow you to quickly get a better idea of how the system functions, and allow you to more efficiently modify the system as needed. Please also familiarize yourself with the specifications for theme and template design, as they generate css, html, and javascript in a dynamic manner that is a bit different from standard stylesheets or scripts. Whenever possible, avoid directly editing the css or javascript templates and apply additional stylesheets or scripts through the system infrastructure, or alternately attach separate stylesheets or scripts to the load chain if you want to write your own externally, and specify the load order, theme, template, and/or site(s)/page(s) in which they should be loaded. The system will automatically generate a minified and validated stylesheet and script file for the end user based on the assets required for that specific page view based on access rights if the system is used correctly, and these will then be cached until changes are made on the backend. This process requires working through the system functionality, but great effort has been made to insure that it is as natural and intuitive as possible, and is not restrictive or difficult compared to standard styling or scripting. There are also a number of features provided to insure that colors, margins, typefaces and other aspects of your workflow can be made easily consistent throughout the design, even where different stylistic classes are concerned, without the need to pepper the markup with a million classes in order to attach the same functionality. You will be able to write a small number of css rules, and individual aspects of design can be referenced through the template or theme settings specifications. This can also be done in the javascript codebase, where classes, variables, and parameters can be automatically generated by the server to insure that all data remains consistent, and there are never broken variables (or if they are broken, they will be broken everywhere they are applied, and consequently easier to isolate and correct). If you have developer access, you can write php directly into the script files or stylesheets. If you do not have developer access, you can still use content or function hooks to call upon core system functionality, but these are limited to only the content and functionality defined by the development team so they cannot be used in a subversive manner. The design system is actually quite powerful once you get the hang of it, and it is designed to replicate normal design methodology as closely as possible. There are also tools provided to edit images, modify typeface and typesetting (just as you would for normal print design), automatically convert units (px, %, em, ex, etc), and generate in-depth color schemes with a broad range of color schemes, color sheets with variants of tints, shades, and tones that will be automatically saved as a swatch set based on your own color scheme selections, and then can be referenced throughout your design process to insure that you have a dynamic and unified presentation. These can also be exported for offsite use if desired, if you want to comp in photoshop, illustrator, or another design program of your choosing.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;Thank you for choosing to develop on the Oroboros platform. We have put great effort into insuring that our design platform is as intuitive and powerful as possible, and we hope you enjoy your design experience. We welcome your efforts and expertise to our family of design professionals wholeheartedly, and we hope the functionality of the system meets (and exceeds) your expectations. \n</p>\n</span>','DESIGNERDOCS',0002,1,1,NULL,'0000-00-00 00:00:00','2014-08-02 22:15:22','developer'),(51,'docs','default','default','html','<span id=\"devtools-logs-main\">\n<h4>System Logs</h4>\n<p>\n<h5><b>Location:</b> <a href=\"[]HOSTURL[/]/logs\">System Logs</a></h5>\n&nbsp;&nbsp;&nbsp;&nbsp;The system logs panel will allow you to quickly view how each aspect of the system is functioning, and get a broader overview of errors and warnings that indicate that the site is not performing up to expectations. These are logged both in the database as well as to flatfiles, so if the system becomes inoperable for some reason the logs can still be consulted to troubleshoot the problem. Either of these can be disabled to conserve memory and storage space if required, and automatic archiving can be set up to further reduce the storage overhead but still keep logs accessible. Both logs and archives can be flushed or exported on a schedule to insure that the server is never overburdened, and that the correct parties always have quick access to logs in the event of a problem.\n<br><br>\n&nbsp;&nbsp;&nbsp;&nbsp;System logs are broken down into several individual logs that each evaluate the performance of various components. These can be modified from the settings for each actual part of the system. By default, logs are written in the following pattern (if not configured otherwise):\n<ul>\n<li>\n<h6>System Log</h6>\n<p><b>Location: </b>[]LOGFILES[/]/system.log</p>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;This is the system log, which logs errors compiled within the core and local system directory based on the primary serverside load order. If classes have broken methods, un-included dependencies, or throw other errors that affect overall system performance, those instances will be logged into this file. This is probably the most important set of problems to debug when issues arise, as any issues here will affect all sites in the network if they cause erratic behavior.\n</p>\n</li>\n<li>\n<h6>Staging Log</h6>\n<p><b>Location: </b>[]LOGFILES[/]/staging.log</p>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;This is the staging log, which handles errors thrown by the built in sandbox environment. This log generates similarly to the system log, however errors logged here will not affect end users, and only are thrown when the page is loaded within the integrated sandbox environment by the development team. This file should be used to insure that system modifications and extensions that you are working on are correctly addressed before flagging the changes as complete and moving them to the local directory.\n</p>\n</li>\n<li>\n<h6>Site Log</h6>\n<p><b>Location: </b>[]LOGFILES[/]/site.log</p>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;This is the site log, which will log errors thrown by specific sites that are not relevant to the core codebase. These may be from broken links, missing content, or other concerns which affect the end user experience but are generally not related to errors in the codebase itself. Most of these errors can be corrected by applying the correct site settings somewhere in the backend, or insuring that links, content templates or database entries, and media are correctly accounted for.\n</p>\n</li>\n<li>\n<h6>Database Log</h6>\n<p><b>Location: </b>[]LOGFILES[/]/sql.log</p>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;This is the database log, which will log sql errors and warnings (and optionally null queries, which is disabled by default to prevent the log file from growing enormous). This log should be referenced when links are broken or assets are missing even when they still exist in the file structure. Most of these entries can be corrected by insuring that the correct database records are in place, and that database connection details are correct, and that in the case of remote databases, both ends have the correct access rights on file.\n</p>\n</li>\n<li>\n<h6>Ajax Log</h6>\n<p><b>Location: </b>[]LOGFILES[/]/ajax.log</p>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;This is the ajax log, which tracks issues with the ajax engine when dynamically retrieving content or applying functionality. These errors are usually the result of an inaccessible ajax function, a poorly formed request, an incorrect nonce, or incorrect user input. Some of these errors may indicate deeper issues in the codebase, but often they can be corrected through adjusting the correct settings or insuring that the class being referenced is listed in the ajax function register. Ajax errors may also indicate a connection issue, in which case the port and address specifications should be examined to insure that they are functioning correctly.\n</p>\n</li>\n<li>\n<h6>Stylesheet Log</h6>\n<p><b>Location: </b>[]LOGFILES[/]/css.log</p>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;This is the stylesheet log, which tracks problems with the stylesheet generation engine. It will log missing files, validation errors, and problems with the stylesheet codebase in the process of generating a stylesheet. These errors generally indicate a malformed stylesheet, which is usually due to an incorrectly configured theme, template, component, module, site, or page stylesheet, or may also reference files that have been manually removed outside the core system, and are still registered as present within the system. Most of these errors can be easily remedied by insuring that the relevant settings are well formed and accurate, all stylesheets are accounted for or removed from the database, or errors in css syntax are corrected when logged by the validator.\n</p>\n</li>\n<li>\n<h6>Javascript Log</h6>\n<p><b>Location: </b>[]LOGFILES[/]/js.log</p>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;This log will track issues with the javascript render engine, and also returns errors thrown at runtime so that errors in scripting can be corrected. The vast majority of these errors indicate broken javascript, however there may also be issues with the render engine itself, in which case some server side modifications may be needed to correct the issue. These will generally not occur unless the javascript engine has been extended or modified with additional installation-specific functionality. Scripting errors may arise from custom scripts or poorly written third-party components, modules, theme or template script files that contain errors or do not adhere to the Oroboros scripting specifications.\n</p>\n</li>\n<li>\n<h6>Module Log</h6>\n<p><b>Location: </b>[]LOGFILES[/]/module.log</p>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;This log tracks errors with modules. The system is designed to disable modules when they throw fatal or critical errors in order to insure that the core system is not compromised by a poorly written module, or occasionally by incorrect file permissions. Errors in this log usually indicate a badly written module, which should not occur if the module has been properly imported and validated before being activated. If you are developing a module directly through the system, or have performed a manual installation without validating the module correctly, it is likely that you may see some errors here. As modules can be recycled on a number of pages and sites, you should expect that bad behavior will occur wherever the specific offending module is active.\n</p>\n</li>\n<li>\n<h6>Component Log</h6>\n<p><b>Location: </b>[]LOGFILES[/]/component.log</p>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;This log tracks errors with component functionality. Components typically do not perform much core system functionality, but errors with components may make the interface buggy and detract from the end user experience, thereby driving traffic away from your site. Component errors are generally the result of misconfigured settings files, malformed or unregistered hooks, or template files that have been renamed or modified outside the system. They may also indicate that file permissions are incorrect. These issues are generally pretty easy to correct, but should be addressed promptly to insure that the end user experience is not compromised.\n</p>\n</li>\n<li>\n<h6>User Behavior Log</h6>\n<p><b>Location: </b>[]LOGFILES[/]/user.log</p>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;This log tracks user behavior that is questionable or troublesome. Users who repeatedly try to access pages they are not allowed to view, or pages that normally would only be referenced by a form submission or other backend function will be logged here. This file will also log repetitive connection attempts that are beyond the scope of human interaction, or attempts to reference content in ways that would not be human achievable. Numerous invalid password attempts within a short timespan will also be logged here, as well as instances when the permission system fails and users gain access to content they should not have. These log entries can help determine possible hack attempts, attack vectors, and content-scraping attempts. If you are seeing a lot of errors or warnings here, you should consider blacklisting the offending accounts or ip addresses, or limiting their access until the problem subsides. Users who routinely show up on this list will automatically be added to the behavior monitoring registry, though this can be configured to suit your needs.\n</p>\n</li>\n<li>\n<h6>Security Log</h6>\n<p><b>Location: </b>[]LOGFILES[/]/security.log</p>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;This log tracks security concerns, such as ftp and ssh connection attempts, pings, portscans, accounts connecting from numerous ip addresses simultaneously, questionable form entries, requests that did not come from the appropriate channels, and a number of other concerns that may indicate someone is attempting to compromise your system. All remote file and database connection attempts are logged here if the remote party does not come from a trusted source or fails the validation handshake. These issues do not directly affect system performance, but should be addressed urgently, as they generally indicate that some subversive party is attempting to compromise your system.\n</p>\n</li>\n</ul>\n</p>\n<hr>\n</span>','DEVTOOLS',0800,1,1,NULL,'0000-00-00 00:00:00','2014-08-03 00:05:14','developer'),(52,'docs','default','default','html','<span id=\"devtools-serverinfo-main\">\n<h4>Server Information</h4>\n<p>\n<h5><b>Location:</b> <a href=\"[]HOSTURL[/]/server-info\">Server Information</a></h5>\n&nbsp;&nbsp;&nbsp;&nbsp;This page displays helpful information and settings relating to the server, it\'s capacity, available functionality, and the current load upon it. There are a handful of dashboard modules and widgets associated with tracking and analyzing this information, each of which is displayed below in detail.\n<br><br>\n<ul>\n<li>\n<h6>Server Details</h6>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;This dashboard module displays a number of useful facts about the webserver and it\'s configuration. You can determine the server type, port, language, admin contact email, and numerous other pieces of information here. You will also be able to determine what the command line user of the webserver is in the event that the system cannot read or access files, so you can fix the file ownership or permissions. You will be informed whether or not SSL is available, whether or not the server signature is enabled, and the include path(s) available to install additional component functionality into if you wish to extend the core functionality. If there are issues that may affect performance or security, some notes will be provided as to the possible effects and how to mitigate them.\n</p>\n</li>\n<li>\n<h6>Server Capabilities</h6>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;This dashboard module displays whether or not you have access to various programming languages, common libraries, server extensions, and other useful features that can extend your system capabilities. You can also automatically enable adapters for any other languages listed here as supported for your system, so your Oroboros installation will be able to directly communicate with code libraries in those languages if there is an adapter available to act as an intermediary. Source control support is also listed here, so if you have the ablility to use git or svn for example, the core system will be able to automatically initialize repositories, push and pull code, and perform a number of other source control tasks as needed, which will make your development life significantly easier. All repositories registered in this manner will be recorded so you can also access them externally if needed through the command line or some repository tracking system.\n</p>\n</li>\n<li>\n<h6>Server Load</h6>\n<p>\n&nbsp;&nbsp;&nbsp;&nbsp;This dashboard module displays the current load on the server, and graphs how heavy the burden has been over however long you have it configured to record. This will give you a better picture of how well your sites are performing, and how much of a burden the traffic you are getting is putting on your codebase. It also can track average pageload speed for your users, so you can tighten up times if needed. Further information on end user views can be accessed through the webmaster analytics panel, this dashmodule is primarily concerned with server performance, not usage statistics, and as such only includes usage data as it pertains to server load.\n</p>\n</li>\n</ul>\n</p>\n<hr>\n</span>','DEVTOOLS',0200,1,1,NULL,'0000-00-00 00:00:00','2014-08-03 00:02:28','developer'),(58,'dashboard','default','default','template','scratchbox.phtml','DASHMODULES',0000,1,1,NULL,'0000-00-00 00:00:00','2014-08-03 01:50:10','architect'),(61,'accounts','default','default','template','sidebar.phtml','SIDEBAR',0000,1,1,NULL,'0000-00-00 00:00:00','2014-08-09 23:57:14','admin');
/*!40000 ALTER TABLE `pages_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_content_types`
--

DROP TABLE IF EXISTS `pages_content_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_content_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `display` varchar(64) NOT NULL DEFAULT 'Untitled Content Type',
  `permission` varchar(16) NOT NULL DEFAULT 'view',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `display` (`display`),
  KEY `permission` (`permission`),
  CONSTRAINT `pages_content_types_ibfk_1` FOREIGN KEY (`permission`) REFERENCES `permissions` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_content_types`
--

LOCK TABLES `pages_content_types` WRITE;
/*!40000 ALTER TABLE `pages_content_types` DISABLE KEYS */;
INSERT INTO `pages_content_types` VALUES (1,'template','Content Template File','view',1,1),(2,'html','HTML Content','view',1,1),(3,'image','Image Content','view',1,1),(4,'audio','Audio Content','view',1,1),(5,'video','Video Content','view',1,1),(6,'component','Site UI Component','view',1,1),(7,'linkset','Navigation Links','view',1,1),(8,'flash','Flash Media Component','view',1,1);
/*!40000 ALTER TABLE `pages_content_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_dependencies`
--

DROP TABLE IF EXISTS `pages_dependencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_dependencies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site` varchar(16) NOT NULL DEFAULT 'default',
  `subdomain` varchar(16) NOT NULL DEFAULT 'default',
  `page` varchar(64) NOT NULL DEFAULT 'index',
  `device` varchar(16) DEFAULT NULL,
  `dependency` varchar(16) NOT NULL DEFAULT '',
  `version` varchar(32) NOT NULL DEFAULT '',
  `source_method` varchar(16) NOT NULL DEFAULT '',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `hierarchy` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `site_3` (`site`,`subdomain`,`page`,`dependency`,`version`),
  KEY `site` (`site`),
  KEY `dependency` (`dependency`),
  KEY `version` (`version`),
  KEY `source_method` (`source_method`),
  KEY `subdomain` (`subdomain`),
  KEY `page` (`page`),
  KEY `site_2` (`site`,`subdomain`,`page`),
  KEY `device` (`device`),
  CONSTRAINT `pages_dependencies_ibfk_1` FOREIGN KEY (`source_method`) REFERENCES `assets_items` (`source_method`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pages_dependencies_ibfk_2` FOREIGN KEY (`site`) REFERENCES `sites` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pages_dependencies_ibfk_3` FOREIGN KEY (`dependency`) REFERENCES `assets_items` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pages_dependencies_ibfk_4` FOREIGN KEY (`version`) REFERENCES `assets_items` (`version`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pages_dependencies_ibfk_5` FOREIGN KEY (`subdomain`) REFERENCES `sites_subdomains` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pages_dependencies_ibfk_6` FOREIGN KEY (`page`) REFERENCES `pages` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pages_dependencies_ibfk_7` FOREIGN KEY (`site`, `subdomain`, `page`) REFERENCES `pages` (`site`, `subdomain`, `slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pages_dependencies_ibfk_8` FOREIGN KEY (`device`) REFERENCES `device_types` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_dependencies`
--

LOCK TABLES `pages_dependencies` WRITE;
/*!40000 ALTER TABLE `pages_dependencies` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_dependencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_types`
--

DROP TABLE IF EXISTS `permission_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `title` varchar(64) NOT NULL DEFAULT 'Untitled Permission',
  `description` text NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='This table maintains the usage rights options selectable for each permission. Default is allow or deny, though other options may be built at user discretion.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_types`
--

LOCK TABLES `permission_types` WRITE;
/*!40000 ALTER TABLE `permission_types` DISABLE KEYS */;
INSERT INTO `permission_types` VALUES (1,'ALLOW','Allow Access','This option will allow access to the selected permission for a user or group.',1),(2,'DENY','Deny Access','This option will deny access to a selected permission for a user or group.',1);
/*!40000 ALTER TABLE `permission_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `display` varchar(64) DEFAULT 'Untitled Permission Node',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `site` varchar(16) NOT NULL DEFAULT 'default',
  `group` varchar(16) DEFAULT 'default',
  `description` text COMMENT 'A brief description of what the permission does',
  `show` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'This regulates whether or not the permission can be managed by the user. System required permissions will not be displayed.',
  `hierarchy` int(4) unsigned zerofill NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `site` (`site`),
  KEY `active` (`active`),
  KEY `group` (`group`),
  CONSTRAINT `permissions_ibfk_1` FOREIGN KEY (`site`) REFERENCES `sites` (`slug`) ON UPDATE CASCADE,
  CONSTRAINT `permissions_ibfk_2` FOREIGN KEY (`group`) REFERENCES `permissions_groups` (`slug`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=utf8 COMMENT='This table contains all of the possible permission nodes used throughout the system. This table does not manage individual or user rights to those permissions, only whether they exist within the system or not.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (0,'SYSTEM','SYSTEM PERMISSION NODE',1,'SYSTEM','SYSTEM','This is the system permission node. It has all permissions so it can perform automated tasks without conflicting with the permission system.',0,0000),(1,'view','View Site',1,'default','site_access','Allows the user to view the site.',1,0000),(2,'login','Login',1,'default','site_access','Allows the user to login to the site.',1,0000),(3,'comment','Post Comments',1,'default','content_mod','Allows the user to post comments on the site.',1,0000),(4,'reply','Reply to Comments',1,'default','content_mod','Allows the user to reply to comments posted by others on the site.',1,0000),(5,'edit_account','Edit Account Settings',1,'default','user_mod','Allows the user to edit their basic account details, including bio, age, location, user image, etc.',1,0000),(6,'edit_other','Edit Others Accounts',1,'default','user_mod','Allows an administrator to alter details of others accounts.',1,0000),(7,'restrict_account','Restrict Account',1,'default','user_mod','Allows a moderator to put an account in restricted status to prevent abusive behavior.',1,0000),(8,'ban_account','Ban Account',1,'default','user_mod','Allows a moderator to ban a troublesome account from the site.',1,0000),(9,'grant_perms','Grant Permissions',1,'default','user_mod','Allows a site operator to grant or remove additional usage permissions to or from other users.',1,0000),(10,'change_group','Change User Group',1,'default','user_mod','Allows a site operator to promote or demote a user to another user group. The user may not promote another user above his own group level.',1,0000),(11,'maintenance','Maintenance Mode',1,'default','site_mod','Allows the user to place the site into maintenance mode, which suspends user access while work is being done on the back end, or to remove the site from maintenance mode and re-enable access.',1,0000),(12,'alter_content','Alter Page Content',1,'default','content_mod','Allows an administrator to alter page content for existing pages on the site.',1,0000),(13,'add_page','Add Pages',1,'default','site_mod','Allows a site operator to add additional pages to the site.',1,0000),(14,'delete_page','Delete Pages',1,'default','site_mod','Allows a site operator to delete pages from the site, including all of their content.',1,0000),(15,'email_users','Email Users',1,'default','user_access','Allows an admin to send emails to the user base, if they have opted to recieve emails from the site.',1,0000),(16,'add_post','Add Site Update',1,'default','content_mod','Allows an admin to add additional site updates to the front page.',1,0000),(17,'p_inherit_css','Edit Page Stylesheet Inheritance',1,'default','design_mod','Allows an admin to change the a pages parent stylesheet. This node does not allow for sitewide changes.',1,0000),(18,'p_inherit_js','Edit Page Script Inheritance',1,'default','design_mod','Allows an admin to change a pages javascript inheritance. This node does not allow for sitewide changes.',1,0000),(19,'s_inherit_css','Edit Site Stylesheet Inheritance',1,'default','design_mod','Allows the webmaster to add inheritance between sites on the network. This node will allow sitewide changes.',1,0000),(20,'s_inherit_js','Edit Site Script Inheritance',1,'default','design_mod','Allows the webmaster to edit javascript inheritance between sites on the network. This node will allow sitewide changes.',1,0000),(21,'create_subdomain','Create Subdomain',1,'default','site_mod','Allows a site operator to create a subdomain within the site.',1,0000),(22,'create_site','Create Website',1,'default','site_mod','Allows the webmaster to create a new site on the network, or to export a subdirectory of an existing site as a new site.',1,0000),(23,'default','Default Inheritance Node',1,'default','group_membership','This is a default node that acts as an anchor for permission inheritance. Do not remove this node.',0,0000),(24,'registered','Registered User',1,'default','group_membership','This node grants permission to registered users.',1,0000),(25,'group','Group Access',1,'default','user_mod','This node grants group access to users, provided they are added to the user group access is registered with.',1,0000),(26,'developer','Developer Access',1,'default','group_membership','This node grants access to registered developers.',1,0000),(27,'self','Author Access',1,'default','user_access','This node grants permission only to the author of the site asset.',1,0000),(28,'node','Node Access',1,'default','installation','This node grants access to users with registered accounts on any remote node with which the asset is shared.',1,0000),(29,'node_internal','Internal Node Access',1,'default','installation','This node grants access to users on any remote node, so long as they exist within the access group defined by the parent node.',1,0000),(30,'node_developer','Developer Node Access',1,'default','installation','This node grants access to developers on any remote node, so long as they exist within the user group defined by the parent node.',1,0000),(32,'edit_links','Edit Links',1,'default','site_mod','You may add or change links and link sets, not to exceed your access rights.',1,0000),(33,'add_components','Add Site Component',1,'default','component_mod','You may add site components to the site to extend sitewide functionality.',1,0000),(34,'moderator','Moderator Access',1,'default','group_membership','This node grants access to registered moderators.',1,0000),(35,'admin','Admin Access',1,'default','group_membership','This node grants access to registered admins.',1,0000),(37,'designer','Designer Access',1,'default','group_membership','This node grants access to registered designers.',1,0000),(38,'operator','Operator Access',1,'default','group_membership','This node grants permission to registered operators.',1,0000),(39,'master_developer','Master Developer Access',1,'default','group_membership','This node grants access to registered master developers.',1,0000),(41,'webmaster','Webmaster Access',1,'default','group_membership','This node grants access to registered webmasters.',1,0000),(42,'sys_display_all','Display all system items',1,'default','SYSTEM','Displays non-showing system items for core development purposes.',0,0000),(43,'owner','Owner Access',1,'default','group_membership','This node grants access to site owners.',1,0000),(44,'architect','Architect Access',1,'default','group_membership','This node grants access to system architects.',1,0000),(45,'author','Author Access',1,'default','group_membership','This node grants access to content authors.',1,0000),(48,'g_site_edit','Global Site Edit',1,'global','installation','You may edit settings for all sites globally.',1,0043),(49,'g_page_edit','Global Page Edit',1,'global','installation','You may edit settings for all pages globally',1,0044),(50,'g_content_edit','Global Content Edit',1,'global','installation','You may edit content for all sites globally.',1,0045),(51,'node_login','Node Login',1,'global','installation','You may log in to remote nodes using your local credentials, provided they have enabled this feature and granted access to your account level.',1,0046),(52,'serv_webshell_b','Basic Webshell Access',1,'global','server_mod','You may execute basic serverside command line actions through the webshell terminal',1,0047),(53,'serv_webshell_a','Advanced Webshell Access',1,'global','server_mod','You may execute complex serverside command line actions through the webshell terminal',1,0048),(54,'serv_git_init','Initialize Git Repo',1,'global','server_mod','You may initialize a new git repository',1,0049),(55,'serv_git_clone','Clone Git Repository',1,'global','server_mod','You may clone a remote git repository into the server installation filebase',1,0050),(56,'serv_git_commit','Commit Git Repository',1,'global','server_mod','You may commit changes to a git repository on the server.',1,0051),(57,'db_update_core','Update Core Database Credentials',1,'global','installation','You may change the system\'s core database credentials',1,0052),(58,'g_backup_system','Backup Installation',1,'global','installation','You may perform an installation backup of the entire system.',1,0053),(59,'serv_git_branch','Checkout Git Branch',1,'global','server_mod','You may checkout a git branch on a server repository',1,0054),(60,'serv_git_bran_c','Create Git Branch',1,'global','server_mod','You may create a new git branch on a server repository',1,0055),(61,'serv_git_push','Push Git Commit to Remote',1,'global','server_mod','You may push a server repository commit to a remote repo',1,0056),(62,'serv_git_stash','Stash Git Changes',1,'global','server_mod','You may stash changes to the filebase on a server repository',1,0057),(63,'serv_git_blame','View Git Blame Report',1,'global','server_access','You may view a git blame report of various commits within a serverside repository',1,0058),(64,'serv_git_diff','View Git Diff Report',1,'global','server_access','You may make visual commit comparisons between two commits of a serverside git repository',1,0059),(65,'serv_view_env','View Server Environment',1,'global','server_access','You may view information about the installation server environment',1,0060),(66,'serv_edit_env','Edit Server Environment',1,'global','server_mod','You may perform edits to the server environment',1,0061),(67,'g_view_dash','View Global Dashboard',1,'global','site_access','You may login to the global dashboard',1,0062),(68,'g_view_front','View Global Frontend',1,'global','server_access','You may view the global site frontend',1,0063),(69,'p_view_dash','Private Dashboard View',1,'global','site_access','You may view the dashboard even when it is set to private status',1,0064),(70,'p_view_frontend','Private Frontend View',1,'global','site_access','You may view the frontend even when it is set to private',1,0065),(71,'p_node_dash','Private Remote Node Dashboard View',1,'global','site_access','You may login to the dashboard from a remote node even when it is set to private access',1,0066),(72,'p_node_frontend','Private Remote Node Frontend View',1,'global','site_access','You may view the site frontend from a remote node even when it is set to private',1,0067),(73,'file_chown','Change File Ownership',1,'global','filebase','You may change file ownership for files on the server',1,0068),(74,'file_chmod','Change File Permissions',1,'global','filebase','You may chane file permissions for files on the server',1,0069),(75,'file_archive','Archive Files',1,'global','filebase','You may create an archive of existing files on the server',1,0070),(76,'file_copy','Copy Files',1,'global','filebase','You may create copies of files, or move them to another directory on the server',1,0071),(77,'file_del_own','Delete Personal Files',1,'global','filebase','You may delete files that you own on the server',1,0072),(78,'file_del_site','Delete Site Files',1,'global','filebase','You may delete files within the site directory on the server',1,0073),(79,'file_del_other','Delete User Files',1,'global','filebase','You may delete files owned by other users from the server. You will not be able to delete those that belong to a user with a higher group level than your own, even with this permission.',1,0074),(80,'file_dl_own','Download Personal Files',1,'global','filebase','You may download files that you own from the server arbitrarily',1,0075),(81,'file_dl_site','Download Site Files',1,'global','filebase','You may download files from the site directory on the server',1,0076),(82,'file_dl_user','Download User Files',1,'global','filebase','You may download files that belong to other users from the server. You may not exceed the privacy restrictions for accounts with higher group authorization than your own.',1,0077),(83,'g_site_export','Export Sites Globally',1,'global','installation','You may create an export package of any site on this installation globally',1,0078),(84,'g_site_import','Import Sites Globally',1,'global','installation','You may import an existing site package into the system',1,0079),(85,'g_lockdown','Global Site Lockdown',1,'global','installation','You may lockdown all of the sites on the entire installation',1,0080),(86,'s_lockdown','Site Lockdown',1,'global','site_mod','You may lockdown a site to prevent all login attempts from accounts that do not have permission to bypass lockdown protocol',1,0081),(87,'g_lock_login','Global Lockdown Login',1,'global','installation','You may login to any site on the network during lockdowns',1,0082),(88,'s_lock_login','Site Lockdown Login',1,'global','site_access','You may login to the site during lockdowns',1,0083),(89,'g_maint','Global Maintenance Mode',1,'global','installation','You may enable or disable maintenance mode globally',1,0084),(90,'g_maint_login','Global Maintenance Login',1,'global','installation','You may login or view sites globally when in maintenance mode',1,0085),(91,'s_maint','Site Maintenance Mode',1,'global','site_mod','You may enable or disable site maintenance mode',1,0086),(92,'s_maint_login','Site Maintenance Login',1,'global','site_access','You may login or view the site during maintenance mode',1,0087),(93,'view_code','View Server Codebase',1,'global','codebase','You may view the contents of serverside code through the code browser',1,0088),(94,'edit_code','Edit Codebase',1,'global','codebase','You may edit existing serverside code through the code editor',1,0089),(95,'create_code','Create Server Code',1,'global','codebase','You may create new serverside classes, php templates, and other serverside scripts through the code editor',1,0090),(96,'import_code','Import Server Code',1,'global','codebase','You may import serverside classes, php templates or other code from external sources',1,0091),(97,'export_code','Export Server Code',1,'global','codebase','You may export existing serverside classes, php templates and other scripts directly',1,0092),(98,'db_create','Create Database Connection',1,'global','database','You may add additional database connections to the system',1,0093),(99,'db_modify','Modify Database Connection',1,'global','database','You may change the database credentials for existing database connections within the system',1,0094),(100,'db_remove','Remove Database Connection',1,'global','database','You may remove an existing database connection from the system',1,0095),(101,'db_create_rm','Create Remote Database Connection',1,'global','database','You may add remote database connections to the system',1,0096),(102,'db_edit_rm','Edit Remote Database Connection',1,'global','database','You may edit the credentials for remote database connections',1,0097),(103,'db_remove_rm','Remove Remote Database Connection',1,'global','database','You may remove remote database connections from the system',1,0098),(104,'db_view','View Database Details',1,'global','database','You may view the details for a database connection',1,0099),(105,'db_view_rm','View Remote Database Details',1,'global','database','You may view the connection details for a remote database connection',1,0100),(106,'db_query_view','View Database Query',1,'global','database','You may view queries being executed against the database',1,0101),(107,'db_query_exec','Execute Query',1,'global','database','You may execute queries against a local database',1,0102),(108,'db_query_ex_rm','Execute Remote Query',1,'global','database','You may execute a query against a remote database connection',1,0103),(109,'db_backup','Backup Database',1,'global','database','You may perform a database archive request against a local database',1,0104),(110,'db_backup_rm','Remote Database Backup',1,'global','database','You may peform a remote database backup operation',1,0105),(111,'db_sql_editor','Enable SQL Editor',1,'global','database','You may use the administrative SQL editor utility',1,0106),(112,'p_post','Create Page Post',1,'global','content_mod','You may create a page post for pages you have permission to modify',1,0107),(113,'s_post','Create Site Post',1,'global','content_mod','You may create a site post for sites you are authorized to modify',1,0108),(114,'s_create_page','Create Page',1,'global','content_mod','You may add pages to the site',1,0109),(115,'g_create_page','Create Global Pages',1,'global','content_mod','You may create pages that render on all sites in the network',1,0110),(116,'s_subdm_create','Create Subdomain',1,'global','site_mod','You may create a subdomain within the site',1,0111),(117,'g_subdm_create','Create Global Subdomain',1,'global','installation','You may create a subdomain that exists on all sites in the network',1,0112),(118,'s_subsct_create','Create Subsection',1,'global','site_mod','You may create a new subsection within the site, and define access parameters',1,0113),(119,'g_subsct_create','Create Global Subsection',1,'global','installation','You may create a new global subsection that appears on all sites in the network, and define access parameters',1,0114),(120,'perm_view','View Permissions',1,'global','user_mod','You may view existing permissions nodes',1,0115),(121,'perm_create','Create New Permission',1,'global','user_mod','You may create new permission nodes',1,0116),(122,'perm_edit','Edit Permission Nodes',1,'global','user_mod','You may edit the details of existing permission nodes',1,0117),(123,'perm_remove','Delete Permissions',1,'global','user_mod','You may remove permissions from the system',1,0118),(124,'perm_assign_u','Assign User Permissions',1,'global','user_mod','You may assign user permissions or restrictions to account levels below your own',1,0119),(125,'perm_assign_g','Assign Group Permissions',1,'global','user_mod','You may assign permissions to user groups',1,0120),(126,'g_perm_create','Create Global Permissions',1,'global','installation','You may create new permission nodes in the global scope',1,0121),(127,'g_perm_edit','Edit Global Permissions',1,'global','installation','You may edit permissions in the global scope',1,0122),(128,'g_perm_remove','Remove Global Permissions',1,'global','installation','You may remove permissions in the global scope',1,0123),(129,'g_perm_gr_add','Add Global Group Permissions',1,'global','installation','You may assign group permissions in the global scope',1,0124),(130,'g_perm_u_add','Add Global User Permissions',1,'global','installation','You may edit user permissions in the global scope',1,0125),(131,'serv_view_sched','View Server Schedule',1,'global','server_access','You may view the existing server schedule',1,0126),(132,'serv_edit_sched','Edit Server Schedule',1,'global','server_mod','You may make changes to the server schedule',1,0127),(133,'u_view_inf','View User Information',1,'global','user_access','You may view information about other users, provided it does not exceed privacy rights',1,0128),(134,'u_view_inf_p','View Private User Information',1,'global','user_access','You may view private user information for accounts whose permission levels do not exceed your own',1,0129),(135,'u_message','Message Users',1,'global','user_access','You may send private messages to other users',1,0130),(136,'u_email','Email Users',1,'global','user_access','You may send emails to other users',1,0131),(137,'u_content_share','Share User Content',1,'global','user_access','You may share content with other users',1,0132),(138,'u_content_view','View User Content',1,'global','user_access','You may view user contributed content if the privacy rights do not exceed your access level',1,0133),(139,'u_content_view_p','View Private User Content',1,'global','user_access','You may view private user content, as long as their access rights and privacy settings do not exceed your own',1,0134),(140,'sched_content','Schedule Content',1,'global','content_mod','You may add content to the server schedule',1,0135),(141,'sched_page','Schedule Page',1,'global','site_mod','You may add pages to the server schedule',1,0136),(142,'sched_site','Schedule Site',1,'global','installation','You may add sites to the server schedule',1,0137),(143,'sched_subdm','Schedule Subdomain',1,'global','site_mod','You may add subdomain access to the server schedule',1,0138),(144,'sched_subsct','Schedule Subsection',1,'global','site_mod','You may add site subsections to the server schedule',1,0139),(145,'sched_post','Schedule Post',1,'global','content_mod','You may add posts to the server schedule',1,0140),(146,'sched_message','Schedule Message',1,'global','user_access','You may add messages to the server schedule',1,0141),(147,'sched_email','Syndicate Email',1,'global','content_mod','You may add emails to the server schedule',1,0142),(148,'sched_rss','Syndicate RSS',1,'global','content_mod','You may add rss feeds to the server schedule',1,0143),(149,'sched_cal','Syndicate Calendar Events',1,'global','content_mod','You may add calendar event notifications to the server schedule',1,0144),(150,'cpt_dashbar','Use Dashbar',1,'global','component_access','You may utilize the dashbar component',1,0145),(151,'cpt_drawer','Use Drawer',1,'global','component_access','You may utilize the drawer component',1,0146),(152,'cpt_controller','Use Component Controller',1,'global','component_access','You may utilize the component controller component',1,0147),(153,'cpt_scroller','Use Scroller Component',1,'global','component_access','You may utilize the page scroller component',1,0148),(154,'cpt_workspace','Use Workspaces Component',1,'global','component_access','You may utilize the workspaces component',1,0149),(155,'dev_sandbox','Use Development Sandbox Environment',1,'global','codebase','You may utilize the features of the development sandbox environment',1,0150),(156,'des_sandbox','Use Design Sandbox Environment',1,'global','design_mod','You may utilize the features of the design sandbox environment',1,0151),(157,'auth_sandbox','Use Author Sandbox Environment',1,'global','content_access','You may utilize the features of the content author sandbox environment',1,0152),(158,'seo_analytics','View Site Analytics',1,'global','site_access','You may view site analytics reports',1,0153),(159,'seo_tracking','View Tracking Reports',1,'global','site_access','You may view reports on user tracking and browsing information',1,0154),(160,'seo_keywords','Edit SEO Keywords',1,'global','site_access','You may modify the site keyword list and utilize the built in seo functionality',1,0155),(161,'cpt_add','Add Site Component',1,'global','component_mod','you may add new site components',1,0156),(162,'cpt_edit','Edit Components',1,'global','component_mod','You may edit existing components, what they display, as well as where and how they perform',1,0157),(163,'cpt_create','Create Component',1,'global','component_mod','You may create new components from the code editor, and install them directly either into the staging environment or the live site',1,0158),(164,'cpt_package','Export Component',1,'global','component_mod','You may create an export package for an existing component',1,0159),(165,'cpt_archive','Backup Component',1,'global','component_mod','You may create an archive of an existing component',1,0160),(166,'cpt_remove','Remove Component',1,'global','component_mod','You may remove a component from the system',1,0161),(167,'mod_add','Add Module',1,'global','module_mod','You may add a new module to the system',1,0162),(168,'mod_edit','Edit Module',1,'global','module_mod','You may edit the module settings of an existing module',1,0163),(169,'mod_archive','Backup Module',1,'global','module_mod','You may create an archive of a module',1,0164),(170,'mod_create','Create Module',1,'global','module_mod','You may create a new module with the code editor, and install it into either the staging environment or the live site',1,0165),(171,'mod_remove','Remove Module',1,'global','module_mod','You may remove an existing module from the system',1,0166),(172,'mod_export','Export Module',1,'global','module_mod','You may create an export package for an existing module in the system',1,0167),(173,'ftp_connect','FTP Connection',1,'global','filebase','You may connect to the site via FTP with your standard login credentials',1,0168),(174,'ssh_connect','SSH Connection',1,'global','server_mod','You may connect to the server via SSH with your standard login credentials',1,0169),(175,'ssh_add','Add SSH Connection',1,'global','server_mod','You may add new SSH connections to the system',1,0170),(176,'ssh_edit','Edit SSH Connections',1,'global','server_mod','You may edit the details of existing SSH connections',1,0171),(177,'ssh_remove','Remove SSH Connections',1,'global','installation','You may remove existing SSH connections from the system',1,0172),(178,'dns_add','Add DNS',1,'global','server_mod','You may register a new domain name into the system',1,0173),(179,'dns_define','Define DNS',1,'global','server_mod','You may assign an existing registered domain to a site',1,0174),(180,'dns_edit','Edit DNS',1,'global','server_mod','You may edit the assignment of existing DNS entries',1,0175),(181,'dns_remove','Deauthorize DNS',1,'global','server_mod','You may remove existing DNS entries from the system',1,0176),(182,'file_up_u','Upload User Files',1,'global','filebase','You may upload files to your user directory',1,0177),(183,'file_up_site','Upload Site Files',1,'global','filebase','You may upload files to the site directory',1,0178),(184,'file_up_u_other','Upload Other User Files',1,'global','filebase','You may upload files into other users directories, provided your access level exceeds their own',1,0179),(185,'file_up_sql','Upload SQL',1,'global','database','You may upload SQL files to the server',1,0180),(186,'file_up_js','Upload Javascript',1,'global','codebase','You may upload javascript content to the server',1,0181),(187,'file_up_php','Upload Server Files',1,'global','codebase','You may upload executable server side scripts to the server',1,0182),(188,'file_up_css','Upload Stylesheets',1,'global','design_mod','You may upload css stylesheets to the server',1,0183),(189,'note_flag','View Flags',1,'global','user_access','You may view flags set on account activity',1,0184),(190,'note_alert_site','View Site Alerts',1,'global','user_access','You may view site alerts in your notifications',1,0185),(191,'note_alert_serv','View Server Alerts',1,'global','user_access','You may view alerts from the server in your notifications',1,0186),(192,'note_req_u','View User Requests',1,'global','user_access','You may view user requests in your notifications',1,0187),(193,'note_req_node','View Node Requests',1,'global','user_access','You may view node requests in your notifications',1,0188),(194,'note_msg_u','View User Messages',1,'global','user_access','You may receive messages from other users in your notifications',1,0189),(195,'note_msg_priv','View Private Messages',1,'global','user_access','You may monitor private messages between other users in your notifications, provided neither of the other parties exceed your own access rights',1,0190),(196,'flag_acct','Flag Account',1,'global','user_access','You may set a flag on a user account and monitor their activity, provided their access rights do not exceed your own',1,0191),(197,'flag_grp','Flag Group',1,'global','user_access','You may set a flag on a user group and monitor their activity, provided the group access rights do not exceed your own',1,0192),(198,'flag_action','Flag Action',1,'global','user_access','You may set a flag on a specific action, which will notify you whenever the action is taken by any account with lesser access rights than your own',1,0193),(199,'flag_msg','Flag Message Content',1,'global','user_access','You may set a flag on messages that contain specific content, which will notify you whenever any other users transmit the defined content provided their access rights do not exceed your own',1,0194),(200,'flag_read','Flag Read Message',1,'global','user_access','You may set a flag on a message, email, or other correspondence which will notify you when the message has been opened by the other party',1,0195),(201,'user_suspend','Suspend User',1,'global','user_mod','You may suspend a user account with access rights beneath your own, provided they have not had their account whitelisted or protected by a higher account',1,0196),(202,'user_ban','Ban User',1,'global','user_mod','You may ban a lower user account, provided they have not had their account protected or whitelisted by a higher account',1,0197),(203,'user_protect','Protect Account',1,'global','user_mod','You may set an account status as protected, which will prevent disciplinary action by accounts lower than your own',1,0198),(204,'user_whitelist','Whitelist User',1,'global','user_mod','You may place a user on the whitelist, which will insure they can always login unimpeded',1,0199),(205,'user_blacklist','Blacklist User',1,'global','user_mod','You may place a user on the blacklist, which will insure that they can never login or view the site at all if they are identified as the same user, ip address, or other sequence of identifying information generated by their usage patterns that can uniquely identify them with relative accuracy',1,0200),(206,'user_greylist','Greylist User',1,'global','user_mod','You may place a user account on the greylist, which means that they must either perform some specific extra step to login, or that their site viewing attempt will be modified from the standard login in some way (ie: all pages load statically for web crawlers, etc.)',1,0201),(207,'user_restrict','Restrict User',1,'global','user_mod','You may place a lower account in restricted status, in which case some site features will be temporarily unavailable to them',1,0202),(208,'user_logout','Logout User',1,'global','user_mod','You may force a logout for a lower level account. This can be done as a disciplinary warning, or to force their permissions to update when they login again',1,0203),(209,'group_logout','Logout Group',1,'global','user_mod','You may force a logout for an entire group whose access rights are lower than your own to reset their permissions upon relog',1,0204),(210,'all_logout','Logout All',1,'global','user_mod','You may force all logged in users to logout to reset their permissions or prevent access temporarily (this will automatically be done on site lockdown or maintenance mode)',1,0205),(211,'sys_edit_perms','Edit System Permissions',1,'global','installation','You may make changes to system permission nodes',1,0206),(212,'sys_group_all','Display System Groups',1,'global','installation','You may view system groups that are not otherwise visible',1,0207),(213,'sys_user_all','View System Users',1,'global','installation','You may view system users that are not otherwise visible',1,0208),(214,'des_temp_add','Create Template',1,'global','design_mod','You may create new template files',1,0209),(215,'des_temp_edit','Edit Templates',1,'global','design_mod','You may edit existing template files',1,0210),(216,'des_temp_pkg','Package Templates',1,'global','design_mod','You may create export packages of existing templates',1,0211),(217,'des_temp_arch','Archive Templates',1,'global','design_mod','You may create archive backups of template files',1,0212),(218,'des_temp_rem','Remove Templates',1,'global','design_mod','You may remove existing template files from the system',1,0213),(219,'des_thm_add','Add Themes',1,'global','design_mod','You may create new theme files',1,0214),(220,'des_thm_edit','Edit Themes',1,'global','design_mod','You may edit existing theme files',1,0215),(221,'des_thm_pkg','Package Themes',1,'global','design_mod','You may create export packages of existing themes',1,0216),(222,'des_thm_arch','Archive Themes',1,'global','design_mod','You may create backup archives of existing themes',1,0217),(223,'des_thm_rem','Remove Themes',1,'global','design_mod','You may remove existing themes from the system',1,0218),(224,'des_typ_add','Add Fonts',1,'global','design_mod','You may add new fonts and typographic parameters to the system',1,0219),(225,'des_typ_edit','Edit Fonts',1,'global','design_mod','You may edit existing typographic settings',1,0220),(226,'des_typ_pkg','Package Fonts',1,'global','design_mod','You may create export packages of existing typographic settings',1,0221),(227,'des_typ_arch','Archive Fonts',1,'global','design_mod','You may create backup archives of existing typographic settings',1,0222),(228,'des_typ_rem','Remove Fonts',1,'global','design_mod','You may remove existing typographic settings from the system',1,0223),(229,'des_col_add','Add Color Schemes',1,'global','design_mod','You may create new color schemes',1,0224),(230,'des_col_edit','Edit Color Schemes',1,'global','design_mod','You may edit the settings of existing color schemes within the system',1,0225),(231,'des_col_pkg','Package Color Schemes',1,'global','design_mod','You may create export packages of existing color schemes',1,0226),(232,'des_col_arch','Archive Color Schemes',1,'global','design_mod','You may create backup archives of existing color schemes',1,0227),(233,'des_col_rem','Remove Color Schemes',1,'global','design_mod','You may remove existing color schemes from the system',1,0228),(234,'des_img_mod','Modify Images',1,'global','design_mod','You may modify existing site images',1,0229),(235,'des_vid_mod','Modify Video',1,'global','design_mod','You may modify existing video files if the system provides the appropriate codec to do so',1,0230),(236,'reg_set_params','Set Registration Parameters',1,'global','site_mod','You may set the account creation parameters for the site',1,0231),(237,'g_reg_set_params','Set Global Registration Parameters',1,'global','installation','You may set account registration parameters in the global scope. These will override individual site settings if they are more restrictive, and will be overridden by any more restrictive parameters set at the site scope',1,0232),(238,'forum_post','Create Forum Post',1,'global','user_access','You may create new forum posts',1,0233),(239,'forum_reply','Create Forum Reply',1,'global','user_access','You may reply to posts on the forum',1,0234),(240,'forum_edit_own','Edit Own Forum Content',1,'global','user_access','You may edit your own forum posts and replies',1,0235),(241,'forum_edit_other','Edit All Forum Posts',1,'global','user_access','You may edit the forum posts and replies of other users whose access rights do not exceed your own',1,0236),(242,'forum_ban','Ban Forum User',1,'global','user_mod','You may restrict a user from posting in the forums indefinitely',1,0237),(243,'forum_suspend','Suspend Forum User',1,'global','user_mod','You may restrict a user from posting in the forums temporarily',1,0238),(244,'forum_mute','Mute Forum User',1,'global','user_mod','You may prevent a user from posting in the forums for an arbitrary time period (or indefinitely)',1,0239),(245,'user_promote','Promote User',1,'global','user_mod','You may elevate another user to a group below your own',1,0240),(246,'user_demote','Demote User',1,'global','user_mod','You may demote a user to a lower user group provided their access rights are lower than your own',1,0241),(247,'g_login','Global Login',1,'global','site_access','You may login to any site in the network with your standard credentials',1,0242),(248,'view_dash','View Dashboard',1,'default','site_access','You may view the dashboard section of the site.',1,0243),(249,'serv_view_sec','View Server Security',1,'global','installation','You may view a detailed analysis of the current security protocols, warnings, and information for the server',1,0244),(250,'db_view_core','View Core Database Credentials',1,'global','installation','You may view detailed information about the core database connection',1,0245);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions_groups`
--

DROP TABLE IF EXISTS `permissions_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `display` varchar(64) NOT NULL DEFAULT '',
  `description` varchar(512) DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `show` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `hierarchy` int(4) unsigned zerofill NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `display` (`display`),
  KEY `description` (`description`(255))
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions_groups`
--

LOCK TABLES `permissions_groups` WRITE;
/*!40000 ALTER TABLE `permissions_groups` DISABLE KEYS */;
INSERT INTO `permissions_groups` VALUES (1,'SYSTEM','System Permissions','These are system permissions. For best results, do not modify this group or you may break your installation.',1,0,0000),(2,'installation','Installation Permissions','These permissions apply to the modification of the core installation. This is a very highly elevated permission set, as changes to these permissions affect all sites in the entire network and the functionality of the entire system.',1,1,0002),(3,'filebase','Filebase Permissions','These permissions apply to making changes to the filesystem. Some of these are generalized, such as the ability to upload or download files, though many belong to elevated groups for security purposes.',1,1,0005),(4,'codebase','Codebase Permissions','These permissions apply to viewing and modifying the codebase. This permission set is generally avaliable only to the developer user group and higher.',1,1,0006),(5,'database','Database Permissions','These permissions apply to making direct changes to the database. This permission group generally is reserved for developers and higher user groups.',1,1,0007),(6,'site_access','Site Access Permissions','These permissions apply to the level of site access a user has authorization to view. All user groups will have some permission nodes from within this group.',1,1,0008),(7,'site_mod','Site Modification Permissions','These permissions apply to the ability to make modifications to the site settings, structure and performance. This group is generally available to the operator user group and higher.',1,1,0009),(8,'content_access','Content Access Permissions','These permissions apply to the ability to view content of various types.',1,1,0010),(9,'content_mod','Content Modification Permissions','These permissions apply to the ability to modify content. Some of these permissions (such as the ability to comment on posts) may be available to most users. Many are elevated to higher user groups.',1,1,0011),(10,'user_access','User Access Permissions','These permissions apply to the ability to access user data and account settings. The ability to view other users data will additionally adhere to the individual account privacy settings if the viewer is not of a sufficient privelege level to override these settings.',1,1,0012),(11,'user_mod','User Modification Permissions','These permissions apply to the ability to modify user data and account settings. Modifying other users data is reserved for moderators and above, and no user may modify another user that outranks their account level even if they have permission to modify accounts.',1,1,0013),(12,'component_access','Component Access Permissions','These permissions regulate access to component functionality.',1,1,0014),(13,'component_mod','Component Modification Permissions','These permissions regulate the ability to modify components, define component content, or otherwise manipulate the display and access level of components. This set is generally available to admins and higher user groups.',1,1,0015),(14,'module_access','Module Access Permissions','These permissions regulate the ability to view module output and interact with their functionality. Access levels are designated by the module itself, though they may be reassigned by site admins, or any higher user group with access to module modification.',1,1,0016),(15,'module_mod','Module Modification Permissions','These permissions apply to the modification of module settings. This set is of a higher elevation than the ability to view and use modules, as it deals with direct manipulation of what the module outputs and how it performs. These are also designated originally by the individual module, though may be changed if desired by users with sufficient access rights.',1,1,0017),(16,'server_access','Server Access Permissions','These permissions apply to the view details about the webserver hosting the installation. This is a highly elevated permission set, and is generally only available to webmasters and higher user groups.',1,1,0003),(17,'server_mod','Server Modification Permissions','These permissions apply to making modifications to the webserver hosting the installation. This is a highly elevated permission set, and is generally only available to the webmaster user group or higher.',1,1,0004),(18,'design_mod','Design Permissions','These permissions apply to the creation, modification, and management of site and page design. This set is generally assigned to the designer user group and higher level accounts.',1,1,0018),(19,'syn_access','Syndication Access Permissions','These permissions grant access to syndicated content over email, rss, or other methods.',1,1,0021),(20,'syn_mod','Syndication Modification Permissions','These permissions grant modification rights to management of syndicated content.',1,1,0022),(21,'script_access','Scripting Access Permissions','These permissions grant view rights to the generation of javascript functionality throughout the site. This permission set is generally available to designers and developers, as well as higher ranking user accounts.',1,1,0019),(22,'script_mod','Scripting Modification Permissions','These permissions grant creation, modification, and management rights pertaining to javascript functionality throughout the site. This permission set is generally designated to the designer, developer, and higher level user groups.',1,1,0020),(23,'custom','Custom Permissions','These permissions are user defined or arbitrarily set, and may manage whatever functionality they are assigned to.',1,1,0023),(24,'group_membership','Group Membership Base Permissions','These are the base level perms for group membership.',1,0,0001);
/*!40000 ALTER TABLE `permissions_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sites`
--

DROP TABLE IF EXISTS `sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) DEFAULT NULL,
  `dns` varchar(253) DEFAULT NULL,
  `display` varchar(64) NOT NULL DEFAULT 'untitled site',
  `parent` varchar(16) NOT NULL DEFAULT 'default',
  `scope` varchar(16) DEFAULT 'local',
  `permission` varchar(16) DEFAULT NULL,
  `connect_type` varchar(8) NOT NULL DEFAULT 'http',
  `url` varchar(256) NOT NULL DEFAULT '',
  `stylesheet` varchar(32) DEFAULT NULL,
  `theme` varchar(64) NOT NULL DEFAULT 'default',
  `description` text,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  `access_level` varchar(16) NOT NULL DEFAULT 'open',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  UNIQUE KEY `dns` (`dns`),
  UNIQUE KEY `slug_2` (`slug`,`dns`),
  KEY `parent` (`parent`),
  KEY `url` (`url`(255)),
  KEY `permission` (`permission`),
  KEY `theme` (`theme`),
  KEY `access_level` (`access_level`),
  KEY `stylesheet` (`stylesheet`),
  KEY `scope` (`scope`),
  CONSTRAINT `page_dns` FOREIGN KEY (`slug`, `dns`) REFERENCES `dns` (`site`, `dns`) ON DELETE CASCADE ON UPDATE SET NULL,
  CONSTRAINT `sites_ibfk_5` FOREIGN KEY (`parent`) REFERENCES `sites` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sites_ibfk_7` FOREIGN KEY (`access_level`) REFERENCES `access_level` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sites_ibfk_8` FOREIGN KEY (`permission`) REFERENCES `permissions` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='This table manages all of the sites available within the system, as well as their access rights by various users and permission groups.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sites`
--

LOCK TABLES `sites` WRITE;
/*!40000 ALTER TABLE `sites` DISABLE KEYS */;
INSERT INTO `sites` VALUES (0,'SYSTEM','127.0.0.1','System','self','global','SYSTEM','ssh','',NULL,'RAW','DO NOT DELETE: This entry is a placeholder for the global system user group, and is required for the system to perform automated tasks correctly. It does not display in the site configuration.',1,0,'private'),(1,'self',NULL,'No Inheritance','self','global','view','ssh','',NULL,'RAW','DO NOT DELETE: This entry is a placeholder for sites that do not have any inheritance associated with them. It does not display in the site configuration.',1,0,'private'),(2,'default','framework.local','Oroboros','global','global','view','http','framework.local','stylesheet.css','default','This is the default layout provided for sites upon initial installation.',1,1,'open'),(3,'global','oroboros.local','Global Inheritance','self','global','SYSTEM','http','','stylesheet.css','default','DO NOT DELETE. This manages global inheritance.',1,0,'private');
/*!40000 ALTER TABLE `sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sites_components`
--

DROP TABLE IF EXISTS `sites_components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites_components` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `component` varchar(16) NOT NULL DEFAULT '',
  `site` varchar(16) NOT NULL DEFAULT 'default',
  `permission` varchar(16) NOT NULL DEFAULT 'view',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `component_2` (`component`,`site`),
  KEY `component` (`component`),
  KEY `site` (`site`),
  KEY `permission` (`permission`),
  CONSTRAINT `sites_components_ibfk_1` FOREIGN KEY (`permission`) REFERENCES `permissions` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `sites_components_ibfk_2` FOREIGN KEY (`site`) REFERENCES `sites` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sites_components_ibfk_3` FOREIGN KEY (`component`) REFERENCES `components` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sites_components`
--

LOCK TABLES `sites_components` WRITE;
/*!40000 ALTER TABLE `sites_components` DISABLE KEYS */;
INSERT INTO `sites_components` VALUES (1,'core_controller','default','registered',0),(3,'core_dashbar','default','registered',0),(4,'core_drawer','default','registered',0);
/*!40000 ALTER TABLE `sites_components` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sites_content`
--

DROP TABLE IF EXISTS `sites_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites_content` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site` varchar(16) NOT NULL DEFAULT 'default',
  `type` varchar(16) NOT NULL DEFAULT 'html',
  `content` text,
  `hook` varchar(32) NOT NULL DEFAULT 'BODY',
  `hierarchy` int(4) unsigned zerofill NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  `author` varchar(16) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `permission` varchar(16) NOT NULL DEFAULT 'view',
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `author` (`author`),
  KEY `type_2` (`type`),
  KEY `hook` (`hook`),
  KEY `permission` (`permission`),
  KEY `site` (`site`),
  KEY `site_2` (`site`),
  KEY `site_3` (`site`),
  CONSTRAINT `sites_content_ibfk_1` FOREIGN KEY (`site`) REFERENCES `sites` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sites_content_ibfk_2` FOREIGN KEY (`permission`) REFERENCES `permissions` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sites_content_ibfk_4` FOREIGN KEY (`type`) REFERENCES `pages_content_types` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sites_content_ibfk_5` FOREIGN KEY (`hook`) REFERENCES `hooks` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sites_content`
--

LOCK TABLES `sites_content` WRITE;
/*!40000 ALTER TABLE `sites_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `sites_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sites_links`
--

DROP TABLE IF EXISTS `sites_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites_links` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT 'index',
  `parent` varchar(16) DEFAULT NULL,
  `linkset` varchar(16) NOT NULL DEFAULT 'default',
  `site` varchar(16) NOT NULL DEFAULT 'default',
  `link` varchar(256) NOT NULL DEFAULT '#',
  `link_type` varchar(16) NOT NULL DEFAULT 'internal',
  `script_namespace` varchar(256) DEFAULT NULL,
  `icon` varchar(256) DEFAULT NULL,
  `display` varchar(128) NOT NULL DEFAULT 'Untitled Link',
  `permission` varchar(16) DEFAULT 'view',
  `hierarchy` int(3) unsigned zerofill NOT NULL DEFAULT '000',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `linkset` (`linkset`),
  KEY `site` (`site`),
  KEY `slug` (`slug`),
  KEY `link` (`link`(255)),
  KEY `display` (`display`),
  KEY `permission` (`permission`),
  KEY `parent` (`parent`),
  KEY `icon` (`icon`(255)),
  KEY `link_type` (`link_type`),
  KEY `script_namespace` (`script_namespace`(255)),
  CONSTRAINT `sites_links_ibfk_2` FOREIGN KEY (`site`) REFERENCES `sites` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sites_links_ibfk_3` FOREIGN KEY (`permission`) REFERENCES `permissions` (`slug`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sites_links_ibfk_4` FOREIGN KEY (`parent`) REFERENCES `sites_links` (`slug`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sites_links_ibfk_5` FOREIGN KEY (`linkset`) REFERENCES `sites_links_sets` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sites_links_ibfk_6` FOREIGN KEY (`link_type`) REFERENCES `sites_links_types` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=366 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sites_links`
--

LOCK TABLES `sites_links` WRITE;
/*!40000 ALTER TABLE `sites_links` DISABLE KEYS */;
INSERT INTO `sites_links` VALUES (1,'index',NULL,'toplinks','default','framework.local','internal',NULL,NULL,'Home','view',000,1),(2,'about',NULL,'toplinks','default','framework.local/about','internal',NULL,NULL,'About','default',100,1),(3,'login',NULL,'toplinks','default','framework.local/login','internal',NULL,NULL,'Login','default',999,1),(6,'profile',NULL,'toplinks','default','framework.local/profile','internal',NULL,NULL,'Profile','registered',010,1),(7,'docs',NULL,'help','default','/docs','internal',NULL,NULL,'Docs','registered',020,1),(14,'links',NULL,'sites','default','/admin','internal',NULL,NULL,'Link Management','edit_links',005,1),(15,'logout',NULL,'profilelinks','default','/dashboard/logout','internal',NULL,NULL,'Logout','registered',999,1),(16,'frontend','index','toplinks','default','framework.local/','internal',NULL,NULL,'Site Frontend','registered',000,1),(19,'user-settings',NULL,'profilelinks','default','/profile/settings','internal',NULL,NULL,'Preferences','registered',001,1),(20,'profile',NULL,'profilelinks','default','/profile','internal',NULL,NULL,'Profile','registered',002,1),(21,'notifications',NULL,'profilelinks','default','/notifications','internal',NULL,NULL,'Notification Settings','registered',003,1),(22,'privacy',NULL,'profilelinks','default','/profile/privacy','internal',NULL,NULL,'Privacy','registered',004,1),(23,'dashboard','index','toplinks','default','framework.local/dashboard','internal',NULL,NULL,'Dashboard','registered',001,1),(25,'accounts',NULL,'admin','default','/accounts','internal',NULL,NULL,'Accounts','login',000,1),(31,'dns',NULL,'webmaster','default','/dns','internal',NULL,NULL,'DNS','webmaster',002,1),(32,'server',NULL,'developer','default','/server','internal',NULL,NULL,'Server','operator',001,1),(33,'nodes',NULL,'webmaster','default','/nodes','internal',NULL,NULL,'Node Control','webmaster',000,1),(34,'groups',NULL,'admin','default','/users/groups','internal',NULL,NULL,'User Groups','operator',001,1),(35,'/users/blacklist','/users/security','moderator','default','/users/blacklist','internal',NULL,NULL,'Blacklist Settings','moderator',010,1),(36,'/users/whitelist','/users/security','moderator','default','/users/whitelist','internal',NULL,NULL,'Whitelist Settings','moderator',009,1),(37,'debug',NULL,'developer','default','/debug','internal',NULL,NULL,'Debug','developer',001,1),(38,'maintenance',NULL,'developer','default','/maintenance','internal',NULL,NULL,'Maintenance','webmaster',000,1),(39,'modules',NULL,'developer','default','/modules','internal',NULL,NULL,'Modules','operator',002,1),(40,'components',NULL,'developer','default','/components','internal',NULL,NULL,'Components','admin',003,1),(41,'staging',NULL,'developer','default','/staging','internal',NULL,NULL,'Staging','developer',004,1),(42,'webshell',NULL,'developer','default','/webshell','internal',NULL,NULL,'Web Shell','developer',005,1),(43,'logs',NULL,'developer','default','/logs','internal',NULL,NULL,'System Logs','developer',006,1),(44,'permissions',NULL,'admin','default','/permissions','internal',NULL,NULL,'Permissions','operator',000,1),(45,'sites',NULL,'webmaster','default','/sites','internal',NULL,NULL,'Sites','operator',003,1),(46,'database',NULL,'developer','default','/database','internal',NULL,NULL,'Database','developer',007,1),(47,'sourcecontrol',NULL,'developer','default','/source-control','internal',NULL,NULL,'Source Control','developer',008,1),(48,'cgi',NULL,'developer','default','/cgi','internal',NULL,NULL,'CGI','developer',009,1),(49,'ide',NULL,'developer','default','/ide','internal',NULL,NULL,'Code Editor','developer',010,1),(50,'ftp',NULL,'developer','default','/ftp','internal',NULL,NULL,'FTP Panel','developer',011,1),(51,'ssh',NULL,'developer','default','/ssh','internal',NULL,NULL,'SSH Panel','developer',012,1),(52,'seo',NULL,'webmaster','default','/seo','internal',NULL,NULL,'SEO Panel','webmaster',005,1),(53,'analytics',NULL,'webmaster','default','/analytics','internal',NULL,NULL,'Site Analytics','webmaster',001,1),(54,'pages',NULL,'content','default','/pages','internal',NULL,NULL,'Pages','admin',000,1),(55,'/users/manage',NULL,'moderator','default','/users/manage','internal',NULL,NULL,'User Management','moderator',000,1),(56,'content',NULL,'content','default','/content','internal',NULL,NULL,'Content','admin',003,1),(57,'templates',NULL,'designer','default','/templates','internal',NULL,NULL,'Templates','admin',000,1),(58,'themes',NULL,'designer','default','/themes','internal',NULL,NULL,'Themes','admin',001,1),(59,'media',NULL,'designer','default','/media','internal',NULL,NULL,'Media','admin',002,1),(60,'links',NULL,'content','default','/links','internal',NULL,NULL,'Links','admin',002,1),(61,'syndication',NULL,'content','default','/syndication','internal',NULL,NULL,'Syndication','webmaster',004,1),(62,'typography',NULL,'designer','default','/typography','internal',NULL,NULL,'Typography','designer',003,1),(63,'pallates',NULL,'designer','default','/palletes','internal',NULL,NULL,'Color Schemes','designer',004,1),(64,'local-node',NULL,'nodes','default','/nodes/local','internal',NULL,NULL,'Local Node','webmaster',000,1),(65,'node-requests',NULL,'nodes','default','/nodes/requests','internal',NULL,NULL,'Node Requests','webmaster',001,1),(66,'add-node',NULL,'nodes','default','/nodes/add','internal',NULL,NULL,'Add Node','webmaster',002,1),(67,'access-rights',NULL,'nodes','default','/nodes/permissions','internal',NULL,NULL,'Node Access Rights','webmaster',003,1),(68,'node-map',NULL,'nodes','default','/nodes/map','internal',NULL,NULL,'Node Map','webmaster',004,1),(69,'shared-assets',NULL,'nodes','default','/nodes/share','internal',NULL,NULL,'Shared Assets','webmaster',005,1),(70,'node-mirrors',NULL,'nodes','default','/nodes/mirror','internal',NULL,NULL,'Site Mirrors','webmaster',006,1),(71,'node-hierarchy',NULL,'nodes','default','/nodes/hierarchy','internal',NULL,NULL,'Node Hierarchy','webmaster',007,1),(72,'lock-node',NULL,'nodes','default','/nodes/lock','internal',NULL,NULL,'Lock Node','webmaster',008,1),(73,'private-networks',NULL,'nodes','default','/nodes/networks','internal',NULL,NULL,'Private Node Networks','webmaster',009,1),(74,'node-settings',NULL,'nodes','default','/nodes/settings','internal',NULL,NULL,'Node Settings','webmaster',010,1),(75,'server',NULL,'server','default','/server','internal',NULL,NULL,'Overview','operator',000,1),(76,'file-permissions',NULL,'server','default','/files/permissions','internal',NULL,NULL,'File Permissions','developer',001,1),(77,'webshell',NULL,'server','default','/webshell','internal',NULL,NULL,'Web Shell','developer',002,1),(78,'ssh',NULL,'server','default','/ssh','internal',NULL,NULL,'SSH Tunnels','developer',003,1),(79,'cgi',NULL,'server','default','/cgi','internal',NULL,NULL,'CGI Interface','developer',004,1),(80,'cron',NULL,'server','default','/cron','internal',NULL,NULL,'Cron Scheduler','operator',005,1),(81,'server-installer',NULL,'server','default','/server/install','internal',NULL,NULL,'Server Installation Manager','login',006,1),(82,'core-database',NULL,'database','default','/database/core','internal',NULL,NULL,'Core Database Manager','developer',000,1),(83,'database-specs',NULL,'database','default','/database','internal',NULL,NULL,'Database Overview','developer',001,1),(84,'edit-databases',NULL,'database','default','/database/edit','internal',NULL,NULL,'Edit Databases','master_developer',002,1),(85,'remote-databases',NULL,'database','default','/database/remote','internal',NULL,NULL,'Remote Databases','master_developer',003,1),(86,'database-bridge',NULL,'database','default','/database/bridge','internal',NULL,NULL,'Database Data Bridge','master_developer',004,1),(87,'sql-editor',NULL,'database','default','/database/sql','internal',NULL,NULL,'SQL Editor','developer',005,1),(88,'table-map',NULL,'database','default','/database/tables','internal',NULL,NULL,'Relational Table Map','developer',006,1),(89,'database-users',NULL,'database','default','/database/users','internal',NULL,NULL,'Database Users','master_developer',007,1),(90,'table-editor',NULL,'database','default','/database/tables/edit','internal',NULL,NULL,'SQL Table Editor','developer',008,1),(91,'sql-archiver',NULL,'database','default','/database/backup','internal',NULL,NULL,'Database Backup','developer',009,1),(92,'sql-rollback',NULL,'database','default','/database/rollback','internal',NULL,NULL,'Database Rollback','developer',010,1),(93,'dns',NULL,'sites','default','/dns','internal',NULL,NULL,'DNS','webmaster',000,1),(94,'site-manager',NULL,'sites','default','/sites/manage','internal',NULL,NULL,'Website Manager','webmaster',001,1),(95,'subdomain-manage',NULL,'sites','default','/subdomains/manage','internal',NULL,NULL,'Subdomain Manager','operator',002,1),(96,'site-inheritance',NULL,'sites','default','/sites/inherit','internal',NULL,NULL,'Site Inheritance','webmaster',003,1),(97,'site-access',NULL,'sites','default','/sites/permissions','internal',NULL,NULL,'Site Access','webmaster',004,1),(98,'site-specs',NULL,'sites','default','/sites','internal',NULL,NULL,'Site Overview','webmaster',005,1),(99,'site-analytics',NULL,'sites','default','/analytics','internal',NULL,NULL,'Site Analytics','webmaster',006,1),(100,'page-map',NULL,'pages','default','/pages/map','internal',NULL,NULL,'Page Map','admin',000,1),(101,'edit-pages',NULL,'pages','default','/pages/edit','internal',NULL,NULL,'Edit Pages','admin',001,1),(102,'page-inheritance',NULL,'pages','default','/pages/inherit','internal',NULL,NULL,'Page Inheritance','admin',002,1),(103,'page-scheduler',NULL,'pages','default','/pages/schedule','internal',NULL,NULL,'Page Scheduler','admin',003,1),(104,'page-access',NULL,'pages','default','/pages/permissions','internal',NULL,NULL,'Page Access Rights','admin',004,1),(105,'page-redirects',NULL,'pages','default','/pages/redirects','internal',NULL,NULL,'Page Redirection','admin',005,1),(106,'create-content','content','content','default','/content/create','internal',NULL,NULL,'Create Content','admin',010,1),(107,'edit-content','content','content','default','/content/edit','internal',NULL,NULL,'Edit Content','admin',011,1),(108,'content-access','content','content','default','/content/access','internal',NULL,NULL,'Content Access Rights','admin',012,1),(109,'import-content','content','content','default','/content/import','internal',NULL,NULL,'Import Content','admin',013,1),(110,'archive-content','content','content','default','/content/archive','internal',NULL,NULL,'Archive Content','admin',014,1),(111,'content-schedule','content','content','default','/content/schedule','internal',NULL,NULL,'Schedule Content','admin',015,1),(112,'content-export','content','content','default','/content/export','internal',NULL,NULL,'Export Content','admin',016,1),(113,'content-sharing','content','content','default','/content/share','internal',NULL,NULL,'Content Sharing','admin',017,1),(114,'module-manager',NULL,'modules','default','/modules/manage','internal',NULL,NULL,'Module Manager','admin',000,1),(115,'module-editor',NULL,'modules','default','/modules/edit','internal',NULL,NULL,'Module Editor','developer',001,1),(116,'module-check',NULL,'modules','default','/modules/validate','internal',NULL,NULL,'Module Diagnostics','admin',002,1),(117,'module-history',NULL,'modules','default','/modules/history','internal',NULL,NULL,'Module History','operator',003,1),(118,'module-access',NULL,'modules','default','/modules/permissions','internal',NULL,NULL,'Module Access Rights','operator',004,1),(119,'module-import',NULL,'modules','default','/modules/import','internal',NULL,NULL,'Module Importer','admin',005,1),(120,'module-export',NULL,'modules','default','/modules/export','internal',NULL,NULL,'Module Exporter','developer',006,1),(121,'module-builder',NULL,'modules','default','/modules/create','internal',NULL,NULL,'Module Builder','developer',007,1),(122,'module-inherit',NULL,'modules','default','/modules/inherit','internal',NULL,NULL,'Module Inheritance','operator',008,1),(124,'logout',NULL,'toplinks','default','framework.local/dashboard/logout','internal',NULL,NULL,'Logout','registered',999,1),(125,'dashboard',NULL,'go','default','/dashboard','internal',NULL,NULL,'Dashboard','registered',000,1),(126,'frontend',NULL,'go','default','/','internal',NULL,NULL,'Frontend','registered',001,1),(127,'ssl',NULL,'webmaster','default','/ssl','internal',NULL,NULL,'SSL Certificates','webmaster',004,1),(128,'newsite',NULL,'file','default','/sites/create','internal',NULL,NULL,'New Site...','webmaster',000,1),(129,'newpage',NULL,'file','default','/pages/create','internal',NULL,NULL,'New Page...','operator',002,1),(130,'browsefiles',NULL,'file','default','/files/browse','internal',NULL,NULL,'Browse Files...','operator',003,1),(131,'uploadfiles',NULL,'file','default','/files/upload','internal',NULL,NULL,'Upload Files...','operator',004,1),(132,'downloadfiles',NULL,'file','default','/files/download','internal',NULL,NULL,'Download Files...','operator',005,1),(133,'import',NULL,'file','default','/files/import','internal',NULL,NULL,'Import...','operator',006,1),(134,'export',NULL,'file','default','/files/export','internal',NULL,NULL,'Export...','operator',007,1),(135,'archive',NULL,'file','default','/files/archive','internal',NULL,NULL,'Archive...','operator',008,1),(136,'restore',NULL,'file','default','/files/restore','internal',NULL,NULL,'Restore...','operator',009,1),(137,'newsubdomain',NULL,'file','default','/subdomains/create','internal',NULL,NULL,'New Subdomain...','webmaster',001,1),(138,'editsite',NULL,'edit','default','/sites/edit','internal',NULL,NULL,'Edit Site...','webmaster',010,1),(139,'editsubdomain',NULL,'edit','default','/subdomains/edit','internal',NULL,NULL,'Edit Subdomain...','webmaster',011,1),(140,'editpage',NULL,'edit','default','/pages/edit','internal',NULL,NULL,'Edit Page...','operator',012,1),(141,'editfile',NULL,'edit','default','/files/edit','internal',NULL,NULL,'Edit File...','operator',014,1),(142,'editcontent',NULL,'edit','default','/content/edit','internal',NULL,NULL,'Edit Content...','admin',003,1),(143,'undo',NULL,'edit','default','undo','script','oroboros.ui.content',NULL,'Undo','registered',000,1),(144,'redo',NULL,'edit','default','redo','script','oroboros.ui.content',NULL,'Redo','registered',001,1),(145,'actionhistory',NULL,'edit','default','history','script','oroboros.ui.content',NULL,'Action History...','registered',002,1),(146,'cut',NULL,'edit','default','cut','script','oroboros.ui.content',NULL,'Cut','registered',003,1),(147,'copy',NULL,'edit','default','copy','script','oroboros.ui.content',NULL,'Copy','registered',004,1),(148,'paste',NULL,'edit','default','paste','script','oroboros.ui.content',NULL,'Paste','registered',005,1),(149,'zoomin',NULL,'view','default','zoomIn','script','oroboros.ui.view',NULL,'Zoom In','registered',000,1),(150,'zoomout',NULL,'view','default','zoomOut','script','oroboros.ui.view',NULL,'Zoom Out','registered',001,1),(151,'fullsize',NULL,'view','default','zoomFull','script','oroboros.ui.view',NULL,'Full Size','registered',002,1),(152,'hooks',NULL,'content','default','/hooks','internal',NULL,NULL,'Hooks','webmaster',001,1),(153,'profile',NULL,'userboxlinks','default','/profile','internal',NULL,NULL,'Profile','registered',000,1),(154,'settings',NULL,'userboxlinks','default','/profile/settings','internal',NULL,NULL,'Settings','registered',001,1),(155,'notifications',NULL,'userboxlinks','default','/profile/notifications','internal',NULL,NULL,'Notifications','registered',002,1),(156,'messages',NULL,'userboxlinks','default','/profile/messages','internal',NULL,NULL,'Messages','registered',003,1),(157,'logout',NULL,'userboxlinks','default','/dashboard/logout','internal',NULL,NULL,'Log out','registered',999,1),(158,'/users/greylist','/users/manage','moderator','default','/users/greylist','internal',NULL,NULL,'Greylist Settings','moderator',011,1),(159,'/users/groups','/users/security','moderator','default','/users/groups','internal',NULL,NULL,'User Groups','moderator',001,1),(160,'/users/security',NULL,'moderator','default','/users/security','internal',NULL,NULL,'User Security','moderator',002,1),(162,'/users/mute','/users/manage','moderator','default','/users/mute','internal',NULL,NULL,'Mute Users','moderator',004,1),(163,'/users/restrict','/users/manage','moderator','default','/users/restrict','internal',NULL,NULL,'Restrict Users','moderator',005,1),(164,'/users/ban','/users/manage','moderator','default','/users/ban','internal',NULL,NULL,'Ban Users','moderator',007,1),(165,'/users/suspend','/users/manage','moderator','default','/users/suspend','internal',NULL,NULL,'Suspend Users','moderator',006,1),(166,'/users/flag','/users/monitor','moderator','default','/users/flag','internal',NULL,NULL,'Flag Users','moderator',008,1),(167,'/users/monitor',NULL,'moderator','default','/users/monitor','internal',NULL,NULL,'Monitor Users','moderator',003,1),(168,'/settings',NULL,'settings','default','/settings','internal',NULL,NULL,'Settings Overview','admin',000,1),(171,'/settings/global',NULL,'settings','default','/settings/globalSettings','internal',NULL,NULL,'Global Settings','owner',001,1),(172,'/settings/site',NULL,'settings','default','/settings/site','internal',NULL,NULL,'Site Settings','operator',002,1),(173,'/settings/page',NULL,'settings','default','/settings/page','internal',NULL,NULL,'Page Settings','admin',003,1),(174,'/settings/conten',NULL,'settings','default','/settings/content','internal',NULL,NULL,'Content Settings','admin',004,1),(175,'/settings/forum',NULL,'settings','default','/settings/forum','internal',NULL,NULL,'Forum Settings','admin',005,1),(176,'/settings/commen',NULL,'settings','default','/settings/comments','internal',NULL,NULL,'Comment Settings','admin',006,1),(177,'/settings/users',NULL,'settings','default','/settings/users','internal',NULL,NULL,'User Settings','admin',007,1),(178,'/settings/regist',NULL,'settings','default','/settings/registration','internal',NULL,NULL,'Registration Settings','admin',008,1),(179,'/settings/module',NULL,'settings','default','/settings/modules','internal',NULL,NULL,'Module Settings','admin',009,1),(180,'/settings/compon',NULL,'settings','default','/settings/components','internal',NULL,NULL,'Component Settings','admin',010,1),(181,'/settings/themes',NULL,'settings','default','/settings/themes','internal',NULL,NULL,'Theme Settings','designer',011,1),(182,'/settings/templa',NULL,'settings','default','/settings/templates','internal',NULL,NULL,'Template Settings','designer',012,1),(183,'/settings/access',NULL,'settings','default','/settings/access','internal',NULL,NULL,'Access Settings','admin',013,1),(184,'/settings/nodes',NULL,'settings','default','/settings/nodes','internal',NULL,NULL,'Node Settings','webmaster',014,1),(186,'perms-add','permissions','admin','default','/permissions/create','internal',NULL,NULL,'Create Permission Node','admin',000,1),(187,'perms-edit','permissions','admin','default','/permissions/edit','internal',NULL,NULL,'Edit Permission Nodes','admin',001,1),(188,'perms-groups','permissions','admin','default','/permissions/groups','internal',NULL,NULL,'Permission Groups','admin',002,1),(189,'sites/create','sites','webmaster','default','/sites/create','internal',NULL,NULL,'Create Site','operator',003,1),(190,'sites/edit','sites','webmaster','default','/sites/edit','internal',NULL,NULL,'Edit Site','operator',003,1),(191,'sites/remove','sites','webmaster','default','/sites/remove','internal',NULL,NULL,'Remove Site','operator',003,1),(192,'sites/import','sites','webmaster','default','/sites/import','internal',NULL,NULL,'Import Site','operator',003,1),(193,'sites/export','sites','webmaster','default','/sites/export','internal',NULL,NULL,'Export Site','operator',003,1),(194,'sites/package','sites','webmaster','default','/sites/package','internal',NULL,NULL,'Package Site','operator',003,1),(195,'sites/lockdown','sites','webmaster','default','/sites/lockdown','internal',NULL,NULL,'Lockdown Site','operator',003,1),(196,'sites/archive','sites','webmaster','default','/sites/archive','internal',NULL,NULL,'Archive Site','operator',003,1),(197,'sites/share','sites','webmaster','default','/sites/share','internal',NULL,NULL,'Share Site','operator',003,1),(198,'accounts/add','accounts','admin','default','/accounts/add','internal',NULL,NULL,'Add Accounts','admin',000,1),(199,'accounts/modify','accounts','admin','default','/accounts/modify','internal',NULL,NULL,'Modify Accounts','admin',000,1),(200,'accounts/import','accounts','admin','default','/accounts/import','internal',NULL,NULL,'Import Accounts','admin',000,1),(201,'accounts/export','accounts','admin','default','/accounts/export','internal',NULL,NULL,'Export Accounts','admin',000,1),(202,'accounts/archive','accounts','admin','default','/accounts/archive','internal',NULL,NULL,'Archive Accounts','admin',000,1),(203,'accounts/restore','accounts','admin','default','/accounts/restore','internal',NULL,NULL,'Restore Accounts','admin',000,1),(204,'accounts/remote','accounts','admin','default','/accounts/remote','internal',NULL,NULL,'Remote Accounts','admin',000,1),(207,'accounts/delete','accounts','admin','default','/accounts/delete','internal',NULL,NULL,'Delete Accounts','admin',000,1),(208,'accounts/clone','accounts','admin','default','/accounts/clone','internal',NULL,NULL,'Clone Accounts','admin',000,1),(209,'analytics/users','analytics','webmaster','default','/analytics/users','internal',NULL,NULL,'User Analytics','webmaster',000,1),(210,'analytics/google','analytics','webmaster','default','/analytics/google','internal',NULL,NULL,'Google Analytics','webmaster',000,1),(211,'analytics/yahoo','analytics','webmaster','default','/analytics/yahoo','internal',NULL,NULL,'Yahoo Analytics','webmaster',000,1),(230,'nodes/local','nodes','webmaster','default','/nodes/local','internal',NULL,NULL,'Local Node','webmaster',000,1),(231,'nodes/connects','nodes','webmaster','default','/nodes/connections','internal',NULL,NULL,'Node Connections','webmaster',000,1),(232,'nodes/requests','nodes','webmaster','default','/nodes/requests','internal',NULL,NULL,'Node Requests','webmaster',000,1),(233,'nodes/accounts','nodes','webmaster','default','/nodes/accounts','internal',NULL,NULL,'Node Accounts','webmaster',000,1),(234,'nodes/access','nodes','webmaster','default','/nodes/access','internal',NULL,NULL,'Node Access','webmaster',000,1),(235,'nodes/sites','nodes','webmaster','default','/nodes/sites','internal',NULL,NULL,'Node Site Sharing','webmaster',000,1),(236,'nodes/mirrors','nodes','webmaster','default','/nodes/mirrors','internal',NULL,NULL,'Node Mirrors','webmaster',000,1),(237,'nodes/data','nodes','webmaster','default','/nodes/data','internal',NULL,NULL,'Node Data Sharing','webmaster',000,1),(238,'nodes/content','nodes','webmaster','default','/nodes/content','internal',NULL,NULL,'Node Content Sharing','webmaster',000,1),(239,'nodes/assets','nodes','webmaster','default','/nodes/assets','internal',NULL,NULL,'Node Assets','webmaster',000,1),(240,'nodes/repos','nodes','webmaster','default','/nodes/repositories','internal',NULL,NULL,'Node Repositories','webmaster',000,1),(241,'nodes/cdns','nodes','webmaster','default','/nodes/cdn','internal',NULL,NULL,'Node CDN\'s','webmaster',000,1),(243,'nodes/distrib','nodes','webmaster ','default','/nodes/distribution','internal',NULL,NULL,'Node Distribution','webmaster',000,1),(244,'nodes/bridge','nodes','webmaster','default','/nodes/bridge','internal',NULL,NULL,'Node Bridge','webmaster',000,1),(245,'nodes/network','nodes','webmaster','default','/nodes/network','internal',NULL,NULL,'Node Network','webmaster',000,1),(246,'nodes/community','nodes','webmaster','default','/nodes/community','internal',NULL,NULL,'Node Community','webmaster',000,1),(247,'nodes/flag','nodes','webmaster','default','/nodes/flag','internal',NULL,NULL,'Flag Nodes','webmaster',000,1),(248,'nodes/auth','nodes','webmaster','default','/nodes/authorize','internal',NULL,NULL,'Authorize Nodes','webmaster',000,1),(249,'nodes/deauth','nodes','webmaster','default','/nodes/deauthorize','internal',NULL,NULL,'Deauthorize Node','webmaster',000,1),(251,'nodes/restrict','nodes','webmaster','default','/nodes/restrict','internal',NULL,NULL,'Restrict Nodes','webmaster',000,1),(252,'seo/keywords','seo','webmaster','default','/seo/keywords','internal',NULL,NULL,'Keywords','webmaster',000,1),(253,'seo/content','seo','webmaster','default','/seo/content','internal',NULL,NULL,'Content Analysis','webmaster',000,1),(254,'seo/robots','seo','webmaster','default','/seo/robots','internal',NULL,NULL,'Robots.txt','webmaster',000,1),(255,'seo/crawlers','seo','webmaster','default','/seo/crawlers','internal',NULL,NULL,'Webcrawlers','webmaster',000,1),(256,'seo/sitemap','seo','webmaster','default','/seo/sitemap','internal',NULL,NULL,'Sitemap','webmaster',000,1),(257,'seo/tracking','seo','webmaster','default','/seo/tracking','internal',NULL,NULL,'Tracking','webmaster',000,1),(258,'seo/backlinks','seo','webmaster','default','/seo/backlinks','internal',NULL,NULL,'Backlinks','webmaster',000,1),(259,'development','about','toplinks','default','framework.local/about/development','internal',NULL,NULL,'Development Capabilities','default',101,1),(260,'design','about','toplinks','default','framework.local/about/design','internal',NULL,NULL,'Design Functionality','default',102,1),(261,'networking','about','toplinks','default','framework.local/about/networking','internal',NULL,NULL,'Distributed Networking','default',103,1),(262,'extensibility','about','toplinks','default','framework.local/about/extensibility','internal',NULL,NULL,'Extensibility Support','default',104,1),(263,'display','about','toplinks','default','framework.local/about/display','internal',NULL,NULL,'Dynamic Display','default',105,1),(264,'inheritance','about','toplinks','default','framework.local/about/inheritance','internal',NULL,NULL,'Multi-Tier Inheritance','default',106,1),(265,'security','about','toplinks','default','framework.local/about/security','internal',NULL,NULL,'Expansive Security Options','default',107,1),(266,'groups/add','groups','admin','default','/groups/add','internal',NULL,NULL,'Add Group','admin',000,1),(267,'groups/modify','groups','admin','default','/groups/modify','internal',NULL,NULL,'Modify Group','admin',000,1),(268,'groups/import','groups','admin','default','/groups/import','internal',NULL,NULL,'Import Group','admin',000,1),(269,'groups/export','groups','admin','default','/groups/export','internal',NULL,NULL,'Export Group','admin',000,1),(270,'groups/archive','groups','admin','default','/groups/archive','internal',NULL,NULL,'Archive Group','admin',000,1),(271,'groups/restore','groups','admin','default','/groups/restore','internal',NULL,NULL,'Restore Group','admin',000,1),(272,'groups/remote','groups','admin','default','/groups/remote','internal',NULL,NULL,'Remote Group','admin',000,1),(273,'groups/delete','groups','admin','default','/groups/delete','internal',NULL,NULL,'Delete Group','admin',000,1),(274,'groups/clone','groups','admin','default','/groups/clone','internal',NULL,NULL,'Clone Group','admin',000,1),(275,'/docs/owner','docs','help','default','/docs/owner','internal',NULL,NULL,'Ownership','registered',020,1),(276,'/docs/developer','docs','help','default','/docs/developer','internal',NULL,NULL,'Developer','registered',020,1),(277,'/docs/webmaster','docs','help','default','/docs/webmaster','internal',NULL,NULL,'Webmaster','registered',020,1),(278,'/docs/designer','docs','help','default','/docs/designer','internal',NULL,NULL,'Designer','registered',020,1),(279,'/docs/operator','docs','help','default','/docs/operator','internal',NULL,NULL,'Operator','registered',020,1),(280,'/docs/admin','docs','help','default','/docs/admin','internal',NULL,NULL,'Admin','registered',020,1),(281,'/docs/moderator','docs','help','default','/docs/moderator','internal',NULL,NULL,'Moderator','registered',020,1),(282,'/docs/author','docs','help','default','/docs/author','internal',NULL,NULL,'Author','registered',020,1),(283,'/docs/registered','docs','help','default','/docs/registered','internal',NULL,NULL,'Registered User','registered',020,1),(284,'/docs/user','docs','help','default','/docs/user','internal',NULL,NULL,'User','registered',020,1),(285,'/docs/nodes','docs','help','default','/docs/nodes','internal',NULL,NULL,'Nodes','registered',020,1),(286,'/docs/navigation','docs','help','default','/docs/navigation','internal',NULL,NULL,'System Navigation','registered',020,1),(287,'/docs/setup','docs','help','default','/docs/setup','internal',NULL,NULL,'System Setup','registered',020,1),(288,'/docs/import','docs','help','default','/docs/import','internal',NULL,NULL,'Asset Import','registered',020,1),(289,'/docs/export','docs','help','default','/docs/export','internal',NULL,NULL,'Asset Export','registered',020,1),(290,'/docs/troublesho','docs','help','default','/docs/troubleshooting','internal',NULL,NULL,'Troubleshooting','registered',020,1),(291,'/synd/email','syndication','content','default','/syndication/email','internal',NULL,NULL,'Email','webmaster',004,1),(292,'/synd/rss','syndication','content','default','/syndication/rss','internal',NULL,NULL,'RSS','webmaster',004,1),(293,'/synd/calendar','syndication','content','default','/syndication/calendar','internal',NULL,NULL,'Calendars','webmaster',004,1),(294,'/synd/announce','syndication','content','default','/syndication/announcements','internal',NULL,NULL,'Announcements','webmaster',004,1),(295,'/synd/node','syndication','content','default','/syndication/node','internal',NULL,NULL,'Node Content','webmaster',004,1),(296,'/pages/add','pages','content','default','/pages/add','internal',NULL,NULL,'Add Page','admin',000,1),(297,'/pages/modify','pages','content','default','/pages/modify','internal',NULL,NULL,'Modify Page','admin',000,1),(298,'/pages/import','pages','content','default','/pages/import','internal',NULL,NULL,'Import Pages','admin',000,1),(299,'/pages/export','pages','content','default','/pages/export','internal',NULL,NULL,'Export Pages','admin',000,1),(300,'/pages/archive','pages','content','default','/pages/archive','internal',NULL,NULL,'Archive Page','admin',000,1),(301,'/pages/restore','pages','content','default','/pages/restore','internal',NULL,NULL,'Restore Page','admin',000,1),(302,'/pages/remote','pages','content','default','/pages/remote','internal',NULL,NULL,'Remote Pages','admin',000,1),(303,'/pages/delete','pages','content','default','/pages/delete','internal',NULL,NULL,'Delete Page','admin',000,1),(304,'/pages/clone','pages','content','default','/pages/clone','internal',NULL,NULL,'Clone Page','admin',000,1),(305,'/hooks/register','hooks','content','default','/hooks/register','internal',NULL,NULL,'Register Hook','webmaster',001,1),(306,'/hooks/modify','hooks','content','default','/hooks/modify','internal',NULL,NULL,'Modify Hook','webmaster',001,1),(307,'/hooks/remove','hooks','content','default','/hooks/remove','internal',NULL,NULL,'Remove Hook','webmaster',001,1),(308,'/hooks/import','hooks','content','default','/hooks/import','internal',NULL,NULL,'Import Hooks','webmaster',001,1),(309,'/hooks/export','hooks','content','default','/hooks/export','internal',NULL,NULL,'Export Hooks','webmaster',001,1),(310,'links/add','links','content','default','/links/add','internal',NULL,NULL,'Add Linkset','admin',000,1),(311,'links/modify','links','content','default','/links/modify','internal',NULL,NULL,'Modify Linkset','admin',000,1),(312,'links/import','links','content','default','/links/import','internal',NULL,NULL,'Import Linksets','admin',000,1),(313,'links/export','links','content','default','/links/export','internal',NULL,NULL,'Export Linksets','admin',000,1),(314,'links/archive','links','content','default','/links/archive','internal',NULL,NULL,'Archive Linkset','admin',000,1),(315,'links/restore','links','content','default','/links/restore','internal',NULL,NULL,'Restore Linkset','admin',000,1),(316,'links/remove','links','content','default','/links/remove','internal',NULL,NULL,'Delete Linkset','admin',000,1),(317,'links/clone','links','content','default','/links/clone','internal',NULL,NULL,'Clone Linkset','admin',000,1),(318,'/dns/register','dns','webmaster','default','/dns/register','internal',NULL,NULL,'Register Domain','webmaster',002,1),(319,'/dns/modify','dns','webmaster','default','/dns/modify','internal',NULL,NULL,'Modify Domain','webmaster',002,1),(320,'/dns/deauthorize','dns','webmaster','default','/dns/deauthorize','internal',NULL,NULL,'Deauthorize Domain','webmaster',002,1),(321,'/dns/assign','dns','webmaster','default','/dns/assign','internal',NULL,NULL,'Assign Domain','webmaster',002,1),(322,'/dns/suspend','dns','webmaster','default','/dns/suspend','internal',NULL,NULL,'Suspend Domain','webmaster',002,1),(323,'/dns/redirect','dns','webmaster','default','/dns/redirect','internal',NULL,NULL,'Redirect Domain','webmaster',002,1),(324,'/components/mng','components','developer','default','/components/manage','internal',NULL,NULL,'Manage Components','admin',003,1),(325,'/components/edt','components','developer','default','/components/edit','internal',NULL,NULL,'Edit Components','admin',003,1),(326,'/components/add','components','developer','default','/components/add','internal',NULL,NULL,'Add Components','admin',003,1),(327,'/components/rmv','components','developer','default','/components/remove','internal',NULL,NULL,'Remove Components','admin',003,1),(328,'/components/val','components','developer','default','/components/validate','internal',NULL,NULL,'Validate Component','admin',003,1),(329,'/components/hist','components','developer','default','/components/history','internal',NULL,NULL,'Component History','admin',003,1),(330,'/components/perm','components','developer','default','/components/permissions','internal',NULL,NULL,'Component Permissions','admin',003,1),(331,'/components/imp','components','developer','default','/components/import','internal',NULL,NULL,'Import Components','admin',003,1),(332,'/components/exp','components','developer','default','/components/export','internal',NULL,NULL,'Export Components','admin',003,1),(333,'/components/pkg','components','developer','default','/components/package','internal',NULL,NULL,'Package Components','admin',003,1),(334,'/components/crea','components','developer','default','/components/create','internal',NULL,NULL,'Create Component','admin',003,1),(335,'/components/inhr','components','developer','default','/components/inherit','internal',NULL,NULL,'Component Inheritance','admin',003,1),(336,'/components/arch','components','developer','default','/components/archive','internal',NULL,NULL,'Archive Components','admin',003,1),(337,'/components/rest','components','developer','default','/components/restore','internal',NULL,NULL,'Restore Components','admin',003,1),(352,'/modules/add','modules','developer','default','/modules/add','internal',NULL,NULL,'Add Modules','admin',003,1),(353,'/modules/arch','modules','developer','default','/modules/archive','internal',NULL,NULL,'Archive Modules','admin',003,1),(354,'/modules/crea','modules','developer','default','/modules/create','internal',NULL,NULL,'Create Modules','admin',003,1),(355,'/modules/edt','modules','developer','default','/modules/edit','internal',NULL,NULL,'Edit Modules','admin',003,1),(356,'/modules/exp','modules','developer','default','/modules/export','internal',NULL,NULL,'Export Modules','admin',003,1),(357,'/modules/hist','modules','developer','default','/modules/history','internal',NULL,NULL,'Module History','admin',003,1),(358,'/modules/imp','modules','developer','default','/modules/import','internal',NULL,NULL,'Import Modules','admin',003,1),(359,'/modules/inhr','modules','developer','default','/modules/inherit','internal',NULL,NULL,'Modules Inheritance','admin',003,1),(360,'/modules/mng','modules','developer','default','/modules/manage','internal',NULL,NULL,'Manage Modules','admin',003,1),(361,'/modules/perm','modules','developer','default','/modules/permissions','internal',NULL,NULL,'Module Permissions','admin',003,1),(362,'/modules/pkg','modules','developer','default','/modules/package','internal',NULL,NULL,'Package Module','admin',003,1),(363,'/modules/rest','modules','developer','default','/modules/restore','internal',NULL,NULL,'Restore Modules','admin',003,1),(364,'/modules/rmv','modules','developer','default','/modules/remove','internal',NULL,NULL,'Remove Modules','admin',003,1),(365,'/modules/val','modules','developer','default','/modules/validate','internal',NULL,NULL,'Validate Module','admin',003,1);
/*!40000 ALTER TABLE `sites_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sites_links_sets`
--

DROP TABLE IF EXISTS `sites_links_sets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites_links_sets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT 'default',
  `parent` varchar(16) DEFAULT NULL,
  `hook` varchar(32) DEFAULT NULL,
  `icon` varchar(256) DEFAULT NULL,
  `link` varchar(256) DEFAULT NULL,
  `title` varchar(64) NOT NULL DEFAULT 'Default Linkset',
  `site` varchar(16) NOT NULL DEFAULT 'default',
  `permission` varchar(16) NOT NULL DEFAULT 'view',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `hierarchy` int(3) unsigned zerofill NOT NULL DEFAULT '000',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_3` (`slug`,`site`,`parent`,`hook`),
  KEY `slug` (`slug`),
  KEY `title` (`title`),
  KEY `site` (`site`),
  KEY `permission` (`permission`),
  KEY `parent` (`parent`),
  KEY `hook` (`hook`),
  KEY `slug_4` (`slug`,`site`),
  KEY `icon` (`icon`(255)),
  CONSTRAINT `sites_links_sets_ibfk_2` FOREIGN KEY (`hook`) REFERENCES `hooks` (`slug`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `sites_links_sets_ibfk_3` FOREIGN KEY (`parent`) REFERENCES `sites_links_sets` (`slug`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sites_links_sets`
--

LOCK TABLES `sites_links_sets` WRITE;
/*!40000 ALTER TABLE `sites_links_sets` DISABLE KEYS */;
INSERT INTO `sites_links_sets` VALUES (1,'headerlinks',NULL,'HEADERLINKS',NULL,NULL,'Header Linkset','default','view',1,000),(2,'user','dashhead',NULL,'ui/light/users.png',NULL,'User','default','registered',1,011),(3,'pages','dashlinks',NULL,NULL,NULL,'Pages','default','admin',1,004),(4,'sites','dashlinks',NULL,NULL,NULL,'Sites','default','webmaster',1,003),(5,'developer','dashhead',NULL,'ui/light/code.png',NULL,'Developer','default','developer',1,007),(6,'webmaster','dashhead',NULL,'ui/light/wrench.png',NULL,'Webmaster','default','webmaster',1,006),(7,'settings','dashhead',NULL,'ui/light/settings.png',NULL,'Settings','default','admin',1,004),(9,'moderator','dashhead',NULL,'ui/light/moderator.png',NULL,'Moderator','default','moderator',1,010),(10,'designer','dashhead',NULL,NULL,NULL,'Designer','default','designer',1,008),(11,'nodes','dashlinks',NULL,NULL,NULL,'Nodes','default','operator',1,000),(12,'server','dashlinks',NULL,NULL,NULL,'Server','default','operator',1,001),(13,'database','dashlinks',NULL,NULL,NULL,'Databases','default','developer',1,002),(14,'file','dashhead',NULL,'ui/light/folder.png',NULL,'File','default','admin',1,000),(15,'edit','dashhead',NULL,'ui/light/pencil.png',NULL,'Edit','default','admin',1,001),(16,'view','dashhead',NULL,NULL,NULL,'View','default','registered',1,002),(17,'dashhead',NULL,'DASHHEAD',NULL,NULL,'Dashhead','default','registered',1,000),(18,'help','dashhead',NULL,'ui/light/info.png',NULL,'Help','default','registered',1,012),(20,'profilelinks',NULL,'PROFILELINKS',NULL,NULL,'User Profile','default','registered',1,000),(21,'modules','dashlinks',NULL,NULL,NULL,'Modules','default','admin',1,006),(22,'components','dashlinks',NULL,NULL,NULL,'Components','default','admin',1,007),(23,'admin','dashhead',NULL,'ui/light/admin.png',NULL,'Admin','default','admin',1,009),(27,'content','dashlinks',NULL,NULL,NULL,'Content','default','admin',1,005),(28,'dashlinks',NULL,'DASHLINKS',NULL,NULL,'Dashboard Index Linkset','default','registered',1,000),(29,'go','dashhead',NULL,'ui/light/redo.png',NULL,'Go','default','registered',1,003),(30,'templates','dashlinks',NULL,NULL,NULL,'Templates','default','designer',1,008),(31,'themes','dashlinks',NULL,NULL,NULL,'Themes','default','designer',1,009),(32,'media','dashlinks',NULL,NULL,NULL,'Media','default','admin',1,010),(33,'content','dashhead',NULL,'ui/light/stack.png',NULL,'Content','default','admin',1,005),(34,'userbox',NULL,'USERBOX',NULL,NULL,'Userbox Linkset','default','registered',1,000),(35,'userboxlinks','userbox',NULL,NULL,NULL,'Userbox Links','default','registered',1,000),(36,'toplinks','headerlinks',NULL,NULL,NULL,'Default Frontend Linkset','default','view',1,000);
/*!40000 ALTER TABLE `sites_links_sets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sites_links_types`
--

DROP TABLE IF EXISTS `sites_links_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites_links_types` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) DEFAULT NULL,
  `title` varchar(32) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sites_links_types`
--

LOCK TABLES `sites_links_types` WRITE;
/*!40000 ALTER TABLE `sites_links_types` DISABLE KEYS */;
INSERT INTO `sites_links_types` VALUES (1,'internal','Internal Link','This is a link back to an internal page within the site.'),(2,'external','External Link','This is a link to an external website.'),(3,'script','Script Link','This link triggers a javascript function.'),(4,'node','Node Link','This link directs to a page or site within the node network.');
/*!40000 ALTER TABLE `sites_links_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sites_modules`
--

DROP TABLE IF EXISTS `sites_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites_modules` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(16) NOT NULL DEFAULT '',
  `type` varchar(16) DEFAULT 'third-party',
  `version` varchar(64) DEFAULT NULL,
  `site` varchar(16) NOT NULL DEFAULT 'default',
  `permission` varchar(16) NOT NULL DEFAULT 'view',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `site` (`site`),
  KEY `permission` (`permission`),
  KEY `module` (`module`),
  KEY `type` (`type`),
  KEY `version` (`version`),
  KEY `module_identifier` (`module`,`type`,`version`),
  CONSTRAINT `module_identifier` FOREIGN KEY (`module`, `type`, `version`) REFERENCES `modules` (`slug`, `type`, `version`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sites_modules_ibfk_1` FOREIGN KEY (`permission`) REFERENCES `permissions` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `sites_modules_ibfk_2` FOREIGN KEY (`site`) REFERENCES `sites` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sites_modules_ibfk_3` FOREIGN KEY (`type`) REFERENCES `modules_types` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sites_modules`
--

LOCK TABLES `sites_modules` WRITE;
/*!40000 ALTER TABLE `sites_modules` DISABLE KEYS */;
INSERT INTO `sites_modules` VALUES (1,'testmodule','core','0.0.1 alpha','default','architect',1);
/*!40000 ALTER TABLE `sites_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sites_sessions`
--

DROP TABLE IF EXISTS `sites_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites_sessions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `IPv4` int(10) unsigned NOT NULL,
  `login` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `site` varchar(16) NOT NULL DEFAULT '',
  `user` varchar(16) NOT NULL DEFAULT '',
  `session_id` varchar(32) DEFAULT NULL,
  `device_nonce` varchar(32) DEFAULT NULL,
  `user_nonce` varchar(32) DEFAULT NULL,
  `idle` datetime NOT NULL,
  `timeout` datetime NOT NULL,
  `last_active` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `session_id` (`session_id`),
  KEY `site` (`site`),
  KEY `user` (`user`),
  KEY `device_nonce` (`device_nonce`),
  KEY `user_nonce` (`user_nonce`)
) ENGINE=InnoDB AUTO_INCREMENT=1075 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sites_sessions`
--

LOCK TABLES `sites_sessions` WRITE;
/*!40000 ALTER TABLE `sites_sessions` DISABLE KEYS */;
INSERT INTO `sites_sessions` VALUES (956,0,0,'default','mopsyd','f5d6b3028f3d2ea138b5f321dc77866a','xoLUllBEK1OtoaQFHwM9oCIi8pHn7XUo','26VcXCuKq7ruTqj1sj7XPY4XSLHmWsFF','2014-09-20 20:31:43','2014-09-20 20:31:43','2014-09-22 09:29:51'),(958,0,0,'','visitor','ecccc226b94f1b328f0fd38217dc776c','0pHKAXUXtoE8XQ3XVe9oPLcUxlVFSBIO','h3jR8Y65IK7SOaCioaOLTv1uO7u7k65x','2014-09-21 01:25:12','2014-09-21 01:25:12','2014-09-21 01:25:12'),(960,0,0,'','visitor','c292172290e0ca143f94e5a8e646cf10','rnchLWJ3F5RVbAvxs3hS4xlCtv1qfxc4','41VFGYlbcp1064Pns0eMBtM61tLG43Xn','2014-09-21 01:25:12','2014-09-21 01:25:12','2014-09-21 01:25:12'),(962,0,0,'','visitor','b5ff8b12a568d14b3e88920ed2f33cb2','F6EY6jtu0XHT1n9UuLcm9wuz1NXWJvDx','bWhC8FVtC5FEb9Sbc3jFsi4vqNmBFcHR','2014-09-21 01:27:46','2014-09-21 01:27:46','2014-09-21 01:27:46'),(964,0,0,'','visitor','017d588a54f3a42220dace96e37f88b4','hLYOEbL7o2EkkGVTQqR385ylG2TwFkus','NgzOtUyUMUvr5TOM1diEwMhMOSrpKe7U','2014-09-21 01:27:46','2014-09-21 01:27:46','2014-09-21 01:27:46'),(967,0,0,'default','mopsyd','a4162f277ada55de526514dab36c75f6','1HKZ9WsU35uwn0xReeBzD95MJCq7vUwF','T5r0HU6DplrsKHO9OjhttaJjffMlNWg2','2014-09-21 01:32:54','2014-09-21 01:32:54','2014-09-21 01:34:02'),(970,0,0,'default','mopsyd','4cab8b81df35f231b7e19c99a5e594cd','ol0gLUGOkbsj19PJ5G0QUWhIKAmT0c28','anZ0mTa92I9kGxKz3wr1XDjB0016BgVt','2014-09-22 19:25:10','2014-09-22 19:25:10','2014-09-22 23:02:13'),(973,0,0,'default','mopsyd','4648a2ce3174c54719a5babc6519b68b','JG617DvcbrnXugnAoLdbl4ZRcrqWZwq9','iR30sZ0TfhBMcRmMOQTliQkzj2dn7Yyp','2014-09-25 21:48:04','2014-09-25 21:48:04','2014-09-26 19:56:07'),(976,0,0,'default','mopsyd','e01040af274e6889909307d42b25b11c','OOxQFP8du41iN9UlOz2ZATG35SflYxfE','Y39HGTL17KsuTiuG8Mzb0ekGodzEL1KW','2014-09-27 10:49:33','2014-09-27 10:49:33','2014-09-28 09:49:40'),(1016,0,0,'default','mopsyd','4cb0e51ca713734953bb33a05dca16e4','HKBf9WDyxk115xltbQ7naKpEmkH8xaVZ','ZLSU61kHG0OohXm1HLwzooaSqf0KUx8t','2014-09-29 01:02:44','2014-09-29 01:02:44','2014-10-03 18:15:38'),(1018,0,0,'','visitor','5cb59131808931076c0026928e7e4f42','kMk8S0c0Zt9lL9KQms42JsClr73sd8Qv','Ed3tDANQNfiG6Nt1V3Jd2xba4bhWiQIA','2014-10-03 20:46:07','2014-10-03 20:46:07','2014-10-03 20:46:11'),(1021,0,0,'default','mopsyd','040da2c6f339c0c13f99b539bc13fb19','MKjxRSt17EOYSwh0QuYH2ReCtlpZ41OF','iSEo9Kvi4E3LUNH6xFtS8YP9AnuA0Z2w','2014-10-03 21:07:48','2014-10-03 21:07:48','2014-10-05 23:35:25'),(1023,0,0,'','visitor','77c1a8d44cda0799b7fcc31654f8fa46','MYNXmRsm1zq0PqOTrTEaduM8hNtLh020','8fs8yi2kozzcixxIsvOej7w81bP3AMbD','2014-10-05 22:31:37','2014-10-05 22:31:37','2014-10-05 22:31:37'),(1026,0,0,'default','mopsyd','9eb43d9aae2fe68189824b4a0e9de7a1','21a761bxXgFZvjhiD2b8S02EW0mY2umM','0thzqUwVKmNye5vEXTyE8246n4XocH04','2014-10-07 20:57:42','2014-10-07 20:57:42','2014-10-07 21:34:55'),(1028,0,0,'','visitor','09cd9eb80e12debde4a423fb95fc33c9','xV4TJjf22jCTF4yWz691op9ovgL9HYtH','AgjW6KFWPCvlTdybStknkQe4b0aGIBXC','2014-10-09 21:28:03','2014-10-09 21:28:03','2014-10-09 21:28:03'),(1030,0,0,'','visitor','48dd37dffdf31181d033c98b96ba029c','7BHcUCY4000FdEGA4CNTcPU6afTzLnrh','Q9fYOyqLZ80rA33Y6TWdrjkbG1aSdqL6','2014-10-11 14:14:16','2014-10-11 14:14:16','2014-10-11 14:14:16'),(1035,0,0,'','visitor','2a93a78fa8c1ac1461c6432abc64badd','mEaAVh891XxDlTCP4CL9yeD4QiOyrFhV','7bxKindqtd0OY9lPFuKQuhTvbFfvK0XL','2014-10-11 14:16:01','2014-10-11 14:16:01','2014-10-11 14:16:01'),(1040,0,0,'','visitor','cf7aeab1246db87486c41b09b13ee5ac','AAVWyXEDcnvxA6sQ9jRbht5gXGmxy01L','zLe7NaF0AOVaJtkiYe2l3jYFqdzHMOUs','2014-10-13 21:17:13','2014-10-13 21:17:13','2014-10-13 21:17:19'),(1042,0,0,'default','mopsyd','a63c72e440a73698861e7947c8ec7a1c','zb3UkdYjfInb1ldpyJWKpPABAeOhBduN','7svvtipQjws60MNaQKa4yEVmeihgaAvJ','2014-10-13 21:18:06','2014-10-13 21:18:06','2014-10-13 21:18:07'),(1045,0,0,'','visitor','91c5dd6ed730765a604a7e993df54712','YqnnM0Y2AnRpdb4EjxA9jyiE0I4Y5OH1','czO5R90I0PB0cgkaZCV3Oz07FFFpdoFm','2014-10-14 18:46:43','2014-10-14 18:46:43','2014-10-14 18:46:43'),(1047,0,0,'default','mopsyd','fc6cdc38bed46eb364d96c78a35a3df7','NO7BHUgaCdmJAKczpQ06mXhHXTnaaXQM','7s49HPsuyHUDodnlj43qnxCGxXTqIdJZ','2014-10-15 19:19:56','2014-10-15 19:19:56','2014-10-16 21:57:00'),(1050,0,0,'','visitor','c415ebbdde5ae3a9da9ca529bb6c4bb3','J1nBSVgbRlBc0o6DsNCZWzgJVsNULGY5','P5TyEIgAu3jph7gqvaBENZ9IwEwMdOTL','2014-10-16 18:38:24','2014-10-16 18:38:24','2014-10-16 18:38:24'),(1053,0,0,'','visitor','611e1cb90f4dd2095dc2e6dbb0f8380b','OuzeWJVZ71UWQJikCjq3HzlOXvxM434Z','jHqqy3W59VdXQc1sgxwMYAjM3jhMCouU','2014-10-16 21:03:28','2014-10-16 21:03:28','2014-10-16 21:03:28'),(1056,0,0,'default','mopsyd','ca728c6831b0dc8e0eaa314f0d7df65c','RVUr1J1qNgXPIqjtcwQDvAlZ3qdQrJc8','nu2FnWfXUNN1YpU5sZbQPPM7BIpZ8Pen','2014-10-18 12:49:27','2014-10-18 12:49:27','2014-10-19 19:52:13'),(1058,0,0,'','visitor','20ee5c3818831c8982d5e52c88108028','V0GkZWYbRX9opNPs9xCd5TvOCjmX13if','tOW1YEqtKRp6I1h3ctqJqbsMLB2SoY8D','2014-10-21 20:28:47','2014-10-21 20:28:47','2014-10-21 20:28:47'),(1061,0,0,'default','mopsyd','d88663ed9c92124dd35bb3364e6caf4d','UFYTcLttXXAV91ykMVWsPmqXc3XScZCp','AS8sOY0Jn7NEqWfunBioaRMZCi4q16yQ','2014-10-25 02:41:50','2014-10-25 02:41:50','2014-10-27 00:11:39'),(1063,0,0,'','visitor','19f71b5ddba138049b19c54159824fb9','zvfTgXLuL7Ylna3KzCIvqOijXvSZhDhI','hUlMA4o89dwPimnLyLg3Wa0yzM7gzLPk','2014-10-25 03:26:54','2014-10-25 03:26:54','2014-10-25 03:26:55'),(1066,0,0,'default','mopsyd','cebd50f2fb2d984095fb5cf8937bfb1f','T26iPsU03LyRDvtq7czbh3pazLi8nOe8','T11tPJKlE10TZZTUDyCHwVLiuFKQq0PU','2014-10-28 17:03:51','2014-10-28 17:03:51','2014-10-28 17:03:52'),(1069,0,0,'default','mopsyd','2a147451ab50e85926db6df03b957050','6lmztU5GiJhMgDikFOE3cRpBmMUPAku3','cbAzRr6O4rKq1zXyyY3PWOnSFLJDAs6o','2014-11-02 07:51:36','2014-11-02 07:51:36','2014-11-02 21:34:02'),(1071,0,0,'','visitor','a71587bfca4a34e1331deb0a47e7ff93','G34hHPUct1SxM7bVWPS7Xgh1DStUfvsx','6Iwmn1XbQXK7S7skBv6fRpRj5A3usw1K','2014-11-06 08:24:26','2014-11-06 08:24:26','2014-11-06 08:24:26'),(1074,0,0,'default','mopsyd','749131860446c564c3766fc09a2a2ce1','5tqJowa3p3DLlvfr9hGtPelr10TWvxjC','0OCeqyh0lbFzQNm2tEWU0hUGBPOMwP3f','2014-11-06 08:27:12','2014-11-06 08:27:12','2014-11-06 08:30:51');
/*!40000 ALTER TABLE `sites_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sites_subdomains`
--

DROP TABLE IF EXISTS `sites_subdomains`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites_subdomains` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT 'default',
  `name` varchar(63) DEFAULT NULL,
  `alias` varchar(63) DEFAULT NULL,
  `site` varchar(16) NOT NULL DEFAULT 'default',
  `title` varchar(64) DEFAULT NULL,
  `scope` varchar(16) NOT NULL DEFAULT 'site',
  `access_level` varchar(16) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_2` (`slug`,`site`,`scope`),
  UNIQUE KEY `name_2` (`name`,`alias`,`site`),
  KEY `slug` (`slug`),
  KEY `site` (`site`),
  KEY `title` (`title`),
  KEY `scope` (`scope`),
  KEY `access_level` (`access_level`),
  KEY `name` (`name`),
  KEY `alias` (`alias`),
  CONSTRAINT `sites_subdomains_ibfk_1` FOREIGN KEY (`site`) REFERENCES `sites` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sites_subdomains_ibfk_3` FOREIGN KEY (`access_level`) REFERENCES `access_level` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `sites_subdomains_ibfk_4` FOREIGN KEY (`scope`) REFERENCES `sites_subdomains_scope` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sites_subdomains`
--

LOCK TABLES `sites_subdomains` WRITE;
/*!40000 ALTER TABLE `sites_subdomains` DISABLE KEYS */;
INSERT INTO `sites_subdomains` VALUES (1,'default',NULL,'www','default','Default Site','global','open',1),(2,'default',NULL,'www','global','Global Default Subdomain','global','open',1);
/*!40000 ALTER TABLE `sites_subdomains` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sites_subdomains_scope`
--

DROP TABLE IF EXISTS `sites_subdomains_scope`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites_subdomains_scope` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `display` varchar(64) NOT NULL DEFAULT '',
  `description` varchar(256) NOT NULL DEFAULT 'No description provided',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `display` (`display`),
  KEY `description` (`description`(255))
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sites_subdomains_scope`
--

LOCK TABLES `sites_subdomains_scope` WRITE;
/*!40000 ALTER TABLE `sites_subdomains_scope` DISABLE KEYS */;
INSERT INTO `sites_subdomains_scope` VALUES (1,'global','Global Scope','This subdomain is applicable to all render methods',1,1),(2,'site','Browser Scope','This subdomain is applicable to desktop browsers',1,1),(3,'mobile','Mobile Scope','This subdomain is applicable to mobile devices',1,1),(4,'tablet','Tablet Scope','This subdomain is applicable to tablet devices',1,1),(5,'accessibility','Accessibility Scope','This subdomain is applicable to devices using accessibility features',1,1),(6,'seo','SEO Scope','This subdomain is applicable to web crawlers, and is intended to lay out content in a machine readable format for SEO optimization',1,1),(7,'rss','RSS Scope','This subdomain is applicable to RSS feeds, and displays a simplified layout for quick export',1,1),(8,'dev','Development Scope','This subdomain is used for site development, and is only accessible to registered developers',1,1);
/*!40000 ALTER TABLE `sites_subdomains_scope` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sites_subsections`
--

DROP TABLE IF EXISTS `sites_subsections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sites_subsections` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site` varchar(16) NOT NULL DEFAULT '',
  `subdomain` varchar(16) NOT NULL DEFAULT 'default',
  `section` varchar(16) NOT NULL DEFAULT 'frontend',
  `slug` varchar(16) DEFAULT NULL,
  `display` varchar(32) NOT NULL DEFAULT 'Untitled Section',
  `description` varchar(256) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `site_2` (`site`,`subdomain`,`section`),
  KEY `site` (`site`),
  KEY `subdomain` (`subdomain`),
  KEY `section` (`section`),
  CONSTRAINT `sites_subsections_ibfk_1` FOREIGN KEY (`site`) REFERENCES `sites` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sites_subsections_ibfk_2` FOREIGN KEY (`subdomain`) REFERENCES `sites_subdomains` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sites_subsections`
--

LOCK TABLES `sites_subsections` WRITE;
/*!40000 ALTER TABLE `sites_subsections` DISABLE KEYS */;
INSERT INTO `sites_subsections` VALUES (1,'default','default','frontend',NULL,'Frontend','This is the default installation frontend. This site section is globally visible unless otherwise defined after installation.',1),(2,'default','default','backend','dashboard','Dashboard','This is the default installation backend, and is visible to registered users by default unless otherwise defined after installation. Elevated accounts can manage all sites in the network from this section.',1),(3,'global','default','backend','dashboard','Dashboard','This is the global backend. This site section can manage all accounts, sites, subdomains, pages, permissions, files, modules, components, and other assets for any site in the network.',0),(4,'global','default','frontend',NULL,'Global Frontend','This is the global frontend. It handles frontend page requests where they are not otherwise defined.',1);
/*!40000 ALTER TABLE `sites_subsections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templates`
--

DROP TABLE IF EXISTS `templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templates` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `title` varchar(64) NOT NULL DEFAULT 'Untitled Theme',
  `parent` varchar(16) DEFAULT NULL,
  `version` varchar(32) NOT NULL,
  `html_type` varchar(16) NOT NULL DEFAULT 'html5',
  `type` varchar(16) NOT NULL DEFAULT 'global',
  `skinnable` tinyint(1) NOT NULL DEFAULT '0',
  `extendable` tinyint(1) NOT NULL DEFAULT '1',
  `stylesheet` varchar(64) NOT NULL DEFAULT '',
  `javascript` varchar(64) DEFAULT NULL,
  `image_folder` tinyint(1) NOT NULL DEFAULT '0',
  `config_file` varchar(32) NOT NULL DEFAULT 'NONE',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_2` (`slug`,`title`,`version`),
  KEY `slug` (`slug`),
  KEY `title` (`title`),
  KEY `version` (`version`),
  KEY `parent` (`parent`),
  CONSTRAINT `templates_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `templates` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templates`
--

LOCK TABLES `templates` WRITE;
/*!40000 ALTER TABLE `templates` DISABLE KEYS */;
INSERT INTO `templates` VALUES (1,'default','Default Template',NULL,'0.0.1','html5','global',0,1,'default.phtml',NULL,0,'settings.ini',1,1),(2,'default-admin','Default Admin Template',NULL,'0.0.1','html5','global',0,1,'default.phtml','scripting.js',0,'settings.ini',1,1);
/*!40000 ALTER TABLE `templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templates_content`
--

DROP TABLE IF EXISTS `templates_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templates_content` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `template` varchar(16) NOT NULL DEFAULT 'default',
  `type` varchar(16) NOT NULL DEFAULT 'html',
  `content` text,
  `hook` varchar(32) NOT NULL DEFAULT 'BODY',
  `hierarchy` int(4) unsigned zerofill NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `permission` varchar(16) NOT NULL DEFAULT 'view',
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `type_2` (`type`),
  KEY `hook` (`hook`),
  KEY `permission` (`permission`),
  KEY `template` (`template`),
  CONSTRAINT `templates_content_ibfk_2` FOREIGN KEY (`permission`) REFERENCES `permissions` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `templates_content_ibfk_4` FOREIGN KEY (`type`) REFERENCES `pages_content_types` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `templates_content_ibfk_5` FOREIGN KEY (`hook`) REFERENCES `hooks` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `templates_content_ibfk_6` FOREIGN KEY (`template`) REFERENCES `templates` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templates_content`
--

LOCK TABLES `templates_content` WRITE;
/*!40000 ALTER TABLE `templates_content` DISABLE KEYS */;
INSERT INTO `templates_content` VALUES (1,'default-admin','template','sidebar.phtml','SIDEBAR',0000,1,1,'0000-00-00 00:00:00','2014-08-01 01:17:54','registered'),(2,'default-admin','template','dash-index.phtml','DASHINDEX',0000,1,1,'0000-00-00 00:00:00','2014-07-27 02:51:02','registered'),(3,'default-admin','linkset',NULL,'DASHHEAD',0000,1,1,'0000-00-00 00:00:00','2014-07-27 04:18:58','registered'),(4,'default-admin','linkset',NULL,'DASHLINKS',0000,1,1,'0000-00-00 00:00:00','2014-07-27 02:59:19','registered'),(5,'default-admin','template','footer_browser.phtml','FOOTERTEMPLATE',0000,1,1,'0000-00-00 00:00:00','2014-08-03 06:30:36','registered'),(6,'default-admin','template','footer_workspaces.phtml','FOOTERTEMPLATE',0001,1,1,'0000-00-00 00:00:00','2014-08-03 06:30:42','registered'),(7,'default-admin','template','footer_details.phtml','FOOTERTEMPLATE',0002,1,1,'0000-00-00 00:00:00','2014-08-03 06:30:46','registered'),(8,'default-admin','linkset',NULL,'USERBOX',0000,1,1,'0000-00-00 00:00:00','2014-08-03 06:23:37','registered');
/*!40000 ALTER TABLE `templates_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `templates_details`
--

DROP TABLE IF EXISTS `templates_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templates_details` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `title` varchar(64) NOT NULL DEFAULT 'Untitled Theme',
  `version` varchar(32) NOT NULL,
  `type` varchar(16) NOT NULL DEFAULT 'global',
  `author` varchar(128) NOT NULL DEFAULT '',
  `source` varchar(512) DEFAULT NULL,
  `author_email` varchar(128) DEFAULT NULL,
  `author_site` varchar(512) DEFAULT NULL,
  `theme_site` varchar(512) DEFAULT NULL,
  `bug_site` varchar(512) DEFAULT NULL,
  `doc_site` varchar(512) DEFAULT NULL,
  `sales_mail` varchar(128) DEFAULT NULL,
  `account_mail` varchar(128) DEFAULT NULL,
  `support_mail` varchar(128) DEFAULT NULL,
  `sales_phone` varchar(128) DEFAULT NULL,
  `account_phone` varchar(128) DEFAULT NULL,
  `support_phone` varchar(128) DEFAULT NULL,
  `license` varchar(128) DEFAULT NULL,
  `license_link` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_2` (`slug`,`title`,`version`),
  KEY `slug` (`slug`),
  KEY `title` (`title`),
  KEY `version` (`version`),
  CONSTRAINT `templates_details_ibfk_1` FOREIGN KEY (`slug`) REFERENCES `themes` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templates_details`
--

LOCK TABLES `templates_details` WRITE;
/*!40000 ALTER TABLE `templates_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `templates_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `themes`
--

DROP TABLE IF EXISTS `themes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `themes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `title` varchar(64) NOT NULL DEFAULT 'Untitled Theme',
  `version` varchar(32) NOT NULL,
  `parent` varchar(16) DEFAULT NULL,
  `html_type` varchar(16) NOT NULL DEFAULT 'html5',
  `type` varchar(16) NOT NULL DEFAULT 'global',
  `skinnable` tinyint(1) NOT NULL DEFAULT '0',
  `extendable` tinyint(1) NOT NULL DEFAULT '1',
  `stylesheet` varchar(64) DEFAULT NULL,
  `javascript` varchar(64) DEFAULT NULL,
  `image_folder` tinyint(1) NOT NULL DEFAULT '0',
  `config_file` varchar(32) NOT NULL DEFAULT 'NONE',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_2` (`slug`,`title`,`version`),
  KEY `slug` (`slug`),
  KEY `title` (`title`),
  KEY `version` (`version`),
  KEY `parent` (`parent`),
  CONSTRAINT `themes_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `themes` (`slug`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `themes`
--

LOCK TABLES `themes` WRITE;
/*!40000 ALTER TABLE `themes` DISABLE KEYS */;
INSERT INTO `themes` VALUES (0,'RAW','Raw HTML','',NULL,'html5','global',0,1,NULL,NULL,0,'NONE',1,0),(1,'INHERIT','Inherit Theme from Parent','',NULL,'html5','global',0,1,NULL,NULL,0,'NONE',1,0),(2,'default','Oroboros Default Global Theme','0.0.1 (alpha)',NULL,'html5','global',0,1,'default.phtml',NULL,1,'settings.ini',1,1),(3,'default-admin','Oroboros Default Global Admin Theme','0.0.1 (alpha)',NULL,'html5','global',0,1,'default.phtml',NULL,1,'settings.ini',1,1);
/*!40000 ALTER TABLE `themes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `themes_content`
--

DROP TABLE IF EXISTS `themes_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `themes_content` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `theme` varchar(16) NOT NULL DEFAULT 'default',
  `type` varchar(16) NOT NULL DEFAULT 'html',
  `content` text,
  `hook` varchar(32) NOT NULL DEFAULT 'BODY',
  `hierarchy` int(4) unsigned zerofill NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `permission` varchar(16) NOT NULL DEFAULT 'view',
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `type_2` (`type`),
  KEY `hook` (`hook`),
  KEY `permission` (`permission`),
  KEY `theme` (`theme`),
  CONSTRAINT `themes_content_ibfk_2` FOREIGN KEY (`permission`) REFERENCES `permissions` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `themes_content_ibfk_3` FOREIGN KEY (`type`) REFERENCES `pages_content_types` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `themes_content_ibfk_4` FOREIGN KEY (`hook`) REFERENCES `hooks` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `themes_content_ibfk_5` FOREIGN KEY (`theme`) REFERENCES `themes` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `themes_content`
--

LOCK TABLES `themes_content` WRITE;
/*!40000 ALTER TABLE `themes_content` DISABLE KEYS */;
/*!40000 ALTER TABLE `themes_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `themes_details`
--

DROP TABLE IF EXISTS `themes_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `themes_details` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `title` varchar(64) NOT NULL DEFAULT 'Untitled Theme',
  `version` varchar(32) NOT NULL,
  `type` varchar(16) NOT NULL DEFAULT 'global',
  `author` varchar(128) NOT NULL DEFAULT '',
  `source` varchar(512) DEFAULT NULL,
  `author_email` varchar(128) DEFAULT NULL,
  `author_site` varchar(512) DEFAULT NULL,
  `theme_site` varchar(512) DEFAULT NULL,
  `bug_site` varchar(512) DEFAULT NULL,
  `doc_site` varchar(512) DEFAULT NULL,
  `sales_mail` varchar(128) DEFAULT NULL,
  `account_mail` varchar(128) DEFAULT NULL,
  `support_mail` varchar(128) DEFAULT NULL,
  `sales_phone` varchar(128) DEFAULT NULL,
  `account_phone` varchar(128) DEFAULT NULL,
  `support_phone` varchar(128) DEFAULT NULL,
  `license` varchar(128) DEFAULT NULL,
  `license_link` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_2` (`slug`,`title`,`version`),
  KEY `slug` (`slug`),
  KEY `title` (`title`),
  KEY `version` (`version`),
  CONSTRAINT `themes_details_ibfk_1` FOREIGN KEY (`slug`) REFERENCES `themes` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `themes_details`
--

LOCK TABLES `themes_details` WRITE;
/*!40000 ALTER TABLE `themes_details` DISABLE KEYS */;
INSERT INTO `themes_details` VALUES (1,'RAW','Raw HTML','','global','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'INHERIT','Inherit Theme from Parent','','global','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'default','Oroboros Default Global Theme','0.0.1 (alpha)','global','Brian Dayhoff',NULL,'bdayhoff@gmail.com','http://mopsyd.net',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'MIT License','http://opensource.org/licenses/MIT'),(4,'default-admin','Oroboros Default Global Admin Theme','0.0.1 (alpha)','global','Brian Dayhoff',NULL,'bdayhoff@gmail.com','http://mopsyd.net',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'MIT License','http://opensource.org/licenses/MIT');
/*!40000 ALTER TABLE `themes_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `site` varchar(16) NOT NULL DEFAULT 'default',
  `login` varchar(25) NOT NULL,
  `email` varchar(256) DEFAULT NULL,
  `password` varchar(64) NOT NULL,
  `group` varchar(16) NOT NULL DEFAULT 'default',
  `hierarchy` int(11) unsigned zerofill NOT NULL DEFAULT '00000000099',
  `suspended` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `banned` tinyint(1) unsigned DEFAULT '0',
  `disabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `global` tinyint(1) NOT NULL DEFAULT '0',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`userid`),
  UNIQUE KEY `site_2` (`site`,`login`),
  KEY `password` (`password`),
  KEY `group` (`group`),
  KEY `site` (`site`),
  KEY `email` (`email`(255)),
  KEY `login` (`login`),
  CONSTRAINT `user_ibfk_2` FOREIGN KEY (`site`) REFERENCES `sites` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_ibfk_3` FOREIGN KEY (`group`) REFERENCES `user_groups` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COMMENT='This is the old user table used for initial development. This will be removed when the login model has been modified to accommodate the new structure.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (0,'default','SYSTEM',NULL,'System does not use a password. This prevents hash evaluation.','SYSTEM',00000000000,0,0,0,1,0,0),(14,'default','mopsyd',NULL,'b5a5a7dcd5860fcea7ef1b269cba252462ec4311c87194e0fc427fb2cde01210','architect',00000000001,0,0,0,1,1,1),(16,'default','admin-test',NULL,'b5a5a7dcd5860fcea7ef1b269cba252462ec4311c87194e0fc427fb2cde01210','admin',00000000008,0,0,0,1,0,1),(17,'global','visitor',NULL,'Visitors cannot login. This prevents hash evaluation.','default',00000000011,0,0,0,1,0,1),(18,'default','moderator-test',NULL,'b5a5a7dcd5860fcea7ef1b269cba252462ec4311c87194e0fc427fb2cde01210','moderator',00000000009,0,0,0,1,0,1),(19,'default','user-test',NULL,'b5a5a7dcd5860fcea7ef1b269cba252462ec4311c87194e0fc427fb2cde01210','registered',00000000010,0,0,0,1,0,1),(20,'default','operator-test',NULL,'b5a5a7dcd5860fcea7ef1b269cba252462ec4311c87194e0fc427fb2cde01210','operator',00000000007,0,0,0,1,0,1),(21,'default','webmaster-test',NULL,'b5a5a7dcd5860fcea7ef1b269cba252462ec4311c87194e0fc427fb2cde01210','webmaster',00000000005,0,0,0,1,0,1),(22,'default','owner-test',NULL,'b5a5a7dcd5860fcea7ef1b269cba252462ec4311c87194e0fc427fb2cde01210','owner',00000000002,0,0,0,1,0,1),(23,'default','mdev-test',NULL,'b5a5a7dcd5860fcea7ef1b269cba252462ec4311c87194e0fc427fb2cde01210','master_developer',00000000003,0,0,0,1,0,1),(24,'default','designer-test',NULL,'b5a5a7dcd5860fcea7ef1b269cba252462ec4311c87194e0fc427fb2cde01210','designer',00000000006,0,0,0,1,0,1),(25,'default','developer-test',NULL,'b5a5a7dcd5860fcea7ef1b269cba252462ec4311c87194e0fc427fb2cde01210','developer',00000000004,0,0,0,1,0,1),(29,'global','mopsyd',NULL,'b5a5a7dcd5860fcea7ef1b269cba252462ec4311c87194e0fc427fb2cde01210','architect',00000000000,0,0,0,1,1,1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_details`
--

DROP TABLE IF EXISTS `user_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_details` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `site` varchar(16) NOT NULL DEFAULT 'default',
  `user` varchar(16) NOT NULL DEFAULT '',
  `icon` varchar(128) NOT NULL DEFAULT 'default.png',
  `name_first` varchar(32) DEFAULT NULL,
  `name_last` varchar(64) DEFAULT NULL,
  `phone` int(10) DEFAULT NULL,
  `addr_1` varchar(64) DEFAULT NULL,
  `addr_2` varchar(64) DEFAULT NULL,
  `addr_city` varchar(32) DEFAULT NULL,
  `addr_state` varchar(2) DEFAULT NULL,
  `addr_zip` int(5) unsigned DEFAULT NULL,
  `addr_zip_ext` int(6) unsigned DEFAULT NULL,
  `addr_country` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `site_2` (`site`,`user`),
  KEY `name` (`name_first`,`name_last`),
  KEY `phone` (`phone`),
  KEY `address` (`addr_1`,`addr_2`,`addr_city`,`addr_state`,`addr_zip`,`addr_zip_ext`,`addr_country`),
  KEY `site` (`site`),
  KEY `user` (`user`),
  CONSTRAINT `user_id` FOREIGN KEY (`site`, `user`) REFERENCES `user` (`site`, `login`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='This table contains the personal data for registered accounts, if they have opted to include it (or if required by the system administrator for registration)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_details`
--

LOCK TABLES `user_details` WRITE;
/*!40000 ALTER TABLE `user_details` DISABLE KEYS */;
INSERT INTO `user_details` VALUES (1,'default','mopsyd','default.png','Brian','Dayhoff',NULL,'5536 Lewis Ct.','Unit 203','Arvada','CO',80002,NULL,'USA'),(6,'global','mopsyd','default.png','Brian','Dayhoff',NULL,'5536 Lewis Ct.','Unit 203','Arvada','CO',80002,NULL,'USA'),(9,'global','visitor','default.png',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,'default','user-test','default.png',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,'default','admin-test','default.png',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,'default','owner-test','default.png',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(13,'default','operator-test','default.png',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(14,'default','moderator-test','default.png',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(16,'default','webmaster-test','default.png',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(17,'default','mdev-test','default.png',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(18,'default','developer-test','default.png',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(19,'default','designer-test','default.png',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_groups`
--

DROP TABLE IF EXISTS `user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(16) NOT NULL DEFAULT '',
  `display` varchar(64) NOT NULL DEFAULT 'Untitled User Group',
  `inherit` varchar(16) DEFAULT NULL,
  `site` varchar(16) NOT NULL DEFAULT 'default',
  `description` text,
  `hierarchy` int(1) NOT NULL DEFAULT '0',
  `global` tinyint(1) NOT NULL DEFAULT '0',
  `show` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug_2` (`slug`,`site`),
  KEY `inherit` (`inherit`),
  KEY `site` (`site`),
  KEY `display` (`display`,`slug`,`inherit`,`site`),
  KEY `slug` (`slug`),
  CONSTRAINT `user_groups_ibfk_2` FOREIGN KEY (`site`) REFERENCES `sites` (`slug`) ON UPDATE CASCADE,
  CONSTRAINT `user_groups_ibfk_3` FOREIGN KEY (`inherit`) REFERENCES `user_groups` (`slug`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='This table contains all of the user groups within the system, used for managing permissions at the group level.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_groups`
--

LOCK TABLES `user_groups` WRITE;
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;
INSERT INTO `user_groups` VALUES (0,'SYSTEM','SYSTEM',NULL,'SYSTEM','DO NOT DELETE. This is the user group for system functions.',0,1,0),(1,'default','Default User Group',NULL,'global','This is the default user group. Default users may view public facing pages on the site, and may also perform any actions that the site administrator grants to unregistered users.',0,1,1),(2,'moderator','Moderator','author','default','This is the moderator user group, responsible for arbitrating user disputes, managing post content and user behavior. Moderators have elevated permissions to manage content, but cannot edit the system configuration or site structure.',3,0,1),(3,'admin','Administrator','moderator','default','This is the administrator user group. Admins are responsible for managing site structure and content, and can additionally perform all responsibilities delegated to moderators. They have elevated permissions and can affect site structure and content in addition to user submitted content.',4,0,1),(4,'operator','Operators','admin','default','This is the operator user group. They are responsible for managing site performance and structure, and have sitewide permission access. They also have all permissions delegated to admins and moderators.',5,0,1),(5,'webmaster','Webmaster','operator','default','This is the webmaster user group. Webmasters have mostly the same privileges as operators, however they can manage nodes, apply changes to all sites in the network, manage hierarchies, inheritance, and make network-wide changes. They also have all of the same permissions as operators, admins, and moderators.',6,1,1),(6,'restricted','Restricted User',NULL,'default','This is the restricted user group. This group has basic functionality common to basic users suspended, such as posting comments, contacting other users, and other site interactions that are globally granted due to abusive behavior. Restricted users can still log in and access sites, however they have diminished functionality until their account is returned to good standing through administrative decision. What actions are restricted can be changed on a site by site basis.',0,0,0),(7,'banned','Banned User',NULL,'default','This is the banned user group. Users in this group are prevented from viewing any pages on the site (or alternately in the network of sites, by administrative decision). Users in this group will be redirected to an error page when they attempt to log in stating that they are banned and supplying any notes as to why furnished by the administrator that issued the ban, if applicable. Their IP address and user agent will be recorded and heuristically matched if they attempt to log in using a proxy or vpn in an attempt to prevent efforts at circumventing penalties through alternate accounts, user agent spoofing, proxies, or other efforts to mask identity. ',0,0,0),(8,'developer','Developer','webmaster','default','This is the user group for developers. It grants all of the functionality of site operators, and additionally allows the developer to perform server side changes to file structure. This group does not allow access to multiple site directories. If the webmaster wishes the developer to work on multiple sites in their network, they should duplicate their account permissions for each site in which the developer works, or alternately assign them to the master developer group if they need global access.',8,1,1),(9,'master_developer','Master Developer','developer','default','This is an extended developer user group, in which case members have the same permissions as developers, but have global access to the file structure.',9,1,1),(10,'registered','Registered User','default','default','This is the registered user group, indicating that the user has activated their account and may log in to the site.',1,0,1),(11,'self','Self Only',NULL,'default','This user group manages permissions that are only granted to the individual account, but need to be registered with a group for system functionality purposes.',0,1,0),(12,'designer','Web Designer','admin','default','This user group allows the user to perform stylistic changes to the site.',7,0,1),(13,'architect','System Architect','master_developer','default','This group is reserved for the development team that works on the core architecture.',11,0,0),(14,'owner','Network Ownership','webmaster','default','This is the group for installation ownership. They have access to all permissions registered within the system, except those that may break core functionality. The individual that registered the system on firstrun is automatically assigned to this group. Only members of this group may assign others to this group.',10,0,0),(15,'author','Content Author','registered','default','This group is for content authors. They have slightly elevated permissions above normal registered users, but not as many as moderators or admins.',2,0,1);
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_groups_permissions`
--

DROP TABLE IF EXISTS `user_groups_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_groups_permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group` varchar(16) NOT NULL DEFAULT 'default',
  `permission` varchar(16) NOT NULL DEFAULT '',
  `site` varchar(16) DEFAULT 'global',
  `inherit` varchar(16) NOT NULL DEFAULT 'default',
  `mode` varchar(16) NOT NULL DEFAULT 'ALLOW',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `added` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_2` (`group`,`permission`,`site`,`mode`),
  KEY `group` (`group`),
  KEY `permission` (`permission`),
  KEY `inherit` (`inherit`),
  KEY `mode` (`mode`),
  KEY `site` (`site`),
  CONSTRAINT `user_groups_permissions_ibfk_2` FOREIGN KEY (`permission`) REFERENCES `permissions` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_groups_permissions_ibfk_3` FOREIGN KEY (`inherit`) REFERENCES `permissions` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_groups_permissions_ibfk_5` FOREIGN KEY (`mode`) REFERENCES `permission_types` (`slug`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `user_groups_permissions_ibfk_6` FOREIGN KEY (`group`) REFERENCES `user_groups` (`slug`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=293 DEFAULT CHARSET=utf8 COMMENT='This table handles group permissions. Members of any given group will have all of the access rights and restrictions applied to their group in this table.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_groups_permissions`
--

LOCK TABLES `user_groups_permissions` WRITE;
/*!40000 ALTER TABLE `user_groups_permissions` DISABLE KEYS */;
INSERT INTO `user_groups_permissions` VALUES (2,'registered','login','global','default','ALLOW',1,'0000-00-00 00:00:00','2014-06-05 13:59:32'),(3,'registered','comment','global','default','ALLOW',1,'0000-00-00 00:00:00','2014-06-05 13:59:32'),(4,'registered','reply','global','default','ALLOW',1,'0000-00-00 00:00:00','2014-06-05 13:59:32'),(6,'moderator','restrict_account','global','default','ALLOW',1,'0000-00-00 00:00:00','2014-06-05 13:59:32'),(7,'moderator','ban_account','global','default','ALLOW',1,'0000-00-00 00:00:00','2014-06-05 13:59:32'),(8,'admin','edit_other','global','default','ALLOW',1,'0000-00-00 00:00:00','2014-06-05 13:59:32'),(9,'admin','alter_content','global','default','ALLOW',1,'0000-00-00 00:00:00','2014-06-05 13:59:32'),(10,'admin','add_post','global','default','ALLOW',1,'0000-00-00 00:00:00','2014-06-05 13:59:32'),(11,'admin','email_users','global','default','ALLOW',1,'0000-00-00 00:00:00','2014-06-05 13:59:32'),(12,'admin','p_inherit_css','global','default','ALLOW',1,'0000-00-00 00:00:00','2014-06-05 13:59:32'),(13,'admin','p_inherit_js','global','default','ALLOW',1,'0000-00-00 00:00:00','2014-06-05 13:59:32'),(14,'operator','grant_perms','global','default','ALLOW',1,'0000-00-00 00:00:00','2014-06-05 13:59:32'),(16,'operator','maintenance','global','default','ALLOW',1,'0000-00-00 00:00:00','2014-06-05 13:59:32'),(19,'operator','create_subdomain','global','default','ALLOW',1,'0000-00-00 00:00:00','2014-06-05 13:59:32'),(20,'webmaster','s_inherit_css','global','default','ALLOW',1,'0000-00-00 00:00:00','2014-07-22 22:21:08'),(21,'webmaster','s_inherit_js','global','default','ALLOW',1,'0000-00-00 00:00:00','2014-07-22 22:21:03'),(22,'webmaster','create_site','global','default','ALLOW',1,'0000-00-00 00:00:00','2014-07-22 22:22:40'),(23,'developer','designer','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(24,'designer','designer','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(26,'webmaster','webmaster','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(30,'SYSTEM','SYSTEM','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(31,'self','self','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(32,'registered','registered','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(35,'registered','node_internal','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(36,'registered','node','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(37,'registered','group','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(39,'registered','edit_account','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(40,'architect','SYSTEM','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(41,'operator','operator','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(42,'moderator','moderator','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(43,'master_developer','master_developer','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(44,'developer','node_developer','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(45,'developer','developer','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(46,'admin','edit_links','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(47,'admin','delete_page','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(48,'admin','change_group','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(49,'admin','admin','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(50,'admin','add_page','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(51,'admin','add_components','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(52,'owner','owner','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(53,'architect','architect','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(54,'author','author','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(55,'architect','owner','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(56,'architect','sys_edit_perms','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(57,'architect','sys_group_all','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(58,'architect','sys_user_all','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(59,'owner','g_reg_set_params','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(60,'master_developer','ssh_remove','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(61,'webmaster','sched_site','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(62,'owner','g_perm_u_add','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(63,'owner','g_perm_gr_add','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(64,'owner','g_perm_remove','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(65,'owner','g_perm_edit','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(66,'owner','g_perm_create','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(67,'owner','g_subsct_create','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(68,'owner','g_subdm_create','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(69,'webmaster','g_maint_login','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(70,'webmaster','g_maint','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(71,'webmaster','g_lock_login','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(72,'webmaster','g_lockdown','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(73,'webmaster','g_site_import','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(74,'webmaster','g_site_export','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(75,'master_developer','g_backup_system','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(76,'developer','db_update_core','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(77,'webmaster','node_login','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(78,'operator','g_content_edit','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(79,'operator','g_page_edit','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(80,'operator','g_site_edit','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(82,'admin','node_internal','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(85,'admin','file_up_u_other','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(86,'admin','file_up_site','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(87,'registered','file_up_u','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(88,'webmaster','ftp_connect','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(89,'admin','file_dl_user','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(90,'operator','file_dl_site','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(91,'registered','file_dl_own','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(92,'admin','file_del_other','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(93,'operator','file_del_site','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(94,'registered','file_del_own','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(95,'operator','file_copy','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(96,'operator','file_archive','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(97,'developer','file_chmod','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(98,'developer','file_chown','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(99,'designer','des_vid_mod','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(100,'designer','des_img_mod','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(101,'designer','des_col_rem','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(102,'designer','des_col_arch','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(103,'designer','des_col_pkg','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(104,'designer','des_col_edit','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(105,'designer','des_col_add','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(106,'designer','des_typ_rem','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(107,'designer','des_typ_arch','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(108,'designer','des_typ_pkg','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(109,'designer','des_typ_edit','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(110,'designer','des_typ_add','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(111,'designer','des_thm_rem','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(112,'designer','des_thm_arch','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(113,'designer','des_thm_pkg','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(114,'designer','des_thm_edit','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(115,'designer','des_thm_add','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(116,'designer','des_temp_rem','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(117,'designer','des_temp_arch','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(118,'designer','des_temp_pkg','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(119,'designer','des_temp_edit','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(120,'designer','des_temp_add','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(121,'designer','file_up_css','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(122,'designer','des_sandbox','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(123,'designer','s_inherit_js','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(124,'designer','s_inherit_css','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(125,'designer','p_inherit_js','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(126,'designer','p_inherit_css','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(127,'developer','file_up_sql','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(128,'developer','db_sql_editor','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(129,'master_developer','db_backup_rm','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(130,'developer','db_backup','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(131,'master_developer','db_query_ex_rm','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(132,'developer','db_query_exec','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(133,'developer','db_query_view','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(134,'developer','db_view_rm','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(135,'webmaster','db_view','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(136,'master_developer','db_remove_rm','global','default','ALLOW',1,'0000-00-00 00:00:00','2014-09-16 00:48:08'),(137,'master_developer','db_edit_rm','global','default','ALLOW',1,'0000-00-00 00:00:00','2014-09-16 00:48:14'),(138,'master_developer','db_create_rm','global','default','ALLOW',1,'0000-00-00 00:00:00','2014-09-16 00:48:02'),(139,'webmaster','db_remove','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(140,'webmaster','db_modify','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(141,'webmaster','db_create','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(142,'admin','sched_cal','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(143,'admin','sched_rss','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(144,'admin','sched_email','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(145,'author','sched_post','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(146,'admin','sched_content','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(147,'operator','g_create_page','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(148,'admin','s_create_page','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(149,'admin','s_post','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(150,'author','p_post','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(155,'author','auth_sandbox','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(156,'operator','cpt_remove','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(157,'operator','cpt_archive','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(158,'developer','cpt_package','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(159,'developer','cpt_create','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(160,'operator','cpt_edit','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(161,'operator','cpt_add','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(162,'operator','add_components','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(163,'registered','cpt_workspace','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(165,'registered','cpt_controller','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(166,'registered','cpt_drawer','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(167,'registered','cpt_dashbar','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(168,'developer','file_up_php','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(169,'designer','file_up_js','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(170,'developer','dev_sandbox','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(171,'developer','export_code','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(172,'developer','import_code','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(173,'developer','create_code','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(174,'developer','edit_code','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(175,'developer','view_code','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(176,'operator','mod_add','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(177,'operator','mod_edit','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(178,'operator','mod_archive','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(179,'developer','mod_create','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(180,'operator','mod_remove','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(181,'operator','mod_export','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(182,'developer','serv_git_blame','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(183,'developer','serv_git_diff','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(184,'webmaster','serv_view_env','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(185,'operator','g_view_front','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(186,'operator','serv_view_sched','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(187,'developer','serv_webshell_b','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(188,'master_developer','serv_webshell_a','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(189,'developer','serv_git_init','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(190,'developer','serv_git_clone','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(191,'developer','serv_git_commit','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(192,'developer','serv_git_branch','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(193,'developer','serv_git_bran_c','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(194,'developer','serv_git_push','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(195,'developer','serv_git_stash','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(196,'webmaster','serv_edit_env','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(197,'operator','serv_edit_sched','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(198,'developer','ssh_connect','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(199,'developer','ssh_add','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(200,'developer','ssh_edit','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(201,'webmaster','dns_add','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(202,'webmaster','dns_define','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(203,'webmaster','dns_edit','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(204,'webmaster','dns_remove','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(205,'operator','g_view_dash','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(206,'operator','p_view_dash','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(207,'operator','p_view_frontend','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(208,'webmaster','p_node_dash','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(209,'webmaster','p_node_frontend','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(210,'admin','s_lock_login','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(211,'moderator','s_maint_login','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(212,'webmaster','seo_analytics','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(213,'webmaster','seo_tracking','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(214,'webmaster','seo_keywords','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(215,'admin','maintenance','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(218,'admin','create_subdomain','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(221,'operator','s_lockdown','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(222,'admin','s_maint','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(223,'operator','s_subdm_create','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(224,'operator','s_subsct_create','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(225,'admin','sched_page','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(226,'webmaster','sched_subdm','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(227,'webmaster','sched_subsct','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(228,'operator','reg_set_params','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(230,'architect','sys_display_all','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(231,'moderator','email_users','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(232,'author','self','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(233,'registered','u_view_inf','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(234,'moderator','u_view_inf_p','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(235,'registered','u_message','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(236,'moderator','u_email','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(237,'registered','u_content_share','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(238,'registered','u_content_view','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(239,'moderator','u_content_view_p','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(240,'moderator','sched_message','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(241,'moderator','note_flag','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(242,'admin','note_alert_site','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(243,'webmaster','note_alert_serv','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(244,'registered','note_req_u','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(245,'webmaster','note_req_node','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(246,'registered','note_msg_u','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(247,'moderator','note_msg_priv','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(248,'moderator','flag_acct','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(249,'admin','flag_grp','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(250,'moderator','flag_action','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(251,'moderator','flag_msg','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(252,'registered','flag_read','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(253,'registered','forum_post','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(254,'registered','forum_reply','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(255,'registered','forum_edit_own','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(256,'moderator','forum_edit_other','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(261,'admin','grant_perms','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(263,'admin','perm_view','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(264,'webmaster','perm_create','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(265,'webmaster','perm_edit','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(266,'webmaster','perm_remove','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(267,'admin','perm_assign_u','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(268,'admin','perm_assign_g','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(269,'moderator','user_suspend','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(270,'moderator','user_ban','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(271,'admin','user_protect','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(272,'admin','user_whitelist','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(273,'admin','user_blacklist','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(274,'admin','user_greylist','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(275,'moderator','user_restrict','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(276,'moderator','user_logout','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(277,'admin','group_logout','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(278,'operator','all_logout','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(279,'moderator','forum_ban','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(280,'moderator','forum_suspend','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(281,'moderator','forum_mute','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(282,'admin','user_promote','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(283,'admin','user_demote','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(286,'developer','serv_view_sec','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(287,'default','view','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(288,'default','login','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(289,'default','g_view_front','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(290,'default','default','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(291,'developer','db_view_core','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),(292,'registered','view_dash','global','default','ALLOW',1,'0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `user_groups_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_permissions`
--

DROP TABLE IF EXISTS `user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_permissions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user` varchar(16) NOT NULL DEFAULT '',
  `permission` varchar(16) NOT NULL DEFAULT '',
  `mode` varchar(16) NOT NULL DEFAULT 'ALLOW',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`mode`,`permission`,`user`),
  KEY `user` (`user`),
  KEY `permission` (`permission`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='This table handles individual account permissions. Any permissions registered in this table will be appended to the user''s access rights after their group permissions are applied, so that individual restrictions or access rights can be applied to an account without changing their permission group.';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_permissions`
--

LOCK TABLES `user_permissions` WRITE;
/*!40000 ALTER TABLE `user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_permissions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-11-06  9:15:28
