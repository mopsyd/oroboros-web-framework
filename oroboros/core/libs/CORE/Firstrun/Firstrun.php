<?php
/**
 * This file will build the core system upon initial configuration. 
 * It can be instructed to rebuild the system through the core settings file if required.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Firstrun;

class Firstrun {
    
    function __construct() {
        $this->init();
    }
    
    private function init() {
        //load dataflow class
        //load filemap.xml into array
        //load settings.ini into array
        //load init.sql into array
        //ask user for default installation url, set in settings.ini
        //ask user for default account settings
        //ask user for core database credentials
        //check file permissions and ownership, ask user to correct settings if messed up (chown as `whoami`, set all files to 777, installation will then set them to the correct permissions)
        //run file permission correction script (chown all files as server user and set permissions correctly for all core files and directories)
        //attempt to connect to database, if successful, generate core.ini file with supplied credentials
        //generate initial hash settings
        //assign unique registration key
        //generate master user account
        //build initial codebase table from filemap.xml
        //build initial site and page structure
        //build initial module, component, template and theme tables from core layout.xml files
        //save initial settings.ini file
        //check for updates to core
        //check for updates to initial modules, components, templates, and themes
        //define local node name
        //evaluate server capabilities
        //configure cron if possible
        //ask for ssh key, ssl certificate, etc (optional)
        //check if server is git compatible and initialize git repo if so, then generate first commit of entire core filebase
        //check connection to Oroboros master server, send registration key if accessible
        //ask user if they wish to load adapters for any other programming languages detected (optional)
        //configure server core settings
        //ask for core email account (optional)
        //configure core email if supplied
        //load core dependencies from cdn if available (jquery, etc)
        //ask user if they wish to register additional DNS entries (optional)
        //ask user if they wish to register additional databases (optional)
        //ask user if they wish to create a whitelist and blacklist for the server (optional)
        //ask user if they wish to connect to initialize connections to other nodes (optional)
        //ask user if they wish to import content (optional)
        //ask user if they wish to import subsystems (wordpress, magento, joomla, drupal, etc)
        //if multiple databases added, ask user if they wish to create a data bridge (optional)
        //place site in maintenance mode until disabled by user.
        //complete installation and send user to dashboard. If user selects walkthrough, send to welcome page which will route them to docs, training vids, etc as needed.
    }
    
    function __destruct() {
        //generate initial logs, log installation success or failure.
        //if installation fails, instruct user how to correct issues.
        //ask user how they want to handle logging.
        //profile user's needs
        //suggest initial modules and components for them to consider, send suggestions to their notifications
    }
}

?>
