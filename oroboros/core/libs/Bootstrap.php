<?php

namespace oroboros\core\libs;

class Bootstrap {
    
    const SCOPE = _DOC;

    public $_bootstrap = NULL;

    public function __construct($method=NULL) {
        if (isset($method)) {
            $this->init($method);
        } else {
            $this->init(self::SCOPE);
        }
    }

    /**
     * Starts the Bootstrap
     * 
     * @return boolean
     */
    private function init($method) {
        $this->handlerMethod($method);
    }

    private function handlerMethod($method) {
        switch ($method) {
            case 'css':
                //loads the css system
                session_start();
                $this->_bootstrap = new \oroboros\core\libs\Bootstrap\Css($this->_packageSession());
                break;
            case 'js':
                //loads the javascript system
                session_start();
                $this->_bootstrap = new \oroboros\core\libs\Bootstrap\Js($this->_packageSession());
                break;
            case 'robots':
                //loads the robots system
                $this->_bootstrap = new \oroboros\core\libs\Bootstrap\Robots();
                break;
            case 'sitemap':
                //loads the sitemap system
                $this->_bootstrap = new \oroboros\core\libs\Bootstrap\Sitemap();
                break;
            case 'ajax':
                //loads the ajax system
                session_start();
                $this->_bootstrap = new \oroboros\core\libs\Bootstrap\Ajax($this->_packageSession());
                break;
            case 'rss':
                //loads the rss system
                $this->_bootstrap = new \oroboros\core\libs\Bootstrap\Rss();
                break;
            case 'email':
                //loads the email system
                $this->_bootstrap = new \oroboros\core\libs\Bootstrap\Email();
                break;
            case 'cron':
                //loads the cron system
                $this->_bootstrap = new \oroboros\core\libs\Bootstrap\Cron();
                break;
            case 'framework':
                //loads the system as a development framework
                $this->_bootstrap = new \oroboros\core\libs\Bootstrap\Framework();
                break;
            case 'wrapper':
                //loads the system as a wrapper for another site
                $this->_bootstrap = new \oroboros\core\libs\Bootstrap\Wrapper();
                break;
            case 'site':
                //loads the system as an independent website
                $this->_bootstrap = new \oroboros\core\libs\Bootstrap\Site();
                break;
        }
    }
    
    private function _packageSession() {        
        $package = array(
            'core' => array(
                'utilities' => $_SESSION['utilities'],
                'device' => $_SESSION['device'],
            ),
            'site' => $_SESSION['site'],
            'subdomain' => $_SESSION['subdomain'],
            'page' => $_SESSION['page']
        );
        return $package;
    }
}
