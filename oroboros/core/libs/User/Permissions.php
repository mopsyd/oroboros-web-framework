<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of permissions_model
 *
 * @author briandayhoff
 */

namespace oroboros\core\libs\User;

class Permissions {

    protected $perms = array();
    protected $groups = array();
    protected $user = NULL;
    protected $basegroup = NULL;
    protected $site = NULL;

    public function __construct($db = NULL) {
        if (!is_null($db)) {
            $this->db = $db;
        } else {
            $this->db = new \oroboros\core\libs\Database\Database();
        }
    }

    public function getUserPerms($user, $site = NULL, $group = NULL) {
        $this->user = $user;
        $this->site = isset($site) ? $site : $this->checkSite();
        $this->basegroup = isset($group) ? $group : $this->checkGroup($user, $this->site);
        $group = $this->basegroup;
        $this->groups[] = $this->basegroup;
        do {
            $parent = $this->checkParent($group);
            if (!is_array($parent)) {
                $this->groups[] = is_array($parent) ? $parent['slug'] : $parent;
                $group = isset($parent) ? $parent : 'default';
            } else {
                $group = 'default';
            }
        } while ($group != 'default');
        if(is_array($this->groups[0])) {
            $this->groups = array($this->groups[0]['slug']);
        }
        $groups = array();
        foreach($this->groups as $group) {
            if(is_array($group)) {
                $groups[] = $group['slug'];
            } else {
                $groups[] = $group;
            }
        }
        $query = 'SELECT `permission` FROM `user_groups_permissions` WHERE `active`=1 AND `group` IN ("' . implode('","', $groups) . '") AND `mode`="DENY";';
        $restrictions = $this->db->select($query);
        $query = 'SELECT `permission` FROM `user_groups_permissions` WHERE `active`=1 AND `group` IN ("' . implode('","', $groups) . '") AND `mode`="ALLOW" AND `permission` NOT IN ("' . implode('","', $restrictions) . '");';
        $groupPerms = $this->db->select($query);
        $query = 'SELECT `permission` FROM `user_permissions` WHERE `active`=1 AND `user`="' . $this->user . '" AND `mode`="DENY";';
        $restrictions = $this->db->select($query);
        $query = 'SELECT `permission` FROM `user_permissions` WHERE `active`=1 AND `user`="' . $this->user . '" AND `mode`="ALLOW" AND `permission` NOT IN ("' . implode('","', $restrictions) . '");';
        $userPerms = $this->db->select($query);
        foreach ($groupPerms as $perm) {
            $this->perms[] = $perm['permission'];
        }
        foreach ($userPerms as $perm) {
            $this->perms[] = $perm['permission'];
        }
        return $this->perms;
    }

    protected function checkUser($user, $site) {
        $x = $this->db->select('SELECT `login`,`group`,`hierarchy`,`active` FROM `user` WHERE `login`="' . $user . '" AND `site` IN("' . $site . '", "global")');
        return $x;
    }

    public function checkSite() {
        $x = $this->db->select('SELECT `slug` FROM `sites` WHERE `url`="' . $_SERVER['HTTP_HOST'] . '" LIMIT 1');
        return isset($x[0]['slug']) ? $x[0]['slug'] : 'global';
    }

    protected function checkGroup($user, $site) {
        $user = isset($user) ? $user : 'visitor';
        $site = isset($site) ? $site : $this->checkSite();
        $query = 'SELECT `group` FROM `user` WHERE `login`="' . $user . '" AND `site` IN ("' . $site . '", "global") LIMIT 1;';
        $group = $this->db->select($query);
        $val = $this->db->select('SELECT `slug`,`display`,`inherit`,`site`,`hierarchy` FROM `user_groups` WHERE `slug`="' . $group[0]['group'] . '"');
        return $val[0];
    }

    public function checkParent($parent) {
        $group = is_array($parent) ? $parent['slug'] : $parent;
        $query = 'SELECT `inherit` FROM `user_groups` WHERE `slug`="' . $group . '"';
        $x = $this->db->select($query);
        if (isset($x[0])) {
            return $x[0]['inherit'];
        } else
            return FALSE;
    }

    public function userPerms($user, $string = FALSE) {
        if ($string == TRUE) {
            return '\'' . implode("','", $this->buildPermArray($user)) . '\'';
        } else {
            return $this->buildPermArray($user);
        }
    }

    private function buildPermArray($user) {
        $group = array_diff($_SESSION['user']['permissions']['group'], $_SESSION['user']['restrictions']['group']);
        $user = array_diff($_SESSION['user']['permissions']['individual'], $_SESSION['user']['restrictions']['individual']);
        $perms = array_unique(array_merge($group, $user));
        return $perms;
    }

    public function initUser($user) {
        try {
            $x = $this->checkUser($user, $this->checkSite());
            if (!$x[0] == NULL) {
                $_SESSION['user'] = $x[0];
                $this->initGroup($_SESSION['user']['login']);
            } else {
                return false;
            }
        } catch (Exception $e) {
            throw new Exception($e);
            return false;
        }
    }

    public function initGroup($user) {
        try {
            $group = $this->checkGroup($user);
            $_SESSION['user']['group'] = array(
                'slug' => $group['slug'],
                'display' => $group['display'],
                'parent' => $group['inherit'],
                'site' => $group['site'],
                'hierarchy' => $group['hierarchy']
            );
        } catch (Exception $e) {
            throw new Exception($e);
            return false;
        }
    }

    public function checkPerms($user, $group) {
        $_SESSION['user']['permissions'] = array(
            'group' => array(),
            'individual' => array()
        );
        $_SESSION['user']['restrictions'] = array(
            'group' => array(),
            'individual' => array()
        );
        //set group permissions
        $this->checkGroupPerms($_SESSION['user']['group']['slug']);
        $parent = $_SESSION['user']['group']['parent'];
        if ($_SESSION['user']['group']['slug'] != 'default') {
            while ($parent != 'default') {
                //fetch perms for parent group
                $x = $this->checkParent($parent);
                $this->checkGroupPerms($x);
                $parent = $x;
            }
            $this->checkGroupPerms($parent);
            unset($parent);
            //set group restrictions
            $this->checkGroupRestrictions($_SESSION['user']['group']['slug']);
            $parent = $_SESSION['user']['group']['parent'];
            while ($parent != 'default') {
                //fetch perms for parent group
                $x = $this->checkParent($parent);
                $this->checkGroupRestrictions($x);
                $parent = $x;
            }
            $this->checkGroupRestrictions($parent);
            unset($parent);
        }

        //set individual permissions
        $this->checkUserPerms($_SESSION['user']['login']);

        //set individual restrictions
        $this->checkUserRestrictions($_SESSION['user']['login']);
    }

    protected function checkGroupPerms($group) {
        $x = $this->db->select('SELECT `permission` FROM `user_groups_permissions` WHERE `active`=1 AND `group`="' . $group . '" AND `mode`="ALLOW"');
        foreach ($x as $i) {
            if (!in_array($i, $_SESSION['user']['permissions']['group'])) {
                $_SESSION['user']['permissions']['group'][] = $i['permission'];
            }
        }
    }

    protected function checkGroupRestrictions($group) {
        $x = $this->db->select('SELECT `permission` FROM `user_groups_permissions` WHERE `active`=1 AND `group`="' . $group . '" AND `mode`="DENY"');
        foreach ($x as $i) {
            if (!in_array($i, $_SESSION['user']['restrictions']['group'])) {
                $_SESSION['user']['restrictions']['group'][] = $i['permission'];
            }
        }
    }

    protected function checkUserPerms($user) {
        $x = $this->db->select('SELECT `permission` FROM `user_permissions` WHERE `user`="' . $user . '" AND `mode`="ALLOW"');
        foreach ($x as $i) {
            if (!in_array($i, $_SESSION['user']['permissions']['individual'])) {
                $_SESSION['user']['permissions']['individual'][] = $i['permission'];
            }
        }
    }

    protected function checkUserRestrictions($user) {
        $x = $this->db->select('SELECT `permission` FROM `user_permissions` WHERE `user`="' . $user . '" AND `mode`="DENY"');
        foreach ($x as $i) {
            if (!in_array($i, $_SESSION['user']['restrictions']['individual'])) {
                $_SESSION['user']['restrictions']['individual'][] = $i['permission'];
            }
        }
    }

    protected function applyUserPerms() {
        $_SESSION['permissions'] = $this->perms;
    }
    
    public function checkPermission($user, $perm) {
        if ($user = $_SESSION['user']['login']) {
            //current user, use session
            if (in_array($perm, $_SESSION['user']['permissions'])) {
                return TRUE;
            } else return FALSE;
        } else {
            //alternate user, get permissions array then perform check
        }
    }

    public function __destruct() {
        $this->db = NULL;
    }

}

?>
