<?php

/**
 * This file will detect the user's browser and version, and set the session variables
 * for the detected browser so the rest of the system will render data in accordance with
 * the user's browser capabilities
 *
 * @author Your Name <brian@mopsyd.me>
 */
namespace oroboros\core\libs\User;

class Browser extends \oroboros\core\libs\Utilities\Browser {
    
    const ICONDIR = '/public/media/images/icons/browser/';

    protected static $browser;

    public function __construct() {
        
    }
    
    public function init() {
        $this->Browser();
        $this->browser = $this->get_user_browser();
        return $this->browser;
    }
    
    public function get_user_browser() {
        $title = NULL;
        $icon = NULL;
        $mode = NULL;
        $settings = NULL;
        $version = NULL;
        $browser = NULL;
        $browser = $this->getBrowser();
        $version = $this->getVersion();
        switch ($browser) {
            case 'Opera':
                $title = 'Opera';
                $icon = self::ICONDIR . 'opera.png';
                $mode = 'browser';
                
                break;
            case 'Opera Mini':
                $title = 'Opera Mini';
                $icon = self::ICONDIR . 'opera.png';
                $mode = 'mobile';
                
                break;
            case 'WebTV':
                $title = 'WebTV';
                //$icon = self::ICONDIR . 'opera.png';
                $mode = 'embedded';
                
                break;
            case 'Internet Explorer':
                $title = 'Internet Explorer';
                $icon = self::ICONDIR . 'explorer.png';
                $mode = 'browser';
                
                break;
            case 'Pocket Internet Explorer':
                $title = 'Internet Explorer Mobile';
                $icon = self::ICONDIR . 'explorer.png';
                $mode = 'mobile';
                
                break;
            case 'Konqueror':
                $title = 'Konqueror';
                //$icon = self::ICONDIR . 'opera.png';
                $mode = 'browser';
                
                break;
            case 'iCab':
                $title = 'iCab';
                //$icon = self::ICONDIR . 'opera.png';
                $mode = 'browser';
                
                break;
            case 'OmniWeb':
                $title = 'OmniWeb';
                //$icon = self::ICONDIR . 'opera.png';
                $mode = 'browser';
                
                break;
            case 'Firebird':
                $title = 'Firebird';
                //$icon = self::ICONDIR . 'opera.png';
                $mode = 'browser';
                
                break;
            case 'Firefox':
                $title = 'Firefox';
                $icon = self::ICONDIR . 'firefox.png';
                $mode = 'browser';
                
                break;
            case 'Iceweasel':
                $title = 'Iceweasel';
                //$icon = self::ICONDIR . 'opera.png';
                $mode = 'browser';
                
                break;
            case 'Shiretoko':
                $title = 'Shiretoko';
                //$icon = self::ICONDIR . 'opera.png';
                $mode = 'browser';
                
                break;
            case 'Mozilla':
                $title = 'Mozilla';
                $icon = self::ICONDIR . 'firefox.png';
                $mode = 'browser';
                
                break;
            case 'Amaya':
                $title = 'Amaya';
                //$icon = self::ICONDIR . 'opera.png';
                $mode = 'browser';
                
                break;
            case 'Lynx':
                $title = 'Lynx';
                //$icon = self::ICONDIR . 'opera.png';
                $mode = 'browser';
                
                break;
            case 'Safari':
                $title = 'Safari';
                $icon = self::ICONDIR . 'safari.png';
                $mode = 'browser';
                
                break;
            case 'iPhone':
                $title = 'Safari Mobile';
                $icon = self::ICONDIR . 'safari.png';
                $mode = 'browser';
                
                break;
            case 'iPod':
                $title = 'Safari Mobile';
                $icon = self::ICONDIR . 'safari.png';
                $mode = 'browser';
                
                break;
            case 'iPad':
                $title = 'Safari Mobile';
                $icon = self::ICONDIR . 'safari.png';
                $mode = 'browser';
                
                break;
            case 'Chrome':
                $title = 'Chrome';
                $icon = self::ICONDIR . 'chrome.png';
                $mode = 'browser';
                
                break;
            case 'Android':
                $title = 'Android';
                $icon = self::ICONDIR . 'chrome.png';
                $mode = 'browser';
                
                break;
            case 'GoogleBot':
                $title = 'GoogleBot';
                $icon = self::ICONDIR . 'spider.png';
                $mode = 'crawler';
                
                break;
            case 'Yahoo! Slurp':
                $title = 'Yahoo! Slurp';
                $icon = self::ICONDIR . 'spider.png';
                $mode = 'crawler';
                
                break;
            case 'W3C Validator':
                $title = 'W3C Markup Validation Servic';
                $icon = self::ICONDIR . 'spider.png';
                $mode = 'crawler';
                
                break;
            case 'BlackBerry':
                $title = 'Blackberry';
                $icon = self::ICONDIR . 'blackberry.png';
                $mode = 'mobile';
                
                break;
            case 'IceCat':
                $title = 'IceCat';
                $icon = self::ICONDIR . 'opera.png';
                $mode = 'browser';
                
                break;
            case 'Nokia S60 OSS Browser':
                $title = 'Nokia S60 OSS Browser';
                $icon = self::ICONDIR . 'nokia.png';
                $mode = 'mobile';
                
                break;
            case 'Nokia Browser':
                $title = 'Nokia Browser';
                $icon = self::ICONDIR . 'nokia.png';
                $mode = 'mobile';
                
                break;
            case 'MSN Bot':
                $title = 'MSN Bot';
                $icon = self::ICONDIR . 'spider.png';
                $mode = 'crawler';
                
                break;
            case 'Bing Bot':
                $title = 'Bing Bot';
                $icon = self::ICONDIR . 'spider.png';
                $mode = 'crawler';
                
                break;
            case 'Netscape Navigator':
                $title = 'Netscape Navigator';
                $icon = self::ICONDIR . 'netscape.png';
                $mode = 'browser';
                
                break;
            case 'Galeon':
                $title = 'Galeon';
                //$icon = self::ICONDIR . 'opera.png';
                $mode = 'browser';
                
                break;
            case 'NetPositive':
                $title = 'NetPositive';
                //$icon = self::ICONDIR . 'opera.png';
                $mode = 'browser';
                
                break;
            case 'Phoenix':
                $title = 'Phoenix';
                //$icon = self::ICONDIR . 'opera.png';
                $mode = 'browser';
                
                break;
            default:
                $title = 'Unknown';
                //$icon = self::ICONDIR . 'opera.png';
                $mode = 'browser';
                
                break;
        }
        if (!isset($icon)) {
            $icon = self::ICONDIR . 'unknown.png';
        }
        return $this->packageBrowser($title, $icon, $mode, $version);
    }
    
    public function packageBrowser($title, $icon, $mode, $version) {
        $browser = array(
            'title' => $title,
            'version' => $version,
            'icon' => $icon,
            'mode' => $mode
        );
        return $browser;
    }
    
    public function __destruct() {
        
    }

}
?>
