<?php

/**
 * This class handles user defined settings
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */

namespace oroboros\core\libs\User;

class User extends \oroboros\core\libs\Abstracts\Site\Lib {

    const DIR = 'oroboros/users/';

    public $site = NULL;
    public $subdomain = NULL;
    public $page = NULL;

    function __construct($package = NULL) {
        parent::__construct($package);
        $this->init();
    }

    public function init() {
        if (isset($this->package)) {
            $this->settings = new \oroboros\core\libs\Settings\User($this->package);
        } else {
            $this->settings = new \oroboros\core\libs\Settings\User();
        }
        $this->settings->init();
        $this->security = new \oroboros\core\libs\User\Security($this->package);
        $this->session = new \oroboros\core\libs\User\Session($this->package);
        $this->cookie = new \oroboros\core\libs\User\Cookies();
        $this->device = new \oroboros\core\libs\User\Device();
        $this->browser = new \oroboros\core\libs\User\Browser();
        $this->permissions = new \oroboros\core\libs\User\Permissions($this->db);
        $this->file = new \oroboros\core\libs\Filebase\Filebase($this->package);
    }

    public function initUser($user) {
        $device_nonce = $this->security->setSecurityCookie();
        $session_nonce = $this->security->setSessionNonce();
        $query = "INSERT IGNORE INTO `sites_sessions` SET `site`='" . $this->site['slug'] . "', `user`='" . $user . "', `session_id`='" . session_id() . "', `device_nonce`='" . $device_nonce . "', `user_nonce`='" . $session_nonce . "', `idle`='" . $this->time->now('database') . "', `timeout`='" . $this->time->now('database') . "', `last_active`='" . $this->time->now('database') . "';";
        $this->db->query($query);
        $this->session->buildSession($device_nonce, $session_nonce);
        unset($_SESSION['user']);
        $this->session->pushIndex('user', $this->userBase($user, $session_nonce));
        $this->userperms = $_SESSION['user']['permissions'];
        $this->userEnvironment();
    }

    public function updateUserData($user) {
        $device_nonce = $this->security->resetSecurityCookie();
        $session_nonce = $this->security->setSessionNonce();
        $this->db->update('sites_sessions', array('device_nonce' => $device_nonce, 'user_nonce' => $session_nonce, 'last_active' => $this->time->now('database')), '`session_id`="' . session_id() . '"');
        $this->session->buildSession($device_nonce, $session_nonce);
        unset($_SESSION['user']);
        $this->session->pushIndex('user', $this->userBase($user, $session_nonce));
        $this->userperms = $this->perms->getUserPerms($user);
        $this->userEnvironment();
    }

    public function checkUser($user = NULL) {
        $registered = $this->session->init();
        if (!isset($user)) {
            $user = isset($_SESSION['user']['login']) ? $_SESSION['user']['login'] : 'visitor';
        }

        if ($registered == TRUE) {
            //session data exists
            if ($this->validateUser($user) != FALSE) {
                $this->updateUserData($user);
            } else {
                $message = 'User validation failed, forcing logout to retain account integrity.';
                $this->resetUser();
                $this->security->securityLogout($message);
            }
        } else {
            //session data does not exist
            $this->initUser($user);
        }
    }

    protected function validateUser($user) {
        $cookie = $this->cookie->getCookie('security');
        //var_dump($cookie[0]);
        if ($user == 'visitor') {
            return TRUE;
        }
        if ($this->security->validateSessionNonce($_SESSION['user']['nonce']) == FALSE) {
            return FALSE;
        }

        return TRUE;
    }

    public function resetUser() {
        unset($_SESSION['user']);
        $this->cookie->deleteCookie('security');
        $_SESSION['security']['nonce'] = FALSE;
        $_SESSION['security']['cookie'] = FALSE;
        $this->db->delete('sites_sessions', 'session_id="' . session_id() . '"');
        $this->session->destroy();
        $this->session->init();
        $this->initUser('visitor');
    }

    public function updateUser($user) {
        $this->session->destroy();
        $this->session->init();
        $this->checkUser($user);
    }

    public function userEnvironment() {
        $this->browser->init();
        $this->device->init();
        $this->session->pushIndex('browser', $this->browser->get_user_browser());
        $this->session->pushIndex('device', $this->device->get_user_device());
        $this->session->pushIndex('utilities', NULL);
    }

    public function userBase($user, $session_nonce) {
        $details = $this->getUser($user, $this->site['slug']);
        $base = array(
            'login' => $details['login'],
            'loggedIn' => ($details['login'] == 'visitor') ? FALSE : TRUE,
            'group' => $details['group'],
            'active' => $this->time->now('database'),
            'permissions' => $this->perms->getUserPerms($details['login'], $this->site['slug'], $details['group']),
            'settings' => $this->getUserSettings($details['login'], $this->site['slug']),
            'global' => $details['global'],
            'details' => $this->getUserDetails($details['login'], $this->site['slug']),
            'nonce' => $session_nonce
        );
        return $base;
    }

    public function handleLogin() {
        if (isset($_SESSION['user']['loggedIn']) && $_SESSION['user']['loggedIn'] != FALSE) {
            $logged = $_SESSION['user']['loggedIn'];
        } else
            $logged = false;
        if ($logged == FALSE) {
            $this->resetUser();
            header('Location: ' . '/login');
            exit();
        }
    }

    public function renderUser($user, $method = NULL) {
        switch ($method) {
            case 'userbox':
                return $this->userBox($user);
                break;
            case 'record':
                return $this->userRecord($user);
                break;
            case 'icon':
                return $this->userIcon($user);
                break;
            case 'profile':
                return $this->userProfile($user);
                break;
            case 'concise':
                return $this->userConcise($user);
                break;
            default:
                return $this->userRaw($user);
                break;
        }
    }

    private function userBox($user) {
        $user = $this->getUserDetails($user);
        if (isset($user)) {
            $userbox = '<div class="user"><span class="submenu"><a href="[]HOSTURL[/]/profile/' . $user['login'] . '" class="profile"><span class="userpic"><img src="' . $user['icon'] . '" class="user-icon" alt="' . $user['login'] . '" /></span>';
            $userbox .= '<span class="user-details"><p class="user-name">' . $user['first'] . ' ' . $user['last'] . '</p></span></a>';
            $this->linkrender = !isset($this->links) ? $this->linkrender = new \oroboros\core\libs\Render\Links($this->package, $this->_site['slug'], $this->_subdomain, $this->_page['slug']) : $this->linkrender;
            $this->linkbuild = !isset($this->linkbuild) ? new \oroboros\core\views\Site\Content\Browser\Menu() : $this->linkbuild;
            $render = $this->linkrender->buildMenu($_SESSION['user']['login'], $this->_site['slug'], 'userbox', 'registered', '#');
            $result = $this->linkbuild->listMenuRender($render);
            $userbox .= $result;
            $userbox .= '</span></div>';
            return $userbox;
        } else
            return false;
    }

    private function userRecord($user, $site = 'default') {
        $user = $this->getUser($user);
        if (isset($user)) {
            
        } else
            return false;
    }

    private function userIcon($user, $site = 'default') {
        $user = $this->getUser($user, $site);
        if (isset($user)) {
            
        } else
            return false;
    }

    private function userProfile($user, $site = 'default') {
        $user = $this->getUser($user);
        if (isset($user)) {
            
        } else
            return false;
    }

    private function userConcise($user, $site = 'default') {
        $user = $this->getUser($user);
        if (isset($user)) {
            
        } else
            return false;
    }

    private function userRaw($user, $site = 'default') {
        $user = $this->getUser($user);
        if (isset($user)) {
            return $user;
        } else
            return false;
    }

    protected function getUser($user, $site = NULL) {
        if (!isset($site) && isset($_SESSION['site'])) {
            $site = $_SESSION['site'];
        } elseif (!isset($site) && isset($this->site)) {
            $site = $this->site['slug'];
        }
        $query = "SELECT `login`,`group`,`global`,`site`,`active` FROM `user` WHERE `login`='" . $user . "' AND `show`='1' AND `site` IN ('" . $site . "','global') LIMIT 1;";
        $userData = $this->db->select($query);
        if (isset($userData[0])) {
            $site = $userData[0]['site'];
            return $userData[0];
        } else
            return false;
    }

    protected function getUserDetails($user, $site = 'default') {
        $user = isset($user) ? $user : 'visitor';
        $site = isset($site) ? $site : $this->perms->checkSite();
        $site = ($user == 'visitor') ? 'global' : $site;
        $query = "SELECT `user`,`icon`,`name_first`,`name_last`,`phone`,`addr_1`,`addr_2`,`addr_city`,`addr_state`,`addr_zip`,`addr_zip_ext`,`addr_country` FROM `user_details` WHERE `user`='$user' AND `site`='" . $site . "' LIMIT 1";
        $details = $this->db->select("SELECT `user`,`icon`,`name_first`,`name_last`,`phone`,`addr_1`,`addr_2`,`addr_city`,`addr_state`,`addr_zip`,`addr_zip_ext`,`addr_country` FROM `user_details` WHERE `user`='$user' AND `site`='" . $site . "' LIMIT 1");
        if (isset($details)) {
            $userDetails = array(
                'login' => $details[0]['user'],
                'first' => $details[0]['name_first'],
                'last' => $details[0]['name_last'],
                'phone' => $details[0]['phone'],
                'address' => array(
                    'addr1' => $details[0]['addr_1'],
                    'addr2' => $details[0]['addr_2'],
                    'city' => $details[0]['addr_city'],
                    'state' => $details[0]['addr_state'],
                    'zip' => $details[0]['addr_zip'],
                    'country' => $details[0]['addr_country']
                )
            );
            if (!is_null($details[0]['addr_zip_ext'])) {
                $userDetails['address']['zip'] .= '-' . $details[0]['addr_zip_ext'];
            }
            if (is_null($details[0]['icon'])) {
                $userDetails['icon'] = _USER . $site . '/default.png';
            } else {
                $userDetails['icon'] = _USER . $site . '/' . $details[0]['user'] . '/';
                $userDetails['icon'] .= $details[0]['icon'];
            }
            return $userDetails;
        } else
            return FALSE;
    }

    public function getUserSettings($user, $site) {
        $site = is_array($site) && isset($site['slug']) ? $site['slug'] : $site;
        $file = _BASE . self::DIR . '/' . $site . '/' . $user . '/settings.ini';
        if (is_readable($file)) {
            $settings = $this->settings->handleSettings($file);
            return $settings;
        } else {

            return FALSE;
        }
    }

    public function getSiteUsers($site) {
        $query = "SELECT `login`,`email`,`group`,`suspended`,`banned`,`disabled`,`global` FROM `user` WHERE `site`='$site' AND `active`='1' AND `show`='1' ORDER BY `hierarchy`;";
        $result = $this->db->select($query);
        return $result;
    }

    public function makeUser($data, $site) {
        $details = array();
        $details['login'] = $data['username'];
        $details['password'] = \oroboros\core\libs\Encryption\Hash::create('sha256', $data['password'], HASH_PASSWORD_KEY);
        $details['group'] = $data['group'];
        $details['email'] = $data['email'];
        $details['site'] = $site;
        $details['disabled'] = isset($data['enabled']) ? 0 : 1;
        $details['global'] = isset($data['global']) ? 1 : 0;
        $details['show'] = 1;
        $details['banned'] = 0;
        $details['suspended'] = 0;
        if ($this->makeUserDirectory($data['username'], $site) == TRUE) {
            //successfully created user directory
            if ($data['image'] == 'group') {
                //use group default icon
                $icon = _APP . 'users/' . $site . '/groups/' . $details['group'] . '.png';
            } else {
                //use site default icon
                $icon = _APP . 'users/' . $site . '/default.png';
            }
            if ($this->makeBaseUserIcon($data['username'], $site, $icon) == TRUE) {
                
            } else {
                $this->removeUserDirectory($data['username'], $site);
                $_SESSION['form'] = array(
                    'formid' => 'create-user',
                    'server-message-type' => 'fail',
                    'server-message-response' => 'Could not create user icon due to file permission error',
                );
                return FALSE;
            }
            if ($this->makeBaseUserSettings($data['username'], $site) == TRUE) {
                
            } else {
                $this->removeUserDirectory($data['username'], $site);
                $_SESSION['form'] = array(
                    'formid' => 'create-user',
                    'server-message-type' => 'fail',
                    'server-message-response' => 'Could not create user settings due to file permission error',
                );
                return FALSE;
            }
            $this->db->insert('user', $details);
        } else {
            //could not create user directory
            $_SESSION['form'] = array(
                'formid' => 'create-user',
                'server-message-type' => 'fail',
                'server-message-response' => 'Could not create user directory due to file permission error',
            );
            return FALSE;
        }
        return TRUE;
    }

    public function checkForExistingUser($user, $site) {
        $query = "SELECT `login` FROM `user` WHERE `login`='$user' AND `site`='$site' LIMIT 1;";
        $result = $this->db->select($query);
        $check = empty($result) ? FALSE : TRUE;
        return $check;
    }

    public function validateUserData($data) {
        $valid = TRUE;
        $valid = $data['username'] == '' ? FALSE : $valid;
        $valid = $data['password'] == '' ? FALSE : $valid;
        $valid = $data['group'] == '' ? FALSE : $valid;
        $valid = $data['image'] == '' ? FALSE : $valid;
        $valid = $data['email'] == '' ? FALSE : $valid;
        if ($valid != TRUE) {
            $valid = $this->getInvalidFieldInfo($data);
        }
        return $valid;
    }

    public function getInvalidFieldInfo($data) {
        $missing = array();
        if ($data['username'] == '') {
            $missing[] = 'Username must be supplied.';
        }
        if ($data['password'] == '') {
            $missing[] = 'Password must be supplied.';
        }
        if ($data['email'] == '') {
            $missing[] = 'User must have a valid email address.';
        }
        if ($data['group'] == '') {
            $missing[] = 'User group must be supplied.';
        }
        if ($data['image'] == '') {
            $missing[] = 'Initial user image was not set.';
        }
        return $missing;
    }

    public function makeUserDetails($data, $site = NULL, $update = FALSE) {
        if ($update == TRUE) {
            //update existing user data
            $site = isset($site) ? $site : $this->site['slug'];
            $query = "SELECT `site`,`user`,`icon`,`name_first`,`name_last`,`phone`,`addr_1`,`addr_2`,`addr_city`,`addr_state`,`addr_zip`,`addr_zip_ext`,`addr_country` FROM `user_details` WHERE `site`='$site' AND `user`='" . $data['user'] . "' LIMIT 1;";
            $results = $this->db->select($query);
            $results = $results[0];
            $update = array();
            $update['icon'] = (isset($results['icon']) && $results['icon'] == $data['image']) ? $results['icon'] : $data['image'];
            $update['name_first'] = ((isset($results['name_first']) && $results['name_first'] == $data['name-first']) || $data['name-first'] == '') ? $results['name_first'] : $data['name-first'];
            $update['name_last'] = ((isset($results['name_last']) && $results['name_last'] == $data['name-last']) || $data['name-last'] == '') ? $results['name_last'] : $data['name-last'];
            $update['phone'] = ((isset($results['phone']) && $results['phone'] == $data['phone']) || $data['phone'] == '') ? $results['phone'] : $data['phone'];
            $update['addr_1'] = ((isset($results['addr_1']) && $results['addr_1'] == $data['addr_1']) || $data['addr_1'] == '') ? $results['addr_1'] : $data['addr_1'];
            $update['addr_2'] = ((isset($results['addr_2']) && $results['addr_2'] == $data['addr_2']) || $data['addr_2'] == '') ? $results['addr_2'] : $data['addr_2'];
            $update['addr_city'] = ((isset($results['addr_city']) && $results['addr_city'] == $data['addr_city']) || $data['addr_city'] == '') ? $results['addr_city'] : $data['addr_city'];
            $update['addr_state'] = ((isset($results['addr_state']) && $results['addr_state'] == $data['addr_state']) || $data['addr_state'] == '') ? $results['addr_state'] : $data['addr_state'];
            $update['addr_zip'] = ((isset($results['addr_zip']) && $results['addr_zip'] == $data['addr_zip']) || $data['addr_zip'] == '') ? $results['addr_zip'] : $data['addr_zip'];
            $update['addr_zip_ext'] = ((isset($results['addr_zip_ext']) && $results['addr_zip_ext'] == $data['addr_zip_ext']) || $data['addr_zip_ext'] == '') ? $results['addr_zip_ext'] : $data['addr_zip_ext'];
            $update['addr_country'] = ((isset($results['addr_country']) && $results['addr_country'] == $data['addr_country']) || $data['addr_country'] == '') ? $results['addr_country'] : $data['addr_country'];
        } else {
            //create new user data
            $details = array();
            if ($data['name-first'] != '') {
                $details['name_first'] = $data['name-first'];
            }
            if ($data['name-last'] != '') {
                $details['name_last'] = $data['name-last'];
            }
            if ($data['phone'] != '') {
                $details['phone'] = $data['phone'];
            }
            if ($data['addr_1'] != '') {
                $details['addr_1'] = $data['addr_1'];
            }
            if ($data['addr_2'] != '') {
                $details['addr_2'] = $data['addr_2'];
            }
            if ($data['addr_city'] != '') {
                $details['addr_city'] = $data['addr_city'];
            }
            if ($data['addr_state'] != '') {
                $details['addr_state'] = $data['addr_state'];
            }
            if ($data['addr_country'] != '') {
                $details['addr_country'] = $data['addr_country'];
            }
            if ($data['addr_zip'] != '') {
                $details['addr_zip'] = $data['addr_zip'];
            }
            if ($data['addr_zip_ext'] != '') {
                $details['addr_zip_ext'] = $data['addr_zip_ext'];
            }
            $items = $details;
            $items['site'] = isset($site) ? $site : $this->site['slug'];
            $items['icon'] = $data['image'] == 'site' ? 'default.png' : $data['group'] . '.png';
            $items['user'] = $data['username'];
            $this->db->insert('user_details', $items);
            if (!empty($details)) {
                $return = array();
                foreach ($details as $key => $value) {
                    $return[] = array($key => $value);
                }
                return $return;
            } else {
                return FALSE;
            }
        }
    }

    protected function makeUserDirectory($user, $site) {
        $userdir = _APP . 'users/' . $site . '/' . $user;
        $result = $this->file->makedir($userdir);
        if ($result == TRUE) {
            $dir = $userdir . '/archive';
            $this->file->makedir($dir);
            $dir = $userdir . '/files';
            $this->file->makedir($dir);
            return TRUE;
        } else
            return FALSE;
    }

    protected function removeUserDirectory($user, $site) {
        $userdir = _APP . 'users/' . $site . '/' . $user;
        return $this->file->removedir($userdir);
    }

    protected function makeBaseUserIcon($user, $site, $icon) {
        $targetDir = _APP . 'users/' . $site . '/' . $user . '/';
        if ($this->file->copy($icon, $targetDir) == TRUE) {
            return TRUE;
        } else
            return FALSE;
    }

    protected function makeBaseUserSettings($user, $site) {
        $target = _APP . 'users/' . $site . '/' . $user . '/';
        $base = _APP . 'users/' . $site . '/settings.ini';
        if ($this->file->copy($base, $target) == TRUE) {
            return TRUE;
        } else
            return FALSE;
    }

    function __destruct() {
        ;
    }

}

?>
