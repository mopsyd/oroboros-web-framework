<?php

/**
 * Description of _Device
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\User;

class Device extends \oroboros\core\libs\Utilities\Browser {
    
    const ICONDIR = '/public/media/images/icons/os/';

    private $uagent = NULL;
    public  $device = NULL;

    function __construct() {

    }

    public function init() {
        $this->Browser();
        $this->device = $this->get_user_device();
        return $this->device;
    }
    
    public function get_user_device() {
        $devicemode=NULL;
        $title=NULL;
        $icon=NULL;
        $settings = NULL;
        $os = $this->getPlatform();
        switch ($os) {
            case 'Apple':
                //apple desktop
                $devicemode = 'browser';
                $icon = self::ICONDIR . 'apple.png';
                $title = 'Mac OSX desktop operating system';
                $settings = $this->osxHandler();
                break;
            case 'Windows':
                //windows desktop
                $devicemode = 'browser';
                $icon = self::ICONDIR . 'win02.png';
                $title = 'Windows operating system';
                $settings = $this->windowsHandler();
                break;
            case 'Windows CE':
                //windows embedded
                $devicemode = 'embedded';
                $icon = self::ICONDIR . 'win03.png';
                $title = 'Windows CE embedded system';
                $settings = $this->windowsHandler();
                break;
            case 'Linux':
                //generic linux kernel
                $devicemode = 'browser';
                //$icon = self::ICONDIR . 'apple.png';
                $title = 'Linux, unknown build';
                $settings = $this->linuxHandler();
                break;
            case 'OS/2':
                //OS/2 embedded
                $devicemode = 'embedded';
                //$icon = self::ICONDIR . 'apple.png';
                $title = 'OS/2 embedded operating system';
                $settings = $this->outdatedHandler();
                break;
            case 'BeOS':
                //BeOS desktop or embedded
                $devicemode = 'embedded';
                //$icon = self::ICONDIR . 'apple.png';
                $title = 'BeOS operating system';
                $settings = $this->outdatedHandler();
                break;
            case 'iPhone':
                //iPhone mobile
                $devicemode = 'mobile';
                $icon = self::ICONDIR . 'ios.png';
                $title = 'iPhone iOS mobile';
                $settings = $this->mobileHandler();
                break;
            case 'iPod':
                //iPod mobile
                $devicemode = 'mobile';
                $icon = self::ICONDIR . 'ios.png';
                $title = 'iPod iOS';
                $settings = $this->mobileHandler();
                break;
            case 'iPad':
                //iPad tablet
                $devicemode = 'tablet';
                $icon = self::ICONDIR . 'ios.png';
                $title = 'iPad iOS';
                $settings = $this->tabletHandler();
                break;
            case 'BlackBerry':
                //Blackberry mobile or embedded
                $devicemode = 'mobile';
                $icon = self::ICONDIR . 'blackberry.png';
                $title = 'Blackberry mobile operating system';
                $settings = $this->mobileHandler();
                break;
            case 'Nokia':
                //Nokia mobile
                $devicemode = 'mobile';
                //$icon = self::ICONDIR . 'apple.png';
                $title = 'Nokia mobile operating system';
                $settings = $this->mobileHandler();
                break;
            case 'FreeBSD':
                //FreeBSD linux kernel
                $devicemode = 'browser';
                //$icon = self::ICONDIR . 'apple.png';
                $title = 'FreeBSD linux kernel';
                $settings = $this->linuxHandler();
                break;
            case 'OpenBSD':
                //OpenBSD linux kernel
                $devicemode = 'browser';
                //$icon = self::ICONDIR . 'apple.png';
                $title = 'OpenBSD linux kernel';
                $settings = $this->linuxHandler();
                break;
            case 'NetBSD':
                //NetBSD linux kernel
                $devicemode = 'browser';
                //$icon = self::ICONDIR . 'apple.png';
                $title = 'NetBSD linux kernel';
                $settings = $this->linuxHandler();
                break;
            case 'SunOS':
                //Sun OS linux kernel
                $devicemode = 'browser';
                //$icon = self::ICONDIR . 'apple.png';
                $title = 'SunOS linux kernel';
                $settings = $this->linuxHandler();
                break;
            case 'OpenSolaris':
                //Open Solaris linux kernel
                $devicemode = 'browser';
                //$icon = self::ICONDIR . 'apple.png';
                $title = 'OpenSolaris linux kernel';
                $settings = $this->linuxHandler();
                break;
            case 'Android':
                //Android mobile
                $devicemode = 'mobile';
                $icon = self::ICONDIR . 'android.png';
                $title = 'Android mobile operating system';
                $settings = $this->mobileHandler();
                break;
            default:
                //unknown operating system
                $devicemode = 'browser';
                $title = 'Unknown operating system';
                //$icon = self::ICONDIR . 'apple.png';
                $info = $this->getOs();
                break;
        }
        if (!isset($icon)) {
            $icon = self::ICONDIR . 'unknown.png';
        }
        return $this->packageDevice($devicemode, $title, $icon, $settings);
    }
    
    protected function packageDevice($devicemode, $title, $icon, $settings) {
        $info = array(
            'mode' => $devicemode,
            'title' => $title,
            'icon' => $icon,
            'settings' => $settings
        );
        return $info;
    }

    function os_info() {
        // the order of this array is important
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $this->uagent = $_SERVER['HTTP_USER_AGENT'];
        } else
            return false;
        global $uagent;
        $oses = array(
            'Win311' => 'Win16',
            'Win95' => '(Windows 95)|(Win95)|(Windows_95)',
            'WinME' => '(Windows 98)|(Win 9x 4.90)|(Windows ME)',
            'Win98' => '(Windows 98)|(Win98)',
            'Win2000' => '(Windows NT 5.0)|(Windows 2000)',
            'WinXP' => '(Windows NT 5.1)|(Windows XP)',
            'WinServer2003' => '(Windows NT 5.2)',
            'WinVista' => '(Windows NT 6.0)',
            'Windows7' => '(Windows NT 6.1)',
            'Windows8' => '(Windows NT 6.2)',
            'WinNT' => '(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)',
            'OpenBSD' => 'OpenBSD',
            'SunOS' => 'SunOS',
            'Ubuntu' => 'Ubuntu',
            'Android' => 'Android',
            'Linux' => '(Linux)|(X11)',
            'iPhone' => 'iPhone',
            'iPad' => 'iPad',
            'MacOS' => '(Mac_PowerPC)|(Macintosh)',
            'QNX' => 'QNX',
            'BeOS' => 'BeOS',
            'OS2' => 'OS/2',
            'SearchBot' => '(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp)|(MSNBot)|(Ask Jeeves/Teoma)|(ia_archiver)'
        );
        $uagent = strtolower($this->uagent ? $this->uagent : $_SERVER['HTTP_USER_AGENT']);
        foreach ($oses as $os => $pattern) {
            if (preg_match('/' . $pattern . '/i', $uagent)) {
                $_SESSION['device']['name'] = $os;
                return $os;
            }
        }
        return 'unknown';
    }

    public function getOs() {
        $os = $this->os_info();
        if (is_null($os)) {
            $os = 'default';
        }
        $_SESSION['device']['name'] = $os;
        $mediapath = _IMAGES . 'icons/os/';
        switch ($os) {
            case 'Win311|Win95|WinME|Win98|Win2000|WinXP|WinServer2003|WinVista|Windows7|Windows8|WinNT':
                //return windows icon, detect specific windows version
                $_SESSION['device']['mode'] = 'desktop';
                $_SESSION['device']['icon'] = $mediapath . 'windows.png';
                $this->windowsHandler();
                break;
            case 'OpenBSD':
                //return OpenBSD icon
                $_SESSION['device']['mode'] = 'desktop';
                //$_SESSION['device']['icon'] = $mediapath . 
                $this->linuxHandler();
                break;
            case 'SunOS':
                //return SunOS icon
                $_SESSION['device']['mode'] = 'desktop';
                //$_SESSION['device']['icon'] = $mediapath . 
                $this->linuxHandler();
                break;
            case 'Ubuntu':
                //Return ubuntu icon
                $_SESSION['device']['mode'] = 'desktop';
                $_SESSION['device']['icon'] = $mediapath . 'ubuntu.png';
                $this->linuxHandler();
                break;
            case 'Android':
                //return Android icon
                $_SESSION['device']['mode'] = 'mobile';
                $_SESSION['device']['icon'] = $mediapath . 'android.png';
                $this->mobileHandler();
                break;
            case 'Linux':
                //return generic Linux icon
                $_SESSION['device']['mode'] = 'desktop';
                //$_SESSION['device']['icon'] = $mediapath . 
                $this->linuxHandler();
                break;
            case 'iPhone':
                //return iOS icon
                $_SESSION['device']['mode'] = 'tablet';
                $_SESSION['device']['icon'] = $mediapath . 'iOS.png';
                $this->mobileHandler();
                break;
            case 'iPad':
                //return iOS icon
                $_SESSION['device']['mode'] = 'mobile';
                $_SESSION['device']['icon'] = $mediapath . 'iOS.png';
                $this->tabletHandler();
                break;
            case 'MacOS':
                //return OSX icon
                $_SESSION['device']['mode'] = 'desktop';
                $_SESSION['device']['icon'] = $mediapath . 'apple.png';
                $this->osxHandler();
                break;
            case 'QNX':
                //return QNX icon
                $_SESSION['device']['mode'] = 'mobile';
                $_SESSION['device']['icon'] = $mediapath . 'blackberry.png';
                $this->unixHandler();
                break;
            case 'BeOS':
                //return BeOS icon
                $_SESSION['device']['mode'] = 'desktop';
                //$_SESSION['device']['icon'] = $mediapath . 
                $this->outdatedHandler();
                $this->linuxHandler();
                break;
            case 'OS2':
                //return OS2 icon
                $_SESSION['device']['mode'] = 'desktop';
                //$_SESSION['device']['icon'] = $mediapath . 
                $this->outdatedHandler();
                break;
            case 'Searchbot':
                //return crawler icon
                $_SESSION['device']['mode'] = 'crawler';
                //$_SESSION['device']['icon'] = $mediapath . 
                $this->crawlerHandler();
                break;
        }
    }

    private function windowsHandler() {
        
        return 'incomplete';
    }

    private function osxHandler() {
        
        return 'incomplete';
    }

    private function linuxHandler() {
        
        return 'incomplete';
    }

    private function unixHandler() {
        
        return 'incomplete';
    }

    private function tabletHandler() {
        
        return 'incomplete';
    }

    private function mobileHandler() {
        
        return 'incomplete';
    }

    private function outdatedHandler() {
        
        return 'incomplete';
    }

    private function crawlerHandler() {
        
        return 'incomplete';
    }
    
    public function __destruct() {
        
    }

}
?>
