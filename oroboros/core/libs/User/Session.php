<?php

/**
 * Description of Session
 * Sets the base session variables used by the 
 * system to handle data that must be passed from
 * one landing page to another.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */

namespace oroboros\core\libs\User;

class Session extends \oroboros\core\libs\Abstracts\Site\Lib {

    protected $session = NULL;
    public $sectionIndex = NULL;
    public $sectionData = array();
    public $existingIndexes = array();
    public $existingData = array();

    function __construct($package) {
        parent::__construct($package);
        $this->init();
    }

    public function init() {
        @session_start();
        if (empty($_SESSION)) {
            return FALSE;
        } else
            return TRUE;
    }

    public function buildSession() {
        if (!isset($_SESSION)) {
            $this->start();
        }
        if (!isset($_SESSION['core'])) {
            $this->core();
        }
        if (!isset($_SESSION['auth'])) {
            $this->auth();
        }
        if (!isset($_SESSION['node'])) {
            $this->node();
        }
        if (!isset($_SESSION['device'])) {
            $this->device();
        }
        if (!isset($_SESSION['settings'])) {
            $this->settings();
        }
        if (!isset($_SESSION['site'])) {
            $this->site();
        }
        if (!isset($_SESSION['subdomain'])) {
            $this->subdomain();
        }
        if (!isset($_SESSION['utilities'])) {
            $this->utilities();
        }
        if (!isset($_SESSION['page'])) {
            $this->page();
        }
        if (!isset($_SESSION['modules'])) {
            $this->modules();
        }
        if (!isset($_SESSION['components'])) {
            $this->modules();
        }
        if (!isset($_SESSION['template'])) {
            $this->template();
        }
        if (!isset($_SESSION['theme'])) {
            $this->theme();
        }
        if (!isset($_SESSION['sandbox'])) {
            $this->sandbox();
        }
        if (!isset($_SESSION['scripts'])) {
            $this->scripts();
        }
        if (!isset($_SESSION['stylesheets'])) {
            $this->stylesheets();
        }
        if (!isset($_SESSION['security'])) {
            $this->security();
        }
    }

    public function core() {
        $this->setBase('core');
    }

    public function device() {
        $this->setBase('device');
    }

    public function node() {
        $this->setBase('node');
    }

    public function site() {
        $this->setBase('site');
    }

    public function subdomain() {
        $this->setBase('subdomain');
    }

    public function settings() {
        $this->setBase('settings');
    }

    public function sandbox() {
        $this->setBase('sandbox');
    }

    public function auth() {
        $this->setBase('auth');
    }

    public function stylesheets() {
        $this->setBase('stylesheets');
    }

    public function scripts() {
        $this->setBase('scripts');
    }

    public function page() {
        $this->setBase('page');
    }

    public function template() {
        $this->setBase('template');
    }

    public function theme() {
        $this->setBase('theme');
    }

    public function modules() {
        $this->setBase('modules');
    }

    public function components() {
        $this->setBase('components');
    }

    public function dependencies() {
        $this->setBase('dependencies');
    }

    public function security() {
        $this->setBase('security');
    }
    public function utilities() {
        $this->setBase('utilities');
    }

    protected function checkSessionIndex($index) {
        
    }

    protected function checkSessionData($data) {
        
    }

    protected function getSessionIndex($index) {
        
    }

    protected function getSessionData($index) {
        
    }

    protected function pullExistingIndexes() {
        foreach ($_SESSION as $index => $data) {
            $checkindex = $this->checkIndex($index);
            if ($checkindex == FALSE) {
                $this->existingIndexes[] = $index;
                $this->pullExistingData($index);
            }
        }
    }

    protected function pullExistingData($index) {
        
    }

    protected function checkIndex($value) {
        $check = FALSE;
        foreach ($this->existingIndexes as $index) {
            if ($index == $value) {
                $check = TRUE;
            }
        }
        return $check;
    }

    protected function checkData($index, $data) {
        
    }

    protected function setBase($name) {
        if ($this->checkIndex($name) != FALSE) {
            //index already exists, compare data, update if newer
        } else {
            //index does not exist, register a new one
            $this->registerIndex($name);
        }
    }

    public function pushIndex($index, $data=NULL) {
        if (isset($data)) {
            $_SESSION[$index] = $data;
        } else {
            $_SESSION[$index] = $this->pushData($this->existingData[$index]);
        }
    }

    protected function pushData($index) {
        if (isset($this->existingData[$index])) {
            return $this->existingData[$index];
        } else {
            return NULL;
        }
    }

    protected function registerIndex($index) {
        if (!isset($this->existingIndexes[$index])) {
            $this->existingIndexes[] = $index;
            $this->existingData[$index] = NULL;
        }
    }

    public function registerData($index, $data) {
        $this->registerIndex($index);
        $this->existingData[$index] = $data;
    }

    public function pushIndexes() {
        foreach ($this->existingData as $key => $index) {
            $this->pushIndex($key);
        }
    }

    public function getExistingIndexes() {
        $output = '<sub class="info">DEBUG: getting existing index data...</sub><ul class="info">';
        $indexes = array();
        foreach ($this->existingIndexes as $index) {
            if ($this->getExistingIndex($index) != FALSE) {
                $indexes[] = $this->getExistingIndex($index);
            }
        }
        foreach ($indexes as $index) {
            $output .= '<li>' . $index . '</li>';
        }
        $output .= '</ul>';
        return $output;
    }

    public function getExistingIndex($index) {
        if (isset($this->existingIndexes[$index])) {
            return $this->existingIndexes[$index];
        } else
            return FALSE;
    }

    public function getExistingData($index) {
        $checkindex = $this->getExistingIndex($index);
        if ($checkindex == FALSE) {
            return '<sub class="info">Index ' . $index . ' does not exist</sub>';
        } elseif (!isset($this->existingData[$index]) || is_null($this->existingData[$index])) {
            return '<sub class="info">Index ' . $index . ' exists but does not contain data</sub>';
        } else {
            $render = '<sub class="info">Existing data for index: ' . $index . '...</sub><ul class="info">';
            foreach ($this->existingData[$index] as $data) {
                if (is_string($data)) {
                    $render .= '<li>Data: ' . $data . '</li>';
                } elseif (is_array($data)) {
                    $render .= '<li>Data array: ' . key($data) . '<ul>';
                    foreach ($data as $key => $value) {
                        if (is_string($value)) {
                            $render .= '<li>Subdata for ' . $key . ': ' . $value . '</li>';
                        } elseif (is_array($value)) {
                            $render .= '<li>Subdata for array ' . $key . ': <hr><pre>';
                            $render .= print_r($value);
                            $render .= '</pre><hr></li>';
                        } elseif (is_object($value)) {
                            $render .= '<li>Subdata for object ' . $key . ': <hr><pre>';
                            $render .= var_dump($value);
                            $render .= '</pre><hr></li>';
                        }
                    }
                    $render .= '</ul></li>';
                } elseif (is_object($data)) {
                    $render .= '<li>Data object: ' . key($data) . '<hr><pre>';
                    $render .= var_dump($data);
                    $render .= '<hr></pre></li>';
                }
            }
            $render .='</ul>';
            return $render;
        }
    }

    public function set($key, $value) {
        $_SESSION[$key] = $value;
    }

    public function get($key) {
        if (isset($_SESSION[$key]))
            return $_SESSION[$key];
    }

    public function destroy() {
        $this->deleteSessionRecord(session_id());
        session_destroy();
        
    }
    
    protected function deleteSessionRecord($session) {
        $this->db->delete('sites_sessions', '`session_id`="' . $session . '"');
    }

    public function setBaseSessionVars() {
        if (!isset($_SESSION['user']['loggedIn']) && !isset($_SESSION['user']['login'])) {
            $_SESSION = array(
                'loggedIn' => FALSE,
                'user' => array(
                    'login' => 'visitor',
                    'group' => array(
                        'slug' => 'default',
                        'display' => 'Default User Group',
                        'parent' => NULL,
                        'site' => $_SERVER['HTTP_HOST'],
                        'hierarchy' => '0'
                    ),
                    'hierarchy' => '0',
                    'active' => '1',
                    'permissions' => array(
                        'group' => array(),
                        'individual' => array()
                    ),
                    'restrictions' => array(
                        'group' => array(),
                        'individual' => array()
                    ),
                    'settings' => array()
                ),
                'security' => array(
                ),
                'utilities' => array()
            );
            $this->perms->initUser('visitor');
            $this->perms->checkPerms($_SESSION['user']['login'], $_SESSION['user']['group']['slug']);
        }
    }

    function __destruct() {
        parent::__destruct();
    }

}
