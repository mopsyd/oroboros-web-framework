<?php

/**
 * This file handles the distribution and checking of user cookies. It is referenced
 * both by the core as well as the ajax handler.
 *
 * @author Your Name <brian@mopsyd.me>
 */

namespace oroboros\core\libs\User;

class Cookies {

    const DEFAULTTIMEOUT = 300;
    const DEFAULTIDLE = 250;

    public function __construct() {
        $this->init();
    }

    private function init() {
        
    }

    public function setCookie($name, $data = array(), $timeout = self::DEFAULTTIMEOUT, $dir = NULL, $subdomains = NULL) {
        setcookie($name, serialize($data));
    }

    public function getCookie($name) {
        if (isset($_COOKIE[$name])) {
            $data = unserialize($_COOKIE[$name]);
            $results = array();
            foreach ($data as $item) {
                if (isset($data[$item])) {
                    $results[] = $data[$item];
                }
                //var_dump($_COOKIE);
            }
            return $data;
        } else {
            return FALSE;
        }
    }

    public function updateCookie($name, $indexes = array(), $timeout = self::DEFAULTTIMEOUT) {
        
    }

    public function deleteCookie($name) {
        setcookie($name, '', time() - 3600);
    }

    public function checkCookie($name) {
        if (isset($_COOKIE[$name])) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function __destruct() {
        
    }

}

?>
