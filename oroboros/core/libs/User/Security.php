<?php

namespace oroboros\core\libs\User;

/**
 * Description of Security
 * This class insures that the page is being rendered for the correct viewer,
 * on the same device expected for their individual session.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Security extends \oroboros\core\libs\Abstracts\Site\Lib {

    public function __construct($package) {
        parent::__construct($package);
        $this->init();
    }

    protected function init() {
        $settings = new \oroboros\core\libs\Settings\Security();
        $this->settings = $settings->handleSettings();
    }

    public function checkSecurityCookie() {
        if (isset($_SESSION['security']['cookie']) && $_SESSION['security']['cookie'] == TRUE && !isset($_COOKIE['security'])) {
            //session hijacked, force logout
            $message = 'You have been logged out due to a potential security violation to retain your account integrity. Please log in again.';
            $this->securityLogout($message);
        } elseif (isset($_SESSION['security']['cookie']) && $_SESSION['security']['cookie'] == TRUE && !isset($_COOKIE['security'])) {
            //proceed to IV validation
            $this->checkSecurityIV();
        } elseif (!isset($_SESSION['security']['cookie']) && !isset($_COOKIE['security'])) {
            //user has not yet been issued a security cookie
            $this->setSecurityCookie();
        } elseif (!isset($_SESSION['security']['cookie']) && isset($_COOKIE['security'])) {
            //user may have tampered with their cookie settings, or session has timed out. Log questionable behavior if within timeout window, and perform additional checks. Log user out if checks fail.
        }
    }

    public function setSessionNonce() {
        $val = $this->hash->nonce();
        $_SESSION['security']['nonce'] = TRUE;
        return $val;
    }

    public function getSessionNonce() {
        if (isset($_SESSION['security']['nonce'])) {
            return $_SESSION['security']['nonce'];
        } else {
            return FALSE;
        }
    }
    
    public function validateSessionNonce($nonce) {
        $query = "SELECT `user_nonce` FROM `sites_sessions` WHERE `session_id`='" . session_id() . "' LIMIT 1;";
        $result = $this->db->select($query);
        if ($result[0]['user_nonce'] != $nonce) {
            return FALSE;
        } else return TRUE;
    }

    public function destroySessionNonce() {
        unset($_SESSION['user']['nonce']);
        unset($_SESSION['security']['nonce']);
    }

    public function setSecurityCookie() {
        $val = $this->hash->nonce();
        $this->cookie->setCookie('security', array($val), $this->time->now('raw') + (86400 * 30), $_SERVER['HTTP_HOST']);
        $_SESSION['security']['cookie'] = TRUE;
        return $val;
    }

    public function resetSecurityCookie() {
        $val = $this->hash->nonce();
        $this->cookie->setCookie('security', array($val), $this->time->now('raw') + (86400 * 30), $_SERVER['HTTP_HOST']);
        $_SESSION['security']['cookie'] = TRUE;
        return $val;
    }
    
    public function getSecurityCookie() {
        $result = $this->cookie->getCookie('security');
        if (isset($result[0])) {
            return $result[0];
        } else {
            return FALSE;
        }
    }
    
    public function validateSecurityCookie($nonce) {
        $query = "SELECT `device_nonce` FROM `sites_sessions` WHERE `session_id`='" . session_id() . "' LIMIT 1;";
        $result = $this->db->select($query);
        //var_dump($result[0]['device_nonce']); 
        if ($result[0]['device_nonce'] != $nonce) {
            return FALSE;
        } else return TRUE;
    }

    public function destroySecurityCookie() {
        $this->cookie->deleteCookie('security');
        unset($_SESSION['security']['cookie']);
    }

    public function securityLogout($message) {
        $_POST['server_message'] = $message;
        header('Location: ' . '/dashboard/logout');
        exit();
    }

    public function __destruct() {
        parent::__destruct();
    }

}