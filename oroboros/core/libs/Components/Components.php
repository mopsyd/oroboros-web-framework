<?php

/**
 * Description of Modules
 * This class handles the loading, validation, activation, and packaging of page components
 * for the Oroboros core system, as well as the registration of their dependencies.
 * 
 * @author Brian Dayhoff <brian@mopsyd.me>
 */

namespace oroboros\core\libs\Components;

class Components extends \oroboros\core\libs\Abstracts\Site\Lib {

    const BASEPATH = 'oroboros/components/';
    const CORE = 'core/';
    const LOCAL = 'local/';
    const STAGING = 'staging/';
    const THIRDPARTY = 'third-party/';

    public function __construct($package) {
        parent::__construct($package);
    }

    public function init() {
        $this->settings = new \oroboros\core\libs\Settings\Component($this->package);
        $this->settings->init($this->package);
    }

    public function get_components($site, $subdomain, $page) {
        $components = array();
        $components['site'] = $this->get_site_components($site);
        //$components['subdomain'] = $this->get_subdomain_components($site, $subdomain);
        $components['page'] = $this->get_page_components($site, $subdomain, $page);
        $list = array();
        foreach ($components as $index => $dir) {
            if (is_array($dir)) {
                foreach ($dir as $value) {
                    if (!in_array($value, $list)) {
                        $list[] = $value;
                    }
                }
            }
        }
        $final = array();
        foreach ($list as $component) {
            $item = $this->get_component_details($component);
            if ($item != FALSE) {
                $final[$item['slug']] = $item;
            }
        }
        foreach ($final as $index => $component) {
            $final[$index]['directory'] = _BASE . self::BASEPATH . constant('self::' . strtoupper($component['type'])) . $component['folder'] . '/';
            $final[$index]['settings'] = $this->get_component_settings($final[$index]);
        }
        $this->components = $final;
    }

    protected function get_site_components($site) {
        $query = "SELECT `component` FROM `sites_components` WHERE `site` = '" . $site . "' AND `permission` IN ('" . implode("','", $_SESSION['user']['permissions']) . "') AND `active`=1 ORDER BY `id`;";
        //echo 'Site Component Query: ' . $query . '<br>';
        $components = $this->db->select($query);
        if (!empty($components)) {
            $list = array();
            foreach ($components as $key => $index) {
                $list[] = $index['component'];
            }
            return $list;
        } else
            return FALSE;
    }

    protected function get_subdomain_components($site, $subdomain) {
        $query = "SELECT `component` FROM `sites_subdomains_components` WHERE `site` = '" . $site . "' AND `permission` IN ('" . implode("','", $_SESSION['user']['permissions']) . "') AND `active`=1 ORDER BY `id`;";
        //echo 'Subdomain Component Query: ' . $query . '<br>';
        $components = $this->db->select($query);
        if (!empty($components)) {
            $list = array();
            foreach ($components as $key => $index) {
                $list[] = $index['component'];
            }
            return $list;
        } else
            return FALSE;
    }

    protected function get_page_components($site, $subdomain, $page) {
        $query = "SELECT `component` FROM `pages_components` WHERE `site` = '" . $site . "' AND `subdomain` = '" . $subdomain . "' AND `page`='" . $page . "' AND `permission` IN ('" . implode("','", $_SESSION['user']['permissions']) . "') AND `active`=1 ORDER BY `id`;";
        //echo 'Page Component Query: ' . $query . '<br>';
        $components = $this->db->select($query);
        if (!empty($components)) {
            $list = array();
            foreach ($components as $key => $index) {
                $list[] = $index['component'];
            }
            return $list;
        } else
            return FALSE;
    }

    protected function get_component_details($component) {
        $query = "SELECT `slug`,`display`,`description`,`type`,`folder`,`settings` FROM `components` WHERE `slug`='" . $component . "' AND `active`=1 LIMIT 1;";
        $component = $this->db->select($query);
        if (!empty($component)) {
            return $component[0];
        } else
            return FALSE;
    }

    protected function get_component_settings($component) {
        $file = $component['directory'] . $component['settings'];
        //echo 'Settings file for ' . $component['display'] . ': ' . $file . '<br>';
        if (is_readable($file)) {
            $settings = $this->data->fetch_ini($file);
            return $settings;
        } else
            return FALSE;
    }

    public function load_component_template($component) {
        if (is_readable($component['directory'] . $component['settings'][$component['slug']]['template'])) {
            $ob = new \oroboros\core\libs\Render\Output($this->package);
            $ob->_initBuffer();
            require $component['directory'] . $component['settings'][$component['slug']]['template'];
            $render = $ob->cleanGet();
            return $render;
        } else {
            return FALSE;
        }
    }

    public function load_component_scripting($component) {
        if (is_readable($component['directory'] . $component['settings'][$component['slug']]['scripting'])) {
            return $component['directory'] . $component['settings'][$component['slug']]['scripting'];
        } else {
            return NULL;
        }
    }

    public function load_component_stylesheet($component) {
        if (is_readable($component['directory'] . $component['settings'][$component['slug']]['stylesheet'])) {
            $file = $component['directory'] . $component['settings'][$component['slug']]['stylesheet'];
            return $file;
        } else {
            return NULL;
        }
    }

    public function get_component_layout($component) {
        
    }

    public function enable_component($component, $site, $subdomain = NULL, $page = NULL) {
        
    }

    public function disable_component($component, $site, $subdomain = NULL, $page = NULL) {
        
    }

    public function validate_component($component) {
        
    }

    public function extract_component_package($package) {
        
    }

    public function create_component_package($package) {
        
    }

    public function fetch_remote_component($location, $package) {
        
    }

    public function push_remote_component($destination, $package) {
        
    }

    public function delete_component($component) {
        
    }

    public function archive_component($component) {
        
    }

    public function restore_component($component, $version) {
        
    }

    public function register_component($component) {
        
    }

    public function unregister_component($component) {
        
    }

    public function update_component($component) {
        
    }

    public function fetch_component_dependencies($component) {
        
    }

    public function get_component_conflicts($component, $target) {
        
    }

    public function __destruct() {
        parent::__destruct();
    }

}

?>
