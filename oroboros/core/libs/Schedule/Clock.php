<?php

/**
 * Description of _Clock
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */

namespace oroboros\core\libs\Schedule;

class Clock extends \DateTime {

    const STANDARDIZEDTIMEZONE = 'UTC';

    public $timezone = NULL;
    public $dateFormat = NULL;
    public $timeFormat = NULL;
    protected $days = array(
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday',
        'Sunday'
    );

    function __construct($timezone = NULL) {
        $this->init($timezone);
    }

    public function init($timezone = NULL) {
        if (is_null($timezone)) {
            $this->getSystemTimezone();
        }
    }

    public function now($format = NULL, $separator = NULL, $timezone = NULL) {
        if (!isset($this->timezone)) {
            $this->getSystemTimezone();
        }
        $clock = new \DateTime('now', new \DateTimeZone($this->timezone));
        $time = $clock->getTimestamp();
        unset($clock);
        switch ($format) {
            case 'clock':
                //returns a digital clock format time
                return $this->readable($time, $separator);
                break;
            case 'timer':
                //returns a digital clock format time with milliseconds
                return $this->timerFormat($time, $separator);
                break;
            case 'database':
                //returns a database timestamp format
                return $this->databaseTimestamp($time);
                break;
            case 'military':
                //returns the time in military format
                return $this->militarytime($time, $separator);
                break;
            case 'raw':
                //returns the raw UNIX based timestamp format
                return $time;
                break;
            default:
                //returns the default time display expressed as an array to be
                //manipulated by another object
                $result = $this->fulltime($time, '-', '-');
                $result = explode(' ', $result);
                $result[0] = explode('-', $result[0]);
                $result[1] = explode('-', $result[1]);
                $string['date'] = array(
                    'year' => $result[0][0],
                    'month' => $result[0][1],
                    'day' => $result[0][2]
                );
                $string['time'] = array(
                    'hour' => $result[1][0],
                    'minute' => $result[1][1],
                    'second' => $result[1][2]
                );
                return $string;
                break;
        }
    }

    public function today($mode, $verbose = FALSE, $separator = NULL, $timezone = NULL) {
        if (!isset($this->timezone)) {
            $this->getSystemTimezone();
        }
        if (!isset($separator)) {
            $separator = '/';
        }
        $clock = new \DateTime('now', new \DateTimeZone($this->timezone));
        $time = $clock->getTimestamp();
        switch ($mode) {
            case 'day':
                //returns the day of the month
                if ($verbose == TRUE) {
                    return $this->verboseDay($time);
                } else {
                    $format = 'd\t\h';
                    return $this->format_time($time, $format);
                }
                break;
            case 'week':
                //returns which week of the month it is
                return 'incomplete method';
                break;
            case 'month':
                //returns the current month
                if ($verbose == TRUE) {
                    $format = 'F';
                    return $this->format_time($time, $format);
                } else {
                    $format = 'm';
                    return $this->format_time($time, $format);
                }
                break;
            case 'year':
                //returns the current year
                $format = 'Y';
                return $this->format_time($time, $format);
                break;
            case 'dayyear':
                //returns the current day of the year and year
                if ($verbose == TRUE) {
                    $format = '';
                    return $this->format_time($time, $format);
                } else {
                    $format = '';
                    return $this->format_time($time, $format);
                }
                break;
            case 'daymonth':
                //returns the current day of the month and month
                if ($verbose == TRUE) {
                    $format = 'N\t\h \d\a\y \o\f F';
                    return $this->format_time($time, $format);
                } else {
                    $format = 'N\t\h \o\f \m\o\n\t\h\: m';
                    return $this->format_time($time, $format);
                }
                break;
            case 'dayweek':
                //returns the current day of the week and week of the month
                return 'incomplete method';
                break;
            case 'weekmonth':
                //returns the current week of the month and month
                return 'incomplete method';
                break;
            case 'weekyear':
                //returns the current week of the year and year
                $format = '\w\e\e\k \#W \o\f Y';
                return $this->format_time($time, $format);
                break;
            case 'monthyear':
                //returns the current month and year
                $format = 'm\t\h \m\o\n\t\h \o\f Y';
                return ltrim($this->format_time($time, $format), '0');
                break;
            case 'full':
                //returns the full date string
                if ($verbose == TRUE) {
                    $format = '';
                    $result = $this->verboseDay($time) . ', ';
                    $result .= $this->format_time($time, 'F') . ' ' . $this->format_time($time, 'd') . 'th, ' . $this->format_time($time, 'Y');
                    return $result;
                } else {
                    $format = 'Y' . $separator . 'm' . $separator . 'd';
                    return $this->format_time($time, $format);
                }
                break;
        }
    }

    protected function verboseDay($time) {
        $num = $this->format_time($time, 'N');
        return $this->days[$num - 1];
    }

    public function usaDate($time = NULL, $separator = '/') {
        if (!isset($time)) {
            if (!isset($this->timezone)) {
                $this->getSystemTimezone();
            }
            $clock = new \DateTime('now', new \DateTimeZone($this->timezone));
            $time = $clock->getTimestamp();
        }
        $format = 'm' . $separator . 'd' . $separator . 'Y';
        return $this->format_time($time, $format);
    }

    public function html5Time($time = NULL) {
        if (!isset($time)) {
            return '<time datetime="' . date('c') . '">' . date('Y - m - d') . '</time>';
        } else {
            return '<time datetime="' . $this->format_time($time, 'c') . '">' . $this->format_time($time, 'Y - m - d') . '</time>';
        }
    }

    public function timezonediff($time, $timezone, $targettimezone) {
        
    }

    protected function getSystemTimezone() {
        $this->timezone = date_default_timezone_get();
    }

    protected function standardizeTimezone() {
        date_default_timezone_set(self::STANDARDIZEDTIMEZONE);
    }

    protected function setSystemTimezone($standard = TRUE) {
        if ($standard != TRUE) {
            $this->getSystemTimezone();
            date_default_timezone_set($this->timezone);
        } else {
            $this->getSystemTimezone();
            $this->standardizeTimezone();
            $this->timezoneflag = TRUE;
        }
    }

    public function datediff($start, $end) {
        
    }

    public function timediff($start, $end) {
        
    }

    public function shortdate($time, $separator = NULL) {
        if (!isset($separator)) {
            $separator = '/';
        }
        $format = 'Y' . $separator . 'm' . $separator . 'd';
        return $this->format_time($time, $format);
    }

    public function fulldate($time) {
        $format = "Y-m-d H:i:s";
        return $this->format_time($time, $format);
    }

    public function fulltime($time, $separator = NULL, $dateseparator = NULL) {
        if (!isset($separator)) {
            $separator = ':';
        }
        if (!isset($dateseparator)) {
            $dateseparator = '/';
        }
        $format = 'Y' . $dateseparator . 'm' . $dateseparator . 'd H' . $separator . 'i' . $separator . 's';
        return $this->format_time($time, $format);
    }

    public function militarytime($time, $separator = NULL) {
        if (!isset($separator)) {
            $separator = ':';
        }
        $format = 'H' . $separator . 'i';
        return $this->format_time($time, $format);
    }

    public function databaseTimestamp($time) {
        $format = "Y-m-d H:i:s";
        return $this->format_time($time, $format);
    }

    public function readable($time, $separator = NULL) {
        if (!isset($separator)) {
            $separator = ':';
        }
        $pmcheck = $this->format_time($time, 'H');
        if ($pmcheck > 12) {
            $hour = $pmcheck - 12;
            $min = $this->format_time($time, 'i');
            return $hour . $separator . $min . ' PM';
        } else {
            $format = 'H' . $separator . 'i A';
            return $this->format_time($time, $format);
        }
    }

    public function timerFormat($time, $separator = NULL) {
        if (!isset($separator)) {
            $separator = ':';
        }
        $format = 'H' . $separator . 'i' . $separator . 's';
        return $this->format_time($time, $format);
    }

    protected function format_time($time, $format) {
        $result = date($format, $time);
        return $result;
    }

    public function difference($time, $comparison) {
        
    }

    public function countyears($time, $leapyear=FALSE) {
        $days = 365;
        if (isset($leapyear)) {
            $days++;
        }
        return $this->countdays($time) / $days;
    }

    public function countquarters($time) {
        return $this->countyears($time) * 4;
    }

    public function countmonths($time) {
        return $this->countyears($time) * 12;
    }

    public function countweeks($time) {
        return $this->countdays($time) / 7;
    }

    public function countdays($time) {
        return $this->counthours($time) / 24;
    }
    
    public function counthours($time) {
        return $this->countminutes($time) / 60;
    }

    public function countminutes($time) {
        return $time / 60;
    }

    public function countseconds($time) {
        return $time;
    }

    public function timer($time, $duration) {
        
    }

    function __destruct() {
        ;
    }

}

?>
