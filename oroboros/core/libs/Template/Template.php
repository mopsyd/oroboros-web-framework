<?php

/**
 * Description of _Template
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */

namespace oroboros\core\libs\Template;

class Template extends \oroboros\core\libs\Abstracts\Site\Lib {

    const DEFAULTS = '/oroboros/templates/DEFAULTS/';

    function __construct($package = NULL) {
        parent::__construct($package);
    }

    public function init() {
        $this->settings = new \oroboros\core\libs\Settings\Template();
        $this->settings->init();
    }

    public function initPageTemplate() {
        $template = $this->get_template($this->_page['slug'], $this->_site['slug'], $this->_subdomain);
        if ($template != FALSE) {
            $this->get_template_details($template);
        }
    }

    public function get_template($page, $site, $subdomain) {
        $query = "SELECT `template`,`template_file`,`parent` FROM `pages` WHERE `slug`='" . $page . "' AND `site`='" . $site . "' AND `subdomain`='" . $subdomain . "' AND `active` = 1 LIMIT 1;";
        $result = $this->db->select($query);
        if (!empty($result)) {
            if ($result[0]['template'] == 'INHERIT') {
                //inherit template details from page parent
                if ($result[0]['parent'] != NULL) {
                    return $this->get_template($result[0]['slug'], $site, $subdomain);
                } else {
                    //misconfigured database record, throw error, if debug is enabled, also alert user
                    echo '<sub class="error">Misconfigured template entry in database for page: ' . $page . '</sub>';
                    return FALSE;
                }
            } elseif ($result[0]['template'] == 'RAW') {
                //do not render template
                return FALSE;
            } else
                return $result[0];
        } else
        //no template to return
            return FALSE;
    }

    public function get_template_details($template) {
        $this->template = array(
            'template' => $template['template'],
            'template_file' => $template['template_file']
        );
        $this->initTemplate();
    }

    public function get_template_dependencies($template) {
        
    }

    public function get_template_stylesheet($template) {
        
    }

    public function get_template_scripts($template) {
        
    }

    public function get_parent_template($template) {
        
    }

    public function package_template_settings($template) {
        
    }

    public function package_template_layout($template) {
        
    }

    public function package_template_filebase($template) {
        
    }

    public function package_template($template) {
        
    }

    public function archive_template($template) {
        
    }

    public function restore_template($template) {
        
    }

    public function fetch_remote_template($template) {
        
    }

    public function validate_template($template) {
        
    }

    public function register_template($template) {
        
    }

    public function disable_template($template) {
        
    }

    public function delete_template($template) {
        
    }

    public function clone_template($template) {
        
    }

    public function initTemplate() {
        $check = $this->fetchTemplateSettings();
        $this->setTemplateDefaults();
        if ($check == TRUE) {
            //determine what to load based on settings file
            $this->setTemplateAssets(array('head', 'header', 'body', 'content', 'content-close', 'footer'));
        } else {
            //handle missing settings
            echo 'Template settings are missing<br>';
        }
    }

    protected function setTemplateDefaults() {
        $files = array('head', 'header', 'body', 'content', 'footer');
        foreach ($files as $file) {
            if (is_readable(_BASE . self::DEFAULTS . $file . '.phtml')) {
                $this->template[$file] = _BASE . self::DEFAULTS . $file . '.phtml';
            }
        }
    }

    protected function fetchTemplateSettings() {
        $dir = _TEMPLATE . $this->template['template'] . '/';
        if (file_exists($dir . 'settings.ini')) {
            $file = $dir . 'settings.ini';
        } else {
            $file = _BASE . self::DEFAULTS . 'settings.ini';
        }
        $ini = new \oroboros\core\libs\Dataflow\Dataflow();
        $this->template['settings'] = $ini->fetch_ini($file);
        $config = $this->handleTemplateConfig($this->template['settings']['config']);
        $this->template['settings'] = $this->template['settings']['config'];
        var_dump($this->template['settings']);
        return true;
    }
    
    protected function handleTemplateConfig($config) {
        $custom = array();
        if (isset($config['customHeader'])) {
            
        }
        if (isset($config['customFooter'])) {
            
        }
        if (isset($config['customBody'])) {
            
        }
        if (empty($custom)) {
            return FALSE;
        } else {
            return $custom;
        }

    }

    public function setTemplateAssets($assets) {
        $dir = _TEMPLATE . $this->template['template'] . '/' . $this->template['template_file'] . '/';
        if (is_readable($dir) && is_dir($dir)) {
            foreach ($assets as $asset) {
                if (isset($this->template['settings'][$asset]) && $this->template['settings'][$asset] != FALSE) {
                    $this->setTemplateAsset($dir, $asset);
                }
            }
        } else {
            /**
             * DEFAULT: 404 the page, log missing template, notify webmaster of missing asset
             * OR
             * IF DEFINED BY WEBMASTER: fall back on default template (do not default to this,
             * missing hooks may screw up content render and throw errors
             */
        }
    }

    protected function setTemplateAsset($dir, $type) {
        if (isset($this->template['settings']['usePage' . ucfirst($type)]) && is_readable($dir . $type . '.phtml')) {
            $this->template[$type] = $dir . $type . '.phtml';
        } else {
            //echo 'rendering template default ' . $type . '<br>';
        }
    }

    public function finalizeTemplate() {
        $_SESSION['template'] = $this->template;
        return $this->template;
    }

    function __destruct() {
        parent::__destruct();
    }

}

?>
