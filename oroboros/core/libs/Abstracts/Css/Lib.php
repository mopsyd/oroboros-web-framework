<?php

/**
 * Description of _Lib
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */

namespace oroboros\core\libs\Abstracts\Css;

abstract class Lib {

    const AUTOLOAD_FILE = '/libs/Autoload/Autoload.php';
    const AUTOLOAD_REGISTER = '/libs/';

    protected $package = NULL;

    public function __construct($package = NULL) {
        $this->init($package);
    }

    private function init($package) {
        if (!is_null($package)) {
            $this->initPackage($package);
        }
        if (!isset($this->data)) {
            $this->data = new \oroboros\core\libs\Dataflow\Dataflow($this->package);
            $this->package['core']['data'] = $this->data;
        }
        if (!isset($this->db)) {
            $this->db = new \oroboros\core\libs\Database\Database();
            $this->package['core']['database'] = $this->db;
        }
        if (!isset($this->dependencies)) {
            $this->dependencies = new \oroboros\core\libs\Filebase\Dependencies($this->db, $this->data);
            $this->package['core']['dependencies'] = $this->dependencies;
        }
        if (!isset($this->autoload)) {
            $this->autoload = new \oroboros\core\libs\Autoload\Autoload();
            $this->autoload->register();
            $this->autoload->initDirs();
            $this->package['core']['autoload'] = $this->autoload;
        }
        if (!isset($this->settings['core']) || empty($this->settings['core'])) {
            $settings = new \oroboros\core\libs\Settings\Core($this->db);
            $this->settings['core'] = $settings->handleSettings();
            $this->package['core']['settings']['core'] = $this->settings['core'];
        }
        if (!isset($this->settings['security']) || empty($this->settings['security'])) {
            $settings = new \oroboros\core\libs\Settings\Security();
            $this->settings['security'] = $settings->handleSettings();
            $this->package['core']['settings']['security'] = $this->settings['security'];
        }
        if (!isset($this->settings['debug']) || empty($this->settings['debug'])) {
            $settings = new \oroboros\core\libs\Settings\Debug();
            $this->settings['debug'] = $settings->handleSettings();
            $this->package['core']['settings']['debug'] = $this->settings['debug'];
        }
        if (!isset($this->archive)) {
            $this->archive = new \oroboros\core\libs\Filebase\Archive($this->package);
            $this->package['core']['archive'] = $this->archive;
        }
        if (!isset($this->connection)) {
            $this->connection = new \oroboros\core\libs\Connection\Connection($this->package);
            $this->package['core']['connection'] = $this->connection;
        }
        if (!isset($this->time)) {
            $this->time = new \oroboros\core\libs\Schedule\Clock($this->package);
            $this->package['core']['time'] = $this->time;
        }
        if (!isset($this->perms)) {
            $this->perms = new \oroboros\core\libs\User\Permissions($this->db);
            $this->package['core']['perms'] = $this->perms;
        }
        if (!isset($this->preg)) {
            $this->preg = new \oroboros\core\libs\Pcre\Pcre($this->package);
            $this->package['core']['preg'] = $this->preg;
        }
        if (!isset($this->hash)) {
            $this->hash = new \oroboros\core\libs\Encryption\Hash($this->settings['security']);
            $this->package['core']['hash'] = $this->hash;
        }
        if (!isset($this->cookie)) {
            $this->cookie = new \oroboros\core\libs\User\Cookies();
            $this->package['core']['cookie'] = $this->cookie;
        }

        //load landing specific dependencies
        switch (_DOC) {
            case 'site':
                //load CMS dependencies
                if (!isset($this->browser)) {
                    $this->browser = new \oroboros\core\libs\User\Browser($this->package);
                    $this->package['core']['browser'] = $this->browser;
                }
                if (!isset($this->device)) {
                    $this->device = new \oroboros\core\libs\User\Device($this->package);
                    $this->package['core']['device'] = $this->device;
                }
                if (!array_key_exists('oroboros\\core\\libs\SITE\\', $this->autoload->prefixes)) {
                    $this->autoload->addNamespace('oroboros\\core\\libs\\SITE', '/oroboros/core/libs/SITE');
                    $this->package['core']['autoload'] = $this->autoload;
                }
                
                break;
            case 'css':
                //loads stylesheet engine dependencies
                //$settings = new \oroboros\core\libs\Settings\Css();
                //$this->settings['stylesheet'] = $settings->handleSettings();
                if (!isset($this->settings['css']) || empty($this->settings['css'])) {
                    $settings = new \oroboros\core\libs\Settings\Css();
                    $this->settings['css'] = $settings->handleSettings();
                    $this->package['core']['settings']['css'] = $this->settings['css'];
                }
                break;
            case 'js':
                //loads javascript engine dependencies
                //$settings = new \oroboros\core\libs\Settings\Js();
                //$this->settings['javascript'] = $settings->handleSettings();
                if (!isset($this->settings['javascript']) || empty($this->settings['javascript'])) {
                    $settings = new \oroboros\core\libs\Settings\Js();
                    $this->settings['javascript'] = $settings->handleSettings();
                    $this->package['core']['settings']['javascript'] = $this->settings['javascript'];
                }
                break;
            case 'ajax':
                //loads ajax engine dependencies
                
                break;
            case 'robots':
                //loads robots.txt engine dependencies

                break;
            case 'sitemap':
                //loads sitemap engine dependencies

                break;
            case 'rss':
                //loads rss feed engine dependencies

                break;
            case 'email':
                //loads email engine dependencies

                break;
            case 'cron':
                //loads scheduler engine dependencies
                break;
            case 'framework':
                //loads web framework dependencies
                if (!array_key_exists('oroboros\core\libs\FRAMEWORK\\', $this->autoload->prefixes)) {
                    $this->autoload->addNamespace('oroboros\core\libs\FRAMEWORK', '/oroboros/core/libs/FRAMEWORK');
                }
                break;
            case 'wrapper':
                //loads subsystem wrapper dependencies
                if (!array_key_exists('oroboros\core\libs\WRAPPER\\', $this->autoload->prefixes)) {
                    $this->autoload->addNamespace('oroboros\core\libs\WRAPPER', '/oroboros/core/libs/WRAPPER');
                }
                break;
        }
    }

    private function initPackage($package) {
        //initializes the package data if present
        $this->package = $package;
        if (isset($this->package['core'])) {
            $this->loadCorePackage($this->package['core']);
        }
        if (isset($this->package['site'])) {
            $this->loadSitePackage($this->package['site']);
        }
        if (isset($this->package['subdomain'])) {
            $this->loadSubdomainPackage($this->package['subdomain']);
        }
        if (isset($this->package['page'])) {
            $this->loadPagePackage($this->package['page']);
        }
        if (isset($this->package['modules'])) {
            $this->loadModulePackage($this->package['modules']);
        }
        if (isset($this->package['file'])) {
            $this->loadFilePackage($this->package['file']);
        }
        if (isset($this->package['user'])) {
            $this->loadUserPackage($this->package['user']);
        }
        if (!isset($this->_site) && isset($package['site'])) {
            $this->_site = $package['site'];
        }
        if (!isset($this->_subdomain) && isset($package['subdomain'])) {
            $this->_subdomain = $package['subdomain'];
        }
        if (!isset($this->_page) && isset($package['page'])) {
            $this->_page = $package['page'];
        }
        if (!isset($this->user) && isset($package['user'])) {
            $this->user = $package['user'];
        }
    }

    private function buildPackage($package) {
        
    }

    private function loadCorePackage($package) {
        if (isset($package['classes']['libs']['controller'])) {
            $this->_controller = $package['classes']['libs']['controller'];
            if (isset($this->_controller['data'])) {
                $this->data = $this->_controller['data'];
                $this->package['data'] = $this->data;
            }
            if (isset($this->_controller['archive'])) {
                $this->archive = $this->_controller['archive'];
                $this->package['archive'] = $this->archive;
            }
            if (isset($this->_controller['connection'])) {
                $this->connection = $this->_controller['connection'];
                $this->package['connection'] = $this->connection;
            }
            if (isset($this->_controller['time'])) {
                $this->time = $this->_controller['time'];
                $this->package['time'] = $this->time;
            }
            if (isset($this->_controller['preg'])) {
                $this->preg = $this->_controller['preg'];
                $this->package['preg'] = $this->preg;
            }
            if (isset($this->_controller['browser'])) {
                $this->browser = $this->_controller['browser'];
                $this->package['browser'] = $this->browser;
            }
        }
        if (isset($package['classes']['libs']['controller']['adapters'])) {
            $this->_adapters = $package['classes']['libs']['controller']['adapters'];
        }
        if (isset($package['data'])) {
            $this->data = $package['data'];
            $this->package['data'] = $this->data;
        }
        if (isset($package['database'])) {
            $this->db = $package['database'];
            $this->package['database'] = $this->db;
        }
        if (isset($package['settings'])) {
            $this->settings = $package['settings'];
            $this->package['settings'] = $this->settings;
        }
        if (isset($package['dependencies'])) {
            $this->dependencies = $package['dependencies'];
            $this->package['dependencies'] = $this->dependencies;
        }
        if (isset($package['autoload'])) {
            $this->autoload = $package['autoload'];
            $this->package['autoload'] = $this->autoload;
        }
        if (isset($package['time'])) {
            $this->time = $package['time'];
            $this->package['time'] = $this->time;
        }
        if (isset($package['preg'])) {
            $this->preg = $package['preg'];
            $this->package['preg'] = $this->preg;
        }
        if (isset($package['archive'])) {
            $this->archive = $package['archive'];
            $this->package['archive'] = $this->archive;
        }
        if (isset($package['connections'])) {
            $this->connections = $package['connections'];
            $this->package['connections'] = $this->connections;
        }
        if (isset($package['hash'])) {
            $this->hash = $package['hash'];
            $this->package['hash'] = $this->hash;
        }
        if (isset($package['cookie'])) {
            $this->cookie = $package['cookie'];
            $this->package['cookie'] = $this->cookie;
        }
        if (isset($package['classes']['libs']['perms'])) {
            $this->perms = $package['classes']['libs']['perms'];
            $this->package['perms'] = $this->perms;
        }
        
    }

    public function loadModules($package) {
        if (!isset($this->modules)) {
            $this->modules = new \oroboros\core\libs\Modules\Modules($package);
        }
        switch (_DOC) {
            case 'site':
                $this->modules->get_modules($this->_site['slug'], $this->_subdomain, $this->_page['slug']);
                break;
            case 'css|js':
                $this->modules->get_modules($_SESSION['site']['slug'], $_SESSION['subdomain'], $_SESSION['page']['slug']);
                break;
        }
    }

    private function loadSitePackage($package) {
        $this->_site = $package;
    }

    private function loadFilePackage($package) {
        $this->file = $package;
    }

    private function loadSubdomainPackage($package) {
        $this->_subdomain = $package;
    }

    private function loadPagePackage($package) {
        $this->_page = $package;
    }

    private function loadModulePackage($package) {
        $this->_modules = $package;
    }

    private function loadUserPackage($package) {
        $this->_user = $package;
    }

    public function _initDatabase($database = 'core') {
        $this->db = new \oroboros\core\libs\Database\Database($database);
    }

    public function _closeDatabase() {
        $this->db = NULL;
    }

    public function _reflect($class) {
        $reflect = new \ReflectionClass($class);
        return $reflect;
    }

    public function _classConstant($class, $constant) {
        $class = $this->_reflect($class);
        $constant = $class->constant($constant);
        unset($class);
        return $constant;
    }

    protected function _class() {
        return get_called_class();
    }

    public function __destruct() {
        ;
    }

}

?>
