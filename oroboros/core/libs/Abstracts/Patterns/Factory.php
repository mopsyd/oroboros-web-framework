<?php

namespace oroboros\core\libs\Abstracts\Patterns;

/**
 * Abstract factory pattern for loading and instantiating the correct classes
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
abstract class Factory {

    private $location = NULL;
    private $class = NULL;
    private $namespace = NULL;
    private $params = NULL;
    private $file = NULL;
    private $object = NULL;
    protected static $autoload = NULL;

    /**
     * Constructs the object, returns false if the class file could not be found, otherwise returns the instantiated object
     * 
     * @param type $location
     * @param type $class
     * @param type $params
     * @return type
     */
    public function __construct($location, $class, $params = NULL, $noRequire = FALSE) {
        self::$autoload = isset(self::$autoload) ? self::$autoload : $this->registerAutoloader();
        $this->file = $location . '/' . $class . '.php';
        $this->namespace = '\\' . str_replace(DIRECTORY_SEPARATOR, '\\', str_replace(_BASE, NULL, $location)) . '\\';
        self::$autoload->addNamespace($this->namespace, $location);
        $this->class = $this->namespace . $class;
        return $this->checkFile() == TRUE ? $this->instantiateClass($params, $noRequire) : FALSE;
    }
    /**
     * Registers the class autoloader
     */
    private function registerAutoloader() {
        $autoloader = new \oroboros\core\libs\Autoload\Autoload();
        self::$autoload = $autoloader;
        self::$autoload->register();
        return self::$autoload;
    }

    /**
     * instantiates the class
     */
    protected function instantiateClass($params, $noRequire = FALSE) {
        if (!is_null($params)) {
            $this->params = $this->setParams($params);
            $rc = new \ReflectionClass($this->class);
            $this->object = $rc->newInstanceArgs($this->params);
        } else {
            $this->object = new $this->class();
        }
        return $this->returnObject();
    }

    /**
     * Sets the correct parameters for class instantiation
     * 
     * @param type $params
     * @return null
     */
    protected function setParams($params) {
        if (is_array($params)) {
            $this->params = array();
            foreach ($params as $key => $value) {
                $this->params[$key] = $value;
            }
        } elseif (is_string($params) || is_integer($params)) {
            $this->params = array($params);
        } else {
            return NULL;
        }

        return $this->params;
    }

    /**
     * Checks whether the class file exists or not
     * 
     * @return boolean
     */
    protected function checkFile() {
        if (file_exists($this->file)) {
            return TRUE;
        } else
            return FALSE;
    }

    /**
     * Returns the completed object
     * 
     * @return object
     */
    protected function returnObject() {
        return $this->object;
    }

    public function __destruct() {
        
    }

}