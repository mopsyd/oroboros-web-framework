<?php

namespace oroboros\core\libs\Abstracts\Site\View;

abstract class View extends \oroboros\core\libs\Abstracts\Site\Lib {

    const AUTOLOAD_FILE = '/libs/Autoload/Autoload.php';
    const AUTOLOAD_REGISTER = '/views/';

    protected $session = NULL;

    function __construct($package) {
        parent::__construct($package);
    }
    
    

    public function addController($controller) {
        $this->controller = $controller;
        $this->package['controller'] = $this->controller;
    }

    protected function setDevice($device) {
        $this->device = $device;
    }

    protected function initScripting() {
        
    }

    protected function initFramework() {
        
    }

    protected function initWrapper() {
        
    }

    protected function initEmail() {
        
    }

    protected function initRss() {
        
    }

    protected function initAjax() {
        
    }

    public function buildPageDependencies() {

    }

    public function buildPageContent() {
        
    }

    protected function loadDefaultLayoutAssets($name) {

    }

    protected function handleModules() {
        
    }

    protected function handleComponents() {
        
    }

    protected function handleTemplate() {
        
    }

    protected function handleTheme() {
        
    }

    public function __destruct() {
        parent::__destruct();
    }

}