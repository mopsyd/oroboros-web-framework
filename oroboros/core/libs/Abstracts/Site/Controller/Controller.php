<?php

namespace oroboros\core\libs\Abstracts\Site\Controller;

abstract class Controller extends \oroboros\core\libs\Abstracts\Site\Lib implements \oroboros\core\interfaces\controller\Controller {

    const AUTOLOAD_FILE = '/libs/Autoload/Autoload.php';
    const AUTOLOAD_REGISTER = '/controllers/';

    function __construct($package) {
        parent::__construct($package);
        $this->init($package);
    }

    private function init() {
        if (!isset($this->file)) {
            $this->file = new \oroboros\core\libs\Filebase\Filebase($this->package);
            $this->package['file'] = $this->file;
        }
        $this->defineUser();
        $this->defineView();
        $this->initSettings();
    }

    private function initSettings() {
        if (!isset($_SESSION['stylesheets'])) {
            $_SESSION['stylesheets'] = array(
                'dependencies' => array(),
                'template' => array(),
                'theme' => array(),
                'modules' => array(),
                'components' => array(),
                'site' => array(),
                'subdomain' => array(),
                'subsection' => array(),
                'page' => array()
            );
        }
        if (!isset($_SESSION['scripts'])) {
            $_SESSION['scripts'] = array(
                'dependencies' => array(),
                'template' => array(),
                'theme' => array(),
                'modules' => array(),
                'components' => array(),
                'site' => array(),
                'subdomain' => array(),
                'subsection' => array(),
                'page' => array()
            );
        }
    }

    /**
     * 
     * @param string $name Name of the model
     * @param string $path Location of the models
     */
    public function loadModel($package, $name, $modelPath = 'models/') {

        if (is_readable(_LOCAL . $modelPath . $name . '_model.php')) {
            $modelPath = _LOCAL . $modelPath;
        } else {
            $path = _CORE . $modelPath;
        }
        $path = $modelPath . $name . '_model.php';
        if (is_readable($path)) {
            //echo 'Model found at: ' .$modelPath . $name.'_model.php'. '<br>';
            require $modelPath . $name . '_model.php';

            $modelName = $name . '_Model';
            $this->model = new $modelName($package);
        } else {
            echo 'Model not found at: ' . $modelPath . $name . '_model.php' . '<br>';
        }
    }

    protected function render() {
        $this->view->setControllerContent($this->model->controllerContent);
        $this->view->initBuffer();
        $this->view->initPage($_SESSION['device']);
        $this->view->renderPage();
        $this->view->renderTemplate($data);
        $this->view->finalizePage();
    }

    public function initCoreDatabase($database) {
        $this->db = $database;
    }

    public function defineModel($model) {
        $this->model = $model;
    }

    private function defineUser() {
        $site = (_DOC == 'site') ? $this->_site['slug'] : isset($_SESSION['site']) ? $_SESSION['site'] : NULL;
        $this->user = new \oroboros\core\libs\User\User($this->package);
        if (isset($site)) {
            if ($site == 'global') {
                $this->user->checkUser();
            }
            $this->user->getUserSettings($_SESSION['user']['login'], $site);
        }
    }

    private function defineView() {
        //require _BASE . 'oroboros/core/views/' . ucfirst(_DOC) . '/' . ucfirst($this->device['mode']) . '.php';
        $this->autoload->addNamespace('oroboros\core\views\\' . ucfirst(_DOC), '/oroboros/core/views/' . ucfirst(_DOC));
        $view = '\\oroboros\\core\\views\\' . ucfirst(_DOC) . '\\View';
        $this->view = new $view($this->package);
        if (_DOC == 'site') {
            $this->view->device = $this->device;
            $this->view->browser = $this->browser;
        }
        $this->view->user = $this->user;
        $this->view->registerBaselineAssets();
    }

    private function getSiteSubsection() {
        $query = "SELECT `site_section` FROM `pages` WHERE `site`='" . $this->_site['slug'] . "' AND `site_section` IN ('" . str_replace('/', "','", trim($this->_page['request'], '/')) . "') ORDER BY `site_section` ASC;";
    }

    function __destruct() {
        parent::__destruct();
    }

}