<?php

namespace oroboros\core\libs\Abstracts\Site\Model;

abstract class Model extends \oroboros\core\libs\Abstracts\Site\Lib implements \oroboros\core\interfaces\model\Model {

    const AUTOLOAD_FILE = '/libs/Autoload/Autoload.php';
    const AUTOLOAD_REGISTER = '/models/';

    public $pagedescription = NULL;
    public $subdomain = 'default';
    public $layoutname = NULL;
    public $sitetheme = NULL;
    public $pagetheme = NULL;
    public $themesettings = NULL;
    public $template = NULL;
    public $templatesettings = NULL;
    public $display = NULL;
    public $controllerContent = NULL;
    protected $con = array();

    public function __construct($package = NULL, $error = NULL) {
        parent::__construct($package);
        $this->loadModules($this->package);
        $this->init($error);
    }

    private function init($error) {
        switch (_DOC) {
            case 'site':
                $this->site = isset($this->package['site']) ? $this->package['site'] : NULL;
                $this->page = isset($this->package['page']) ? $this->package['page'] : NULL;
                $this->subdomain = isset($this->package['subdomain']) ? $this->package['subdomain'] : NULL;
                if (!isset($this->file)) {
                    $this->file = new \oroboros\core\libs\Filebase\Filebase($this->package);
                    $this->package['core']['file'] = $this->file;
                }
                break;
            case 'js|css|ajax':
                $this->site = isset($_SESSION['site']) ? $_SESSION['site'] : NULL;
                $this->page = isset($_SESSION['page']) ? $_SESSION['page'] : NULL;
                $this->subdomain = isset($_SESSION['subdomain']) ? $_SESSION['subdomain'] : NULL;
                break;
        }
        $this->file = new \oroboros\core\libs\Filebase\Filebase($this->package);
        $this->form = new \oroboros\core\libs\Form\Form($this->package);
        $this->hash = new \oroboros\core\libs\Encryption\Hash($this->package);
        $this->debug = new \oroboros\core\libs\Debug\Debug($this->package);
        $this->cookie = new \oroboros\core\libs\User\Cookies($this->package);
        $this->user = new \oroboros\core\libs\User\User($this->package);
        $this->getSiteDetails();
        //$this->registerBaselineAssets();
    }
    
    private function registerBaselineAssets() {
        //$this->registerBaselineStylesheets();
        //$this->registerBaselineScripts();
    }
    
    private function registerBaselineStylesheets() {
        //$this->registerStylesheet('dependencies', _BASE . 'public/css/stylesheets/normalize.css');
        //$this->registerStylesheet('dependencies', _BASE . 'public/css/stylesheets/foundation.css');
        //$this->registerStylesheet('dependencies', _BASE . 'public/css/stylesheets/responsive.css');
    }
    
    private function registerBaselineScripts() {
        
    }
    
    protected function getSiteDetails() {
        switch (_DOC) {
            case 'site':
                if (!isset($_COOKIE['security'])) {
                    $message = 'Device authentication not detected, logging out to retain user integrity';
                    $this->user->resetUser();
                    $this->user->security->securityLogout($message);
                }
                break;
            case 'js|css':
                $theme = new \oroboros\core\libs\Theme\Theme($this->package);
                $this->pagetheme = $theme->get_theme_details($_SESSION['theme']);
                $template = new \oroboros\core\libs\Template\Template($this->package);
                $this->template = $template->get_template_details($_SESSION['template']);
                break;
        }
    }

    protected function getThemeData() {
        if (is_readable(_THEME . $this->_page['theme'] . '/settings.ini')) {
            $file = $this->data->fetch_ini(_THEME . $this->_page['theme'] . '/settings.ini');
            foreach ($file as $i => $ii) {
                $this->themesettings[$i] = $ii;
            }
        } else {
            if (!is_readable(_THEME . $this->_page['theme'] . '/settings.ini')) {
                echo 'The theme settings.ini file could not be found, please check file path<br>';
                echo 'Provided location: ' . _THEME . $this->_page['theme'] . '/settings.ini' . '<br>';
            } else {
                echo 'The theme settings.ini file is not readable, check directory permissions<br>';
            }
        }
    }

    protected function getPageTemplate($page) {

        $template = $this->db->select("SELECT `template` FROM `pages` WHERE `slug`='$page' AND `site`='" . $this->_site['slug'] . "' AND `subdomain`='$this->_subdomain' LIMIT 1;");

        if (!empty($template)) {
            $this->template = $this->_page['template'];
            if ($this->template == 'INHERIT') {
                $this->template = $this->getPageParent('template', $this->_page['slug']);
            } elseif ($this->template == 'RAW') {
                $this->template = NULL;
            }
        }
    }

    private function getPageParent($item, $page) {

        $result = $this->db->select("SELECT `$item`,`parent` FROM `pages` WHERE `slug`='$page' AND `site` IN ('" . $this->_site['slug'] . "', 'global') AND `subdomain`='$this->_subdomain' LIMIT 1;");

        if (isset($result[0][$item])) {
            if ($result[0][$item] == 'INHERIT') {
                return $this->getPageParent($item, $result[0]['parent']);
            } elseif ($result[0][$item] == 'RAW') {
                return NULL;
            } else {
                return $result[0][$item];
            }
        } else {
            return false;
        }
    }

    protected function getTemplateData() {
        if (is_readable(_TEMPLATE . $this->template . '/settings.ini')) {
            $file = $this->data->fetch_ini(_TEMPLATE . $this->template . '/settings.ini');
            foreach ($file as $i => $ii) {
                $this->templatesettings[$i] = $ii;
            }
        } else {
            if (!is_readable(_TEMPLATE . $this->template . '/settings.ini')) {
                echo 'The template settings.ini file could not be found, please check file path<br>';
                echo 'Provided location: ' . _TEMPLATE . $this->template . '/settings.ini' . '<br>';
            } else {
                echo 'The template settings.ini file is not readable, check directory permissions<br>';
            }
        }
    }

    public function setContent($page, $content = NULL) {
        //add a switch case to detect frontend/backend/etc here
        $data = array();

        $data['widgets'] = $this->setWidgets($page);
        if ($data['widgets'] == FALSE) {
            unset($data['widgets']);
        } else {
            //set widget content
        }
        if (isset($content)) {
            $data['content'] = $this->setRenderData($content);
        } else {
            //set dashmod content
        }
        $data['dashmods'] = $this->setDashmods($page);
        foreach ($data as $key => $value) {
            if (empty($data[$key])) {
                unset($data[$key]);
            }
        }
        if (!empty($data)) {
            $this->packageControllerContent($data);
        }
    }

    protected function packageControllerContent($data, $type) {
        if (!isset($this->controllerContent)) {
            $this->controllerContent = array();
        }
        foreach ($data as $sections) {
            foreach ($sections as $item) {
                $this->setControllerContentItem($item, $type);
            }
        }
    }

    protected function setWidgets($content = NULL) {
        //fetch widgets by page
        return FALSE;
    }

    protected function setRenderData($content = NULL) {
        //set the render data by page
        if (isset($content)) {
            //render the content
        } else
            return FALSE;
    }

    protected function setDashmods($content = NULL) {
        //set the dashmods by page
        return FALSE;
    }

    protected function setControllerContentItem($item, $type) {
        $this->controllerContent[] = array(
            'type' => $type,
            'content' => $item['content']
        );
    }

    function __destruct() {
        parent::__destruct();
    }

}
