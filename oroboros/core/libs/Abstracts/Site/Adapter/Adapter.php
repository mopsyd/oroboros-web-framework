<?php
/**
 * Description of Adapter
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Abstracts\Site\Adapter;

abstract class Adapter extends \oroboros\core\libs\Abstracts\Site\Lib implements \oroboros\core\interfaces\adapter\Adapter {
    
    const AUTOLOAD_FILE = '/libs/Autoload/Autoload.php';
    const AUTOLOAD_REGISTER = '/adapters/';
    
    function __construct($package=NULL) {
        parent::__construct($package);
        $this->init();
    }
    
    public function init() {

    }
    
    function __destruct() {
        parent::__destruct();
    }
}

?>
