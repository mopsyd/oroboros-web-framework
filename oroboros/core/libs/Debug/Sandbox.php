<?php
/**
 * Description of Sandbox
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Sandbox extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    public function __construct($package = NULL) {
        parent::__construct($package);
    }
    
    public function init() {
        
    }
    
    public function design($user) {
        
    }
    
    public function developer($user) {
        
    }
    
    public function author($user) {
        
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
