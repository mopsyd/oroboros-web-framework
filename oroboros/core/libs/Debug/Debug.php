<?php
/**
 * Description of Debug
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Debug;

class Debug extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    function __construct($package=NULL) {
        parent::__construct($package);
    }
    
    public function init($package) {
        $this->settings = new \oroboros\core\libs\Settings\Debug();
        $this->settings->init($package);
    }
    
    function __destruct() {
        parent::__destruct();
    }
}

?>
