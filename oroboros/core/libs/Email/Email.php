<?php
/**
 * Description of Email
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Email;

class Email extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    function __construct($package=NULL) {
        parent::__construct($package);
    }
    
    public function init() {
        
    }
    
    public function register_email($email, $user, $site) {
        
    }
    
    public function email_login($user, $email, $password) {
        
    }
    
    public function set_email_server($email, $details) {
        
    }
    
    public function get_system_mail($site) {
        
    }
    
    public function send_system_mail($site) {
        
    }
    
    public function send_mailing_list($email, $list) {
        
    }
    
    public function archive_mail($email, $user) {
        
    }
    
    public function get_email_template($email, $template) {
        
    }
    
    public function add_email($user, $site, $email) {
        
    }
    
    public function verify_email($user, $email) {
        
    }
    
    public function validate_email($email) {
        
    }
    
    public function join_mailing_list($email, $list) {
        
    }
    
    public function leave_mailing_list($email, $list) {
        
    }
    
    public function email_attach($email, $file) {
        
    }
    
    function __destruct() {
        parent::__destruct();
    }
}

?>
