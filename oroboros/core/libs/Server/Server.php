<?php

namespace oroboros\core\libs\Server;

/**
 * Description of Server
 * This file determines server settings for the local webserver, and returns information
 * to the requesting class. This is used by various classes to determine if specific
 * functionality can be implemented.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Server extends \oroboros\core\libs\Abstracts\Site\Lib {

    protected $environment = array();

    public function __construct($package) {
        parent::__construct($package);
    }

    private function init() {
        
    }

    public function checkServerType() {
        return $_SERVER['SERVER_SOFTWARE'];
    }

    public function checkServerSig() {
        $sig = $_SERVER['SERVER_SIGNATURE'];
        return isset($sig) && $sig != '' ? $sig : 'disabled';
    }

    public function checkLanguageAvailability($language) {
        
    }

    public function checkServerName() {
        return $_SERVER['SERVER_NAME'];
    }

    public function checkServerAddress() {
        return $_SERVER['SERVER_ADDR'];
    }

    public function checkServerAdmin() {
        return $_SERVER['SERVER_ADMIN'];
    }

    public function checkVersion($language) {
        
    }

    public function checkCgi() {
        return $_SERVER['GATEWAY_INTERFACE'];
    }

    public function checkHttp() {
        return $_SERVER['SERVER_PROTOCOL'];
    }

    public function checkGit() {
        
    }

    public function checkSvn() {
        
    }

    public function checkSsl() {
        if ((empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == 'off') || (($_SERVER['SERVER_SOFTWARE'] == 'nginx' && !isset($_SERVER['SERVER_PORT'])) || (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] != '443'))) {
            return 'not enabled';
        } else {
            return 'enabled';
        }
    }

    public function checkSsh() {
        
    }

    public function checkFtp() {
        
    }

    public function checkSftp() {
        
    }

    public function checkMailSettings() {
        
    }

    public function checkWebserverUser() {
        $user = shell_exec("whoami");
        return $user;
    }

    public function checkDatabaseUser() {
        
    }

    public function getIncludePath() {
        $x = explode(':', $_SERVER['PATH']);
        foreach ($x as $key => $path) {
            $x[$key] .= '/';
        }
        return $x;
    }

    public function getDocRoot() {
        return _BASE;
    }

    public function getPort($type = 'http', $data = NULL) {
        $port = NULL;
        switch ($type) {
            case 'http':
                //return the http protocol port
                $port = $_SERVER['SERVER_PORT'];
                break;
            case 'database':
                //return the sql port
                if (!isset($data)) {
                    return FALSE;
                } else {
                    if (method_exists($data, 'port')) {
                        return $data->port();
                    } else
                        return FALSE;
                }
                break;
            case 'ssh':
                //return the secure shell port, if ssl access exists

                break;
            case 'ftp':
                //return the file transfer protocol port, if it exists

                break;
            case 'telnet':
                //return the telnet port, if it exists

                break;
        }
        return $port;
    }

    public function __destruct() {
        parent::__destruct();
    }

}