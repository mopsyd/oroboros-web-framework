<?php
/**
 * Description of Robots
 *
 * @author Your Name <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Bootstrap;

class Robots extends \oroboros\core\libs\Abstracts\Site\Model\Model {
    
    const SCOPE = 'robots';
    
    public function __construct($package=NULL) {
        parent::__construct($package);
        $this->init();
    }
    
    public function init() {
        
    }
    
    function __destruct() {
        parent::__destruct();
    }
}

?>
