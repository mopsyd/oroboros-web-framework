<?php
/**
 * Description of _Cron
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Bootstrap;

class Cron extends \oroboros\core\libs\Abstracts\Site\Adapter\Adapter {
    
    const SCOPE = 'cron';
    
    function __construct($package=NULL) {
        parent::__construct($package);
    }
    
    public function init() {
        
    }
    
    function __destruct() {
        parent::__destruct();
    }
}

?>
