<?php
/**
 * Description of _Framework
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Bootstrap;

class Framework extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    const SCOPE = 'framework';
    
    function __construct($package=NULL) {
        parent::__construct($package);
    }
    
    public function init() {
        
    }
    
    function __destruct() {
        parent::__destruct();
    }
}

?>
