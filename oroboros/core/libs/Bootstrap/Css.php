<?php

/**
 * Description of Css
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */

namespace oroboros\core\libs\Bootstrap;

class Css extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    const SCOPE = 'css';

    public $coresettings = NULL;
    public $sitesettings = NULL;
    public $pagesettings = NULL;
    public $themesettings = NULL;
    public $templatesettings = NULL;
    protected $_model;
    protected $_controller;
    public $render = NULL;

    public function __construct($package = NULL) {

        $this->cachemode = 'cache';

        parent::__construct($package);
        $this->init();
    }

    private function init() {
        $this->settings = new \oroboros\core\libs\Settings\Css();
        $this->settings = $this->settings->returnSettings();
        $this->output = new \oroboros\core\libs\Render\Output();
        $this->headers = new \oroboros\core\libs\Render\Headers($this->package, 'css');
        $this->handleSettings();
        print $this->render;
    }

    protected function handleSettings() {
        $this->device = $_SESSION['device'];
        $this->browser = $_SESSION['browser'];
        $this->page = $_SESSION['page'];
        $this->site = $_SESSION['site'];
        $this->subdomain = $_SESSION['subdomain'];
        $queue = array();
        $queue[] = 'startOutputBuffer';
        if ($this->settings['cache'] == TRUE && $this->settings['debug'] != TRUE) {
            $queue[] = 'cssCachePageHeaders';
        } else {
            $queue[] = 'cssPageHeaders';
        }
        $queue[] = 'startOutputBuffer';
        if ($this->settings['debug'] == TRUE) {
            $queue[] = 'debug';
        }
        if ($this->settings['core'] == TRUE) {
            $queue[] = 'getCoreCss';
        }
        if ($this->settings['dependency'] == TRUE) {
            $queue[] = 'getDependencyCss';
        }
        if ($this->settings['skeleton'] == TRUE) {
            $queue[] = 'getSkeletonCss';
        }
        if ($this->settings['template'] == TRUE) {
            $queue[] = 'getTemplateCss';
        }
        if ($this->settings['theme'] == TRUE) {
            $queue[] = 'getThemeCss';
        }
        if ($this->settings['module'] == TRUE) {
            $queue[] = 'getModuleCss';
        }
        if ($this->settings['site'] == TRUE) {
            $queue[] = 'getSiteCss';
        }
        if ($this->settings['page'] == TRUE) {
            $queue[] = 'getPageCss';
        }
        if ($this->settings['component'] == TRUE) {
            $queue[] = 'getComponentCss';
        }
        $queue[] = 'getUtilityCss';
        $queue[] = 'finalize';
        if ($this->settings['validate'] == TRUE) {
            $queue[] = 'validate';
        }
        if ($this->settings['minify'] == TRUE) {
            $queue[] = 'minify';
        }
        $this->runQueue($queue);
    }

    protected function runQueue($queue) {
        foreach ($queue as $method) {
            $this->$method();
        }
    }

    public function getSettings($file) {
        $ini = new \oroboros\core\libs\Dataflow\Dataflow($this->package);
        $this->coresettings = $ini->fetch_ini($file);
    }

    protected function getThemeSettings($file) {
        if (is_readable($file)) {
            return $this->data->fetch_ini($file);
        } else {
            //log missing settings.ini file
            echo 'theme settings not found at ' . $file . PHP_EOL;
            return false;
        }
    }

    protected function getTemplateSettings($file) {
        $ini = new \oroboros\core\libs\Dataflow\Dataflow($this->package);
        if (is_readable($file)) {
            $this->templatesettings = $ini->fetch_ini($file);
        } else {
            //log missing settings.ini file
            return false;
        }
    }

    protected function checkSiteCss() {
        if ($this->coresettings['CSS']['loadSiteCss'] == TRUE) {
            $stylesheet = $this->getSiteCss();
            if (is_readable(_SITES . $_SESSION['site'] . '/' . $stylesheet) && $this->coresettings['CSS']['loadSiteCss'] == TRUE) {
                require_once _SITES . $_SESSION['site'] . '/' . $stylesheet;
            } else
                return false;
        }
    }

    protected function checkPageCss() {
        if ($this->coresettings['CSS']['loadPageCss'] == TRUE) {
            if (isset($this->page['stylesheet']) && !is_null($this->page['stylesheet'])) {
                $stylesheet = $this->page['stylesheet'];
            } else {
                return false;
            }
            if (!is_null($stylesheet)) {
                if (is_readable(_SITES . $_SESSION['site'] . '/' . $_SESSION['page'] . '/' . $stylesheet)) {
                    require_once _SITES . $_SESSION['site'] . '/' . $_SESSION['page'] . '/' . $stylesheet;
                } else {
                    //Log disconnected stylesheet into error log
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    protected function checkThemeCss() {
        if ($this->coresettings['CSS']['loadThemeCss'] == TRUE) {
            $this->getPageTheme($_SESSION['page']);
            if (isset($this->themeslug)) {
                $this->getThemeSettings(_THEME . $this->themeslug . '/settings.ini');
            }
            if (!is_null($this->themesettings['settings']['stylesheet'])) {
                if (is_readable(_THEME . $this->themeslug . '/' . $this->themesettings['settings']['stylesheet'])) {
                    //echo 'Theme file is readable at ' . _THEME . $this->themeslug . '/' . $this->themesettings['settings']['stylesheet'] . PHP_EOL;
                    require_once _THEME . $this->themeslug . '/' . $this->themesettings['settings']['stylesheet'];
                } else
                //echo 'Theme file not found at ' . _THEME . $this->themeslug . '/' . $this->themesettings['settings']['stylesheet'] . PHP_EOL;
                    return false;
            }
        }
    }

    protected function checkTemplateCss() {
        if ($this->coresettings['CSS']['loadTemplateCss'] == TRUE) {
            $stylesheet = $_SESSION['template']['template'];
            if (is_readable(_TEMPLATE . $_SESSION['template']['template'] . '/' . $_SESSION['template']['settings']['stylesheet'])) {
                //echo 'Template stylesheet: ' . _TEMPLATE . $_SESSION['template']['template'] . '/' . $_SESSION['template']['settings']['stylesheet'];
                require_once _TEMPLATE . $_SESSION['template']['template'] . '/' . $_SESSION['template']['settings']['stylesheet'];
            } else
                echo 'could not read the template stylesheet at ' . _TEMPLATE . $_SESSION['template']['template'] . '/' . $_SESSION['template']['settings']['stylesheet'];
            return false;
        }
    }

    //incomplete
    protected function getPageTheme($page) {
        $query = 'SELECT `theme`,`parent`,`site` FROM `pages` WHERE `slug`="' . $page . '" AND `site`="' . $_SESSION['site'] . '" AND `subdomain`="' . $this->subdomain . '" AND `active`="1" LIMIT 1;';
        $stylesheet = $this->db->select($query);
        //echo 'Query: ' . 'SELECT `theme`,`parent`,`site` FROM `pages` WHERE `slug`="' . $page . '" AND `site`="' . $_SESSION['site'] . '" AND `subdomain`="' . $this->subdomain . '" AND `active`="1" LIMIT 1;' . PHP_EOL;
        if ($stylesheet[0]['theme'] != 'RAW' && $stylesheet[0]['theme'] != 'INHERIT' && isset($stylesheet[0]['theme'])) {
            //Theme found, returning theme css file
            return $this->getThemeCss($stylesheet[0]['theme']);
        } elseif ($stylesheet[0]['theme'] == 'INHERIT' && !isset($stylesheet[0]['parent']) && isset($stylesheet[0]['site'])) {
            //no page inheritance, getting inheritance from site
            $stylesheet = $this->getSiteTheme($stylesheet[0]['site']);
        } elseif (isset($stylesheet[0]['parent']) && $stylesheet[0]['theme'] == 'INHERIT') {
            //inheriting from parent page
            $stylesheet = $this->getPageTheme($stylesheet[0]['parent']);
        } elseif ($stylesheet[0]['theme'] == 'RAW') {
            //RAW style, no stylesheet returned
            return false;
        } else {
            //returns the stylesheet normally
            return $stylesheet[0]['theme'];
        }
    }

    protected function getSiteTheme($site) {
        //echo 'Site Query: ' . 'SELECT `theme`,`parent` FROM `sites` WHERE `slug`="' . $site . '" LIMIT 1;';
        $stylesheet = $this->db->select('SELECT `theme`,`parent` FROM `sites` WHERE `slug`="' . $site . '" LIMIT 1;');
        if ($stylesheet[0]['theme'] != 'RAW' && $stylesheet[0]['theme'] != 'INHERIT' && isset($stylesheet[0]['theme'])) {
            //Theme found, returning theme css file
            return $this->getThemeCss($stylesheet[0]['theme']);
        } elseif ($stylesheet[0]['theme'] == 'INHERIT') {
            //inherit style from parent site
            return $this->getSiteTheme($stylesheet[0]['parent']);
        } elseif ($stylesheet[0]['theme'] == 'RAW') {
            //RAW style, no stylesheet loaded
            return false;
        }
    }

    public function cssPageHeaders() {
        $this->headers->get_headers('css', 0, $this->cachemode);
    }

    public function startOutputBuffer() {
        $this->output->_initBuffer();
    }

    protected function getCoreCss() {
        
    }

    protected function getDependencyCss($device = NULL) {
        $dependencies = array();
        //get site dependencies
        if (!isset($device)) {
            $devicequery = '`device` IS NULL';
        } else {
            $devicequery = '`device`=\'' . $device . '\'';
        }
        $query = "SELECT `dependency`,`version` FROM `dependencies_assets` WHERE `site` IN ('global', '" . $this->site . "') AND " . $devicequery . " AND `source_method`='css' AND `active`=1 ORDER BY `hierarchy`;";
        $results = $this->db->select($query);
        if (!empty($results)) {
            foreach ($results as $item) {
                $file = _DEPENDENCIES . self::SCOPE . '/' . $item['dependency'] . '-' . $item['version'] . '.css';
                if (is_readable($file)) {
                    require $file;
                } else {
                    //dependency misallocated, log results, if debug is enabled also notify user. Proceed to next queued dependency
                    echo 'Dependency file: ' . $item['dependency'] . ' not found at ' . $file . PHP_EOL;
                }
            }
        }
        if (!isset($device)) {
            $this->getDependencyCss($this->device['mode']);
        }
    }

    protected function getSkeletonCss() {
        $file = _BASE . 'public/css/Skeleton/' . $this->device['mode'] . '/skeleton.css';
        if (is_readable($file)) {
            require $file;
        } else
            echo 'Error: skeleton.css not found at ' . $file . PHP_EOL;
    }

    protected function getTemplateCss() {
        $this->template = $_SESSION['template'];
        if (is_array($this->template)) {
            //template array already defined, proceed to render
            if (isset($this->template['settings'])) {
                //template settings already defined, proceed to fetch stylesheet
                $file = _TEMPLATE . $this->template['template'] . '/' . $this->template['settings']['stylesheet'];
            } else {
                //template settings not defined, get settings then proceed
            }
        } else {
            //proceed to fetch template array
        }
        if (is_readable($file) && is_file($file)) {
            //template css is readable, render
            require $file;
            return true;
        } else {
            //template css not readable, log error, skip to next queue action
            //echo 'Template stylesheet not readable at ' . $file . PHP_EOL;
            return false;
        }
    }

    protected function getThemeCss() {
        $this->theme = $_SESSION['theme'];
        if (is_string($this->theme)) {
            //theme array need to be determined
            $theme = $this->theme;
            echo 'Theme passed as string, need to determine theme details.' . PHP_EOL;
            return false;
        } else {
            //theme array are already determined, proceed to fetch settings
            $filepath = _THEME . $this->theme['slug'] . '/';
            $file = $filepath . 'settings.ini';
            $this->themesettings = $this->getThemeSettings($file);
            if ($this->themesettings != FALSE && $this->themesettings['settings']['stylesheet']) {
                //settings file found, load stylesheet
                $file = $filepath . $this->themesettings['settings']['stylesheet'];
                if (!is_file($file)) {
                    return FALSE;
                }
                if (is_readable($file) && is_file($file)) {
                    require $file;
                    return TRUE;
                } else {
                    //theme stylesheet could not be found, log error, proceed to next queue item
                    //echo 'theme stylesheet could not be loaded at ' . $file . PHP_EOL;
                    return FALSE;
                }
            } else {
                return false;
            }
        }
    }

    protected function getModuleCss() {
        $handler = new \oroboros\core\libs\Modules\Modules($this->package);
    }

    protected function getSiteCss() {
        $stylesheet = $this->db->select('SELECT `stylesheet` FROM `sites` WHERE `slug`="' . $this->site . '" LIMIT 1;');
        if (!empty($stylesheet)) {
            $file = _SITES . $this->site . '/' . $stylesheet[0]['stylesheet'];
            if (is_readable($file)) {
                require $file;
            } else {
                //listed stylesheet is misconfigured, log error, if debug mode is enabled, also alert user. Proceed to next queue item
                echo 'The site stylesheet could not be found at ' . $file . PHP_EOL;
            }
        }
    }

    protected function getPageCss() {
        $stylesheet = $this->db->select('SELECT `stylesheet` FROM `pages` WHERE `slug`="' . $this->page . '" AND `site`="' . $this->site . '" AND `subdomain`="' . $this->subdomain . '" LIMIT 1;');
        if (!is_null($stylesheet)) {
            $file = _SITES . $this->site . '/' . $this->page . '/' . $stylesheet[0]['stylesheet'];
            if (is_readable($file) && is_file($file)) {
                require $file;
            } else {
                //page stylesheet misconfigured. Log error, if debug mode is enabled, also alert user. Proceed to next queue item
                //echo 'Page stylesheet not found at ' . $file . PHP_EOL;
            }
        }
    }

    protected function getComponentCss() {
        $handler = new \oroboros\core\libs\Components\Components($this->package);
        $handler->get_components($this->site, $this->subdomain, $this->page);
        $components = $handler->components;
        foreach ($components as $value) {
            $css = $handler->load_component_stylesheet($value);
            if (isset($css)) {
                require $css;
            }
        }
    }

    protected function getUtilityCss() {
        $this->utilities = $_SESSION['utilities'];
    }

    protected function finalize() {
        $this->render = $this->output->cleanGet();
    }

    protected function minify() {
        $content = $this->render;
        $content = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $content);
        $content = str_replace(': ', ':', $content);
        $content = str_replace(' {', '{', $content);
        $content = str_replace('} ', '}', $content);
        $content = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $content);
        $this->render = $content;
    }

    protected function debug() {
        $content = $this->render;
    }

    protected function validate() {
        $content = $this->render;
    }

    public function __destruct() {
        unset($_SESSION['stylesheets']);
        parent::__destruct();
        exit();
    }

}

?>
