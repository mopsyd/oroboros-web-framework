<?php
/**
 * Description of Js
 *
 * @author Your Name <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Bootstrap;

class Js extends \oroboros\core\libs\Abstracts\Site\Model\Model {
    
    const SCOPE = 'javascript';
    
    public $coresettings = NULL;
    public $sitesettings = NULL;
    public $pagesettings = NULL;
    public $themesettings = NULL;
    public $templatesettings = NULL;
    protected $_model;
    protected $_controller;
    public $render = NULL;

    public function __construct($package=NULL) {
        parent::__construct($package);
        $this->init();
    }
    
    public function init() {
        $this->settings = new \oroboros\core\libs\Settings\Js($this->package);
        $this->settings = $this->settings->returnSettings();
        $this->output = new \oroboros\core\libs\Render\Output();
        $this->headers = new \oroboros\core\libs\Render\Headers($this->package, 'css');
        $this->handleSettings();
        print $this->render;
    }
    
    protected function handleSettings() {
        $this->device = $_SESSION['device'];
        $this->browser = $_SESSION['browser'];
        $this->page = $_SESSION['page'];
        $this->site = $_SESSION['site'];
        $this->subdomain = $_SESSION['subdomain'];
        $queue = array();
        $queue[] = 'startOutputBuffer';
        if ($this->settings['cache'] == TRUE && $this->settings['debug'] != TRUE) {
            $queue[] = 'javascriptCachePageHeaders';
        } else {
            $queue[] = 'javascriptPageHeaders';
        }
        $queue[] = 'startOutputBuffer';
        if ($this->settings['debug'] == TRUE) {
            $queue[] = 'debug';
        }
        if ($this->settings['core'] == TRUE) {
            $queue[] = 'getCoreScripting';
        }
        if ($this->settings['dependency'] == TRUE) {
            $queue[] = 'getDependencyScripting';
        }
        if ($this->settings['skeleton'] == TRUE) {
            $queue[] = 'getSkeletonScripting';
        }
        if ($this->settings['template'] == TRUE) {
            $queue[] = 'getTemplateScripting';
        }
        if ($this->settings['theme'] == TRUE) {
            $queue[] = 'getThemeScripting';
        }
        if ($this->settings['module'] == TRUE) {
            $queue[] = 'getModuleScripting';
        }
        if ($this->settings['site'] == TRUE) {
            $queue[] = 'getSiteScripting';
        }
        if ($this->settings['page'] == TRUE) {
            $queue[] = 'getPageScripting';
        }
        if ($this->settings['component'] == TRUE) {
            $queue[] = 'getComponentScripting';
        }
        $queue[] = 'getUtilityScripting';
        $queue[] = 'finalize';
        if ($this->settings['validate'] == TRUE) {
            $queue[] = 'validate';
        }
        if ($this->settings['minify'] == TRUE) {
            $queue[] = 'minify';
        }
        $this->runQueue($queue);
    }
    
    protected function runQueue($queue) {
        foreach ($queue as $method) {
            $this->$method();
        }
    }

    public function getSettings($file) {
        $ini = new \oroboros\core\libs\Dataflow\Dataflow($this->package);
        $this->coresettings = $ini->fetch_ini($file);
    }
    
    protected function getThemeSettings($file) {
        if (is_readable($file)) {
            return $this->data->fetch_ini($file);
        } else {
            //log missing settings.ini file
            echo 'theme settings not found at ' . $file . PHP_EOL;
            return false;
        }
    }

    public function javascriptPageHeaders() {
        $this->headers->get_headers('js', 0, $this->cachemode);
    }

    public function startOutputBuffer() {
        $this->output->_initBuffer();
    }
    
    protected function getCoreScripting() {
        
    }
    
    protected function getDependencyScripting($device=NULL) {
        $dependencies = array();
        //get site dependencies
        if (!isset($device)) {
            $devicequery = '`device` IS NULL';
        } else {
            $devicequery = '`device`=\'' . $device . '\'';
        }
        $query = "SELECT `dependency`,`version` FROM `dependencies_assets` WHERE `site` IN ('global', '" . $this->site . "') AND " . $devicequery . " AND `source_method`='javascript' AND `active`=1 ORDER BY `hierarchy`;";
        $results = $this->db->select($query);
        if (!empty($results)) {
            foreach ($results as $item) {
                $file = _DEPENDENCIES . self::SCOPE . '/' . $item['dependency'] . '-' . $item['version'] . '.js';
                if (is_readable($file)) {
                    require $file;
                } else {
                    //dependency misallocated, log results, if debug is enabled also notify user. Proceed to next queued dependency
                    echo 'Dependency file: ' . $item['dependency'] . ' not found at ' . $file . PHP_EOL;
                }
            }
        }
        if (!isset($device)) {
            $this->getDependencyScripting($this->device['mode']);
        }
    }
    
    protected function getSkeletonScripting() {
                $file = _BASE . 'public/js/Skeleton/' . $this->device['mode'] . '/skeleton.js';
        if (is_readable($file)) {
            require $file;
        } else
            echo 'Error: skeleton.js not found at ' . $file . PHP_EOL;
    }
    
    protected function getTemplateScripting() {
        $this->template = $_SESSION['template'];
        if (is_array($this->template)) {
            //template array already defined, proceed to render
            if (isset($this->template['settings'])) {
                //template settings already defined, proceed to fetch stylesheet
                $file = _TEMPLATE . $this->template['template'] . '/' . $this->template['settings']['javascript'];
            } else {
                //template settings not defined, get settings then proceed
            }
        } else {
            //proceed to fetch template array
        }
        if (is_readable($file) && is_file($file)) {
            //template css is readable, render
            require $file;
            return true;
        } else {
            //template css not readable, log error, skip to next queue action
            //echo 'Template javascript not readable at ' . $file . PHP_EOL;
            return false;
        }
    }
    
    protected function getThemeScripting() {
        $this->theme = $_SESSION['theme'];
        if (is_string($this->theme)) {
            //theme array need to be determined
            $theme = $this->theme;
            echo 'Theme passed as string, need to determine theme details.' . PHP_EOL;
            return false;
        } else {
            //theme array are already determined, proceed to fetch settings
            $filepath = _THEME . $this->theme['slug'] . '/';
            $file = $filepath . 'settings.ini';
            $this->themesettings = $this->getThemeSettings($file);
            if ($this->themesettings != FALSE && $this->themesettings['settings']['javascript'] != FALSE) {
                //settings file found, load stylesheet
                $file = $filepath . $this->themesettings['settings']['javascript'];
                if (is_readable($file) && is_file($file)) {
                    require $file;
                    return TRUE;
                } else {
                    //theme stylesheet could not be found, log error, proceed to next queue item
                    //echo 'theme javascript could not be loaded at ' . $file . PHP_EOL;
                    return FALSE;
                }
            } else {
                return false;
            }
        }
    }
    
    protected function getModuleScripting() {
        
    }

    protected function getSiteScripting() {
        $this->site = $_SESSION['site'];
        $stylesheet = $this->db->select('SELECT `stylesheet` FROM `sites` WHERE `slug`="' . $_SESSION['site'] . '" LIMIT 1;');
        return $stylesheet[0]['stylesheet'];
    }

    protected function getPageScripting() {
        $this->page = $_SESSION['page'];
        $stylesheet = $this->db->select('SELECT `stylesheet` FROM `pages` WHERE `slug`="' . $_SESSION['page'] . '" AND `site`="' . $_SESSION['site'] . '" AND `subdomain`="' . $this->subdomain . '" LIMIT 1;');
        return $stylesheet[0]['stylesheet'];
    }
    
    protected function getComponentScripting() {
                $handler = new \oroboros\core\libs\Components\Components($this->package);
        $handler->get_components($this->site, $this->subdomain, $this->page);
        $components = $handler->components;
        foreach ($components as $value) {
            $css = $handler->load_component_scripting($value);
            if (isset($css)) {
                require $css;
            }
        }
    }
    protected function getUtilityScripting() {
        $this->utilities = $_SESSION['utilities'];
        
    }
    
    
    protected function finalize() {
        $this->render = $this->output->cleanGet();
    }
    
    protected function minify() {
        $content = $this->render;
//            $content = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $content);
//            $content = str_replace(': ', ':', $content);
//            $content = str_replace(' {', '{', $content);
//            $content = str_replace('} ', '}', $content);
//            $content = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $content);
        $this->render = $content;
    }
    
    protected function debug() {
        $content = $this->render;
    }
    
    protected function validate() {
        $content = $this->render;
    }
    
    public function __destruct() {
        unset($_SESSION['scripts']);
        parent::__destruct();
        exit();
    }
}

?>
