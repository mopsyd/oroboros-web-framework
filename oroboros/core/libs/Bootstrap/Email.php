<?php
/**
 * Description of Email
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Bootstrap;

class Email extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    const SCOPE = 'email';
    
    function __construct($package=NULL) {
        parent::__construct($package);
    }
    
    public function init() {
        
    }
    
    function __destruct() {
        parent::__destruct();
    }
}

?>
