<?php

//namespace Bootstrap\Ajax;

/**
 * Description of Ajax
 *
 * @author Your Name <brian@mopsyd.me>
 */

namespace oroboros\core\libs\Bootstrap;

class Ajax extends \oroboros\core\libs\Abstracts\Site\Model\Model {

    const SCOPE = 'ajax';

    //class variables
    protected $JSON = NULL;
    protected $request = NULL;
    protected $user = NULL;
    protected $nonce = NULL;
    protected $requestedFunction = NULL;
    protected $params = NULL;
    //class objects
    protected $validator = NULL;

    public function __construct($package = NULL) {
        parent::__construct($package);
        $this->init();
    }

    private function init() {
        $this->loadCoreDependencies();
        if (isset($_POST)) {
            $this->_method = 'post';
            $this->JSON = $this->parseRequest($_POST);
        } elseif (isset($_GET)) {
            $this->_method = 'get';
            $this->JSON = $this->parseRequest($_GET);
        } else {
            //no valid data type. Log error, return fail header and exit.
            echo 'Request was not a valid data type' . PHP_EOL;
            $this->denyAccess();
        }
        if (!empty($this->JSON)) {
            //proceed with validation step
            $this->checkNonce();
            $this->checkIV();
            $this->checkMethod();
            $this->checkParams();

            //validation passed, proceed to check access rights
            //access rights passed, proceed to class definition
            $this->loadController();
            //class definition successful, proceed to process request
            $method = $this->method;
            $render = $this->controller->$method($this->params);
            //request processed, return results
            if (!empty($render) && $render != FALSE) {
                $this->successResponse($render);
            } else {
                $this->failResponse('Requested method could not be completed');
            }
        } else {
            //return error header
            $this->denyAccess('Request was empty');
        }
    }

    protected function loadCoreDependencies() {
        $this->validator = new \oroboros\core\libs\Validation\Ajax($this->package);
    }

    protected function parseRequest($data) {
        return $this->data->parse_json($data['datastring']);
    }

    protected function checkNonce() {
        if (isset($this->JSON['nonce'])) {
            $this->validateNonce();
        } else {
            //no nonce passed, log security error and exit.
            $this->denyAccess('invalid security parameters: User nonce not passed with request');
            return FALSE;
        }
    }

    protected function validateNonce() {
        
    }

    protected function checkIV() {
        if (isset($this->JSON['iv'])) {
            $this->validateIV();
        } else {
            //no nonce passed, log security error and exit.
            $this->denyAccess('invalid security parameters: User device IV not passed with request');
            return FALSE;
        }
    }

    protected function validateIV() {
        
    }

    protected function checkUser() {
        
    }

    protected function validateUser() {
        
    }

    protected function checkMethod() {
        if (isset($this->JSON['method'])) {
            $this->validateMethod();
        } else {
            //no nonce passed, log security error and exit.
            $this->denyAccess('invalid request: Required method not passed with request');
            return FALSE;
        }
    }

    protected function validateMethod() {
        
    }

    protected function checkParams() {
        if (isset($this->JSON['params']) && !empty($this->JSON['params'])) {
            $this->validateParams();
        }
    }

    protected function validateParams() {
        
    }

    protected function checkRequestAccessRights() {
        
    }

    protected function executeRequestedCommand() {
        
    }

    protected function getRequestedData() {
        
    }

    protected function denyAccess($reason) {
        echo $reason . PHP_EOL;
        echo 'Access denied' . PHP_EOL;
        exit();
    }

    protected function successResponse($message) {
        echo $message;
        $this->__destruct();
    }

    protected function failResponse($message) {
        echo $message;
        $this->__destruct();
    }

    protected function loadController() {
        $query = "SELECT `controller`,`params`,`permission`,`type` FROM `codebase_ajax` WHERE `controller`='" . $this->JSON['controller'] . "' AND `method`='" . $this->JSON['method'] . "' LIMIT 1;";
        $result = $this->db->select($query);
        if (!empty($result)) {
            //controller found
            $this->method = $this->JSON['method'];
            $this->methodPerm = $result[0]['permission'];
            $this->scope = $result[0]['type'];
            $this->paramCount = $result[0]['params'];
            $this->params = $this->data->parse_json($this->JSON['params']);
            $query = "SELECT `class`,`namespace`,`file`,`path` FROM `codebase` WHERE `id`='" . $result[0]['controller'] . "' LIMIT 1;";
            $result = $this->db->select($query);
            if (isset($result[0]['namespace']) && isset($result[0]['path'])) {
                $this->autoload->addNamespace($result[0]['namespace'], $result[0]['path']);
                $this->autoload->addNamespace('\\oroboros\\core\\views\\Ajax', '/oroboros/core/views/Ajax/');
                $class = $result[0]['namespace'] . '\\' . $result[0]['class'];
                $this->controller = new $class($this->package);
                $this->loadModel();
            } else {
                $this->failResponse('Malformed method configuration!');
            }
        } else {
            //controller not found
            $this->failResponse('Requested method not found');
        }
    }

    protected function loadModel() {
        $query = "SELECT `class`,`namespace`,`file`,`path` FROM `codebase` WHERE `parent`='" . $this->JSON['controller'] . "' LIMIT 1;";
        $result = $this->db->select($query);
        if (isset($result[0]['namespace']) && isset($result[0]['path'])) {
            $this->autoload->addNamespace($result[0]['namespace'], $result[0]['path']);
                $class = $result[0]['namespace'] . '\\' . $result[0]['class'];
                $this->model = new $class($this->package);
                $this->controller->defineModel($this->model);
        } else {
            $this->failResponse('Malformed method configuration!');
        }
    }

    public function __destruct() {
        parent::__destruct();
        exit();
    }

}