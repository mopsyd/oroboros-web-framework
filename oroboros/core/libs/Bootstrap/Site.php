<?php

/**
 * Description of Site
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */

namespace oroboros\core\libs\Bootstrap;

class Site extends \oroboros\core\libs\Abstracts\Site\Lib {

    const SCOPE = 'site';
    const ERRORCONTROLLER = 'Error';
    const ERRORCONTROLLERPATH = 'render';
    const DEFAULTCONTROLLER = 'Pagerender';
    const DEFAULTCONTROLLERPATH = 'render';
    const MAINTENANCECONTROLLER = 'Maintenance';
    const MAINTENANCECONTROLLERPATH = 'render';
    const LOCKDOWNCONTROLLER = 'Lockdown';
    const LOCKDOWNCONTROLLERPATH = 'render';

    //permissions
    const LOCKDOWNPERM = 's_lock_view';
    const GLOBALLOCKDOWNPERM = 'g_lock_view';
    const MAINTENANCEPERM = 's_maint_view';
    const GLOBALMAINTENANCEPERM = 'g_maint_view';

    protected $settings = array();
    protected $debug = array();
    protected $security = array();
    public $_site = NULL;
    public $_domain = NULL;
    public $_subdomain = 'default';
    public $_page = NULL;
    private $_url = NULL;
    private $_render = NULL;
    private $_controller = NULL;
    private $_controllerPath = _CONTROLLERS;
    private $_modelPath = _MODELS;
    private $_adapters = NULL;
    private $_adapterPath = _ADAPTERS;
    private $_errorFile = 'error.php';
    private $_defaultFile = 'pagerender.php';

    function __construct($package = NULL) {
        parent::__construct($package);
        $this->init();
    }

    public function init() {
        $this->domaincheck = new \oroboros\core\libs\SITE\Subdomain($this->package);
        $this->sitecheck = new \oroboros\core\libs\SITE\Site($this->package);
        $this->urlcheck = new \oroboros\core\libs\SITE\Url($this->package);
        $this->pagecheck = new \oroboros\core\libs\SITE\Page($this->package);
        $this->user = new \oroboros\core\libs\User\User($this->package);
        $this->package['user'] = $this->user;
        $this->checkSubdomain();
        $this->checkPage();
        $this->getLoadData();
        $this->siteDetails();
        $this->initUser();
        $this->checkSiteAccess();
        $this->checkSubdomainAccess();
        $this->checkPageAccess();
        // Load the default controller if no URL is set
        // eg: Visit http://localhost it loads Default Controller
        if ($this->_url[0] == 'index' || empty($this->_url[0])) {
            $this->_loadDefaultController();
        } else {
            $this->_loadExistingController();
            $this->_callControllerMethod();
        }
    }

    private function checkSubdomain() {
        $domain = $this->domaincheck->checkSubdomain();
        $this->_subdomain = $domain['subdomain'];
        $this->package['subdomain'] = $domain['subdomain'];
        $this->_domain = $domain['domain'];
        $this->package['domain'] = $domain['domain'];
        $this->user->subdomain = $this->_subdomain;
        $this->checkSite();
    }

    private function checkSite() {
        $result = $this->sitecheck->checkRequest($this->_domain);
        if ($result === 200) {
            //site exists in network, proceed to page check
            $this->_site = $result;
            $this->user->site = $this->_site;
            $this->checkPage();
        } else {
            //site does not exist in network, throw 404 error. If viewer has webmaster access rights, ask if dns should be registered into the database
            switch ($result) {
                case 'hack':
                    //log hack attempts, mitigate further hacks according to site security definitions, add attempt
                    //to hack counter, if threshhold is exceeded perform temporary security lockout on ip address and
                    //throw security flag for administrator
                    echo 'mitigating hack attempt...<br>';
                    $this->_error(403);
                    break;
                case 'malformed':
                    //log malformed errors with request uri for future repair, throw 500 error
                    echo 'a malformed request was detected<br>';
                    $this->_error(500);
                    break;
                case 'unauthorized':
                    //user does not have permission to access request. Throw 403 error, add attempt to
                    //forbidden counter, if unauthorized requests exceed threshold perform temporary
                    //lockout on ip address, log usage pattern in the security log, throw security flag for administrator
                    echo 'an unauthorized request was detected<br>';
                    $this->_error(403);
                    break;
                case 'missing':
                    //log missing asset and throw 404 error
                    echo 'a missing asset was detected<br>';
                    $this->_error(404);
                    break;
                case 'disabled':
                    //This dns entry is disabled. 
                    echo 'site not enabled within the system<br>';
                    $this->_getSiteById('global');
                    $this->_subdomain = 'default';
                    $this->_error('disabled');
                    break;
                case 'sys-only':
                    //This is a system only site entry. Attempts to access may be hack attempts. Throw 404 error, log attempt
                    echo 'system only<br>';
                    $this->_error(403);
                    break;
                case 'unregistered':
                    //dns is not registered in the system. Throw site not found, add dns to dns table as disabled orphan, set
                    //flag for webmaster to notify them that there has been a new dns entry made avaliable for use
                    $this->_getSiteById('global');
                    $this->_subdomain = 'default';
                    $this->_error('nosite');
                    break;
                case 'orphan':
                    //dns exists but is not assigned to a site. Send user to "Coming soon" page.
                    $this->_getSiteById('global');
                    $this->_subdomain = 'default';
                    $this->_error('nosite');
                    break;
                case 'private':
                    //the site has been set to private. If the logged in user matches, allow access. Otherwise throw 403 error.
                    echo 'an unregistered resource was detected<br>';

                    break;
                case 'lockdown':
                    //the site is in lockdown mode. If the user has access rights to bypass lockdown, allow standard access. Otherwise
                    //display lockdown page
                    echo 'this site is currently in lockdown<br>';
                    $this->_error('lockdown');
                    break;
                case 'maintenance':
                    //the site is currently in maintenance mode. Allow users with bypass access rights to enter normally, otherwise 
                    //display maintenance message
                    echo 'the site is currently in maintenence mode<br>';
                    $this->_error('maintenance');
                    break;
            }
        }
    }

    private function checkPage() {
        $string = $_SERVER['REQUEST_URI'];
        $result = $this->pagecheck->resolveUri($string, $this->_subdomain, $this->_site['slug']);
        $this->user->page = $this->_page;
    }

    private function checkAccessRights() {
        
    }

    private function getLoadData() {
        $this->getSettings();
        $this->getDebug();
        $this->getSecurity();
    }

    private function getSettings() {
        $settings = new \oroboros\core\libs\Settings\Core($this->db);
        $this->settings = $settings->handleSettings();
    }

    private function getDebug() {
        $settings = new \oroboros\core\libs\Settings\Debug($this->db);
        $this->debug = $settings->handleSettings();
    }

    private function getSecurity() {
        $settings = new \oroboros\core\libs\Settings\Security($this->db);
        $this->security = $settings->handleSettings();
    }

    private function siteDetails() {
        $this->_getSite($this->_domain);
        $this->_getUrl();
    }

    private function _getSiteById($site) {
        $result = $this->db->select("SELECT `slug`,`display`,`parent`,`scope`,`permission`,`connect_type`,`url`,`stylesheet`,`description`,`access_level` FROM `sites` WHERE `slug`='" . $site . "' AND `active`='1' LIMIT 1;");
        if (isset($result[0])) {
            $this->_site = $result[0];
            $_SESSION['site'] = $this->_site;
        } else
            return FALSE;
    }

    private function _getSite($site) {
        $result = $this->db->select("SELECT `slug`,`display`,`parent`,`scope`,`permission`,`connect_type`,`url`,`stylesheet`,`description`,`access_level` FROM `sites` WHERE `url`='" . $site . "' AND `active`='1' AND `show`='1' LIMIT 1;");
        if (isset($result[0])) {
            $this->_site = $result[0];
        } else {
            //check DNS table for entry.
            //if user has webmaster permissions, add site to dns table if it doesn't exist.
            //Send webmaster to DNS management page with "Domain Added" flag if site is not in table.
            //Send webmaster to Site creation page with "Do you want to apply this DNS to a site?" flag if site is in table.
            //Send all users with lower permissions to "Coming soon" page.
        }
    }

    private function _getUrl() {
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $this->_url = explode('/', $url);
        if (empty($this->_url[0])) {
            $this->_url[0] = 'index';
        }
    }

    private function initUser() {
        $this->user->checkUser();
//        if (empty($_SESSION['user']['permissions']['group'])) {
//            $this->perms->initUser($_SESSION['user']['login']);
//            $this->perms->checkPerms($_SESSION['user']['login'], $_SESSION['user']['group']['slug']);
//        }
    }

    private function checkSiteAccess() {
        //checks access rights for the site, bounces to 403 if no access rights
        $query = "SELECT `slug`,`display`,`parent`,`scope`,`permission`,`connect_type`,`url`,`stylesheet`,`description`,`access_level` FROM `sites` WHERE `slug`='" . $this->_site['slug'] . "' AND `permission` IN ('" . implode("','", $this->user->userperms) . "') LIMIT 1;";
        $result = $this->db->select($query);
        if (!isset($result[0])) {
            $this->_error(403, 'Unauthorized User', 'You do not have access to this site');
        } else {
            $_SESSION['site'] = $result[0]['slug'];
            $this->_site = array(
                'slug' => $result[0]['slug'],
                'title' => $result[0]['display'],
                'parent' => $result[0]['parent'],
                'scope' => $result[0]['scope'],
                'permission' => $result[0]['permission'],
                'connect_type' => $result[0]['connect_type'],
                'url' => $result[0]['url'],
                'stylesheet' => $result[0]['stylesheet'],
                'description' => $result[0]['description'],
                'access_level' => $result[0]['access_level']
            );
        }
    }

    private function checkSubdomainAccess() {
        $_SESSION['subdomain'] = $this->_subdomain;
    }

    private function checkPageAccess($page = NULL) {

        if (!is_null($page)) {
            $query = "SELECT `slug`,`display`,`description`,`parent`,`access_level`,`controller`,`permission` FROM `pages` WHERE `slug`='" . $page . "' AND `site` IN ('" . $this->_site['slug'] . "', 'global') AND `subdomain`='" . $this->_subdomain . "' AND `active`='1' LIMIT 1;";
        } else {
            $query = "SELECT `slug`,`display`,`description`,`parent`,`access_level`,`controller`,`permission` FROM `pages` WHERE `slug`='" . $this->_url[0] . "' AND `site` IN ('" . $this->_site['slug'] . "', 'global') AND `subdomain`='" . $this->_subdomain . "' AND `active`='1' LIMIT 1;";
        }

        //checks access rights for the page, bounces to 403 if no access rights
        $result = $this->db->select($query);
        if (!isset($result[0]) && _DOC == 'site') {
            $this->_error(404);
        } else {
            if (in_array($result[0]['permission'], $this->user->userperms)) {
                $_SESSION['page'] = $result[0]['slug'];
                $this->_page = array(
                    'slug' => $result[0]['slug'],
                    'title' => $result[0]['display'],
                    'description' => $result[0]['description'],
                    'parent' => $result[0]['parent'],
                    'access_level' => $result[0]['access_level']
                );
                if (_DOC == 'site') {
                    $this->_getPageController($result[0]['controller']);
                }
            } else {
                if (_DOC == 'site') {
                    $this->_error(403, 'Insufficient Access Rights', 'You do not have access to this page');
                }
            }
        }
    }

    private function _handleUrl() {
        //determines the url hierarchy based on page inheritance
    }

    private function _getPageController($controller) {
        //loads the controller for the page, bounces to 500 error and logs missing asset if not found
        $result = $this->db->select("SELECT `id`,`class`,`path`,`file`,`namespace` FROM `codebase` WHERE `id`='$controller' LIMIT 1");
        if (file_exists(str_replace('//', '/', _BASE . $result[0]['path'] . $result[0]['file'])) && is_file(str_replace('//', '/', _BASE . $result[0]['path'] . $result[0]['file']))) {
            $class = '\\oroboros\\core\\controllers\\render\\' . $result[0]['class'];
            //echo $class . '<br>';
            $this->_controller = new $class($this->packageData());
            $this->package['controller'] = $this->_controller;
            $this->_controller->initCoreDatabase($this->db);
            $this->_getPageModel();
            $this->_controller->defineModel($this->_model);
        } elseif (empty($result[0]['file'])) {
            //loads the default controller if no controller is defined
            $this->initDefaultRender();
            $this->_controller = $this->_defaultController;
            $this->package['controller'] = $this->_controller;
        } else {
            //controller is not present in the codebase, throw 500 error
            //echo 'error at line ' . __LINE__ . ' of ' . __CLASS__ . '<br>';
            $this->_error(500);
        }
    }

    private function _loadDefaultController() {
        $this->_controller->index('index/index');
    }

    /**
     * Load an existing controller if there IS a GET parameter passed
     * 
     * @return boolean|string
     */
    private function _loadExistingController() {
        $file = $this->_controllerPath . 'render/' . ucfirst($this->_url[0]) . '.php';
        $this->initDefaultRender();
        $this->_render = new \oroboros\core\controllers\render\Pagerender($this->packageData());
    }

    /**
     * If a method is passed in the GET url paremter
     * 
     *  http://localhost/controller/method/(param)/(param)/(param)
     *  url[0] = Controller
     *  url[1] = Method
     *  url[2] = Param
     *  url[3] = Param
     *  url[4] = Param
     */
    private function _callControllerMethod() {
        $length = count($this->_url);
        // Make sure the method we are calling exists
        if ($length > 1) {
            if (!method_exists($this->_controller, $this->_url[1])) {
                $this->_error(404);
            }

            // Determine what to load
            switch ($length) {
                case 5:
                    //Controller->Method(Param1, Param2, Param3)
                    $this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3], $this->_url[4]);
                    break;

                case 4:
                    //Controller->Method(Param1, Param2)
                    $this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3]);
                    break;

                case 3:
                    //Controller->Method(Param1, Param2)
                    $this->_controller->{$this->_url[1]}($this->_url[2]);
                    break;

                case 2:
                    //Controller->Method(Param1, Param2)
                    $this->_controller->{$this->_url[1]}();
                    break;

                default:
                    $this->_controller->index();
                    break;
            }
        } else {
            //render index for page
            $this->_controller->index();
        }
    }

    private function initDefaultRender() {
        $this->initDefaultController();
        $this->initDefaultModel();
    }

    private function initDefaultController() {
        //loads the default controller
        $class = '\\oroboros\\core\\controllers\\' . self::DEFAULTCONTROLLERPATH . '\\' . self::DEFAULTCONTROLLER;
        $this->_defaultController = new $class($this->packageData());
        $this->initDefaultModel();
        $this->_defaultController->defineModel($this->_defaultModel);
    }

    private function initDefaultModel() {
        //loads the default model
        $reflect = new \ReflectionClass($this->_defaultController);
        $class = '\\oroboros\\core\\models\\' . $reflect->getConstant('MODELTYPE') . '\\' . $reflect->getConstant('MODEL');
        unset($reflect);
        $this->_defaultModel = new $class($this->packageData());
    }

    private function _getPageModel() {
        //loads the model for the page
        $reflect = new \ReflectionClass($this->_controller);
        $class = '\\oroboros\\core\\models\\' . $reflect->getConstant('MODELTYPE') . '\\' . $reflect->getConstant('MODEL');
        unset($reflect);
        $this->_model = new $class($this->packageData());
    }

    private function handleAtypicalRequest($flag, $type) {
        //handles non-standard uri string data
        switch ($flag) {
            case 'hack':
                //mitigate hack attempts
                $this->mitigateHackAttempts($type);
                break;
            case 'unregistered':
                //mitigate unregistered dns requests
                $this->handleUnregisteredAsset($type);
                break;
            case 'malformed':
                //handle malformed dns requests
                $this->mitigateMalformedRequests($type);
                break;
            case 'unauthorized':
                //handle unauthorized dns requests
                switch ($type) {
                    case 'dns':
                        $this->_error(403, 'Unauthorized Site Access', 'You are not permitted to access this site.');
                        break;
                    case 'page':
                        $this->_error(403);
                        break;
                    case 'subdomain':
                        $this->_error(403, 'Unauthorized Subdomain Access', 'You are not permitted to access this subdomain.');
                        break;
                    case 'subsection':
                        $this->_error(403, 'Unauthorized', 'You are not permitted to view this section of the site.');
                        break;
                }
                break;
            case 'missing':
                //handle requests for missing assets
                switch ($type) {
                    case 'dns':

                        break;
                    case 'page':
                        $this->_error(404);
                        break;
                    case 'subdomain':
                        $this->_error(404, 'Subdomain Not Found', 'The requested subdomain was not found on this server.');
                        break;
                    case 'subsection':
                        $this->_error(404);
                        break;
                }
                break;
            case 'private':
                //handle requests for private assets
                $this->checkPrivateAssetPermissions($type);
                break;
            case 'lockdown':
                //handle requests for resources currently in lockdown mode
                $this->checkLockdownPermissions($type);
                break;
            case 'maintenance':
                //handle requests for resources currently in maintenance mode
                $this->checkMaintenancePermissions($type);
                break;
        }
    }

    private function checkMaintenancePermissions($type) {
        
    }

    private function checkLockdownPermissions($type) {
        
    }

    private function checkPrivateAssetPermissions($type) {
        
    }

    private function mitigateMalformedRequests($type) {
        
    }

    private function mitigateHackAttempts($type) {
        
    }

    private function handleUnregisteredAsset($type) {
        
    }

    private function _error($error, $title = NULL, $message = NULL, $logfile = NULL, $logentry = NULL) {
        $this->initErrorRender($error, $title, $message, $logfile, $logentry);
    }

    private function initErrorRender($error, $title = NULL, $message = NULL, $logfile = NULL, $logentry = NULL) {
        $query = "SELECT `slug`,`display`,`description`,`parent`,`access_level`,`controller`,`permission` FROM `pages` WHERE `slug` IN ('error', 'global-error') AND `site` IN ('" . $this->_site['slug'] . "', 'global') AND `subdomain` IN ('" . $this->_subdomain . "', 'default') AND `active`='1' LIMIT 1;";
        //checks access rights for the page, bounces to 403 if no access rights
        $result = $this->db->select($query);
        if (isset($_SESSION['user'])) {
            //handle errors for instantiated users
            if (in_array($result[0]['permission'], $_SESSION['user']['permissions'])) {
                $_SESSION['page'] = $result[0]['slug'];
                $this->_page = array(
                    'slug' => $result[0]['slug'],
                    'title' => $result[0]['display'],
                    'description' => $result[0]['description'],
                    'parent' => $result[0]['parent'],
                    'access_level' => $result[0]['access_level']
                );
            }
        } else {
            //handle errors for uninstantiated users
            $_SESSION['page'] = $result[0]['slug'];
            $this->_page = array(
                'slug' => $result[0]['slug'],
                'title' => $result[0]['display'],
                'description' => $result[0]['description'],
                'parent' => $result[0]['parent'],
                'access_level' => $result[0]['access_level']
            );
        }

        $this->getErrorController($error, $title = NULL, $message = NULL, $logfile = NULL, $logentry = NULL);
    }

    private function getErrorController($error, $title = NULL, $message = NULL, $logfile = NULL, $logentry = NULL) {
        //loads the error controller
        $class = '\\oroboros\\core\\controllers\\' . self::ERRORCONTROLLERPATH . '\\' . self::ERRORCONTROLLER;
        $this->_controller = new $class($this->packageData(), $error, $title, $message, $logfile, $logentry);
        $this->package['controller'] = $this->_controller;
        $this->getErrorModel();
        $this->_controller->defineModel($this->_model);
        $this->_controller->init($error, $title, $message);
        exit();
    }

    private function getErrorModel() {
        //loads the default model
        $reflect = new \ReflectionClass($this->_controller);
        $class = '\\oroboros\\core\\models\\' . $reflect->getConstant('MODELTYPE') . '\\' . $reflect->getConstant('MODEL');
        unset($reflect);
        $this->_model = new $class($this->packageData());
    }

    private function packageData() {
        $package = array(
            'core' => array(
                'database' => $this->db,
                'settings' => array(
                    $this->settings,
                    $this->debug,
                    $this->security
                ),
                'classes' => array(
                    'libs' => array(
                        'perms' => $this->perms
                    ),
                    'controller' => $this->_controller,
                    'adapters' => $this->_adapters
                )
            ),
            'site' => $this->_site,
            'subdomain' => $this->_subdomain,
            'page' => $this->_page,
        );
        return $package;
    }

    function __destruct() {
        parent::__destruct();
        exit();
    }

}

?>
