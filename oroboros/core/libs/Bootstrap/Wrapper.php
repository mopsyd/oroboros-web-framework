<?php
/**
 * Description of _Wrapper
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Bootstrap;

class Wrapper extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    const SCOPE = 'wrapper';
    
    function __construct($package=NULL) {
        parent::__construct($package);
    }
    
    public function init() {
        
    }
    
    function __destruct() {
        parent::__destruct();
    }
}

?>
