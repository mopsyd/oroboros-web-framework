<?php
/**
 * Description of _Author
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\libs\Author;

class Author extends \oroboros\core\libs\Abstracts\Site\Lib {
    
    function __construct($package) {
        parent::__construct($package);
    }
    
    public function init() {
        
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
