<?php

namespace oroboros\core\adapters;

/**
 * Description of Python
 * handles requests to and from Python scripts
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Python extends \oroboros\core\libs\Abstracts\Site\Adapter\Adapter {
    
    public function __construct($package = NULL) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}