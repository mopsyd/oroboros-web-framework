<?php

namespace oroboros\core\adapters;

/**
 * Description of Ruby
 * handles request to and from Ruby scripts
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Ruby extends \oroboros\core\libs\Abstracts\Site\Adapter\Adapter {
    
    public function __construct($package = NULL) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}