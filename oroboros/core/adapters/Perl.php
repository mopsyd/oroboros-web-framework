<?php

namespace oroboros\core\adapters;

/**
 * Description of Perl
 * handles transfer of data between Oroboros and Perl scripts
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Perl extends \oroboros\core\libs\Abstracts\Site\Adapter\Adapter {
    
    public function __construct($package = NULL) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}