<?php

namespace oroboros\core\adapters;

/**
 * Description of Bash
 * handles bash scripting files, commands, and requests.
 * Also handles command line interactions on UNIX based systems
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Bash extends \oroboros\core\libs\Abstracts\Site\Adapter\Adapter {
    
    public function __construct($package = NULL) {
        parent::__construct($package);
    }
    
    public function check() {
        
    }
    
    public function process($command, $return=FALSE, $sanitize=TRUE) {
        if ($sanitize=TRUE) {
            $command = $this->sanitize($command);
        }
        if ($return=TRUE) {
            $result = shell_exec($command);
            return $result;
        } else {
            return exec($command);
        }
    }
    
    public function exec($command, $return=FALSE, $sanitize=TRUE) {
        if ($sanitize=TRUE) {
            $command = $this->sanitize($command);
        }
        if ($return=TRUE) {
            $result = shell_exec($command);
            return $result;
        } else {
            return exec($command);
        }
    }
    
    public function run_script() {
        
    }
    
    public function create_script() {
        
    }
    
    public function initialize_script() {
        
    }
    
    protected function sanitize($command) {
        
        
        return $command;
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}