<?php

namespace oroboros\core\interfaces\model;

/**
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
interface Render {
    
    public function index();
}

?>
