<?php

namespace oroboros\core\interfaces\libs;

/**
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
interface Lib {
    
    const AUTOLOAD_FILE = '/libs/Autoload/Autoload.php';
    const AUTOLOAD_REGISTER = '/libs/';

}

?>
