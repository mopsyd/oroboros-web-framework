<?php
/**
 * Description of Email
 * This is view handler for email. It will render the correct email formatted
 * content based on the template provided for the mail.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views;

class Email extends \oroboros\core\libs\Abstracts\Site\View\View {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
