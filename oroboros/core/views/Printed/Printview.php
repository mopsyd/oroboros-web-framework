<?php
/**
 * Description of Printview
 * This is the view handler for printed content
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views;

class Printview extends \oroboros\core\libs\Abstracts\Site\View\View {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
