<?php
/**
 * Description of Wrapper
 * This is the view handler for subsystems that are referenced
 * using Oroboros as a site wrapper
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views;

class Wrapper extends \oroboros\core\libs\Abstracts\Site\View\View {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
