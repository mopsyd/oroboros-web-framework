<?php

namespace oroboros\core\views\Wrapper;

/**
 * Description of View
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class View extends \oroboros\core\libs\Abstracts\Site\View\View {

    const RENDERTYPE = 'HTML';
    const VIEWPATH = 'oroboros/core/views/Wrapper';

    public function __construct($package) {
        parent::__construct($package);
    }

    public function __destruct() {
        parent::__destruct();
    }

}