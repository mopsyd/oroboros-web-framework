<?php

namespace oroboros\core\views\Rss;

/**
 * Description of View
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class View extends \oroboros\core\libs\Abstracts\Site\View\View {

    const RENDERTYPE = 'XML';
    const VIEWPATH = 'oroboros/core/views/Rss';

    public function __construct($package) {
        parent::__construct($package);
    }

    public function __destruct() {
        parent::__destruct();
    }

}