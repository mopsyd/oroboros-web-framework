<?php
/**
 * Description of Rss
 * This is view handler for rss feeds. It will render the correct content within the
 * rss feed structure to display correctly in feed readers.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views;

class Rss extends \oroboros\core\libs\Abstracts\Site\View\View {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
