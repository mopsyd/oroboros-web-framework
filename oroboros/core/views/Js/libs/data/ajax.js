/* 
 * Oroboros core ajax connector.
 */

//<?php if(isset($_SESSION['debug']['javascript']) || isset($_SESSION['sandbox']['developer']) ): ?>

/*
 *  Usage:
 * 
 * ------ Internal AJAX calls -------
 * 
 * Parameters:
 * _nonce: user identifier. This value represents a valid login, and can be obtained by
 * using the user.nonce value, which is updated on each page load. This value is reset on each page load, and is in place in order to
 * insure that the request is coming from the same userid passed to the system. It will be checked on the server
 * to insure that the correct nonce matches. If this check fails, the ajax call will return failure and the user will
 * be logged out for security.
 * 
 * _IV: user device identifier. This is stored in the user's cookie on their device and is
 * used as a secondary validation method to prevent session hijacking. Both the _nonce and _IV
 * must match on the server in order for the request to be processed. If the _nonce matches but the _IV does not, the 
 * ajax call will return failure and the user will be logged out, and prompted that someone else is using their session on
 * another device.
 * 
 * _controller: This is the data controller that the request references, and the request will be passed
 * to that controller if the validation step returns success. If the controller cannot be found, the ajax request will return
 * failure and log the error, request, and issuer. If the controller does exist, the ajax engine will proceed to the method
 * identifier step.
 * 
 * _method: This is the controller method that is being requested by the ajax call. If the method does not exist, the ajax request
 * will return failure and log the error, request, and issuer. if the method does exist, the ajax engine will proceed to pass the
 * given parameters into the controller method.
 * 
 * _params: The parameters to be passed to the controller. They must match the specifications for that methods
 * parameter input. If the parameters do not match, the ajax engine will return failure and log the error, request, and issuer. If the 
 * parameters do exist, the controller method will process the request.
 * 
 * --------
 * 
 * internal post requests: 
 * _ajax.send.post(_nonce, _IV, _controller, _method, _params);
 * 
 * These requests are used for any destructive method (any method that changes data on the server). If you are just
 * retrieving information, you should use ajax.send.get(); (see below for more info)
 * 
 * --------
 * 
 * internal get requests: 
 * _ajax.send.get(_nonce, _IV, _controller, _method, _params);
 * 
 * These requests are used for retrieving data from the server without changing any settings or database records. If you need
 * to perform a destructive method (change data on the server), you should use ajax.send.post(); (see above for more info)
 * 
 * --------
 * 
 * ------ External AJAX calls -------
 * 
 * External AJAX calls are made using the function:
 * _ajax.send.external();
 * 
 * this method must have at least the request parameter passed into it, but will accomodate any other parameters required by the 
 * external service. All validation checks should conform to the specifications for the external handler, which are beyond the scope
 * of this instruction. Please refer to your external API that you are connecting to for more information.
 * 
 * ------ Handling AJAX responses -------
 * 
 * Oroboros provides the ajax.handle(); method for handling responses from the server. Using internal ajax methods will automatically 
 * handle responses using this method. For external requests, you will need to specify how to handle the response.
 * 
 * --------
 * 
 * ------ Encoding AJAX data -------
 * 
 * Oroboros provides two means of encoding your data for transmission via AJAX, first, the ajax javascript class has its own built in
 * ajax.encode(); method for insuring that data is transmitted internally using the correct approach. For external calls, you may wish to
 * use the _data.json.encode(); method (see the documentation for the data class for more info).
 * 
 * --------
 */
//<?php endif; ?> 

oroboros.ajax = {
    send: {
        post: function(_controller, _method, _params, callback) {
            jQuery.ajax({
                type: 'POST',
                url: '<?php echo _CON._AJAX; ?>',
                data: 'datastring='+oroboros.data.json.encode({
                    nonce: oroboros.user.nonce,
                    iv: oroboros.user.IV,
                    controller: _controller,
                    method: _method,
                    params: _params
                }),
                success: function(data, status, xhr) {
                    var response = data;
                    callback(response);
                },
                failure: function(xhr, desc, err) {
                    callback(err);
                }
            });
        },
        get: function(_controller, _method, _params, callback) {
            jQuery.ajax({
                url: '<?php echo _CON._AJAX; ?>',
                data: 'datastring='+encodeURI(oroboros.data.json.encode({
                    nonce: oroboros.user.nonce,
                    iv: oroboros.user.IV,
                    controller: _controller,
                    method: _method,
                    params: _params
                })),
                success: function(data, status, xhr) {
                    console.log('response: '+ data);
                    var response = data;
                    callback(response);
                },
                failure: function(xhr, desc, err) {
                    callback(err);
                }
            });
        },
        external: function(request) {
            
        }
    }
}