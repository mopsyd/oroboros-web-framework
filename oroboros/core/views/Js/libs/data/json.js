/* 
 * Oroboros core json data handler.
 * this subclass appends it's methods to the _data javascript class.
 */

oroboros.data.json = {
    encode: function(val) {
        try {
            return JSON.stringify(val); 
        } catch(err) {
            if(!(err instanceof SyntaxError)) {
                throw new Error('Unexpected error in oroboros.data.json.encode: ' + err);
            } else {
                throw new Error('Syntax error in oroboros.data.json.encode' + err);
            }
            
            return null;
        }
    },
    decode: function(val) {
        try {
            return JSON.parse(val);
        } catch(err) {
            if(!(err instanceof SyntaxError)) {
                throw new Error('Unexpected error in oroboros.data.json.decode' + err);
            } else {
                throw new Error('Syntax error in oroboros.data.json.decode' + err);
            }
            
            return null;
        }
    }
};