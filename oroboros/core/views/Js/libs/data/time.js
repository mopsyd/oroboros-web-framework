/* 
 * This is the javascript time handler. It handles timestamps, setting clock values,
 * running timers, tickrate, and any other time specific functionality required throughout
 * the system.
 * 
 */

oroboros.data.time = {
    timestamp: function() {
        
    },
    clock: {
        current: function() {
            
        },
        format: function() {
            
        }
    },
    timer: {
        countdown: function () {
            
        },
        stopwatch: function() {
            
        }
    },
    tick: {
        tickrate: function() {
            
        },
        check: function() {
            
        },
        event: function() {
            
        }
    }
};
