/* 
 * This is the design sandbox javascript environment. This file is used to test
 * site stylistic changes before deployment.
 * This class will append it's methods to the _designer class.
 */

oroboros.groups.designer.sandbox = {};