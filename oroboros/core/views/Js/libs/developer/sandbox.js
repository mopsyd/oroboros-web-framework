/* 
 * This is the developer sandbox javascript environment. This file is used for testing
 * features before deployment.
 * This class will append it's methods to the _developer class.
 */

oroboros.groups.developer.sandbox = {};