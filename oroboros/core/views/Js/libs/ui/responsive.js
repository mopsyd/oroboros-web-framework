/* 
 * Oroboros core responsive javascript view adjustments.
 */
oroboros.ui.responsive = {
    links: {
        toIcon: function(type) {},
        toText: function(type) {}
    },
    table: {},
    form: {},
    header: {
        logo: {},
        links: {},
        userbar: {},
        widgets: {}
    },
    footer: {
        sections: {},
        widgets: {},
        credits: {}
    },
    sidebar: {
        heading: {},
        nav: {},
        content: {}
    },
    content: {
        body: {},
        column: {},
        widgets: {},
        modules: {}
    },
    init: {}
};
