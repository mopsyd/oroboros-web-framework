/* 
 * Oroboros core workspaces client side scripting.
 */

oroboros.ui.workspaces = {
    add: function(){},
    remove: function(){},
    refresh: function(){},
    rewind: function(){},
    next: function(){},
    previous: function(){},
    ui: {
        controller: function(){},
        wrapper: function(){},
        events: function(){}
    },
    logic: {
        fetch: function(){}
    },
    init: function(){}
};