/* 
 * Oroboros core browser client side validation utility.
 */

oroboros.ui.window = {
    height: function(){
        return window.innerHeight || document.body.clientHeight;
    },
    width: function(){
        return window.innerWidth || document.body.clientWidth;
    } 
};