/* 
 * Oroboros core keyboard shortcuts controller.
 */

oroboros.ui.keybind = {
    nav: {
        //use php to detect if on backend
        dashbar: {
            menunav: function(e) {
                    alert('object oriented javascript functional');
            },
            indexnav: function(e) {
                jQuery('ul.nav.dashhead h4#dashhead-file').parent().find('ul.sublinks').css('display', 'block');
                Mousetrap.bind('esc', function(e){
                    jQuery('ul.nav.dashhead h4#dashhead-file').parent().find('ul.sublinks').css('display', 'none');
                    Mousetrap.unbind('up');
                    Mousetrap.unbind('down');
                    Mousetrap.unbind('left');
                    Mousetrap.unbind('right');
                    Mousetrap.unbind('esc');
                });
                Mousetrap.bind('up', function(e){
                    //scroll up one menu item
                    });
                Mousetrap.bind('down', function(e){
                    //scroll down one menu item
                    });
                Mousetrap.bind('left', function(e){
                    //open submenu
                    });
                Mousetrap.bind('right', function(e){
                    //close submenu
                    });
                Mousetrap.bind('enter', function(e){
                    //open link
                    });
            }
        },
        dashindex: {
            menunav: {},
            indexnav: {}
        },
        //use php to detect if on frontend
        indexmenu: {
            menunav: {},
            indexnav: {}
        }
    },
    ui: {},
    user: {},
    sandbox: {}
}

Mousetrap.bind('meta+shift+m', function(e) {
    oroboros.ui.keybind.nav.dashbar.indexnav();
});