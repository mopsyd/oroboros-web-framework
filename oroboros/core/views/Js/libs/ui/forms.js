/* 
 * Oroboros core client side form handler utility.
 */

oroboros.ui.forms = {
    clear: function(form) {},
    autofill: function(form, fields, data) {},
    validate: function(form, fields, criteria, response) {}  
};