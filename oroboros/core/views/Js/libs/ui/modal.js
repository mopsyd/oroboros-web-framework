oroboros.ui.modal = {
    title: 'Modal Test',
    content: 'This is a modal dialogue test',
    footer: 'This is the modal footer',
    active: false,
    create: function() {
        if (oroboros.ui.modal.active === true) {
            oroboros.ui.modal.update();
        } else {
            var modalContent = jQuery('<div id="modal" class="lightbox"><div class="modal"><div class="header"><h2>'+oroboros.ui.modal.title+'</h2><span class="ui"><span class="_modal-remove" title="close"></span></span></div><div class="body"><p>'+oroboros.ui.modal.content+'</p></div><div class="footer"><p>'+oroboros.ui.modal.footer+'</p></div></div></div>');
            jQuery(modalContent).appendTo('span.modal-wrapper');
            oroboros.ui.modal.active = true;
            jQuery('._modal-remove').click(function(){
                oroboros.ui.modal.remove();
            });
        }
    },
    edit: function() {
        jQuery('div#modal div.modal div.header h2').html(oroboros.ui.modal.header);
        jQuery('div#modal div.modal div.body').html('<p>' + oroboros.ui.modal.body + '</p>');
        jQuery('div#modal div.modal div.footer').html('<p>' + oroboros.ui.modal.footer + '</p>');
    },
    update: function() {},
    sendform: function() {},
    remove: function() {
        jQuery('div#modal.lightbox').fadeOut().remove();
        jQuery(this).parent().fadeOut().remove();
        oroboros.ui.modal.active = false;
    }
};

//initializes the modal
jQuery(window).load(function(){
    jQuery('._modal-create').click(function(){
        oroboros.ui.modal.create();
    });
});