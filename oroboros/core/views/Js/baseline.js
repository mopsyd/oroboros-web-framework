/* 
 * Oroboros core baseline scripting.
 * This javascript document sets the base objects for the remaining classes, and 
 * requires the specific class structure required by the page, user, or other system
 * specifications.
 *  
 * All pages that render content will contain this scripting file.
 */

var oroboros = {
    data: {
        array: {
            convert: function(data, format) {}
        },
        object: {
            convert: function(data, format) {}
        },
        get: {
            add: function(key, value){
                self.location=
                    self.location.protocol+'//'
                    +self.location.host
                    +self.location.pathname+'?'+key+'='+value;
            },
            clear: function(){
                self.location=
                    self.location.protocol+'//'
                    +self.location.host
                    +self.location.pathname;
            },
            parse: function(){
                var query = self.location.search;
            }
        },
        json: {
            encode: function(data) {},
            decode: function(data) {},
            convert: function(data, format) {}
        },
        xml: {
            encode: function(data) {},
            decode: function(data) {},
            convert: function(data, format) {}
        },
        csv: {
            encode: function(data) {},
            decode: function(data) {},
            convert: function(data, format) {}
        },
        cookie: {
            test: function(){
                var cookieEnabled = (navigator.cookieEnabled) ? true : false;
                if (typeof navigator.cookieEnabled == "undefined" && !cookieEnabled) { 
                    document.cookie="testcookie";
                    cookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
                }
                return (cookieEnabled);
            },
            set: function(name, value, expires){
                var today = new Date();
                var expire = new Date();
                if (expires==null || expires==0) expires=1;
                expire.setTime(today.getTime() + 3600000*24*expires);
                document.cookie = name+"="+escape(value)
                    + ";expires="+expire.toGMTString();
            },
            remove: function(name){
                oroboros.data.cookie.set(name, '', today.getTime, -2);
            },
            read: function(name){
                var theCookie=" "+document.cookie;
                var ind=theCookie.indexOf(" "+name+"=");
                if (ind==-1) ind=theCookie.indexOf(";"+name+"=");
                if (ind==-1 || name=="") return "";
                var ind1=theCookie.indexOf(";",ind+1);
                if (ind1==-1) ind1=theCookie.length; 
                return unescape(theCookie.substring(ind+name.length+2,ind1));
            },
            search: function(name){
                var re = new RegExp('[; ]'+name+'=([^\\s;]*)');
                var sMatch = (' '+document.cookie).match(re);
                if (name && sMatch) return unescape(sMatch[1]);
                return '';
            }
        },
        soap: {}
    },
    syndication: {
        email: {},
        rss: {
            getFeed: function(source) {},
            registerFeed: function(source, format) {}
        },
        calendar: {
            register: function(calendar, destination) {},
            fetch: function(source) {},
            update: function(source) {}
        }
    },
    ui: {},
    components: {},
    modules: {},
    theme: {},
    template: {},
    page: {
        dom: {},
        content: {},
        inline: {},
        frame: {
            define: function(){},
            update: function(){},
            remove: function(){}
        }
    },
    user: {
        id: '<?= $_SESSION["user"]["login"] ; ?>',
        login: '<?= $_SESSION["user"]["loggedIn"]; ?>',
        device: '<?= $_SESSION["device"]["mode"] ; ?>',
        browser: '<?= $_SESSION["browser"]["title"] ; ?>',
        browserVersion: '<?= $_SESSION["browser"]["version"] ; ?>',
        IV: encodeURI("<? $cookie = $this->cookie->getCookie('security'); echo $cookie[0]; ?>"),
        nonce: "<?= $_SESSION['user']['nonce']; ?>",
        notifications: {
            fetch:function(){},
            update:function(){}
        },
        messages: {
            fetch: function(){},
            send: function(){}
        },
        session: {
            update: function(){},
            timeout: function(){},
            check: function(){}
        }
    },
    groups: {},
    media: {
        audio: {
            load: function(source, format, destination) {},
            play: function(source, loop) {},
            pause: function(source) {},
            stop: function(source) {},
            rewind: function(source, increment) {},
            forward: function(source, increment) {},
            volume: {
                mute: function(source) {},
                full: function(source) {},
                decrease: function(source) {},
                increase: function(source) {}
            }
        },
        video: {
            load: function(source, format, destination) {},
            play: function(source, loop) {},
            pause: function(source) {},
            stop: function(source) {},
            rewind: function(source, increment) {},
            forward: function(source, increment) {},
            volume: {
                mute: function(source) {},
                full: function(source) {},
                decrease: function(source) {},
                increase: function(source) {}
            },
            codec: {},
            resolution: {}
        },
        image: {
            define: function(source, image) {},
            swap: function(source, image) {},
            insert: function(source, image) {},
            remove: function (source) {},
            slideshow: {
                define: function(){},
                update: {
                    load: function(){},
                    remove: function(){}
                },
                rewind: function(){},
                next: function(){},
                previous: function(){},
                remove: function(){},
                menu: {},
                wrapper: {}
            },
            gallery: {
                load: {},
                define: {},
                format: {},
                source: {},
                update: {},
                ui: {}
            }
        },
        flash: {
            device: {
                checkCompatibility: function(){},
                defineFormat: function(){}
            },
            data: {},
            player: {},
            wrapper: {},
            swf: {
                validate: function(){},
                load: function(){},
                remove: function(){},
                update: function(){},
                extend: function(){},
                controls: {
                    volume: {},
                    playback: {},
                    events: {},
                    ui: {}
                }
            }
        },
        canvas: {}
    },
    baseline: {
        init: {},
        errors: {
            handle: function(description, page, line) {
                //handler function here
            },
            init: function() {
                window.onError=oroboros.baseline.errors.handle;
            }
        },
        cancelBubble: function(e) {
            var evt = e ? e:window.event;
            if (evt.stopPropagation)    evt.stopPropagation();
            if (evt.cancelBubble!=null) evt.cancelBubble = true;
        },
        event: {
            addHandler: function(elem,eventType,handler) {
                if (elem.addEventListener)
                    elem.addEventListener (eventType,handler,false);
                else if (elem.attachEvent)
                    elem.attachEvent ('on'+eventType,handler); 
            },
            removeHandler: function(elem,eventType,handler) {
                if (elem.removeEventListener) 
                    elem.removeEventListener (eventType,handler,false);
                if (elem.detachEvent)
                    elem.detachEvent ('on'+eventType,handler); 
            }
        },
        timer: {
            set: function(instance, method, duration) {},
            update: function(instance, duration) {},
            remove: function(instance) {},
            render: function(instance, destination, format) {},
            alarm: function(instance, time, event) {}
        },
        clock: {
            define: function(instance, format, destination) {},
            update: function(instance) {},
            alarm: function(instance, time, event) {}
        },
        calc: {},
        navigation: {
            back: function(){
                history.back();
            },
            forward: function(){
                history.forward();
            },
            go: function(url){
                self.location=url;
            }
        }
    }
};

    <?php
//add data elements
require __DIR__ . '/libs/data/json.js';
require __DIR__ . '/libs/data/xml.js';
require __DIR__ . '/libs/data/cookies.js';
require __DIR__ . '/libs/data/validation.js';
require __DIR__ . '/libs/data/ajax.js';
    
    
//add syndication elements
require __DIR__ . '/libs/syndication/feedreader.js';
    
//add ui elements
require __DIR__ . '/libs/ui/ui.js';
require __DIR__ . '/libs/ui/browser.js';
require __DIR__ . '/libs/ui/keyboard-shortcuts.js';
require __DIR__ . '/libs/ui/modal.js';
require __DIR__ . '/libs/ui/preferences.js';
require __DIR__ . '/libs/ui/workspaces.js';
require __DIR__ . '/libs/ui/responsive.js';
    
//add group priviledged elements
require __DIR__ . '/libs/architect/architect.js';
require __DIR__ . '/libs/architect/sandbox.js';
require __DIR__ . '/libs/owner/owner.js';
require __DIR__ . '/libs/webmaster/webmaster.js';
require __DIR__ . '/libs/operator/operator.js';
require __DIR__ . '/libs/developer/developer.js';
require __DIR__ . '/libs/developer/debug.js';
require __DIR__ . '/libs/developer/sandbox.js';
require __DIR__ . '/libs/designer/designer.js';
require __DIR__ . '/libs/designer/sandbox.js';
require __DIR__ . '/libs/admin/admin.js';
require __DIR__ . '/libs/moderator/moderator.js';
require __DIR__ . '/libs/author/author.js';
require __DIR__ . '/libs/registered/registered.js';
require __DIR__ . '/libs/visitor/visitor.js';
    
    
    ?>