<?php
/**
 * Description of Printview
 * This is view handler for designing content display as it should
 * render when printed as a standalone file or sent to a printer
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views\Design;

class Printview extends \oroboros\core\views\Browser {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
