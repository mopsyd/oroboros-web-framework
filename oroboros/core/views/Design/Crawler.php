<?php
/**
 * Description of Crawler
 * This is view handler for building page content 
 * as it should display to web crawlers
 * rather than standard site visitors
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views\Design;

class Crawler extends \oroboros\core\views\Crawler {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
