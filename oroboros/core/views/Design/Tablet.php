<?php
/**
 * Description of Tablet
 * This is view handler for designing content as it should render
 * on tablet devices
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views\Design;

class Tablet extends \oroboros\core\views\Tablet {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
