<?php
/**
 * Description of Mobile
 * This is view handler for designing content as it should be displayed
 * on mobile devices
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views\Design;

class Mobile extends \oroboros\core\views\Mobile {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
