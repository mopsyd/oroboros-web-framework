<?php
/**
 * Description of Rss
 * This is view handler for designing rss feed layouts. It provides a user interface
 * to create a template that will be applied to outgoing feed subscription content.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views;

class Rss extends \oroboros\core\views\Rss {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
