<?php
/**
 * Description of Browser
 * This is view handler for desktop browsers that are making
 * adjustments to the site in design sandbox mode. It will render a page within
 * an iframe and strip the functionality from all of the links, ajax, and
 * buttons so the site can be correctly designed without compromising security
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views\Design;

class Browser extends \oroboros\core\views\Browser {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
