<?php
/**
 * Description of Embedded
 * This is view handler for designing content as it should be rendered
 * on embedded systems.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views\Design;

class Embedded extends \oroboros\core\views\Embedded {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
