<?php
/**
 * Description of Email
 * This is view handler for designing email templates. It provides a user interface
 * to create a template that will be applied to outgoing mail.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views;

class Email extends \oroboros\core\views\Email {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
