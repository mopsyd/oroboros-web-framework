<?php
/**
 * Description of Wrapper
 * This is view handler designing how core framework features should display, 
 * if at all, when rendering a subsystem through the wrapper method.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views\Design;

class Wrapper extends \oroboros\core\views\Wrapper {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
