<?php

namespace oroboros\core\views\Site\Component\texteditor;

/**
 * Description of texteditor
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class texteditor extends \oroboros\core\libs\Render\Html {
    //permissions

    const useperm = '';
    const visperm = '';
    const previewperm = '';
    const saveperm = '';

    //asset location
    const icons = 'icons/components/texteditor/';
    const script = '';

    //text formatting
    const bold = 'bold.png';
    const italic = 'italic.png';
    const underline = 'underline.png';
    const strikethrough = 'strikethrough.png';
    const quote = 'quotes-left.png';
    const paragraph = 'pilcrow.png';
    const justify = 'paragraph-justify2.png';
    const centerjustify = 'paragraph-center2.png';
    const leftjustify = 'paragraph-left2.png';
    const rightjustify = 'paragraph-right2.png';
    const indentincrease = 'indent-increase.png';
    const indentdecrease = 'indent-decrease.png';

    //content insertion
    const addlist = 'list.png';
    const addnumlist = 'numbered-list.png';
    const addlink = 'link.png';
    const addtable = 'table.png';
    const addimg = 'image2.png';
    const addaudio = 'music.png';
    const addvideo = 'tv.png';
    const addcode = 'code.png';
    const addtemplate = 'insert-template.png';

    //post control
    const visibilitypublic = 'eye.png';
    const visibilityprivate = 'eye-blocked.png';
    const tag = 'tag.png';
    const approve = 'checkmark-circle.png';
    const cancel = 'cancel-circle.png';
    const schedule = 'clock.png';
    const save = 'disk.png';
    const delete = 'remove2.png';
    const spellcheck = 'spell-check.png';

    public function __construct() {
        parent::__construct();
    }

    public function buildObject($action, $cols = 40, $rows = 20) {
        $this->registerScript();
        $this->registerStylesheet();

        $render = array(
            'type' => 'span',
            'id' => 'text-editor-wrapper',
            'method' => 'tag',
            'content' => array(
                array(
                    'type' => 'span',
                    'method' => 'tag',
                    'class' => array('editor-previewer-wrapper'),
                    'content' => array(
                        $this->setPreviewer()
                    )
                ),
                array(
                    'type' => 'span',
                    'method' => 'tag',
                    'class' => array('text-editor-control-wrapper'),
                    'content' => array(
                        $this->setTextarea($action, $cols, $rows)
                    )
                )
            ),
            'params' => array()
        );

        return $this->build('span', array($render));
    }

    protected function registerScript() {
        
    }

    protected function registerStylesheet() {
        
    }

    protected function setTextarea($action, $cols, $rows) {
        $render = array(
            'type' => 'form',
            'method' => 'tag',
            'class' => array('text-editor-form'),
            'content' => array(
                $this->setMenu(),
                array(
                    'type' => 'textarea',
                    'params' => array(
                        'cols' => $cols,
                        'rows' => $rows
                    )
                ),
                $this->setSubmissionForm()
            ),
            'params' => array(
                'name' => 'text-editor',
                'method' => 'post',
                'action' => $action
            )
        );

        return $render;
    }

    protected function setMenu() {
        $menu = array(
            'type' => 'ul',
            'method' => 'tag',
            'class' => array('nav', 'ui-menu'),
            'content' => array(
                //format content
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::bold,
                                        'alt' => 'bold',
                                        'title' => 'bold',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.bold();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::italic,
                                        'alt' => 'italic',
                                        'title' => 'italic',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.italic();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::underline,
                                        'alt' => 'underline',
                                        'title' => 'underline',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.underline();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::strikethrough,
                                        'alt' => 'strikethrough',
                                        'title' => 'strikethrough',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.strikethrough();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::quote,
                                        'alt' => 'quote',
                                        'title' => 'quote',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.quote();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::paragraph,
                                        'alt' => 'paragraph',
                                        'title' => 'paragraph',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.paragraph();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::justify,
                                        'alt' => 'justify',
                                        'title' => 'justify',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.justify();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::leftjustify,
                                        'alt' => 'left justify',
                                        'title' => 'left justify',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.leftjustify();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::rightjustify,
                                        'alt' => 'right justify',
                                        'title' => 'right justify',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.rightjustify();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::indentincrease,
                                        'alt' => 'indentincrease',
                                        'title' => 'increase indent',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.indentincrease();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::indentdecrease,
                                        'alt' => 'indentdecrease',
                                        'title' => 'decrease indent',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.indentdecrease();',
                            ),
                        )
                    )
                ),
                //insert content
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::addlist,
                                        'alt' => 'add list',
                                        'title' => 'add list',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.addlist();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::addnumlist,
                                        'alt' => 'add numbered list',
                                        'title' => 'add numbered list',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.addnumlist();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::addlink,
                                        'alt' => 'add link',
                                        'title' => 'add link',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.addlink();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::addtable,
                                        'alt' => 'add table',
                                        'title' => 'add table',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.addtable();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::addcode,
                                        'alt' => 'add code',
                                        'title' => 'add code',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.addcode();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::addtemplate,
                                        'alt' => 'add template',
                                        'title' => 'add template',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.addtemplate();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::addimg,
                                        'alt' => 'add image',
                                        'title' => 'add image',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.addimg();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::addaudio,
                                        'alt' => 'add audio',
                                        'title' => 'add audio',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.addaudio();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::addvideo,
                                        'alt' => 'add video',
                                        'title' => 'add video',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.addvideo();',
                            ),
                        )
                    )
                ),
                //post control
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::schedule,
                                        'alt' => 'schedule',
                                        'title' => 'schedule content',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.schedule();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::spellcheck,
                                        'alt' => 'spellcheck',
                                        'title' => 'check spelling',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.spellcheck();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::save,
                                        'alt' => 'save',
                                        'title' => 'save content',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.save();',
                            ),
                        )
                    )
                ),
                array(
                    'type' => 'li',
                    'method' => 'tag',
                    'content' => array(
                        array(
                            'type' => 'a',
                            'content' => array(
                                array(
                                    'type' => 'img',
                                    'params' => array(
                                        'src' => _IMAGES . self::icons . self::visibilitypublic,
                                        'alt' => 'visibility',
                                        'title' => 'visibility: public',
                                    )
                                )
                            ),
                            'params' => array(
                                'href' => '#',
                                'onclick' => 'oroboros.components.texteditor.toggleVisibility();',
                            ),
                        )
                    )
                ),
            ),
        );
        //$this->linkbuild = !isset($this->linkbuild) ? new \oroboros\core\views\Site\Content\Browser\Menu() : $this->linkbuild;
        //$result = $this->linkbuild->listMenuRender($menu);
        return $menu;
    }

    protected function setPreviewer() {
        $render = array(
            'type' => 'div',
            'method' => 'tag',
            'class' => array('output-preview'),
            'content' => array(
                array(
                    'type' => 'pre',
                    'class' => array('preview-area')
                )
            )
        );

        return $render;
    }

    protected function setSubmissionForm() {
        $render = array(
            'type' => 'span',
            'method' => 'tag',
            'class' => array('text-editor-submit-form-wrapper'),
            'content' => array(
                array(
                    'type' => 'button',
                    'class' => array('clear-button'),
                    'content' => array(
                        array(
                            'type' => 'img',
                            'params' => array(
                                'src' => _IMAGES . self::icons . self::delete,
                                'alt' => 'clear form',
                            )
                        ),
                        array(
                            'type' => 'p',
                            'content' => 'Clear Form'
                        )
                    ),
                    'params' => array(
                        'type' => 'reset',
                        'value' => 'Clear'
                    )
                ),
                array(
                    'type' => 'button',
                    'class' => array('submit-button'),
                    'content' => array(
                        array(
                            'type' => 'img',
                            'params' => array(
                                'src' => _IMAGES . self::icons . self::approve,
                                'alt' => 'submit form',
                            )
                        ),
                        array(
                            'type' => 'p',
                            'content' => 'Submit'
                        )
                    ),
                    'params' => array(
                        'type' => 'submit',
                        'value' => 'Submit'
                    )
                ),
            )
        );

        return $render;
    }

    public function __destruct() {
        parent::__destruct();
    }

}