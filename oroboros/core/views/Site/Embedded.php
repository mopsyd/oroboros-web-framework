<?php
/**
 * Description of Embedded
 * This is the view handler for embedded systems
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views\Site;

class Embedded extends \oroboros\core\libs\Abstracts\Site\View\View {
    
    const DEVICE = 'embedded';
    
    public function __construct($package) {
        parent::__construct($package);
        $this->setDevice(self::DEVICE);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
