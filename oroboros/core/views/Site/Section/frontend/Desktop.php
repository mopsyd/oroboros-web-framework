<?php
/**
 * Description of Desktop
 * This is the view handler for desktop browsers
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views\Site\Section\frontend;

class Desktop extends \oroboros\core\views\Site\Desktop {
    
    const DEVICE = 'desktop';
    
    public function __construct($package) {
        parent::__construct($package);
        $this->setDevice(self::DEVICE);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
