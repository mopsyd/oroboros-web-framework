<?php
/**
 * Description of Node
 * This is the view handler for node content sharing
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views\Site\Section\backend;

class Node extends \oroboros\core\views\Site\Node {
    
    const DEVICE = 'node';
    
    public function __construct($package) {
        parent::__construct($package);
        $this->setDevice(self::DEVICE);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>