<?php
/**
 * Description of Tablet
 * This is the view handler for tablet devices
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views\Site\Section\backend;

class Tablet extends \oroboros\core\views\Site\Tablet {
    
    const DEVICE = 'tablet';
    
    public function __construct($package) {
        parent::__construct($package);
        $this->setDevice(self::DEVICE);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>