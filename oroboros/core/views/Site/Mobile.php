<?php
/**
 * Description of Mobile
 * This is the view handler for mobile devices
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views\Site;

class Mobile extends \oroboros\core\libs\Abstracts\Site\View\View {
    
    const DEVICE = 'mobile';
    
    public function __construct($package) {
        parent::__construct($package);
        $this->setDevice(self::DEVICE);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>