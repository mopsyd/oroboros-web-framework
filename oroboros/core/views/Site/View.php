<?php

namespace oroboros\core\views\Site;

/**
 * Description of View
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class View extends \oroboros\core\libs\Abstracts\Site\View\View {

    private $type;
    public $pagetitle = NULL;
    public $pageheading = NULL;
    public $dependencies = NULL;
    public $template = array();
    public $linkbar = array();
    public $title = NULL;
    protected $skeleton = NULL;

    const RENDERTYPE = 'HTML';
    const VIEWPATH = 'oroboros/core/views/Site';

    public function __construct($package) {
        parent::__construct($package);
        $this->browserType = $this->browser->init();
        $this->deviceType = $this->device->init();
        $this->defineType($this->deviceType['mode']);
        $this->instantiatePageHead();
        $this->instantiatePageBody();
    }
    
    public function registerBaselineAssets() {
        //$this->registerBaselineStylesheets();
        //$this->registerBaselineScripts();
    }
    
    private function registerBaselineStylesheets() {
        $this->registerStylesheet('dependencies', _BASE . 'public/css/Skeleton/' . $this->deviceType['mode'] . '/skeleton.css');
    }
    
    private function registerBaselineScripts() {
        
    }

    public function renderPage() {
        $this->buildPageData();
    }

    protected function buildPageData() {
        $this->buildPageTitle($this->package);
        $this->buildPageHeading($this->package);
        $this->buildPageDependencies($this->package);
    }

    private function instantiatePageHead() {
        $class = str_replace('/', '\\', self::VIEWPATH) . '\Head';
        $this->head = new $class($this->package, $_SESSION['user']['login']);
        $this->head->registerPageData($this->_site, $this->_subdomain, $this->_page);
        $media = new \oroboros\core\libs\Render\Media($this->package);
        $this->head->defineFavicon($media);
        $meta = new \oroboros\core\libs\Render\Meta($this->package);
        $this->head->defineMeta($meta);
        $pageTitle = $this->buildPageTitle($this->package);
        $this->head->defineTitle($pageTitle);
    }

    private function instantiatePageBody() {
        $class = str_replace('/', '\\', self::VIEWPATH . '/Content/' . ucfirst($this->deviceType['mode'])) . '\Body';
        $this->body = new $class($this->package, $_SESSION['user']['login']);
        $this->body->registerPageData($this->_site, $this->_subdomain, $this->_page);
    }

    protected function initSite() {
        
    }

    protected function initSubdomain() {
        
    }

    protected function initSubsection() {
        
    }

    protected function buildPageTitle($package) {
        $title = NULL;
        if (isset($package['site']['title'])) {
            $title .= $package['site']['title'];
        }
        if (isset($package['page']['title'])) {
            if (!is_null($title)) {
                $title .= ' - ';
            }
            $title .= $package['page']['title'];
        }
        if (isset($package['page']['description'])) {
            if (!is_null($title)) {
                $title .= ' | ';
            }
            $title .= $package['page']['description'];
        }
        return $title;
    }

    public function buildPageDependencies($package) {
        $this->buildPageStylesheets($package);
        $this->buildPageScripts($package);
        $this->buildPageMeta($package);
    }

    protected function buildPageStylesheets($package) {
        $this->_dependencies .= '<link rel="stylesheet" href="<?php echo _URL; ?>public/css/css.phtml" />' . PHP_EOL;
    }

    protected function buildPageScripts($package) {
        $this->_dependencies .= '<script type="text/javascript" src="<?php echo _URL; ?>public/js/js.phtml"></script>' . PHP_EOL;
    }

    protected function buildPageFavicon($package) {
        $this->media->init($package);
    }

    protected function buildPageMeta() {
        $meta = new \oroboros\core\libs\Render\Meta($this->package);
        $meta->init();
    }

    public function initPage($device) {
        $this->sidebar = new \oroboros\core\libs\Render\Sidebar($this->package);
        $this->getSkeleton($device['mode']);
        $this->handleModules();
        $this->handleTemplate();
        $this->handleTheme();
        $this->buildPageContent();
        $this->linkbar = $this->initLinks($this->_site['slug'], 'toplinks', $this->_subdomain, $this->_page['slug'], TRUE);
    }

    protected function defineType($type) {
        $this->initializeBaseClasses($type);
    }

    protected function initializeBaseClasses($type) {
        $this->classFactory = new \oroboros\core\libs\SITE\Patterns\classFactory();
        $this->templateFactory = '';
        $this->scriptFactory = '';
        $this->stylesheetFactory = '';
        $this->templateFactory = '';
        $this->initSite();
        $this->initSubdomain();
        $this->initSubsection();
        //$this->initPage($this->deviceType['mode']);
    }

    public function render($name, $noInclude = false) {
        if (is_readable($this->template[$name])) {
            require $this->template[$name];
        } else {
            echo 'could not find file ' . $name . '<br>';
        }
    }

    public function initBuffer() {
        $this->_output = new \oroboros\core\libs\Render\Output($this->package);
        $this->_output->_initBuffer();
    }

    public function finalizePage() {
        $render = $this->_output->cleanGet();
        $this->hooks = new \oroboros\core\libs\Render\Hooks($this->package);
        print $this->hooks->contentRender($render);
    }

    public function renderTemplate() {
        $this->handleComponents();
        $content = NULL;
        $this->initBuffer();
        $this->render('head');
        if (isset($this->skeleton['head'])) {
            print $this->skeleton['foot'];
        }
        $this->render('header');
        $this->render('body');
        $this->render('content');
        if (isset($this->controllerContent)) {
            $this->renderControllerData();
        }
        $this->render('content-close');
        $this->render('footer');
        if (isset($this->skeleton[1])) {
            print $this->skeleton[1];
        }
        $content = $this->finalizePage();
    }

    public function getSkeleton($device = 'desktop') {
        $skeleton = new \oroboros\core\libs\Render\Skeleton($this->package);
        $this->skeleton = $skeleton->fetchSkeleton($device);
    }

    protected function buildPageHeading($package) {
        $this->_pageheading = $package['page']['title'];
    }

    protected function initLinks($site, $linkset, $subdomain = NULL, $page = NULL, $userperms = FALSE, $package = NULL) {
        $this->links = new \oroboros\core\libs\Render\Links($this->package, $site, $linkset, $subdomain, $page, $userperms);
        return $this->links->siteLinks($site, $linkset, TRUE);
    }

    public function renderControllerData() {
        if (isset($this->controllerContent)) {
            $this->buildControllerContent();
        } else
            return FALSE;
    }

    public function setControllerContent($content = NULL) {
        $this->controllerContent = $content;
    }

    protected function buildControllerContent() {
        foreach ($this->controllerContent as $key => $item) {
            switch ($key) {
                case 'module':
                    //render a linkset
                    print 'rendering a module<br>';
                    break;
                case 'component':
                    //render a linkset
                    print 'rendering a component<br>';
                    break;
                case 'dashwidget':
                    //render a dashwidget
                    if (!isset($this->dashwidget)) {
                        //instantiate the dashwidget class
                        //$this->autoload->addNamespace();
                        $this->dashwidget = new \oroboros\core\views\Site\Content\Browser\Dashwidget();
                    }
                    print '<div class="dashwidgets">' . PHP_EOL;
                    foreach ($item as $widget) {
                        if (isset($widget['permission']) && $this->perms->checkPermission($_SESSION['user']['login'], $widget['permission']) == TRUE) {
                            //render the widget
                            $id = isset($widget['id']) ? $widget['id'] : NULL;
                            $class = isset($widget['class']) && is_array($widget['class']) ? $widget['class'] : NULL;
                            $class = isset($widget['class']) && is_string($widget['class']) ? array($widget['class']) : $class;
                            print $this->dashwidget->render($widget['title'], $widget['content'], $id, $class);
                        }
                    }
                    print '</div>' . PHP_EOL;
                    break;
                case 'dashmod':
                    //render a dashwidget
                    if (!isset($this->dashmod)) {
                        //instantiate the dashwidget class
                        //$this->autoload->addNamespace();
                        $this->dashmod = new \oroboros\core\views\Site\Content\Browser\Dashmod();
                    }
                    print '<div class="dashmods">' . PHP_EOL;
                    foreach ($item as $dashmod) {
                        if (isset($dashmod['permission']) && $this->perms->checkPermission($_SESSION['user']['login'], $dashmod['permission']) == TRUE) {
                            //render the widget
                            $id = isset($dashmod['id']) ? $dashmod['id'] : NULL;
                            $class = isset($dashmod['class']) && is_array($dashmod['class']) ? $dashmod['class'] : NULL;
                            $class = isset($dashmod['class']) && is_string($dashmod['class']) ? array($dashmod['class']) : $class;
                            print $this->dashmod->render($dashmod['title'], $dashmod['content'], $id, $class);
                        }
                    }
                    print '</div>' . PHP_EOL;
                    break;
                case 'content':
                    //render a dashtable
                    print '<div id="page-content">';
                    foreach ($item as $subkey => $subvalue) {
                        switch ($subvalue) {
                            case 'dashlist':
                                //render a dashlist
                                print 'rendering a dashlist<br>';
                                break;
                            case 'media':
                                //render media
                                print 'rendering media<br>';
                                break;
                            case 'form':
                                //render a form
                                print 'rendering form<br>';
                                break;
                            case 'post':
                                //render a post
                                print 'rendering post content<br>';
                                break;
                            case 'api':
                                //render api content
                                print 'rendering api content<br>';
                                break;
                            case 'custom':
                                //render custom content
                                print 'rendering custom content<br>';
                                break;
                            default:
                                //render standard HTML content
                                if (!isset($this->body)) {
                                    $this->body = new \oroboros\core\views\Site\Content\Browser\Body($this->package, $_SESSION['user']['login']);
                                }
                                $id = isset($subvalue['id']) ? $subvalue['id'] : NULL;
                                $class = isset($subvalue['class']) && is_array($subvalue['class']) ? $subvalue['class'] : NULL;
                                $class = isset($subvalue['class']) && is_string($subvalue['class']) ? array($subvalue['class']) : $class;
                                $perm = isset($subvalue['permission']) ? $subvalue['permission'] : 'view';
                                $title = isset($subvalue['title']) ? $subvalue['title'] : 'Untitled Page Section';
                                print $this->body->section($title, $subvalue['content'], $perm, $id, $class);
                                break;
                        }
                    }
                    print '</div>';
                    break;
            }
        }
    }

    protected function loadDefaultLayoutAssets($name) {
        if (is_readable(_LOCAL . _VIEWS . $this->_site['slug'] . '/' . $name . '.phtml')) {
            require _LOCAL . _VIEWS . $name . '.phtml';
        } else {
            require _CORE . _VIEWS . $name . '.phtml';
        }
    }

    protected function buildPageSkeleton() {
        
    }

    protected function handleModules() {
        
    }

    protected function handleComponents() {
        $components = new \oroboros\core\libs\Components\Components($this->package);
        $components->init();
        $components->get_components($this->_site['slug'], $this->_subdomain, $this->_page['slug']);
        foreach ($components->components as $key => $value) {
            echo $components->load_component_template($value);
        }
    }

    protected function handleTemplate() {
        $this->_template = new \oroboros\core\libs\Template\Template($this->package);
        $this->_template->init();
        $this->_template->initPageTemplate();
        $this->package['template'] = $this->template = $this->_template->finalizeTemplate();
    }

    protected function handleTheme() {
        $theme = new \oroboros\core\libs\Theme\Theme($this->package);
        $theme->init();
        $this->package['theme'] = $this->theme = $theme->get_theme($this->_page['slug'], $this->_site['slug'], $this->_subdomain);
    }

    public function __destruct() {
        parent::__destruct();
    }

}