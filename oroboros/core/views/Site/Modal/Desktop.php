<?php

/**
 * Description of Desktop
 * This is the view handler for content that displays
 * in a modal dialogue on the desktop
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Desktop extends \oroboros\core\libs\Abstracts\Site\View\View {

    public function __construct($package) {
        parent::__construct($package);
    }

    public function __destruct() {
        parent::__destruct();
    }

}

?>
