<?php

/**
 * Description of Mobile
 * This is the view handler for content that displays
 * in a modal dialogue on mobile devices
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Mobile extends \oroboros\core\libs\Abstracts\Site\View\View {

    public function __construct($package) {
        parent::__construct($package);
    }

    public function __destruct() {
        parent::__destruct();
    }

}

?>
