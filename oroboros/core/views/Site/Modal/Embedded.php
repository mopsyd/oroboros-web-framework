<?php

/**
 * Description of Embedded
 * This is the view handler for content that displays
 * in a modal dialogue on the embedded systems
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Embedded extends \oroboros\core\libs\Abstracts\Site\View\View {

    public function __construct($package) {
        parent::__construct($package);
    }

    public function __destruct() {
        parent::__destruct();
    }

}

?>
