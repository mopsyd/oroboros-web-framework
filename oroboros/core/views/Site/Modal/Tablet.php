<?php

/**
 * Description of Tablet
 * This is the view handler for content that displays
 * in a modal dialogue on tablet devices
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Tablet extends \oroboros\core\libs\Abstracts\Site\View\View {

    public function __construct($package) {
        parent::__construct($package);
    }

    public function __destruct() {
        parent::__destruct();
    }

}

?>
