<?php
/**
 * Description of Tablet
 * This is view handler for workspaces displayed on tablet devices. It creates an array of partial page content
 * in iframes which can be toggled and manipulated by the end user to provide a series 
 * of pages which function independently from one single dashboard instance.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views\Workspace;

class Tablet extends \oroboros\core\libs\Abstracts\Site\View\View {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
