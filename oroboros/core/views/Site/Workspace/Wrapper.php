<?php
/**
 * Description of Wrapper
 * This is view handler for workspaces used within the Oroboros wrapper method to create subsystem 
 * page sections that can be referenced independently from one single page load instance. 
 * It creates an array of partial page content in iframes which can be toggled and manipulated 
 * by the end user to provide a series of pages from one convenient location that each act independently
 * of each other.
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views\Workspace;

class Wrapper extends \oroboros\core\libs\Abstracts\Site\View\View {
    
    public function __construct($package) {
        parent::__construct($package);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
