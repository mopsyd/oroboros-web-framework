<?php

namespace oroboros\core\views\Site;

/**
 * Generates the HTML document head
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Head extends \oroboros\core\libs\Render\Html {
    
    private $title;
    private $favicon;
    private $meta;
    private $scripts;
    private $stylesheets;

    public function __construct($package, $user) {
        $this->package = $package;
        $this->user = $user;
        parent::__construct($package);
        $this->init();
    }
    
    public function defineTitle($title) {
        $this->title = $title;
    }
    
    public function defineFavicon($favicon) {
        $this->favicon = $favicon;
    }
    
    public function defineMeta($meta) {
        $this->meta = $meta;
    }
    
    public function defineScripts($scripts) {
        $this->scripts = $scripts;
    }
    
    public function defineStylesheets($stylesheets) {
        $this->stylesheets = $stylesheets;
    }

    protected function init() {
        if (isset($this->package['data'])) {
            $this->data = $this->package['data'];
        }
        if (isset($this->package['database'])) {
            $this->db = $this->package['database'];
        }
        if (isset($this->package['perms'])) {
            $this->perms = $this->package['perms'];
        }
    }

    public function registerPageData($site, $subdomain, $page) {
        $this->_site = $site;
        $this->_subdomain = $subdomain;
        $this->_page = $page;
    }
    
    public function setPageTitle() {
        
    }

    public function generatePageHead() {
        $render = array(
            'type' => NULL,
            'method' => 'tag',
            'content' => array(
                $this->setPageTitle(),
                $this->setMeta(),
                $this->setStylesheets(),
                $this->setScripting(),
            ),
        );
        
        return $this->build('head', $render);
    }

    public function generateFavicon() {
        
    }

    public function setMeta() {
        
    }

    public function setScripting() {
        
    }

    public function setStylesheets() {
        
    }

    public function __destruct() {
        parent::__destruct();
    }

}