<?php

namespace oroboros\core\views\Site\Content\Browser;

/**
 * Description of Form
 * Renders forms for desktop browsers
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Form extends \oroboros\core\libs\Render\Html {

    public function makeForm($name, $action, $method, $fields, $sections = FALSE, $classes = NULL, $title = NULL) {
        $render = NULL;
        $this->form = array();
        if (isset($_SESSION['form']['server-message-type']) && isset($_SESSION['form']['server-message-response'])) {
            $this->form['response'] = array(
                'type' => 'span',
                'id' => 'server-response',
                'method' => 'tag',
                'content' => $_SESSION['form']['server-message-response'],
                'class' => array($_SESSION['form']['server-message-type'])
            );
            unset($_SESSION['form']);
        }
        $this->form['nonce'] = array(
            'type' => 'input',
            'class' => array('nonce'),
            'params' => array(
                'type' => 'password',
                'name' => 'nonce',
                'value' => $_SESSION['user']['nonce'],
                'style' => 'display:none; position:fixed; left:-9999px; top:-9999px;',
            )
        );

        if ($sections == TRUE) {
            //multisection form
            foreach ($fields as $field) {
                $title = isset($field['title']) ? ' title="' . $field['title'] . '"' : NULL;
                $class = isset($field['classes']) && is_array($field['classes']) ? $field['classes'] : NULL;
                $class = isset($field['classes']) && is_string($field['classes']) ? array($field['classes']) : $class;
                $this->form[] = array(
                    'type' => 'span',
                    'id' => $title,
                    'class' => $class,
                    'content' => $this->makeFormItems($field['formItems'])
                );
            }
        } else {
            //simple form
            foreach ($fields as $key => $value) {
                $this->form[$key] = $this->$value['fieldtype']($value);
            }
        }


        $class = isset($classes) ? ' class="' . implode(' ', $classes) . '"' : NULL;
        if (isset($classes)) {
            if (is_array($classes)) {
                $this->form['class'] = $classes;
            } elseif (is_string($classes)) {
                $this->form['class'] = array($classes);
            }
        }
        $title = isset($title) ? ' title="' . $title . '"' : NULL;
        $form_id = isset($title) ? $title : NULL;
        $form_class = isset($classes) ? $classes : NULL;
        $form_params = array(
            'method' => $method,
            'action' => $action,
            'name' => $name
        );

        return $this->build('form', $this->form, 'tag', $form_id, $form_class, $form_params);
    }

    public function buildListForm($name, $action, $method, $content, $classes = NULL, $title = NULL) {
        $render = NULL;
        $this->form = array();
        if (isset($_SESSION['form']['server-message-type']) && isset($_SESSION['form']['server-message-response'])) {
            $this->form['response'] = array(
                'type' => 'span',
                'id' => 'server-response',
                'method' => 'tag',
                'content' => $_SESSION['form']['server-message-response'],
                'class' => array($_SESSION['form']['server-message-type'])
            );
            unset($_SESSION['form']);
        }
        $this->form['nonce'] = array(
            'type' => 'input',
            'class' => array('nonce'),
            'params' => array(
                'type' => 'password',
                'name' => 'nonce',
                'value' => $_SESSION['user']['nonce'],
                'style' => 'display:none; position:fixed; left:-9999px; top:-9999px;',
            )
        );

        if (!isset($this->list)) {
            $class = '\\' . __NAMESPACE__ . '\\Listrender';
            $file = _BASE . str_replace('\\', '/', $class).'.php';
            if (is_readable($file)) {
                //instantiate the form object
                require $file;
                $this->list = new $class();
            } else {
                //form class not readable, throw error
                echo 'file not found at ' . $file . '<br>';
            }
        }

        
        //var_dump($content);

        $class = isset($classes) ? ' class="' . implode(' ', $classes) . '"' : NULL;
        if (isset($classes)) {
            if (is_array($classes)) {
                $this->form['class'] = $classes;
            } elseif (is_string($classes)) {
                $this->form['class'] = array($classes);
            }
        }
        $title = isset($title) ? ' title="' . $title . '"' : NULL;
        $form_id = isset($title) ? $title : NULL;
        $form_class = isset($classes) ? $classes : NULL;
        $form_params = array(
            'method' => $method,
            'action' => $action,
            'name' => $name
        );

        return $this->build('form', $this->form, 'tag', $form_id, $form_class, $form_params);
    }
    
    public function getNonceField() {
        $result = array(
            'type' => 'input',
            'class' => array('nonce'),
            'params' => array(
                'type' => 'password',
                'name' => 'nonce',
                'value' => $_SESSION['user']['nonce'],
                'style' => 'display:none; position:fixed; left:-9999px; top:-9999px;',
            )
        );
        return $result;
    }
    
    public function getResponseField() {
        $result = array();
        if (isset($_SESSION['form']['server-message-type']) && isset($_SESSION['form']['server-message-response'])) {
            $result = array(
                'type' => 'span',
                'id' => 'server-response',
                'method' => 'tag',
                'content' => $_SESSION['form']['server-message-response'],
                'class' => array($_SESSION['form']['server-message-type'])
            );
            unset($_SESSION['form']);
        }
        if (!empty($result)) {
            return $result;
        } else return NULL;
    }

    protected function makeFormItems($data) {
        $content = array();
        foreach ($data as $formsection) {
            $content[] = $this->{$formsection['fieldtype']}($formsection);
        }
        return $content;
    }

    protected function makeFormSection($fields) {
        $content = array(
            'type' => 'span',
            'class' => array('form-item'),
            'content' => array()
        );
        foreach ($fields as $key => $value) {
            $content['content'][$key] = $this->$value['fieldtype']($value);
        }
        return $content;
    }

    protected function input($data) {
        $content = array(
            'type' => 'input',
            'method' => 'short'
        );
        $title = isset($data['title']) ? $data['title'] : NULL;
        $name = isset($data['name']) ? $data['name'] : NULL;
        $class = isset($data['classes']) && is_array($data['classes']) ? $classes : NULL;
        $class = isset($data['classes']) && is_string($data['classes']) ? array($data['classes']) : $class;
        $class = isset($data['classes']) ? $classes : NULL;
        $value = isset($data['value']) ? $data['value'] : NULL;
        $label = isset($data['label']) ? $data['label'] : NULL;
        $checked = isset($data['checked']) ? $data['checked'] : NULL;
        $attributes = isset($data['attributes']) ? $data['attributes'] : NULL;
        if (isset($title)) {
            $content['id'] = $title;
        }
        if (isset($name)) {
            $content['params']['name'] = $name;
        }
        if (isset($class)) {
            $content['class'] = $class;
        }
        if (isset($value)) {
            $content['params']['value'] = $value;
        }
        if (isset($checked)) {
            $content['params']['checked'] = $checked;
        }
        if (isset($attributes)) {
            foreach ($attributes as $key => $value) {
                $content['params'][$key] = $value;
            }
        }
        $type = isset($data['type']) ? $data['type'] : 'text';
        $content['params']['type'] = $type;
        $render = array(
            'type' => 'span',
            'class' => array('form-item'),
            'content' => array()
        );
        if (isset($label)) {
            $label = $this->label($label, $content['params']['name'], 1);
            foreach ($label as $item) {
                $render['content'][] = $item;
            }
        }
        $render['content'][] = $content;
        if (isset($data['text'])) {
            $render['content'][] = array(
                'type' => 'p',
                'class' => array('formtext'),
                'content' => $data['text']
            );
        }
        $content = $render;


        return $content;
    }

    protected function button($data) {
        $label = isset($data['label']) ? $data['label'] : NULL;
        $content = array(
            'type' => 'button'
        );
        if (isset($data['title'])) {
            $content['id'] = $title;
        }
        if (isset($data['name'])) {
            $content['params']['name'] = $name;
        }
        if (isset($data['class'])) {
            $content['class'] = is_array($data['class']) ? $data['class'] : array($data['class']);
        }
        if (isset($data['action'])) {
            $content['params']['action'] = $action;
        }
        if (isset($data['value'])) {
            $content['params']['value'] = $data['value'];
        }
        if (isset($attributes)) {
            foreach ($attributes as $key => $value) {
                $content['params'][$key] = $value;
            }
        }
        $render = array(
            'type' => 'span',
            'class' => array('form-item'),
            'content' => array()
        );
        if (isset($label)) {
            $label = $this->label($label, $content['params']['name'], 1);
            foreach ($label as $item) {
                $render['content'][] = $item;
            }
        }
        $render['content'][] = $content;
        if (isset($data['text'])) {
            $render['content'][] = array(
                'type' => 'p',
                'class' => array('formtext'),
                'content' => $data['text']
            );
        }
        $content = $render;
        return $content;
    }

    protected function textarea($data) {
        $label = isset($data['label']) ? $data['label'] : NULL;
        $content = array(
            'type' => 'textarea'
        );







        $render = array(
            'type' => 'span',
            'class' => array('form-item'),
            'content' => array()
        );
        if (isset($label)) {
            $label = $this->label($label, $content['params']['name'], 1);
            foreach ($label as $item) {
                $render['content'][] = $item;
            }
        }
        $render['content'][] = $content;
        if (isset($data['text'])) {
            $render['content'][] = array(
                'type' => 'p',
                'class' => array('formtext'),
                'content' => $data['text']
            );
        }
        $content = $render;

        $title = isset($data['title']) ? ' title="' . $data['title'] . '"' : NULL;
        $name = isset($data['name']) ? ' name="' . $data['name'] . '"' : NULL;
        $rows = isset($data['rows']) ? ' rows="' . $data['rows'] . '"' : NULL;
        $columns = isset($data['columns']) ? ' cols="' . $data['columns'] . '"' : NULL;
        $class = isset($data['classes']) ? ' class="' . implode(' ', $classes) . '"' : NULL;
        $value = isset($data['value']) ? ' value="' . $data['value'] . '"' : NULL;
        $label = isset($data['label']) ? $this->label($data['label'], $data['name']) : NULL;
        $attributes = isset($data['attributes']) ? ' ' . implode(' ', $data['attributes']) . '"' : NULL;
        $render = $label . '<textarea' . $rows . $columns . $name . $class . $attributes . ' >' . $value . '</textarea>' . PHP_EOL;
        return $render;
    }

    protected function select($data) {
        $label = isset($data['label']) ? $data['label'] : NULL;
        $content = array(
            'type' => 'select',
            'params' => array()
        );
        if (isset($data['title'])) {
            $content['id'] = $data['title'];
        }
        if (isset($data['name'])) {
            $content['params']['name'] = $data['name'];
        }
        if (isset($data['classes'])) {
            if (is_array($data['classes'])) {
                $content['class'] = $data['classes'];
            } elseif (is_string($data['classes'])) {
                $content = array($data['classes']);
            }
        }
        if (isset($data['attributes'])) {
            foreach ($data['attributes'] as $key => $value) {
                $content['params'][$key] = $value;
            }
        }

        foreach ($data['options'] as $option) {
            $content['content'][] = array(
                'type' => 'option',
                'content' => $option['text'],
                'params' => array(
                    'value' => $option['value']
                )
            );
        }
        $render = array(
            'type' => 'span',
            'class' => array('form-item'),
            'content' => array()
        );
        if (isset($label)) {
            $label = $this->label($label, $content['params']['name'], 1);
            foreach ($label as $item) {
                $render['content'][] = $item;
            }
        }
        $render['content'][] = $content;
        if (isset($data['text'])) {
            $render['content'][] = array(
                'type' => 'p',
                'class' => array('formtext'),
                'content' => $data['text']
            );
        }
        $content = $render;

        return $content;
    }

    protected function label($data, $rel, $break = FALSE) {
        $content = array(
            'type' => 'label',
            'params' => array(
                'for' => $rel
            ),
            'content' => $data['value']
        );
        if (isset($data['title'])) {
            $content['title'] = $data['title'];
        }
        if (isset($data['classes'])) {
            if (is_array($data['classes'])) {
                $content['class'] = $data['classes'];
            } elseif (is_string($data['classes'])) {
                $content['class'] = array($data['classes']);
            }
        }

        if ($break == TRUE) {
            //create a line break after the label
            $render = array(
                $content,
                array(
                    'type' => 'br',
                    'method' => 'short'
                )
            );
            return $render;
        } else {
            return $content;
        }
    }

    public function __destruct() {
        ;
    }

}