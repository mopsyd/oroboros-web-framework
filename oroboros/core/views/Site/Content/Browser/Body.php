<?php

namespace oroboros\core\views\Site\Content\Browser;

/**
 * Description of Body
 * Builds the page body for desktop browsers
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Body extends \oroboros\core\libs\Render\Html {

    public function __construct($package, $user) {
        $this->package = $package;
        $this->user = $user;
        parent::__construct();
        $this->init();
    }
    
    public function registerPageData($site, $subdomain, $page) {
        $this->_site = $site;
        $this->_subdomain = $subdomain;
        $this->_page = $page;
    }

    protected function init() {
        if (isset($this->package['data'])) {
            $this->data = $this->package['data'];
        }
        if (isset($this->package['database'])) {
            $this->db = $this->package['database'];
        }
        if (isset($this->package['perms'])) {
            $this->perms = $this->package['perms'];
        }
    }

    public function section($title, $content, $permission = 'view', $id = NULL, $classes = NULL) {
        if ($this->perms->checkPermission($this->user, $permission) == TRUE) {
            //render content
            $class = array('page-section');
            if (isset($classes) && is_array($classes)) {
                foreach ($classes as $item) {
                    $class[] = $item;
                }
            } elseif (isset($classes) && is_string($classes)) {
                $class[] = $classes;
            }
            $render = array(
                'heading' => array(
                    'type' => 'span',
                    'class' => array('section-head'),
                    'content' => array(
                        array(
                            'type' => 'h3',
                            'content' => $title
                        )
                    )
                ),
                'body' => array(
                    'type' => 'span',
                    'class' => array('section-content'),
                    'content' => $content
                ),
                'footer' => array(
                    'type' => 'span',
                    'method' => 'short',
                    'class' => array('section-end')
                )
            );
            return $this->build('span', $render, 'tag', $id, $class);
        } else {
            return NULL;
        }
    }

}