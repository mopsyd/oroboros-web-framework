<?php

namespace oroboros\core\views\Site\Content\Browser;

/**
 * Description of Messages
 * Handles creation of drop down menus for desktop browsers
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Menu extends \oroboros\core\libs\Render\Html {

    public function menu($content, $id = NULL, $class = NULL, $toplevel = TRUE) {
        $menu = array(
            'type' => 'ul',
            'class' => $this->setClass($class, 'nav'),
            'content' => $this->buildMenu($content['content']['linkset'], $toplevel),
            'params' => isset($content['params']) ? $content['params'] : NULL
        );
        if (isset($id)) {
            $menu['id'] = $id;
        }
        return $this->build($menu['type'], $menu['content'], 'tag', isset($menu['id']) ? $menu['id'] : NULL, $menu['class'], isset($menu['params']) ? $menu['params'] : NULL);
    }

    protected function buildMenu($content, $toplevel = FALSE) {
        $render = array();
        if (is_array($content) && isset($content[0])) {
            //render full list of links
            foreach ($content as $item) {
                $render[] = $this->buildMenu($item, $toplevel);
            }
        } elseif (is_array($content) && !isset($content[0])) {
            //render individual item
            $render = array(
                'type' => 'li',
                'class' => $this->setClass(isset($content['class']) ? $content['class'] : NULL, $toplevel != FALSE ? 'toplink' : 'sublink'),
            );
            if (isset($content['title'])) {
                $render['id'] = $content['title'];
            }
            if (isset($content['class'])) {
                $render['class'] = $this->setClass($content['class']);
            }
            if (isset($content['permission'])) {
                $render['permission'] = $content['permission'];
            }
            if (isset($content['link'])) {
                $render['content'][] = $this->setLink($content['link'], $content, isset($content['linkset']) ? $content['linkset'] : 'sublink', isset($content['link_type']) ? $content['link_type'] : 'internal', isset($content['subtype']) ? $content['subtype'] : 'http', $toplevel);
            }
            if (isset($content['submenu'])) {
                //$render['content'][] = $this->buildMenu($content['submenu'], isset($content['submenu']['title']) ? $content['submenu']['title'] : NULL, isset($content['submenu']['class']) ? $content['submenu']['class'] : NULL, 0);
                $render['content'][] = $this->menu($content['submenu'], isset($content['submenu']['id']) ? $content['submenu']['id'] : NULL, isset($content['submenu']['class']) ? $content['submenu']['class'] : NULL, 0);
            }
            if (isset($content['icon'])) {
                //adds an image icon to the link or menu
                $render['content'][] = $this->setIcon($content['icon']);
            }
            if (isset($content['title'])) {
                //adds an image icon to the link or menu
                $render['content'][] = $this->setTitle($content['title']);
            }
        }
        return $render;
    }

    protected function setClass($class = NULL, $type = NULL) {
        $type = isset($type) ? $type : 'sublink';
        $render = array();
        switch ($type) {
            case 'nav':
                //sets the base level navigation class
                $render[] = 'nav';
                break;
            case 'toplink':
                //sets the base level navigation class
                $render[] = 'toplink';
                break;
            case 'sublink':
                //sets the sublink class
                $render[] = 'sublink';
                break;
            case 'menu':
                //sets the item as a top level category
                $render[] = 'menu';
                break;
            case 'submenu':
                //sets the item as a submenu
                $render[] = 'submenu';
                break;
            case 'icon':
                //sets the item as a link icon
                $render[] = 'menu-icon';
                break;
            case 'title':
                //sets the item as a link menu title
                $render[] = 'menu-title';
                break;
            case 'subtitle':
                //sets the item as a link title
                $render[] = 'link-title';
                break;
        }
        if (isset($class) && is_array($class)) {
            foreach ($class as $item) {
                $render[] = $item;
            }
        } elseif (isset($class) && is_string($class)) {
            $render[] = $class;
        }
        return $render;
    }

    protected function setIcon($content) {
        $render = array(
            'type' => 'span',
            'class' => array('menu-icon'),
            'content' => array(
                array(
                    'type' => 'img',
                    'method' => 'short',
                    'params' => array(
                        'src' => $content['image'],
                        'alt' => isset($content['alt']) ? $content['alt'] : array_pop(explode('/', $content['image']))
                    )
                )
            )
        );
        return $render;
    }

    protected function setTitle($content, $menuTitle = FALSE) {
        $class = $this->setClass((isset($content['class']) ? $content['class'] : NULL), ($menuTitle == TRUE ? 'title' : 'subtitle'));
        $render = array(
            'type' => 'span',
            'class' => $class,
            'content' => array(
                array(
                    'type' => isset($content['type']) ? $content['type'] : 'p',
                    'content' => $content['content']
                )
            ),
        );
        return $render;
    }

    protected function setLink($link, $content, $linkset = 'sublink', $type = 'internal', $subtype = 'http', $toplevel) {

        $render = array(
            'type' => 'a',
            'class' => $this->setClass(isset($content['class']) ? $content['class'] : NULL, $linkset),
            'params' => array()
        );
        switch ($type) {
            case 'internal':
                //renders an internal link
                $link = $link == '#' ? $link : _URL . $link;
                if (!isset($content['title']) && $link == '#') {
                    //echo 'no title provided<br>';
                    $link = NULL;
                }
                break;
            case 'external':
                //renders an external link
                switch ($subtype) {
                    case 'http':
                        //renders a standard http link
                        $link = 'http://' . $link;
                        break;
                    case 'https':
                        //renders a secure http link
                        $link = 'https://' . $link;
                        break;
                    case 'ftp':
                        //renders a file transfer protocol link
                        $link = 'ftp://' . $link;
                        break;
                    case 'ssh':
                        //renders a secure shell link
                        $link = 'ssh://' . $link;
                        break;
                }
                break;
            case 'script':
                //renders link as a script action
                if (isset($content['namespace'])) {
                    $prefix = NULL;
                    if (is_array($content['namespace'])) {
                        foreach ($content['namespace'] as $item) {
                            $prefix .= $item . '.';
                        }
                    } else {
                        $prefix = $content['namespace'] . '.';
                    }
                }
                $link = $prefix . $link . '(';
                $link .= isset($content['params']) && is_string($content['params']) ? $content['params'] : (isset($content['params']) && is_array($content['params']) ? implode(',', $content['params']) : NULL);
                $link .= ')';
                break;
        }
        if ($type == 'script') {
            $render['params']['onclick'] = $link;
        } elseif ($link == NULL) {
            $render['params'] = NULL;
        } else {
            $render['params']['href'] = $link;
        }
        if (is_array($content['content'])) {
            //recursively render link content
            $subcontent = array();
            foreach ($content['content'] as $key => $value) {
                switch ($key) {
                    case 'icon':
                        //sets the link icon
                        $subcontent[] = $this->setIcon($content['content'][$key]);
                        break;
                    case 'title':
                        //sets the display title of the link
                        $subcontent[] = $this->setTitle($content['content'][$key]);
                        break;
                    case 'linkset':
                        //builds a sublink set within the link
                        $check = $subcontent[] = array(
                            'type' => 'ul',
                            'class' => $this->setClass(NULL, 'submenu'),
                            'content' => $this->buildMenu($content['content'][$key], isset($content['content'][$key]['id']) ? $content['content'][$key]['id'] : NULL, $this->setClass(isset($content['content'][$key]['class']) ? $content['content'][$key]['class'] : NULL, 'submenu'), 0)
                        );
                        //var_dump($check);
                        break;
                    default:
                        //builds html content
                        $subcontent[] = $this->build($content[$key]['type'], $value);
                        break;
                }
            }
            $render['content'] = $subcontent;
        } elseif (is_string($render['content'])) {
            //render displayed value of link
            $render['content'] = $content['content'];
        }
        return $render;
    }

    /* -------- takin a mulligan -------- */

    public function listMenuRender($content, $id = NULL, $class = NULL, $toplevel = TRUE) {
        $render = $toplevel == TRUE ? $this->makeBaseList($content, $id, $class) : $this->makeLinkSubmenu($content, $id, $class);
        return $render;
    }

    protected function makeBaseList($content, $id = NULL, $class = NULL, $toplevel = TRUE) {
        $menu = array(
            'type' => 'ul',
            'class' => $this->setClass($class, 'nav'),
            'content' => array(),
            'params' => isset($content['params']) ? $content['params'] : NULL
        );
        foreach ($content['content']['linkset'] as $key => $value) {
            $menu['content'][] = $this->makeListElement($value, isset($value['id']) ? $value['id'] : NULL, isset($value['class']) ? $value['class'] : NULL, $toplevel);
        }
        if (isset($id)) {
            $menu['id'] = $id;
        }
        return $this->build($menu['type'], $menu['content'], 'tag', isset($menu['id']) ? $menu['id'] : NULL, $menu['class'], isset($menu['params']) ? $menu['params'] : NULL);
    }

    protected function makeListElement($content, $id = NULL, $class = NULL, $toplevel = FALSE) {
        $render = array(
            'type' => 'li',
            'permission' => isset($content['permission']) ? $content['permission'] : 'view',
            'content' => array()
        );

        if (isset($content['link'])) {
            //render the link
            $render['content'][] = $this->makeLink($content['link'], isset($content['content']) ? $content['content'] : NULL, $toplevel);
        }
        
        if (isset($content['content']['linkset']) && $content['content']['linkset'] != FALSE) {
            //render the linkset
            $render['content'][] = $this->makeLinkSubmenu($content['content']['linkset'], isset($content['content']['linkset']['id']) ? $content['content']['linkset']['id'] : NULL, isset($content['content']['linkset']['class']) ? $content['content']['linkset']['class'] : NULL);
        }

        return $render;
    }

    protected function makeLink($link = NULL, $content = NULL, $toplevel = FALSE) {
        $render = array(
            'type' => 'a',
            'permission' => isset($content['permission']) ? $content['permission'] : 'view',
            'content' => array(),
            'params' => array(
                'href' => isset($link) ? $link : '#'
            )
        );

        if (isset($content['icon'])) {
            $render['content'][] = $this->makeLinkIcon($content['icon']['image'], isset($content['icon']['class']) ? $content['icon']['class'] : NULL);
        }
        if (isset($content['title'])) {
            $render['content'][] = $this->makeLinkTitle($content['title'], $toplevel);
        } elseif (!isset($content['icon']) && !isset($content['title'])) {
            
        }

        return $render;
    }

    protected function makeLinkIcon($icon, $class = NULL) {
        $render = array(
            'type' => 'span',
            'class' => array('menu-icon'),
            'content' => array(
                array(
                    'type' => 'img',
                    'method' => 'short',
                    'params' => array(
                        'src' => $icon,
                        'alt' => isset($content['alt']) ? $content['alt'] : array_pop(explode('/', $icon))
                    )
                )
            )
        );
        return $render;
    }

    protected function makeLinkTitle($content, $menuTitle = FALSE) {
        $class = $this->setClass((isset($content['class']) ? $content['class'] : NULL), ($menuTitle == TRUE ? 'title' : 'subtitle'));
        $render = array(
            'type' => 'span',
            'class' => $class,
            'content' => array(
                array(
                    'type' => $menuTitle == TRUE ? 'h4' : 'p',
                    'content' => $content['content']
                )
            ),
        );
        return $render;
    }

    protected function makeLinkSubmenu($content, $id = NULL, $class = NULL) {
        $render = array(
            'type' => 'ul',
            'permission' => isset($content['permission']) ? $content['permission'] : 'view',
            'content' => array()
        );
        foreach ($content as $key => $value) {
            $render['content'][] = $this->makeListElement($value, isset($value['id']) ? $value['id'] : NULL, isset($value['class']) ? $value['class'] : NULL, FALSE);
        }
        
        return $render;
    }

}