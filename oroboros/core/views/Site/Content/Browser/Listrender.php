<?php

namespace oroboros\core\views\Site\Content\Browser;

/**
 * Description of Listrender
 * Handles list rendering for desktop browsers
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Listrender extends \oroboros\core\libs\Render\Html {

    public function render($content, $type = NULL, $id = NULL, $classes = NULL) {
        $class = isset($classes) && is_array($classes) ? $classes : NULL;
        $class = isset($classes) && is_string($classes) ? array($classes) : $class;
        switch ($type) {
            case 'linkset':
                //render a linkset list
                $render = $this->linksetList($content, $id, $classes);
                break;
            case 'content':
                //render a form list
                $render = $this->contentList($content, $id, $classes);
                break;
            case 'simple':
                //render a form list
                $render = $this->simpleList($content, $id, $classes);
                break;
            default:
                //render a simple list
                $render = $this->simpleList($content, $id, $classes);
                break;
        }
        return $this->build($render['type'], $render['content'], 'tag', $id, $class, isset($content['params']) ? $content['params'] : NULL);
    }

    protected function contentList($content, $id = NULL, $classes = NULL) {
        
    }

    protected function linksetList($content, $id = NULL, $classes = NULL) {
        if (!isset($this->links)) {
            //instantiate the links class
            $this->links = new \oroboros\core\views\Site\Content\Browser\Links();
        }
    }

    protected function simpleList($content, $id = NULL, $classes = NULL, $type = 'ul') {
        $class = isset($classes) && is_array($classes) ? $classes : NULL;
        $class = isset($classes) && is_string($classes) ? array($classes) : $class;
        $render = array(
            'type' => $type,
            'method' => 'tag',
            'content' => array()
        );
        if (isset($id)) {
            $render['id'] = $id;
        }
        if (isset($class)) {
            $render['class'] = $class;
        }
        foreach ($content as $item) {
            $render['content'][] = $this->listItem($item['content'], isset($item['id']) ? $item['id'] : NULL, isset($item['class']) ? $item['class'] : NULL);
        }
        return $render;
    }

    public function listItem($content, $id = NULL, $classes = NULL) {
        $class = isset($classes) && is_array($classes) ? $classes : NULL;
        $class = isset($classes) && is_string($classes) ? array($classes) : $class;
        $render = array(
            'type' => 'li',
            'method' => 'tag',
            'content' => array()
        );
        if (isset($id)) {
            $render['id'] = $id;
        }
        if (isset($class)) {
            $render['class'] = $class;
        }
        if (is_array($content)) {
            $item['type'] = isset($content['type']) ? $content['type'] : 'p';
            $item['id'] = isset($content['id']) ? $content['id'] : NULL;
            $item['class'] = isset($content['class']) && is_array($content['class']) ? $content['class'] : NULL;
            $item['class'] = isset($content['class']) && is_string($content['class']) ? array($content['class']) : $item['class'];
            $item['method'] = isset($content['method']) ? $content['method'] : 'tag';
            $item['content'] = isset($content['content']) ? $content['content'] : NULL;
            if (isset($content['params'])) {
                foreach ($item['params'] as $subkey => $subvalue) {
                    $item['params'][$subkey] = $subvalue;
                }

                $render['content'] = $item;
            }
        } else {
            $render['content'] = $content;
        }

        return $render;
    }

}