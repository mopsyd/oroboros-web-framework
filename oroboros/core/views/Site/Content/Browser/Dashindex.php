<?php

namespace oroboros\core\views\Site\Content\Browser;

/**
 * Renders the dashboard index menu
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Dashindex extends \oroboros\core\libs\Render\Html {

    protected $links = NULL;
    protected $linksalso = 'boogity';

    public function __construct() {
        parent::__construct();
        $this->links = new \oroboros\core\views\Site\Content\Browser\Menu();
    }

    public function dashIndex($title, $content, $id = 'dash-index', $class = NULL) {
        $render = array(
            'type' => 'ul',
            'class' => array(),
            'content' => array(
                $this->getHeader($title),
                $this->getMenu($content),
                $this->getUI()
            )
        );
        return $this->build('div', array($render), 'tag', $id);
    }

    protected function getHeader($title) {
        $render = array(
            'type' => 'li',
            'class' => array('dashindex-title'),
            'content' => array(
                array(
                    'type' => 'h2',
                    'content' => $title
                )
            ),
        );

        return $render;
    }

    protected function getMenu($content) {
        $render = $this->links->listMenuRender($content);
        return array(
            'type' => 'li',
            'content' => $render
            );
    }

    protected function getUI() {
        $render = array(
            'type' => 'li',
            'id' => 'dashindex-ui',
            'content' => array(
                array(
                    'type' => 'span',
                    'content' => array(
                        array(
                            'type' => 'span',
                            'class' => array('minimize')
                        ),
                    )
                ),
            )
        );

        return $render;
    }

    public function __destruct() {
        parent::__destruct();
    }

}