<?php

namespace oroboros\core\views\Site\Content\Browser;

/**
 * Description of Dashwidget
 * Handles Dashwidgets for desktop browsers
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Dashwidget extends \oroboros\core\libs\Render\Html {
    
    public function __construct() {
        ;
    }

    protected function init() {
        
    }

    public function render($title, $content, $id = NULL, $classes = NULL) {
        $this->dashwidget = array(
            'header' => array(
                'type' => 'div',
                'class' => array('heading'),
                'content' => array(
                    array(
                        'type' => 'h4',
                        'content' => $title
                    ),
                    array(
                        'type' => 'span',
                        'method' => 'short',
                        'class' => array('ui')
                    )
                )
            ),
            'wrapper' => array(
                'type' => 'div',
                'class' => array('wrapper'),
                'content' => array(
                    array(
                        'type' => 'div',
                        'class' => array('body'),
                        'content' => $content['body']
                    ),
                )
            ),
            'footer' => array(
                'type' => 'div',
                'class' => array('footer'),
                'content' => array(
                    array(
                        'type' => 'span',
                        'class' => array('wrapper'),
                        'content' => $content['footer']
                    )
                )
            )
        );
        if (isset($classes) && is_array($classes)) {
            $class = array('dashwidget');
            foreach ($classes as $i) {
                $class[] = $i;
            }
        }
        if (!isset($class)) {
            $class = array('dashwidget');
            if (isset($classes) && is_string($classes)) {
                $class[] = $classes;
            }
        }
        $render = $this->build('div', $this->dashwidget, 'tag', $id, $class);
        return $render;
    }

    public function __destruct() {
        ;
    }
}