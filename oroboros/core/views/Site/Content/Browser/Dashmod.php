<?php

namespace oroboros\core\views\Site\Content\Browser;

/**
 * Description of Dashmod
 * Handles dashmod rendering for desktop browsers
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Dashmod extends \oroboros\core\libs\Render\Html {

    public function __construct() {
        ;
    }

    protected function init() {
        
    }

    public function render($title, $content, $id = NULL, $classes = NULL) {
        $this->dashmod = array(
            'header' => array(
                'type' => 'div',
                'class' => array('heading'),
                'content' => array(
                    array(
                        'type' => 'h2',
                        'content' => $title
                    ),
                    array(
                        'type' => 'span',
                        'method' => 'short',
                        'class' => 'controlbox'
                    )
                )
            ),
            'wrapper' => array(
                'type' => 'span',
                'class' => array('wrapper'),
                'content' => array(
                    array(
                        'type' => 'div',
                        'class' => array('body'),
                        'content' => $content['body']
                    ),
                )
            ),
            'footer' => array(
                'type' => 'div',
                'class' => array('footer'),
                'content' => array(
                    array(
                        'type' => 'span',
                        'class' => array('wrapper'),
                        'content' => $content['footer']
                    )
                )
            )
        );
        if (isset($classes) && is_array($classes)) {
            $class = array('dashmod');
            foreach ($classes as $i) {
                $class[] = $i;
            }
        } elseif (isset($classes) && is_string($classes)) {
            $class = array('dashmod', $classes);
        }
        if (!isset($class)) {
            $class = array('dashmod');
            if (isset($classes) && is_string($classes)) {
                $class[] = $classes;
            }
        }
        $render = $this->build('div', $this->dashmod, 'tag', $id, $class);
        return $render;
    }

    public function __destruct() {
        ;
    }

}