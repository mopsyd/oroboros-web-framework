<?php

namespace oroboros\core\views\Site;

/**
 * Generates the HTML document body
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
class Body extends \oroboros\core\libs\Render\Html {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}