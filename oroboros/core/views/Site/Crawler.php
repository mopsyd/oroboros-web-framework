<?php
/**
 * Description of Crawler
 * This is the view handler for web crawlers
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views\Site;

class Crawler extends \oroboros\core\libs\Abstracts\Site\View\View {
    
    const DEVICE = 'crawler';
    
    public function __construct($package) {
        parent::__construct($package);
        $this->setDevice(self::DEVICE);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
