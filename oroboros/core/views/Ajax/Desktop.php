<?php
/**
 * Description of Desktop
 * This is the view handler for desktop browsers
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views\Ajax;

class Desktop extends \oroboros\core\libs\Abstracts\Site\View\View {
    
    const DEVICE = 'desktop';
    
    public function __construct($package) {
        parent::__construct($package);
        $this->setDevice(self::DEVICE);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>
