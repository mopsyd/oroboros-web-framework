<?php
/**
 * Description of Node
 * This is the view handler for node content sharing
 *
 * @author Brian Dayhoff <brian@mopsyd.me>
 */
namespace oroboros\core\views\Ajax;

class Node extends \oroboros\core\libs\Abstracts\Site\View\View {
    
    const DEVICE = 'node';
    
    public function __construct($package) {
        parent::__construct($package);
        $this->setDevice(self::DEVICE);
    }
    
    public function __destruct() {
        parent::__destruct();
    }
}

?>