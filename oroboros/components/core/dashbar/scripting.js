/* 
 * Javascript for the dashbar
 */


function json_parse(array) {
    return jQuery.parseJSON(array);
}

function ajax(event,xhrItem,xhrMethod) {
            //console.log('xhrItem: '+JSON.stringify(xhrItem, null, 4) );
            jQuery.ajax({
                    url: '<?php echo _CON._AJAX; ?>',
                    type: xhrMethod,
                    data: xhrItem,
                    success: function(data, status) {
                        if(data == "ok") {
                            console.log('success');
                            console.log('response: '+data);
                        }
                    },
                    error: function(xhr, desc, err) {

                    }
                });
        }
        
        function ajax_encoder(params) {
        params = params || null;
        var data={};
        data.formid = jQuery(this).attr('name');
        data.userid = '<?php if(isset($_SESSION['userid'])){echo $_SESSION['userid'];}else{echo 'visitor';}?>';
        if (params != null) {
            for(i=0;i<params.length;i++){
                data.i = params.i;
            }
        }
        //console.log('ajax_encode: '+JSON.stringify(data, null, 4));
        return data;
        }
        
function dashtoggle(event){
        if(jQuery(event).children('img').attr('src')=='http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userbar-close.png' || jQuery(event).children('img').attr('src')=='http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userbar-close-ro.png') {
                jQuery(event).children('img').attr('src', 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userbar-open.png');
                $( "#component-controllers img" ).fadeTo( 200 , 0.3, function() {
                    // Animation complete.
                });
                jQuery('.dashbar-spacer').animate({
                    height:'0px'
                }, 
                {
                    duration:250,
                    easing:'swing'
                });
                jQuery('#dashbar').animate({
                    top:'-55px'
                }, 
                {
                    duration:250,
                    easing:'swing',
                    complete:function() {
                        jQuery.ajax({
                            url: '<?php echo _CON._AJAX; ?>',
                            type: 'post',
                            data: {'userid':'<?php if(isset($_SESSION['userid'])){echo $_SESSION['userid'];}else{echo 'visitor';}?>', 'action':'toggleDashbar', 'val': ''},
                            success: function(data) {
                                    console.log(data);
                            }
                        });
                    }
                });
                jQuery('#component-controllers img').animate({
                    height:'25px',
                    width:'25px'
                },
                {
                    duration:250,
                    easing:'swing'
                });
            } else if(jQuery(event).children('img').attr('src')=='http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userbar-open.png' || jQuery(event).children('img').attr('src')=='http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userbar-open-ro.png') {
                if (jQuery(event).children('img').attr('src')=='http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userbar-open.png') {
                    jQuery(event).children('img').attr('src', 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userbar-close.png');
                } else {
                    jQuery(event).children('img').attr('src', 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userbar-close-ro.png');
                }
                $( "#component-controllers img" ).fadeTo( 200 , 1, function() {
                    // Animation complete.
                });
                jQuery('.dashbar-spacer').animate({
                    height:'50px'
                }, 
                {
                    duration:250,
                    easing:'swing'
                });
                jQuery('#dashbar').animate({
                    top:'0px'
                    
                }, 
                {
                    duration:250,
                    easing:'swing',
                    complete:function() {
                        jQuery.ajax({
                            url: '<?php echo _CON._AJAX; ?>',
                            type: 'post',
                            data: {'userid':'<?php if(isset($_SESSION['userid'])){echo $_SESSION['userid'];}else{echo 'visitor';}?>', 'action':'toggleDashbar', 'val': ''},
                            success: function(data) {
                                    console.log(data);
                            }
                        });
                    }
                }
                
            );
                jQuery('#component-controllers img').animate({
                    width:'15px',
                    height:'15px'
                }, 
                {
                    duration:250,
                    easing:'swing'
                });
            }
        }
        
function drawertoggle(event) {
    if(jQuery(event).children('img').attr('src')=='http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userdrawer-open.png' || jQuery(event).children('img').attr('src')=='http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userdrawer-open-ro.png') {
                jQuery(event).children('img').attr('src', 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userdrawer-close.png');
                $( "#component-controllers img" ).fadeTo( 200 , 1, function() {
                    // Animation complete.
                });
                jQuery('#primary-wrapper').animate({
                    marginLeft:'35em'
                }, 
                {
                    duration:250,
                    easing:'swing'
                });
                jQuery('#drawer').animate({
                    left:0
                }, 
                {
                    duration:250,
                    easing:'swing',
                    complete:function() {
                        jQuery.ajax({
                            url: '<?php echo _CON._AJAX; ?>',
                            type: 'post',
                            data: {'userid':'<?php if(isset($_SESSION['userid'])){echo $_SESSION['userid'];}else{echo 'visitor';}?>', 'action':'toggleDrawer', 'val': ''},
                            success: function(data) {
                                    console.log(data);
                            }
                        });
                    }
                });
            } else if(jQuery(event).children('img').attr('src')=='http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userdrawer-close.png' || jQuery(event).children('img').attr('src')=='http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userdrawer-close-ro.png') {
                if (jQuery(event).children('img').attr('src')=='http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userdrawer-close.png') {
                    jQuery(event).children('img').attr('src', 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userdrawer-open.png');
                } else {
                    jQuery(event).children('img').attr('src', 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userdrawer-open-ro.png');
                }
                $( "#component-controllers img" ).fadeTo( 200 , 0.3, function() {
                    // Animation complete.
                });
                jQuery('#primary-wrapper').animate({
                    marginLeft:0
                }, 
                {
                    duration:250,
                    easing:'swing'
                });
                jQuery('#drawer').animate({
                    left:'-35em'
                    
                }, 
                {
                    duration:250,
                    easing:'swing',
                    complete:function() {
                        jQuery.ajax({
                            url: '<?php echo _CON._AJAX; ?>',
                            type: 'post',
                            data: {'userid':'<?php if(isset($_SESSION['userid'])){echo $_SESSION['userid'];}else{echo 'visitor';}?>', 'action':'toggleDrawer', 'val': ''},
                            success: function(data) {
                                    console.log(data);
                            }
                        });
                    }
                }
                
            );
            }
}