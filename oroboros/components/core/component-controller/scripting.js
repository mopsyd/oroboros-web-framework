/* 
 * Javascript file for the Oroboros Core Component Controller
 */
jQuery(document).ready(function() {
    jQuery('#dashbar').css({'top':'-55px'});
    jQuery('.dashbar-spacer').css({'height':'0'});
    jQuery( "#component-controllers img" ).fadeTo( 0 , 0.3);
    jQuery('a#drawer-toggle-switch').children('img.toggle-icon').attr('src', 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userdrawer-open-ro.png');
    jQuery('#component-controllers img').css({
                    height:'25px',
                    width:'25px'});
                    
    jQuery('#drawer').css({'left':'0'});
    jQuery('#primary-wrapper').css({'marginLeft':'35em'});
    jQuery('#drawer-toggle-switch').children('img.toggle-icon').attr('src', 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userdrawer-close.png');
});

jQuery(window).load(function(){

    //rollover states for dashbar controller
        jQuery(document).on("mouseover", "#dashbar-toggle-switch", function() {
            if(jQuery(this).children('img.toggle-icon').attr('src')=='http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userbar-close.png' || jQuery(this).children('img.toggle-icon').attr('src')=='http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userbar-close-ro.png') {
                jQuery(this).children('img.toggle-icon').attr('src', 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userbar-close-ro.png');
            } else {
                jQuery(this).children('img.toggle-icon').attr('src', 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userbar-open-ro.png');
            }
        });
        jQuery(document).on("mouseout", "#dashbar-toggle-switch", function() {
            if(jQuery(this).children('img.toggle-icon').attr('src')=='http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userbar-close-ro.png' || jQuery(this).children('img.toggle-icon').attr('src')=='http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userbar-close.png') {
                jQuery(this).children('img.toggle-icon').attr('src', 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userbar-close.png');
            } else {
                jQuery(this).children('img.toggle-icon').attr('src', 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userbar-open.png');
            }
        });
        //rollover states for drawer controller
        jQuery(document).on("mouseover", "#drawer-toggle-switch", function() {
            if(jQuery(this).children('img.toggle-icon').attr('src')=='http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userdrawer-close.png' || jQuery(this).children('img.toggle-icon').attr('src')=='http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userdrawer-close-ro.png') {
                jQuery(this).children('img.toggle-icon').attr('src', 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userdrawer-close-ro.png');
            } else {
                jQuery(this).children('img.toggle-icon').attr('src', 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userdrawer-open-ro.png');
            }
        });
        jQuery(document).on("mouseout", "#drawer-toggle-switch", function() {
            if(jQuery(this).children('img.toggle-icon').attr('src')=='http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userdrawer-close-ro.png' || jQuery(this).children('img.toggle-icon').attr('src')=='http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userdrawer-close.png') {
                jQuery(this).children('img.toggle-icon').attr('src', 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userdrawer-close.png');
            } else {
                jQuery(this).children('img.toggle-icon').attr('src', 'http://<?php echo $_SERVER['HTTP_HOST']; ?>/oroboros/components/core/component-controller/media/images/icons/userdrawer-open.png');
            }
        });
        //toggle action for dashbar controller
        jQuery('#dashbar-toggle a').click(function(){
            event.preventDefault();
            dashtoggle(this);
            return false;
        });
        //toggle action for userdrawer controller
        jQuery('#drawer-toggle a').click(function(){
            event.preventDefault();
            drawertoggle(this);
            return false;
        });
});