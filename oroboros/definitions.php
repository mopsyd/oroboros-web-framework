<?php
define('_INTERFACES', 'interfaces/');
define('_TRAITS', 'traits/');
define('_LIBS', 'libs/');
define('_MODELS', 'models/');
define('_CONTROLLERS', 'controllers/');
define('_ADAPTERS', 'adapters/');
define('_VIEWS', 'views/');
define('_MEDIA', _PUBLIC.'media/');
define('_IMAGES', _MEDIA.'images/');
define('_FAVICON', _MEDIA.'favicon/');
define('_AUDIO', _MEDIA.'audio/');
define('_VIDEO', _MEDIA.'video/');
define('_INTERACTIVE', _MEDIA.'interactive/');
define('_LOGO', _IMAGES.'logos/');
define('_CONTENTIMG', _IMAGES.'content/');
define('_BACKGROUNDIMG', _IMAGES.'background/');
define('_PROFILEIMG', _IMAGES.'profile/');
define('_USER', _URL . 'oroboros/users/');
define('_DEPENDENCIES', _APP . 'dependencies/');

//app content folders
define('_DATA', _APP . 'data/');
define('_THEME', _APP.'themes/');
define('_TEMPLATE', _APP.'templates/');
define('_COMPONENTS', _APP.'components/');
define('_SITES', _APP.'sites/');

define('_LOG', _DATA . 'logs/');
define('_AJAX', $_SERVER['HTTP_HOST'] . '/public/ajax/ajax.phtml');

//array of folders for autoloader to search
$load = array(
    _LIBS,
    _MODELS,
    _CONTROLLERS,
    _ADAPTERS
);
?>
